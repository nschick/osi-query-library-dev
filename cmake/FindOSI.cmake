################################################################################
# Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
#
# Find Package Adapter for OSI (Open Simulation Interface)

set(OSI_PIC_NAMES
  open_simulation_interface_pic.lib
  libopen_simulation_interface_pic.a
)

find_library(OSI_PIC_LIBRARY NAMES ${OSI_PIC_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib/osi3
    lib
    lib64
)

if(OSI_PIC_LIBRARY)
  message(STATUS "Found OSI (open_simulation_interface_pic): ${OSI_PIC_LIBRARY}")
  get_filename_component(OSI_PIC_LIBRARY_DIR "${OSI_PIC_LIBRARY}" DIRECTORY)
  add_library(open_simulation_interface::open_simulation_interface_pic IMPORTED STATIC)
  set_target_properties(open_simulation_interface::open_simulation_interface_pic
                        PROPERTIES
                          IMPORTED_LOCATION ${OSI_PIC_LIBRARY}
                          INTERFACE_INCLUDE_DIRECTORIES ${OSI_PIC_LIBRARY_DIR}/../../include
                          INTERFACE_LINK_LIBRARIES protobuf::libprotobuf_static)
else()
  message(STATUS "Didn't find OSI (open_simulation_interface_pic)")
endif()

unset(OSI_SHARED_LIBRARY)
unset(OSI_STATIC_LIBRARY)
unset(OSI_PIC_LIBRARY)
