/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/ReferenceLine.h"
#include "OsiQueryLibrary/Street/Road/LaneRow.h"

namespace osiql::detail {
struct RoadSpecifications
{
    RoadSpecifications(const ReferenceLine &, Lane &initialLane);

    const ReferenceLine &referenceLine;
    std::vector<LaneRow> rows;
    // Iterator to the lane row whose first lane has the smallest start_s guaranteed to be part of the road specified by this class
    std::ptrdiff_t min;
    // Iterator to the lane row whose last lane has the largest end_s guaranteed to be part of the road specified by this class
    std::ptrdiff_t max;
    // Number of lanes from right to left that are on the right side of the road specified by this class
    std::ptrdiff_t center;

private:
    void InitializeLaneRows(Lane &initialLane);
    void UpdateMin();
    void UpdateMax();
};
} // namespace osiql::detail
