/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Street/Lane.h"

namespace osiql {
class World;
//! \brief A chain of subsequent lanes that are part of the same road
//!
struct LaneRow : Iterable<std::vector<Lane *>>
{
    using Iterable<std::vector<Lane *>>::begin;
    typename std::vector<Lane *>::const_iterator begin() const override;
    typename std::vector<Lane *>::iterator begin();

    using Iterable<std::vector<Lane *>>::end;
    typename std::vector<Lane *>::const_iterator end() const override;
    typename std::vector<Lane *>::iterator end();

    LaneRow() = default;
    LaneRow(std::vector<Lane *> &&lanes);

    //! Returns the driving direction of traversing the lanes of this lane row in the given orientation.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \return Direction Downstream or Upstream
    template <Orientation = Orientation::Forwards>
    Direction GetDirection() const;

    //! Returns the driving direction of traversing the lanes of this lane row in the given orientation.
    //!
    //! \return Direction Downstream or Upstream
    Direction GetDirection(Orientation) const;

    //! Returns the lane in this lane row containing the given s-coordinate. If multiple lanes contain the
    //! coordinate, the latter one in the given orientation is returned. If no lane contains it, returns nullptr.
    //!
    //! \tparam Orientation
    //! \param s
    //! \return const Lane*
    template <Orientation = Orientation::Forwards>
    const Lane *GetLane(double s) const;

    //! Returns the lane in this lane row containing the given s-coordinate. If multiple lanes contain the
    //! coordinate, the latter one in the given direction is returned. If no lane contains it, returns nullptr.
    //!
    //! \tparam Direction
    //! \param s
    //! \return const Lane*
    template <Direction>
    const Lane *GetLane(double s) const;

    //! Returns a collection of all placements of objects of the given type assigned to any lane on this row of lanes.
    //!
    //! \tparam T
    //! \tparam Orientation Forwards or Backwards
    //! \return std::vector<const Placement<T> *> Sorted by ascending distance from the start of this lane
    //! row in the given orientation.
    template <typename T, Orientation = Orientation::Forwards>
    std::vector<const Placement<T> *> GetAll() const;

    //! Returns a collection of all placements of objects of the given type assigned to any lane on this row of lanes.
    //!
    //! \tparam T
    //! \tparam Direction Downstream or Upstream
    //! \return std::vector<const Placement<T> *> Sorted by ascending distance from the start of this lane
    //! row in the given driving direction.
    template <typename T, Direction>
    std::vector<const Placement<T> *> GetAll() const;

    //! Returns a collection of all placements of objects of the given type assigned to any lane on this row
    //! of lanes satisfying the given predicate.
    //!
    //! \tparam T
    //! \tparam Orientation Forwards or Backwards
    //! \tparam Pred Invocable receiving a const Placement<T>& and returning a bool
    //! \param predicate
    //! \return std::vector<const Placement<T>*> Sorted by ascending distance from the start of this lane
    //! row in the given orientation.
    template <typename T, Orientation, typename Pred>
    std::vector<const Placement<T> *> GetAll(Pred &&predicate) const;

    //! Returns a collection of all placements of objects of the given type assigned to any lane on this row
    //! of lanes satisfying the given predicate.
    //!
    //! \tparam T
    //! \tparam Direction Downstream or Upstream
    //! \tparam Pred Invocable receiving a const Placement<T>& and returning a bool
    //! \param predicate
    //! \return std::vector<const Placement<T> *> Sorted by ascending distance from the start of this lane
    //! row in the direction.
    template <typename T, Direction, typename Pred>
    std::vector<const Placement<T> *> GetAll(Pred &&predicate) const;

    // TODO: std::vector<const Placement<T> *> GetAllInRange(double startLatitude, double endLatitude) const;

    //! \brief The set of lanes in this row
    std::vector<Lane *> lanes;
};

//! \brief A chain of adjacent lane rows that holds the entirety of a road's lanes
//!
struct LaneRows
{
    LaneRows(std::vector<LaneRow> &&, std::ptrdiff_t center);

    //! Returns an iterator pointing to to the lane on the given side of the road closest to the
    //! road's other side. The iterator increments away from the road center.
    //!
    //! \tparam S
    //! \return Iterator
    template <Side S>
    ConstIterator<std::vector<LaneRow>, (!GetDirection<S>())> begin() const;

    //! Returns an iterator that increments to the next lane with a greater OpenDRIVE id which
    //! points to the lane with the smallest openDRIVE id on the given side of the road.
    //!
    //! \param sideOfRoad
    //! \return std::vector<LaneRow>::const_iterator
    typename std::vector<LaneRow>::const_iterator begin(Side sideOfRoad) const;

    //! Returns an iterator that increments away from the center of the road pointing past
    //! the last lane on the specified side of the road.
    //!
    //! \tparam S
    //! \return Iterator
    template <Side S>
    ConstIterator<std::vector<LaneRow>, (!GetDirection<S>())> end() const;

    //! Returns an iterator that increments to the next lane with a greater OpenDRIVE id which
    //! points past the lane with the largest OpenDRIVE id on the given side of the road.
    //!
    //! \param sideOfRoad
    //! \return std::vector<LaneRow>::const_iterator
    typename std::vector<LaneRow>::const_iterator end(Side sideOfRoad) const;

    //! Returns the number of lane rows on the given side of this road
    //!
    //! \param direction
    //! \return size_t
    template <Side = Side::Both>
    size_t size() const;

    //! Returns the number of lane rows on the given side of this road
    //!
    //! \param sideOfRoad
    //! \return size_t
    size_t size(Side sideOfRoad) const;

    //! Returns the lane row on the given side of this road with the given number
    //! of lane rows between it and the other side of the road.
    //!
    //! \tparam Side Left or Right
    //! \param index
    //! \return const LaneRow&
    template <Side>
    const LaneRow &at(size_t index) const;

    //! Returns the lane row on the given side of this road with the given number
    //! of lane rows between it and the other side of the road.
    //!
    //! \param sideOfRoad Left or Right
    //! \param index
    //! \return const LaneRow&
    const LaneRow &at(Side sideOfRoad, size_t index) const;

    //! \brief The set of lane rows
    std::vector<LaneRow> rows;

    //! \brief Iterator past the last row on the right side of the road
    std::vector<LaneRow>::const_iterator it;
};

//! Returns what side of the road the given lane row is on
//!
//! \param row
//! \return Side Left or Right
Side GetSideOfRoad(const LaneRow &row);
} // namespace osiql

namespace osiql {
template <Orientation O>
Direction LaneRow::GetDirection() const
{
    return lanes.front()->GetDirection<O>();
}

template <Direction D>
const Lane *LaneRow::GetLane(double s) const
{
    const auto it{Iterable::begin<D>(s)};
    return (it == end<D>() || greater<D>(osiql::GetLatitude<D>(*this), s)) ? nullptr : *it;
}
template <Orientation O>
const Lane *LaneRow::GetLane(double s) const
{
    if (lanes.empty())
    {
        return nullptr;
    }
    return lanes.front()->GetDirection<O>() == Direction::Upstream ? GetLane<Direction::Upstream>(s) : GetLane<Direction::Downstream>(s);
}

template <typename T, Orientation O>
std::vector<const Placement<T> *> LaneRow::GetAll() const
{
    if (GetDirection<O>() == Direction::Upstream)
    {
        return GetAll<T, Direction::Upstream>();
    }
    else
    {
        return GetAll<T, Direction::Downstream>();
    }
}

template <typename T, Direction D>
std::vector<const Placement<T> *> LaneRow::GetAll() const
{
    std::vector<const Placement<T> *> result;

    for (auto lane{begin<D>()}; lane != end<D>(); ++lane)
    {
        const Collection<Placement<T>> &placements{lane->template GetAll<T>()};
        std::vector<const Placement<T> *> tmp;
        for (const Placement<T> &placement : placements)
        {
            tmp.push_back(&placement);
        }
        std::sort(tmp.begin(), tmp.end(), [](const Placement<T> *lhs, const Placement<T> *rhs) { return less<D>(*lhs, *rhs); });
        result.insert(result.end(), std::make_move_iterator(tmp.begin()), std::make_move_iterator(tmp.end()));
    }
    return result;
}

template <typename T, Orientation O, typename Pred>
std::vector<const Placement<T> *> LaneRow::GetAll(Pred &&predicate) const
{
    if (GetDirection<O>() == Direction::Upstream)
    {
        return GetAll<T, Direction::Upstream>(std::forward<Pred>(predicate));
    }
    else
    {
        return GetAll<T, Direction::Downstream>(std::forward<Pred>(predicate));
    }
}

template <typename T, Direction D, typename Pred>
std::vector<const Placement<T> *> LaneRow::GetAll(Pred &&predicate) const
{
    std::vector<const Placement<T> *> result;
    for (auto lane{begin<D>()}; lane != end<D>(); ++lane)
    {
        std::vector<const Placement<T> *> placements{lane->template GetAll<T>(std::forward<Pred>(predicate))};
        std::sort(placements.begin(), placements.end(), [](const Placement<T> *lhs, const Placement<T> *rhs) { return less<D>(*lhs, *rhs); });
        result.insert(result.end(), std::make_move_iterator(placements.begin()), std::make_move_iterator(placements.end()));
    }
    return result;
}

template <Side S>
ConstIterator<std::vector<LaneRow>, (!GetDirection<S>())> LaneRows::begin() const
{
    if constexpr (S == Side::Left)
    {
        return it;
    }
    else
    {
        return std::make_reverse_iterator(it);
    }
}

template <Side S>
ConstIterator<std::vector<LaneRow>, (!GetDirection<S>())> LaneRows::end() const
{
    if constexpr (S == Side::Left)
    {
        return rows.end();
    }
    else
    {
        return rows.rend();
    }
}

template <Side S>
size_t LaneRows::size() const
{
    if constexpr (S == Side::Left)
    {
        return static_cast<size_t>(std::distance(it, rows.end()));
    }
    else if constexpr (S == Side::Right)
    {
        return static_cast<size_t>(std::distance(rows.begin(), it));
    }
    else
    {
        return rows.size();
    }
}

template <Side S>
const LaneRow &LaneRows::at(size_t index) const
{
    if (index >= size<S>())
    {
        throw std::out_of_range("");
    }
    return *std::next(begin<S>(), static_cast<std::ptrdiff_t>(index));
}
} // namespace osiql
