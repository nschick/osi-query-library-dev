/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of a osi3::LaneBoundary

#include <iostream>
#include <osi3/osi_lane.pb.h>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Component/Points.h"
#include "OsiQueryLibrary/Trait/Collection.h"

namespace osiql {
struct Vector2d;
class World;

//! \brief A LaneMarking is a wrapper of a osi3::LaneBoundary (while osiql::LaneBoundary is a wrapper of a osi3::LogicalLaneBoundary).
//! It defines the location and appearance of a lane's driver-visible boundary.
struct LaneMarking : Identifiable<osi3::LaneBoundary>, Points<Container<osi3::LaneBoundary::BoundaryPoint>>
{
    using Identifiable::Identifiable;

    //! \brief Alias for the point type of the underlying OSI object
    using Point = osi3::LaneBoundary::BoundaryPoint;

    using Iterable<Container<Point>>::begin;
    Container<Point>::const_iterator begin() const override;

    using Iterable<Container<Point>>::end;
    Container<Point>::const_iterator end() const override;

    //! \brief Wrapper of LaneMarking::Point. On top of its global position, it holds information
    //! on the width and height of the marking as well as the state of its dash line.
    struct Vertex
    {
        //! Returns the width of the lane marking at this point
        //!
        //! \return double Width in meters
        double GetWidth() const;

        //! Returns the height of the lane marking at this point, which is the amount by
        //! which it protrudes from the ground
        //!
        //! \return double Height in meters
        double GetHeight() const;

        //! Implicit cast to a global xy-coordinate point
        //!
        //! \return Vector2d
        operator Vector2d() const;

        //! Returns the x-coordinate of this point
        //!
        //! \return double
        double x() const;

        //! Returns the y-coordinate of this point
        //!
        //! \return double
        double y() const;

        //! \brief Wrapper enum of osi3::LaneBoundary::BoundaryPoint::Dash with
        //! the same underlying values, allowing static casting between them.
        enum class Dash : std::underlying_type_t<LaneMarking::Point::Dash>
        {
            Unknown = osi3::LaneBoundary_BoundaryPoint_Dash_DASH_UNKNOWN,
            Other = osi3::LaneBoundary_BoundaryPoint_Dash_DASH_OTHER,
            Start = osi3::LaneBoundary_BoundaryPoint_Dash_DASH_START,
            Continue = osi3::LaneBoundary_BoundaryPoint_Dash_DASH_CONTINUE,
            End = osi3::LaneBoundary_BoundaryPoint_Dash_DASH_END,
            Gap = osi3::LaneBoundary_BoundaryPoint_Dash_DASH_GAP,
        };
        //! Returns the state of the visible lane marking at this point.
        //!
        //! \return Dash
        Dash GetDash() const;

        //! \brief Underlying OSI lane boundary point of which this vertex is a wrapper
        const LaneMarking::Point &handle;
    };

    //! \brief A span/contiguous subset of a lane marking.
    struct Partition : Points<Container<LaneMarking::Point>>
    {
        //! Constructs a lane marking partition from its components
        //!
        //! \param marking
        //! \param first
        //! \param pastLast
        Partition(const LaneMarking &marking, Container<LaneMarking::Point>::const_iterator first, Container<LaneMarking::Point>::const_iterator pastLast);

        using Iterable<Container<LaneMarking::Point>>::begin;
        typename Container<LaneMarking::Point>::const_iterator begin() const override;

        using Iterable<Container<LaneMarking::Point>>::end;
        typename Container<LaneMarking::Point>::const_iterator end() const override;

        //! \brief Wrapper of an OSI physical lane boundary of which this is a partition.
        const LaneMarking &marking;

    private:
        Container<LaneMarking::Point>::const_iterator a;
        Container<LaneMarking::Point>::const_iterator z;
    };

    //! \brief Wrapper enum of osi3::LaneBoundary::Classification::Type with
    //! the same underlying values, allowing static casting between them.
    enum class Type : std::underlying_type_t<osi3::LaneBoundary::Classification::Type>
    {
        Unknown = osi3::LaneBoundary_Classification_Type_TYPE_UNKNOWN,
        Other = osi3::LaneBoundary_Classification_Type_TYPE_OTHER,
        NoLine = osi3::LaneBoundary_Classification_Type_TYPE_NO_LINE,
        SolidLine = osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE,
        DashedLine = osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE,
        BottsDots = osi3::LaneBoundary_Classification_Type_TYPE_BOTTS_DOTS,
        RoadEdge = osi3::LaneBoundary_Classification_Type_TYPE_ROAD_EDGE,
        SnowEdge = osi3::LaneBoundary_Classification_Type_TYPE_SNOW_EDGE,
        GrassEdge = osi3::LaneBoundary_Classification_Type_TYPE_GRASS_EDGE,
        GravelEdge = osi3::LaneBoundary_Classification_Type_TYPE_GRAVEL_EDGE,
        SoilEdge = osi3::LaneBoundary_Classification_Type_TYPE_SOIL_EDGE,
        GuardRail = osi3::LaneBoundary_Classification_Type_TYPE_GUARD_RAIL,
        Curb = osi3::LaneBoundary_Classification_Type_TYPE_CURB,
        Structure = osi3::LaneBoundary_Classification_Type_TYPE_STRUCTURE,
        Barrier = osi3::LaneBoundary_Classification_Type_TYPE_BARRIER,
        SoundBarrier = osi3::LaneBoundary_Classification_Type_TYPE_SOUND_BARRIER,
    };
    //! Returns what kind of boundary this lane marking is. It could be a painted line or a wall for instance.
    //!
    //! \return Type
    Type GetType() const;

    //! \brief Wrapper enum of osi3::LaneBoundary::Classification::Color with
    //! the same underlying values, allowing static casting between them.
    enum class Color : std::underlying_type_t<osi3::LaneBoundary::Classification::Color>
    {
        Unknown = osi3::LaneBoundary_Classification_Color_COLOR_UNKNOWN,
        Other = osi3::LaneBoundary_Classification_Color_COLOR_OTHER,
        None = osi3::LaneBoundary_Classification_Color_COLOR_NONE,
        White = osi3::LaneBoundary_Classification_Color_COLOR_WHITE,
        Yellow = osi3::LaneBoundary_Classification_Color_COLOR_YELLOW,
        Red = osi3::LaneBoundary_Classification_Color_COLOR_RED,
        Blue = osi3::LaneBoundary_Classification_Color_COLOR_BLUE,
        Green = osi3::LaneBoundary_Classification_Color_COLOR_GREEN,
        Violet = osi3::LaneBoundary_Classification_Color_COLOR_VIOLET,
        Orange = osi3::LaneBoundary_Classification_Color_COLOR_ORANGE,
    };
    //! Returns what color this lane marking has.
    //!
    //! \return Color
    Color GetColor() const;
};

std::ostream &operator<<(std::ostream &os, LaneMarking::Color color);
std::ostream &operator<<(std::ostream &os, const LaneMarking::Point &point);
std::ostream &operator<<(std::ostream &os, LaneMarking::Type type);
std::ostream &operator<<(std::ostream &os, LaneMarking::Vertex::Dash dash);
std::ostream &operator<<(std::ostream &os, const LaneMarking::Vertex &point);
std::ostream &operator<<(std::ostream &os, const LaneMarking::Partition &partition);
std::ostream &operator<<(std::ostream &os, const LaneMarking &marking);
} // namespace osiql

namespace osiql::detail {
constexpr const std::array<std::string_view, 6> laneMarkingDashToString{
    "Unknown",
    "Other",
    "Start",
    "Continue",
    "End",
    "Gap",
};
constexpr const std::array<std::string_view, 16> laneMarkingTypeToString{
    "Unknown",
    "Other",
    "No Line",
    "Solid Line",
    "Dashed Line",
    "Botts Dots",
    "Road Edge",
    "Snow Edge",
    "Grass Edge",
    "Gravel Edge",
    "Soil Edge",
    "Guard Rail",
    "Curb",
    "Structure",
    "Barrier",
    "Sound Barrier",
};
constexpr const std::array<std::string_view, 10> laneMarkingColorToString{
    "Unknown",
    "Other",
    "None",
    "White",
    "Yellow",
    "Red",
    "Blue",
    "Green",
    "Violet",
    "Orange",
};
} // namespace osiql::detail
