/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of a osi3::ReferenceLine

#include <iostream>
#include <optional>
#include <osi3/osi_referenceline.pb.h>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Component/Points.h"
#include "OsiQueryLibrary/Types/Container.h"
#include "OsiQueryLibrary/Version.h"

namespace osiql {
//! \brief osi3::ReferenceLine wrapper. A reference line is the center line of a road. Local points on the road are expressed
//! relative to the position of this reference line. Lanes to its right travel downstream and have negative relative ids.
//! Local points on them have negative t-coordinates. Lanes to its left travel upstream and have positive relative ids.
//! Local points on the have positive t-coordinates.
struct ReferenceLine : Identifiable<osi3::ReferenceLine>, Points<Container<osi3::ReferenceLine::ReferenceLinePoint>>
{
    //! Constructs a wrapper of a given OSI reference line
    //!
    //! \param handle OSI reference line
    ReferenceLine(const osi3::ReferenceLine &handle, const Version &version);

    //! \brief Alias for the point type of the underlying OSI object
    using Point = osi3::ReferenceLine::ReferenceLinePoint;

    using Points<Container<Point>>::begin;
    typename Container<Point>::const_iterator begin() const override;

    //! Returns an iterator to the start of the edge whose section contains the given point
    //!
    //! \tparam D Downstream or Upstream
    //! \param point
    //! \return ConstIterator<Container<Point>, D>
    template <Direction D = Direction::Downstream>
    ConstIterator<Container<Point>, D> begin(const Vector2d &point) const;

    using Points<Container<Point>>::end;
    typename Container<Point>::const_iterator end() const override;

    //! Returns an iterator to the point past the start of the edge whose section contains the given point
    //!
    //! \tparam D Downstream or Upstream
    //! \param point
    //! \return ConstIterator<Container<Point>, D>
    template <Direction D = Direction::Downstream>
    ConstIterator<Container<Point>, D> end(const Vector2d &point) const;

    //! Returns the local st-coordinate representation of the given global point
    //! according to the coordinate system defined by this reference line.
    //!
    //! \param globalPoint Global xy-coordinate pair
    //! \return Coordinates<Road> Local st-coordinate pair
    Coordinates<Road> GetCoordinates(const Vector2d &globalPoint) const;

    //! Returns the global xy-coordinate representation of the given local point
    //! according to the coordinate system defined by this reference line.
    //!
    //! \param const Coordinates<Road>& Local st-coordinate pair
    //! \return Vector2d Global xy-coordinate pair
    Vector2d GetXY(const Coordinates<Road> &) const;

    //! Returns the version of the OSI interface of the ground truth this reference line's handle is a part of.
    //! The version may affect local coordinate conversion logic.
    //!
    //! \return const Version&
    const Version &GetVersion() const;

private:
    //! \brief Reference to the version of the ground truth this reference line's handle is a part of.
    //! The version affects localization/globalization logic.
    std::reference_wrapper<const Version> version;
    //! Returns the direction vector describing the longitudinal axis at the point
    //! on this reference line with the given index
    //!
    //! \tparam Direction Downstream or Upstream Determines the iterator type
    //! \param point Forward or reverse iterator to a point on this reference line
    //! \return const Vector2d&
    template <Direction D = Direction::Downstream>
    const Vector2d &GetLongitudinalAxis(ConstIterator<Container<Point>, D> point) const;

    mutable std::vector<std::unique_ptr<Vector2d>> longitudinalAxes;

    const std::optional<Vector2d> &GetLongitudinalAxisIntersection(size_t) const;
    //! \brief Lazy-evaluated intersection of the start and end point's longitudinal axis for each edge.
    //! Used for point localization/globalization in OSI 3.6 and up.
    mutable std::vector<std::unique_ptr<std::optional<Vector2d>>> intersections;
};

std::ostream &operator<<(std::ostream &, const ReferenceLine &);
} // namespace osiql

namespace osiql {
template <Direction D>
const Vector2d &ReferenceLine::GetLongitudinalAxis(ConstIterator<Container<Point>, D> it) const
{
    if constexpr (!HAS_MEMBER(Point, has_t_axis_yaw()))
    {
        static_assert(always_false<decltype(D)>, "GetLongitudinalAxis may only be called inside a \"if constexpr (HAS_MEMBER(Point, has_t_axis_yaw()))\" block.");
    }
    else if constexpr (D == Direction::Downstream)
    {
        std::unique_ptr<Vector2d> &result{*std::next(osiql::begin<D>(longitudinalAxes), std::distance(begin<D>(), it))};
        if (!result)
        {
            if (it->has_t_axis_yaw())
            {
                result = std::make_unique<Vector2d>(Vector2d{std::cos(it->t_axis_yaw()), std::sin(it->t_axis_yaw())});
            }
            else // Set t-axis to the normalized counter-clockwise normal of the s-axis
            {
                if (it == begin<D>())
                {
                    result = std::make_unique<Vector2d>(GetPerpendicular<Winding::CounterClockwise>(Norm(*std::next(it) - *it)));
                }
                else if (std::next(it) == end<D>())
                {
                    result = std::make_unique<Vector2d>(GetPerpendicular<Winding::CounterClockwise>(Norm(*it - *std::prev(it))));
                }
                else
                {
                    result = std::make_unique<Vector2d>(GetPerpendicular<Winding::CounterClockwise>(Norm(Norm(*it - *std::prev(it)) + Norm(*std::next(it) - *it))));
                }
            }
        }
        return *result;
    }
    else if constexpr (D == Direction::Upstream)
    {
        std::unique_ptr<Vector2d> &result{*std::next(osiql::begin<D>(longitudinalAxes), std::distance(begin<D>(), it))};
        if (!result)
        {
            if (it->has_t_axis_yaw())
            {
                result = std::make_unique<Vector2d>(Vector2d{std::cos(it->t_axis_yaw()), std::sin(it->t_axis_yaw())});
            }
            else // Set t-axis to the normalized counter-clockwise normal of the s-axis
            {
                result = std::make_unique<Vector2d>(GetPerpendicular<Winding::CounterClockwise>(Norm((std::next(it) == end<D>()) ? (*std::prev(it) - *it) : (*it - *std::next(it)))));
            }
        }
        return *result;
    }
    else
    {
        static_assert(always_false<decltype(D)>, "Direction must be Downstream or Upstream");
    }
}

template <Direction D>
ConstIterator<Container<ReferenceLine::Point>, D> ReferenceLine::begin(const Vector2d &point) const
{
    if constexpr (HAS_MEMBER(Point, has_t_axis_yaw()))
    {
        ConstIterator<Container<Point>, D> it{Points<Container<Point>>::begin<D>(point)};
        while ((it != begin<D>()) && (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) == !GetSideOfRoad(D)))
        {
            --it;
        }
        if (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) == !GetSideOfRoad(D))
        {
            return it;
        }
        ++it;
        while ((std::next(it) != end<D>()) && (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) != !GetSideOfRoad(D)))
        {
            ++it;
        }
        return --it;
    }
    else
    {
        return Points<Container<Point>>::begin<D>(point);
    }
}
template <Direction D>
ConstIterator<Container<ReferenceLine::Point>, D> ReferenceLine::end(const Vector2d &point) const
{
    if constexpr (HAS_MEMBER(Point, has_t_axis_yaw()))
    {
        ConstIterator<Container<Point>, D> it{Points<Container<Point>>::end<D>(point)};
        while ((std::next(it) != end<D>()) && (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) != !GetSideOfRoad(D)))
        {
            ++it;
        }
        if (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) != !GetSideOfRoad(D))
        {
            return it;
        }
        --it;
        while ((it != begin<D>()) && (point.GetSide(*it, *it + GetLongitudinalAxis<D>(it)) == !GetSideOfRoad(D)))
        {
            --it;
        }
        return ++it;
    }
    else
    {
        return Points<Container<Point>>::begin<D>(point);
    }
}
} // namespace osiql
