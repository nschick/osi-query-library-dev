/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of a osi3::LogicalLaneBoundary

#include <osi3/osi_logicallane.pb.h>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Street/LaneMarking.h"

namespace osiql {
//! A LaneBoundary is a wrapper of a osi3::LogicalLaneBoundary (while osiql::LaneMarking is a wrapper of a osi3::LaneBoundary).
//! Its points hold global xy- and local st-coordinate information.
struct LaneBoundary : Identifiable<osi3::LogicalLaneBoundary>, Points<Container<osi3::LogicalLaneBoundary::LogicalBoundaryPoint>>
{
    using Identifiable::Identifiable;

    //! \brief Alias for the point type of the underlying OSI object
    using Point = osi3::LogicalLaneBoundary::LogicalBoundaryPoint;

    using Iterable<Container<Point>>::begin;
    typename Container<Point>::const_iterator begin() const override;

    using Iterable<Container<Point>>::end;
    typename Container<Point>::const_iterator end() const override;

    //! Returns to which side an object may pass over this boundary in the given direction.
    //!
    //! \tparam Direction
    //! \return Side
    template <Direction = Direction::Downstream>
    Side GetPassingRule() const;

    //! Returns to which side an object may pass over this boundary in the given direction.
    //!
    //! \param direction
    //! \return Side
    Side GetPassingRule(Direction direction) const;

    //! Returns whether passing this boundary to the given side is legal.
    //!
    //! \tparam Side
    //! \tparam Direction
    //! \return bool
    template <Side, Direction = Direction::Downstream>
    bool AllowsPassing() const;

    //! Returns whether passing this boundary to the given side is legal.
    //!
    //! \tparam Side Left or Right
    //! \param direction
    //! \return bool
    template <Side>
    bool AllowsPassing(Direction direction) const;

    //! Returns whether passing this boundary to the given side is legal.
    //!
    //! \param side Left or Right
    //! \param direction
    //! \return bool
    bool AllowsPassing(Side side, Direction direction = Direction::Downstream) const;

    //! \brief Range of a lane marking partition over an s-coordinate interval
    struct MarkingRange
    {
        //! \brief Partition of a lane marking
        LaneMarking::Partition markingPartition;
        //! \brief start of the range
        double min;
        //! \brief end of the range
        double max;
    };
    //! \brief Set of lane markings sorted right to left (guaranteed by OSI)
    std::vector<MarkingRange> markingRanges;
};

double GetX(const LaneBoundary::Point &);

double GetY(const LaneBoundary::Point &);

std::ostream &operator<<(std::ostream &os, const LaneBoundary::Point &point);
std::ostream &operator<<(std::ostream &os, const LaneBoundary::MarkingRange &range);
std::ostream &operator<<(std::ostream &os, const LaneBoundary &boundary);

} // namespace osiql

// ----- template definitions

namespace osiql {
namespace {
template <Side Increasing, Side Decreasing>
Side GetPassingRule(const LaneBoundary &boundary)
{
    if (boundary.GetHandle().has_passing_rule())
    {
        switch (boundary.GetHandle().passing_rule())
        {
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_INCREASING_T:
            return Increasing;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_DECREASING_T:
            return Decreasing;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_NONE_ALLOWED:
            return Side::None;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_BOTH_ALLOWED:
            return Side::Both;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_OTHER:
            return Side::Other;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_UNKNOWN:
        default:
            return Side::Undefined;
        }
    }
    else if (boundary.GetHandle().physical_boundary_id().empty())
    {
        return Side::Undefined; // TODO: Try to derive passing rule from physical boundary
    }
    else
    {
        return Side::Undefined;
    }
}
} // namespace

template <Direction D>
Side LaneBoundary::GetPassingRule() const
{
    if constexpr (D == Direction::Downstream)
    {
        return osiql::GetPassingRule<Side::Left, Side::Right>(*this);
    }
    else if constexpr (D == Direction::Upstream)
    {
        return osiql::GetPassingRule<Side::Right, Side::Left>(*this);
    }
    else
    {
        return Side::Undefined;
    }
}

template <Side S, Direction D>
bool LaneBoundary::AllowsPassing() const
{
    const Side passingRule{GetPassingRule<D>()};
    return (passingRule == Side::Both) || (passingRule == S);
}

template <Side S>
bool LaneBoundary::AllowsPassing(Direction direction) const
{
    return direction == Direction::Upstream ? AllowsPassing<S, Direction::Upstream>() : AllowsPassing<S, Direction::Downstream>();
}
} // namespace osiql
