/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Wrapper of an OSI logical lane

#include <map>
#include <osi3/osi_logicallane.pb.h>
#include <type_traits>
#include <vector>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Point/Assignment.h"
#include "OsiQueryLibrary/Street/Boundary.h"
#include "OsiQueryLibrary/Street/Lane/Adjoinable.h"
#include "OsiQueryLibrary/Trait/Placement.h"
#include "OsiQueryLibrary/Types/Bounds.h"
#include "OsiQueryLibrary/Types/Orientation.h"

namespace osiql {
struct MovingObject;
template <typename Point>
struct Pose;
struct RoadMarking;
template <typename T, typename U>
struct Point;
class Road;
struct RoadPoint;
struct StaticObject;
struct TrafficLight;
struct TrafficSign;
class World;

//! \brief A wrapper for a osi3::LogicalLane that offers multiple utility methods and access to assigned objects
class Lane : public detail::Adjoinable<Lane>
{
public:
    using detail::Adjoinable<Lane>::Adjoinable;

    //! \brief The type of a lane
    enum class Type : std::underlying_type_t<Handle::Type>
    {
        Undefined = Handle::Type::LogicalLane_Type_TYPE_UNKNOWN, // default
        Other = Handle::Type::LogicalLane_Type_TYPE_OTHER,
        Normal = Handle::Type::LogicalLane_Type_TYPE_NORMAL,
        Biking = Handle::Type::LogicalLane_Type_TYPE_BIKING,
        Sidewalk = Handle::Type::LogicalLane_Type_TYPE_SIDEWALK,
        Parking = Handle::Type::LogicalLane_Type_TYPE_PARKING,
        Stop = Handle::Type::LogicalLane_Type_TYPE_STOP,
        Restricted = Handle::Type::LogicalLane_Type_TYPE_RESTRICTED,
        Border = Handle::Type::LogicalLane_Type_TYPE_BORDER,
        Shoulder = Handle::Type::LogicalLane_Type_TYPE_SHOULDER,
        Exit = Handle::Type::LogicalLane_Type_TYPE_EXIT,
        Entry = Handle::Type::LogicalLane_Type_TYPE_ENTRY,
        OnRamp = Handle::Type::LogicalLane_Type_TYPE_ONRAMP,
        OffRamp = Handle::Type::LogicalLane_Type_TYPE_OFFRAMP,
        ConnectingRamp = Handle::Type::LogicalLane_Type_TYPE_CONNECTINGRAMP,
        Median = Handle::Type::LogicalLane_Type_TYPE_MEDIAN,
        Curb = Handle::Type::LogicalLane_Type_TYPE_CURB,
        Rail = Handle::Type::LogicalLane_Type_TYPE_RAIL,
        Tram = Handle::Type::LogicalLane_Type_TYPE_TRAM
    };
    //! \brief Regular lane types that are meant to be driven on by vehicles
    static inline const std::vector<Type> defaultTypes{Lane::Type::Normal, Lane::Type::Exit, Lane::Type::OnRamp, Lane::Type::OffRamp, Lane::Type::Stop};

    //! Returns the type of this lane, which designates the intended kinds of vehicles and manners of navigation on them.
    //!
    //! \return Type
    Type GetType() const;

    //! Returns a container of all objects of the given type that are on or assigned to this lane.
    //!
    //! \tparam T
    //! \return const Collection<Placement<T>>&
    template <typename T>
    const Collection<Placement<T>> &GetAll() const;

    //! Returns a container of objects of the specified type sorted by distance from the
    //! start of the lane in the given orientation that satisfy the given predicate.
    //!
    //! \tparam T type of the returned objects
    //! \tparam Pred Invocable receiving a const placement_<T>& and returning a bool.
    //! \param predicate
    //! \return std::vector<const Placement<T>*>
    template <typename T, typename Pred>
    std::vector<const Placement<T> *> GetAll(Pred &&predicate) const;

    //! Returns the width of the lane at the given relative latitude. If it lies outside this lane's length,
    //! connected lanes are traversed and the width of the first found lane at that distance is returned.
    //! Returns zero if no connected lane exists at that distance.
    //!
    //! \param latitude
    //! \return double
    double GetWidth(double latitude) const;

    //! Returns the shortest distance between the given global point and this lane. If the point lies inside the lane, returns 0.0.
    //!
    //! \param point
    //! \return double
    double GetDistanceTo(const Vector2d &point) const;

    // clang-format off
    
    //! Returns a local point representing the given global point using the lane closest to the global point in euclidean distance
    //! on a road that can be reached from this lane. This method travels forwards and backwards from this lane as long as the distance
    //! of a lane's end in search direction to the global point is lesser than that of the lane's start. This method is used for
    //! instance to get the local front of a vehicle from its local center even if the front happens to lie outside any reachable lanes.
    //!
    //! \param globalPoint
    //! \return RoadPoint
    [[deprecated("The result of FindPointNearby is ambiguous in the case of overlapping roads. Prefer Route::GetRoadPoint")]]
    RoadPoint FindPointNearby(const Vector2d &globalPoint) const;
    // clang-format on

    //! Returns an s,t-coordinate pair relative to this lane describing the given global point.
    //!
    //! \param point
    //! \return RoadPoint
    RoadPoint GetPoint(const Vector2d &point) const;

    //! Returns an s,t-coordinate pair with an angle relative to this lane describing the given global point and yaw.
    //!
    //! \param globalPoint
    //! \param globalYaw
    //! \return Pose
    Pose<RoadPoint> GetPose(const Vector2d &globalPoint, double globalYaw) const;

    //! Returns the side in regards to the given orientation on this lane and the number of lane changes from this lane
    //! to the given lane, assuming both are on the same road. If there is no lane change, the returned side is always Left.
    //!
    //! \tparam Orientation If Forwards, then the returned side is relative to this lane's driving direction
    //! \param lane Target lane assumed to be on the same road as this lane. Still works on lanes of different roads,
    //! but the result is only useful if the layout of the other road's adjacent lanes is the same as this one's.
    //! \return std::pair<Side, size_t>
    template <Orientation = Orientation::Forwards>
    std::pair<Side, size_t> GetLaneChangesTo(const Lane &lane) const;

    //! Returns the side in regards to the given orientation on this lane and the number of lane changes from this lane
    //! to the given lane, assuming both are on the same road. If there is no lane change, the returned side is always Left.
    //!
    //! \param orientation Forwards or Backwards
    //! \param lane
    //! \return std::pair<Side, size_t>
    std::pair<Side, size_t> GetLaneChangesTo(Orientation orientation, const Lane &lane) const;

    //! Returns the road this lane is a part of.
    //!
    //! \return const Road&
    const Road &GetRoad() const;

    //! Returns the road this lane is a part of.
    //!
    //! \return Road&
    Road &GetRoad();

    //! Returns the openDRIVE id of this lane. Its signedness represents whether it is on the right
    //! side of the road and its magnitude corresponds to the number of lanes between this lane's
    //! right boundary (in driving direction) and the reference line of this lane's road.
    //!
    //! \return int
    int GetOpenDriveId() const;

    //! \brief Number of lanes between this lane and the other side of the road.
    size_t index{0u};

    // NOTE: osiql does not currently use osi::LogicalLaneAssignments for MovingObjects

    //! \brief Container of all objects touching this lane, including the local bounds of their intersection
    std::unordered_set<LocalBounds<const MovingObject>, Hash<Id>, Equal> movingObjects;

    //! \brief Assignments of road markings to this lane
    std::vector<Assignment<RoadMarking>> markings;

    //! \brief Assignments of static objects to this lane
    std::vector<Assignment<StaticObject>> objects;

    //! \brief Assignments of traffic lights to this lane
    std::vector<Assignment<TrafficLight>> lights;

    //! \brief Assignments of traffic signs to this lane
    std::vector<Assignment<TrafficSign>> signs;

private:
    friend class osiql::World; // The world can add assigned objects and needs access to the non-const version of Lane::GetAll
    friend struct osiql::MovingObject; // MovingObjects can detach themselves from lanes when they get destroyed

    friend std::ostream &operator<<(std::ostream &, const Lane &);

    template <typename T>
    Collection<Placement<T>> &GetAll();

    template <Orientation O = Orientation::Any>
    const Lane *FindLaneOfClosestRoad(const Vector2d &globalPoint, std::set<RoadId> &pool, double currentDistance, double *distance = nullptr) const;

    Road *road{nullptr};
};

//! Returns what side of the road the given lane is on based on the definition order of
//! the road's boundary points.
//!
//! \return Side Left or Right
Side GetSideOfRoad(const Lane &);

bool operator<(const Lane &lhs, const Lane &rhs);
bool operator==(const Lane &lhs, const Lane &rhs);
bool operator!=(const Lane &lhs, const Lane &rhs);

std::ostream &operator<<(std::ostream &os, Lane::Type type);

std::ostream &operator<<(std::ostream &os, const Lane &lane);

} // namespace osiql

namespace osiql {
//! Returns the s-coordinate at the end of the given lane in the given orientation
//!
//! \tparam Orientation
//! \param lane
//! \return double
template <Orientation O>
double GetEnd(const Lane &lane)
{
    return GetEnd(lane, lane.GetDirection<O>());
}

template <typename T>
const Collection<Placement<T>> &Lane::GetAll() const
{
    if constexpr (std::is_same_v<T, MovingObject>)
    {
        return movingObjects;
    }
    else if constexpr (std::is_same_v<T, RoadMarking>)
    {
        return markings;
    }
    else if constexpr (std::is_same_v<T, StaticObject>)
    {
        return objects;
    }
    else if constexpr (std::is_same_v<T, TrafficLight>)
    {
        return lights;
    }
    else if constexpr (std::is_same_v<T, TrafficSign>)
    {
        return signs;
    }
    else
    {
        std::cout << "Lane::GetAll does not store objects of the given type. Only MovingObject, RoadMarking, StaticObject, TrafficLight and TrafficSign is supported.\n";
        static const std::vector<Placement<T>> result{};
        return result;
    }
}
template <typename T>
Collection<Placement<T>> &Lane::GetAll()
{
    return const_cast<Collection<Placement<T>> &>(std::as_const(*this).GetAll<T>());
}

template <typename T, typename Pred>
std::vector<const Placement<T> *> Lane::GetAll(Pred &&predicate) const
{
    std::vector<const Placement<T> *> result;
    result.reserve(GetAll<T>().size());
    for (const Collection<Placement<T>> &placement : GetAll<T>())
    {
        if (predicate(placement))
        {
            result.push_back(&placement);
        }
    }
    return result;
}

template <Orientation O>
std::pair<Side, size_t> Lane::GetLaneChangesTo(const Lane &target) const
{
    if (GetDirection() == target.GetDirection())
    {
        return index > target.index                                   //
                   ? std::make_pair(Side::Left, index - target.index) //
                   : std::make_pair(Side::Right, target.index - index);
    }
    return std::make_pair(GetDirection<O>() == Direction::Downstream ? Side::Left : Side::Right, index + target.index + 1);
}

namespace detail {
constexpr const std::array<std::string_view, 19> laneTypeToString{
    "Undefined",
    "Other",
    "Normal",
    "Biking",
    "Sidewalk",
    "Parking",
    "Stop",
    "Restricted",
    "Border",
    "Shoulder",
    "Exit",
    "Entry",
    "On Ramp",
    "Off Ramp",
    "Connecting Ramp",
    "Median",
    "Curb",
    "Rail",
    "Tram",
};
}
} // namespace osiql
