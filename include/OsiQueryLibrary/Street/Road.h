/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Chain of lane rows

#include <map>
#include <numeric>
#include <vector>

#include "OsiQueryLibrary/Point/Pose.h"
#include "OsiQueryLibrary/Point/RoadPoint.h"
#include "OsiQueryLibrary/Street/Boundary.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/ReferenceLine.h"
#include "OsiQueryLibrary/Street/Road/LaneRow.h"
#include "OsiQueryLibrary/Street/Road/RoadSpecifications.h"
#include "OsiQueryLibrary/Trait/Position.h"
#include "OsiQueryLibrary/Types/Orientation.h"
#include "OsiQueryLibrary/Types/Side.h"

namespace osiql {
class World;

//! \brief An OpenDRIVE road with the limitation that all lanes within it have the same start and end values.
//! While a road has an Id, it is not part of osi3 and thus not an osiql::Identifiable.
//! Bidirectional lanes are not yet supported.
class Road
{
public:
    static Id quantity;
    //! Constructs a road
    //!
    //! \param detail::RoadSpecifications Details of the road's lanes
    Road(detail::RoadSpecifications &&);

    //! Returns the unique id of this road
    //!
    //! \return Id
    Id GetId() const;

    const RoadId &GetStreetId() const;

    //! Returns the lane closest to the given point. If the point is closest to multiple lanes, the one with
    //! the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned.
    //!
    //! \param point s- & t-coordinate
    //! \return const Lane&
    const Lane &GetClosestLane(const Coordinates<Road> &point) const;

    //! Returns the lane closest to the given point. If the point is closest to multiple lanes, the one with
    //! the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned.
    //!
    //! \param point s- & t-coordinate
    //! \return Lane&
    Lane &GetClosestLane(const Coordinates<Road> &point);

    //! Returns the lane closest to the given point on the given side. If the is closest to multiple lanes, the one
    //! with the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned. If no such lane exists, returns nullptr.
    //!
    //! \tparam Side Left or Right
    //! \param point
    //! \return const Lane*
    template <Side>
    const Lane *GetClosestLane(const Coordinates<Road> &point) const;

    //! Returns the lane closest to the given point on the given side. If the is closest to multiple lanes, the one
    //! with the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned. If no such lane exists, returns nullptr.
    //!
    //! \tparam Side Left or Right
    //! \param point
    //! \return Lane*
    template <Side>
    Lane *GetClosestLane(const Coordinates<Road> &point);

    //! Returns the lane closest to the given point on the given side. If the is closest to multiple lanes, the one
    //! with the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned. If no such lane exists, returns nullptr.
    //!
    //! \param point
    //! \param sideOfRoad Left or Right
    //! \return const Lane*
    const Lane *GetClosestLane(const Coordinates<Road> &point, Side sideOfRoad) const;

    //! Returns the lane closest to the given point on the given side. If the is closest to multiple lanes, the one
    //! with the smaller starting s-coordinate is returned. If all candidates have the same starting s-coordinate,
    //! the one closest to the other side of the road is returned. If no such lane exists, returns nullptr.
    //!
    //! \param point
    //! \param sideOfRoad Left or Right
    //! \return Lane*
    Lane *GetClosestLane(const Coordinates<Road> &point, Side sideOfRoad);

    //! Returns the lane that is the given number of lane changes away on the given side of the given lane.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \param lane
    //! \param offset
    //! \return const Lane*
    template <Side, Orientation = Orientation::Forwards>
    const Lane *GetAdjacentLane(const Lane &lane, size_t offset) const;

    //! Returns the lane that is the given number of lane changes away on the given side of the given lane.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \param lane
    //! \param offset
    //! \return Lane*
    template <Side, Orientation = Orientation::Forwards>
    Lane *GetAdjacentLane(const Lane &lane, size_t offset);

    //! Returns the lane that is the given number of lane changes away on the given side of the given lane.
    //!
    //! \tparam Orientation
    //! \param side
    //! \param lane
    //! \param offset
    //! \return const Lane*
    template <Orientation = Orientation::Forwards>
    const Lane *GetAdjacentLane(Side side, const Lane &lane, size_t offset) const;

    //! Returns the lane that is the given number of lane changes away on the given side of the given lane.
    //!
    //! \tparam Orientation
    //! \param side
    //! \param lane
    //! \param offset
    //! \return Lane*
    template <Orientation = Orientation::Forwards>
    Lane *GetAdjacentLane(Side side, const Lane &lane, size_t offset);

    //! Converts the given container of lanes to a container of unique roads containing said lanes.
    //!
    //! \param lanes
    //! \return std::vector<const Road *>
    static std::vector<const Road *> GetRoads(const std::vector<Lane *> &lanes);

    //! Returns all connected lanes at the end of the lanes on the given side of this road in the given orientation.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \return const std::vector<Lane *>&
    template <Side, Orientation>
    const std::vector<Lane *> &GetConnectedLanes() const;

    //! Returns all connected lanes at the end of the lanes on the given side of this road in the given orientation.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \param sideOfRoad Left or Right
    //! \return const std::vector<Lane *>&
    template <Orientation>
    const std::vector<Lane *> &GetConnectedLanes(Side sideOfRoad) const;

    //! Returns all connected lanes at the end of the lanes on the given side of this road in the given orientation.
    //!
    //! \tparam Side Left or Right
    //! \param Orientation Forwards or Backwards
    //! \return const std::vector<Lane *>&
    template <Side>
    const std::vector<Lane *> &GetConnectedLanes(Orientation) const;

    //! Returns all connected lanes at the end of the lanes on the given side of this road in the given orientation.
    //!
    //! \param sideOfRoad Left or Right
    //! \param Orientation Forwards or Backwards
    //! \return std::vector<Lane *>&
    const std::vector<Lane *> &GetConnectedLanes(Side sideOfRoad, Orientation) const;

    //! A relation of a road to an object of the given type.
    //! It holds the placement of the object and the lane on which it is placed.
    //!
    //! \tparam T
    template <typename T>
    struct Relation
    {
        //! \brief Constructs a road relation from a lane and an object placement
        Relation(const Lane &, const Placement<const T> &);

        //! Returns the lane associated with the object placement
        //!
        //! \return const Lane&
        const Lane &GetLane() const;

        //! Returns the object placement of this relation
        //!
        //! \return const Placement<const T>&
        const Placement<const T> &GetPlacement() const;

        //! \brief Returns the object referred to by the placement of this relation
        //!
        //! \return const T&
        const T &GetEntity() const;

    private:
        std::reference_wrapper<const Lane> lane;
        std::reference_wrapper<const Placement<const T>> placement;
    };

    //! Returns all object assignments of the given type assigned to any lane on the given side of this road.
    //!
    //! \tparam T
    //! \tparam Side Left or Right
    //! \return std::vector<Relation<T>>
    template <typename T, Side>
    std::vector<Relation<T>> GetAll() const;

    //! Returns all objects assigned to any lane of this road.
    //!
    //! \tparam T
    //! \param sideOfRoad Left or Right
    //! \return std::vector<Relation<T>>
    template <typename T>
    std::vector<Relation<T>> GetAll(Side sideOfRoad) const;

    //! Returns a local road point of the lane of this road closest to the given global point
    //!
    //! \param globalPoint
    //! \return RoadPoint
    RoadPoint GetPoint(const Vector2d &globalPoint) const;

    //! Returns a local road point of the lane of this road closest to the given global point
    //!
    //! \param globalPoint
    //! \return detail::RoadPoint
    detail::RoadPoint GetPoint(const Vector2d &globalPoint);

    //! Returns a local road point of the lane on the given side of this road closest to the given global point,
    //! Throws if this road does not have a lane in the given direction
    //!
    //! \tparam Side
    //! \param globalPoint
    //! \return RoadPoint
    template <Side>
    RoadPoint GetPoint(const Vector2d &globalPoint) const;

    //! Returns a local road point of the lane on the given side of this road closest to the given global point,
    //! Throws if this road does not have a lane in the given direction
    //!
    //! \tparam Side
    //! \param globalPoint
    //! \return detail::RoadPoint
    template <Side>
    detail::RoadPoint GetPoint(const Vector2d &globalPoint);

    //! Returns a local point of the lane in the given direction closest to or containing the given global point.
    //! Throws if this road does not have a lane in the given direction
    //!
    //! \param globalPoint
    //! \param sideOfRoad
    //! \return RoadPoint
    RoadPoint GetPoint(const Vector2d &globalPoint, Side sideOfRoad) const;

    //! Returns a local point of the lane in the given direction closest to or containing the given global point.
    //! Throws if this road does not have a lane in the given direction
    //!
    //! \param globalPoint
    //! \param sideOfRoad
    //! \return detail::RoadPoint
    detail::RoadPoint GetPoint(const Vector2d &globalPoint, Side sideOfRoad);

    //! Returns a local road pose of the lane of this road closest to the given global point
    //!
    //! \param globalPoint
    //! \param globalAngle
    //! \return Pose
    Pose<RoadPoint> GetPose(const Vector2d &globalPoint, double globalAngle) const;

    //! Returns a local road pose of the lane of this road closest to the given global point
    //!
    //! \param globalPoint
    //! \param globalAngle
    //! \return Pose
    Pose<detail::RoadPoint> GetPose(const Vector2d &globalPoint, double globalAngle);

    //! Returns a local road pose of the lane on the given side of this road closest to the given global point
    //!
    //! \tparam Side
    //! \param globalPoint
    //! \param globalAngle
    //! \return Pose
    template <Side>
    Pose<RoadPoint> GetPose(const Vector2d &globalPoint, double globalAngle) const;

    //! Returns a local road pose of the lane on the given side of this road closest to the given global point
    //!
    //! \tparam Side
    //! \param globalPoint
    //! \param globalAngle
    //! \return Pose
    template <Side>
    Pose<detail::RoadPoint> GetPose(const Vector2d &globalPoint, double globalAngle);

    //! Returns a local road pose of the lane on the given side of this road closest to the given global point
    //!
    //! \param globalPoint
    //! \param globalAngle
    //! \param sideOfRoad
    //! \return Pose
    Pose<RoadPoint> GetPose(const Vector2d &globalPoint, double globalAngle, Side sideOfRoad) const;

    //! Returns a local road pose of the lane on the given side of this road closest to the given global point
    //!
    //! \param globalPoint
    //! \param globalAngle
    //! \param sideOfRoad
    //! \return Pose
    Pose<detail::RoadPoint> GetPose(const Vector2d &globalPoint, double globalAngle, Side sideOfRoad);

    //! Returns the shortest distance between the closest lane of this road in the given driving direction and
    //! the given global point. If one such lane contains the given point, 0.0 is returned.
    //!
    //! \tparam Side
    //! \param globalPoint
    //! \return double
    template <Side>
    double GetDistanceTo(const Vector2d &globalPoint) const;

    //! Returns the shortest distance between the closest lane of this road in the given driving direction and
    //! the given global point. If one such lane contains the given point, 0.0 is returned.
    //!
    //! \param globalPoint
    //! \param sideOfRoad
    //! \return double
    double GetDistanceTo(const Vector2d &globalPoint, Side sideOfRoad) const;

    //! Returns the shortest distance between the closest lane of this road in the given driving direction and
    //! the given global point. If one such lane contains the given point, 0.0 is returned.
    //!
    //! \param globalPoint
    //! \return double
    double GetDistanceTo(const Vector2d &globalPoint) const;

    //! Returns the length of this road's reference line
    //!
    //! \return double
    double GetLength() const;

    //! \brief The OpenDRIVE Id of this road
    Id id; // TODO: To be removed.

    //! \brief The reference line that defines the road coordinate system of all lanes on this road
    const ReferenceLine &referenceLine; // TODO: To be deprecated

    //! \brief The lane rows that contain all lanes of this road
    LaneRows laneRows;

    //! \brief The boundaries from right to left of all lanes on this road
    std::vector<Boundary> boundaries;

private:
    friend class osiql::World; // Needs to modify connected lanes

    std::vector<Lane *> downstreamPredecessors;
    std::vector<Lane *> upstreamPredecessors;
    std::vector<Lane *> downstreamSuccessors;
    std::vector<Lane *> upstreamSuccessors;
};

bool operator<(const Road &lhs, const Road &rhs);
bool operator<(const Road &lhs, Id rhs);
bool operator<(Id lhs, const Road &rhs);
bool operator==(const Road &lhs, const Road &rhs);
bool operator!=(const Road &lhs, const Road &rhs);

std::ostream &operator<<(std::ostream &, const Road &);
template <typename T>
std::ostream &operator<<(std::ostream &, const Road::Relation<T> &);
} // namespace osiql

// ---------- Template definitions ----------

namespace osiql {
template <typename T>
Road::Relation<T>::Relation(const Lane &lane, const Placement<const T> &placement) :
    lane{lane}, placement{placement}
{
}

template <typename T>
const Lane &Road::Relation<T>::GetLane() const
{
    return lane;
}
template <typename T>
const Placement<const T> &Road::Relation<T>::GetPlacement() const
{
    return placement;
}
template <typename T>
const T &Road::Relation<T>::GetEntity() const
{
    return GetPlacement().GetEntity();
}

template <Direction D>
double GetLatitude(const Road &road)
{
    return GetLatitude<D>(road.referenceLine);
}

template <typename T, Side S>
std::vector<Road::Relation<T>> Road::GetAll() const
{
    std::vector<Relation<T>> result;
    const size_t size{std::accumulate(laneRows.begin<S>(), laneRows.end<S>(), 0u, [](size_t entries, const LaneRow &row) {
        return entries + std::accumulate(row.begin(), row.end(), 0u, [](size_t entries, const Lane *lane) {
                   return entries + lane->GetAll<T>().size();
               });
    })};
    result.reserve(size);
    for (auto it{laneRows.begin<S>()}; it != laneRows.end<S>(); ++it)
    {
        for (const Lane *lane : *it)
        {
            std::transform(lane->GetAll<T>().begin(), lane->GetAll<T>().end(), std::back_inserter(result), [lane](const auto &placement) {
                return Road::Relation<T>{*lane, placement};
            });
        }
    }
    std::sort(result.begin(), result.end(), [](const Relation<T> &lhs, const Relation<T> &rhs) {
        return (lhs.GetEntity() == rhs.GetEntity()) ? (lhs.GetLane() < rhs.GetLane()) : (lhs.GetEntity() < rhs.GetEntity());
    });
    return result;
}

template <typename T>
std::vector<Road::Relation<T>> Road::GetAll(Side sideOfRoad) const
{
    return sideOfRoad == Side::Left ? GetAll<T, Side::Left>() : GetAll<T, Side::Right>();
}

template <Side S, Orientation O>
const std::vector<Lane *> &Road::GetConnectedLanes() const
{
    if constexpr (O == Orientation::Forwards && S == Side::Left)
    {
        return upstreamSuccessors;
    }
    else if constexpr (O == Orientation::Forwards && S == Side::Right)
    {
        return downstreamSuccessors;
    }
    else if constexpr (S == Side::Left)
    {
        return upstreamPredecessors;
    }
    else
    {
        return downstreamPredecessors;
    }
}

template <Side S>
const std::vector<Lane *> &Road::GetConnectedLanes(Orientation o) const
{
    return o == Orientation::Forwards ? GetConnectedLanes<S, Orientation::Forwards>() : GetConnectedLanes<S, Orientation::Backwards>();
}

template <Orientation O>
const std::vector<Lane *> &Road::GetConnectedLanes(Side sideOfRoad) const
{
    return sideOfRoad == Side::Left ? GetConnectedLanes<Side::Left, O>() : GetConnectedLanes<Side::Right, O>();
}

template <Side S, Orientation O>
const Lane *Road::GetAdjacentLane(const Lane &lane, size_t offset) const
{
    if constexpr ((S == Side::Left) == (O == Orientation::Forwards))
    {
        const std::ptrdiff_t laneOffset{static_cast<std::ptrdiff_t>(lane.index) - static_cast<std::ptrdiff_t>(offset)};
        if (GetSideOfRoad(lane) == Side::Left)
        {
            if (laneOffset < -static_cast<std::ptrdiff_t>(laneRows.size<Side::Right>()))
            {
                return nullptr;
            }
            const LaneRow &row{*std::next(laneRows.begin<Side::Left>(), laneOffset)};
            return row.GetLane<O>(GetLatitude<O>(lane));
        }
        else
        {
            if (laneOffset < -static_cast<std::ptrdiff_t>(laneRows.size<Side::Left>()))
            {
                return nullptr;
            }
            const LaneRow &row{*std::next(laneRows.begin<Side::Right>(), laneOffset)};
            return row.GetLane<O>(GetLatitude<O>(lane));
        }
    }
    else
    {
        const size_t i{lane.index + offset};
        if (i >= laneRows.size(GetSideOfRoad(lane)))
        {
            return nullptr;
        }
        return laneRows.at(GetSideOfRoad(lane), i).GetLane<O>(GetLatitude<O>(lane));
    }
}
template <Side S, Orientation O>
Lane *Road::GetAdjacentLane(const Lane &lane, size_t offset)
{
    return const_cast<Lane *>(const_cast<const Road *>(this)->GetAdjacentLane<S, O>(lane, offset));
}

template <Orientation O>
const Lane *Road::GetAdjacentLane(Side side, const Lane &lane, size_t offset) const
{
    return side == Side::Left ? GetAdjacentLane<Side::Left, O>(lane, offset) : GetAdjacentLane<Side::Right, O>(lane, offset);
}
template <Orientation O>
Lane *Road::GetAdjacentLane(Side side, const Lane &lane, size_t offset)
{
    return const_cast<Lane *>(const_cast<const Road *>(this)->GetAdjacentLane<O>(side, lane, offset));
}

template <Side S>
const Lane *Road::GetClosestLane(const Coordinates<Road> &point) const
{
    if (laneRows.size<S>() == 0u)
    {
        return nullptr;
    }
    const LaneRow &lanes{laneRows.at<S>(0u)};
    constexpr const osiql::Direction D{osiql::GetDirection<S>()};
    // First lane in driving direction that ends after the point's latitude
    const Lane *lane{*std::upper_bound(lanes.begin<D>(), std::prev(lanes.end<D>()), point.latitude, [](double latitude, const Lane *lane) {
        return osiql::greater<D>(lane->GetEndLatitude<D>(), latitude);
    })};
    const auto hasAdjacentLanes = [](const Lane &lane) { return !lane.GetAdjacentLanes<S, Direction::Downstream>().empty(); };
    const auto hasNotPassedTarget = [&point](const Lane &lane) { return osiql::greater<D>(lane.GetBoundaryPoints<S, Direction::Downstream>().GetLongitude(point.latitude), point.longitude); };
    while (hasAdjacentLanes(*lane) && hasNotPassedTarget(*lane))
    {
        const Lane *nextLane{lane->GetAdjacentLane<S, D>(point.latitude)};
        if (nextLane->GetSideOfRoad() != S)
        {
            break;
        }
        lane = nextLane;
    }
    return lane;
}
template <Side S>
Lane *Road::GetClosestLane(const Coordinates<Road> &point)
{
    return const_cast<Lane *>(const_cast<const Road *>(this)->GetClosestLane<S>(point));
}

template <Side S>
RoadPoint Road::GetPoint(const Vector2d &globalPoint) const
{
    Coordinates<Road> coordinates{referenceLine.GetCoordinates(globalPoint)};
    const Lane *lane{GetClosestLane<S>(coordinates)};
    if (!lane)
    {
        std::ostringstream stream;
        stream << "Road::GetPoint<" << S << ">: No lanes on the given side of this road exist.\n";
        throw std::runtime_error(stream.str());
    }
    return {*lane, std::move(coordinates)};
}

template <Side S>
detail::RoadPoint Road::GetPoint(const Vector2d &globalPoint)
{
    Coordinates<Road> coordinates{referenceLine.GetCoordinates(globalPoint)};
    Lane *lane{GetClosestLane<S>(coordinates)};
    if (!lane)
    {
        std::ostringstream stream;
        stream << "Road::GetPoint<" << S << ">: No lanes on the given side of this road exist.\n";
        throw std::runtime_error(stream.str());
    }
    return {*lane, std::move(coordinates)};
}

template <Side S>
Pose<RoadPoint> Road::GetPose(const Vector2d &globalPoint, double angle) const
{
    Pose<Coordinates<Road>> pose{referenceLine.GetPose(globalPoint, angle)};
    const Lane *lane{GetClosestLane<S>(pose)};
    if (!lane)
    {
        std::ostringstream stream;
        stream << "Road::GetPose<" << S << ">: No lanes on the given side of this road exist.\n";
        throw std::runtime_error(stream.str());
    }
    return Pose<RoadPoint>{*lane, std::move(pose)};
}

template <Side S>
Pose<detail::RoadPoint> Road::GetPose(const Vector2d &globalPoint, double angle)
{
    Pose<Coordinates<Road>> pose{referenceLine.GetPose(globalPoint, angle)};
    Lane *lane{GetClosestLane<S>(pose)};
    if (!lane)
    {
        std::ostringstream stream;
        stream << "Road::GetPose<" << S << ">: No lanes on the given side of this road exist.\n";
        throw std::runtime_error(stream.str());
    }
    return Pose<detail::RoadPoint>{*lane, std::move(pose)};
}

template <Side S>
double Road::GetDistanceTo(const Vector2d &globalPoint) const
{
    const Coordinates<Road> coordinates{referenceLine.GetCoordinates(globalPoint)};
    const Lane *lane{GetClosestLane<S>(coordinates)};
    return lane ? lane->GetDistanceTo(globalPoint) : std::numeric_limits<double>::quiet_NaN();
}
template <typename T>
std::ostream &operator<<(std::ostream &os, const Road::Relation<T> &relation)
{
    return os << '{' << relation.GetPlacement() << " on Lane " << relation.GetLane().GetId() << '}';
}
} // namespace osiql
