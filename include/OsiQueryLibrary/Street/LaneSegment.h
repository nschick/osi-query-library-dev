/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/Boundary.h"

namespace osiql {
class Lane;
//! \brief A LaneSegment is a triangle that is part of a Lane or Road. Its bounds are
//! used as a key in location queries to find whether a point lies on a lane.
struct LaneSegment
{
    //! Constructs a lane segment from its components
    //!
    //! \param lane
    //! \param triangle
    LaneSegment(Lane &lane, std::array<Vector2d, 3> &&triangle);

    //! Returns the global bounds of this segment's shape
    //!
    //! \return Box
    Box GetBounds() const;

    //! Returns the shortest distance from this segment to a global point. Returns 0.0 if the point is inside the segment.
    //!
    //! \param globalPoint
    //! \return double
    double GetDistanceTo(const Vector2d &globalPoint) const;

    //! Returns whether the given point lies inside or on the edges of this segment.
    //!
    //! \param point global x,y-coordinate point
    //! \return whether the given point lies inside or on the edges of this segment
    bool Contains(const Vector2d &point) const;

    //! Returns the lane this segment is a part of
    //!
    //! \return const Lane&
    const Lane &GetLane() const;

    //! Returns the lane this segment is a part of
    //!
    //! \return Lane&
    Lane &GetLane();

    //! \brief The triangle defining the lane segment's shape. It has counter-clockwise winding.
    std::array<Vector2d, 3> triangle;

    // Lane must be a reference_wrapper instead of a reference because boost copy
    // assigns values in its rtree when rearranging. (can't have default ctor with reference)
private:
    //! \brief The lane this segment is a part of. Never null, but still a pointer because LaneSegments are used in
    //! boost's rTree, which lacks robust copy/move semantics.
    std::reference_wrapper<Lane> lane;
};

std::ostream &operator<<(std::ostream &os, const LaneSegment &segment);
} // namespace osiql
