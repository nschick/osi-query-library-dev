/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Component of a lane that allows it to access its driving direction and associated distances

#include <osi3/osi_logicallane.pb.h>

#include "OsiQueryLibrary/Component/Comparable.h"
#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Types/Direction.h"
#include "OsiQueryLibrary/Types/Orientation.h"
#include "OsiQueryLibrary/Types/Side.h"

namespace osiql {
namespace detail {
struct OrientedLane : Identifiable<osi3::LogicalLane>
{
    using Identifiable<osi3::LogicalLane>::Identifiable;

    //! Returns the driving direction of traversing this lane in the given orientation. Downstream means
    //! traversal in increasing s-coordinate whereas Upstream means traversal in decreasing s-coordinate.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \return Direction Downstream or Upstream
    template <Orientation = Orientation::Forwards>
    Direction GetDirection() const;

    //! Returns the driving direction of traversing this lane in the given orientation. Downstream means
    //! traversal in increasing s-coordinate whereas Upstream means traversal in decreasing s-coordinate.
    //!
    //! \return Direction
    Direction GetDirection(Orientation) const;

    //! Returns what orientation the given direction corresponds to relative to this lane's direction.
    //!
    //! \param direction
    //! \return Orientation
    Orientation GetOrientation(Direction) const;

    //! Returns what side of the road this lane is on.
    //!
    //! \return Side Left if this lane's driving direction is Upstream, otherwise Right.
    Side GetSideOfRoad() const;

    //! Returns the latitude (s-coordinate) at the given distance from the start of this lane in the given direction
    //!
    //! \tparam Direction
    //! \param distanceFromStartOfLane Distance from the start of the lane in the given direction
    //! \return double
    template <Direction>
    double GetLatitude(double distanceFromStartOfLane) const;

    //! Returns the latitude (s-coordinate) at the given distance from the start of this lane in the given direction
    //!
    //! \param distanceFromStartOfLane Distance from the start of the lane in the given direction
    //! \param d Downstream or Upstream
    //! \return double
    double GetLatitude(double distanceFromStartOfLane, Direction d) const;

    //! Returns the road latitude at the start of this lane in driving direction
    //!
    //! \tparam O Forwards or Backwards
    //! \return double
    template <Orientation O = Orientation::Forwards>
    double GetStartLatitude() const;

    //! Returns the road latitude at the end of this lane in driving direction.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \return double
    template <Orientation = Orientation::Forwards>
    double GetEndLatitude() const;

    //! Returns the road latitude at the start of this lane in the given driving direction.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \return double
    template <Direction>
    double GetStartLatitude() const;

    //! Returns the road latitude at the start of this lane in the given driving direction.
    //!
    //! \return double
    double GetStartLatitude(Direction) const;

    //! Returns the road latitude at the end of this lane in the given driving direction.
    //!
    //! \tparam D Downstream or Upstream
    //! \return double
    template <Direction D>
    double GetEndLatitude() const;

    //! Returns the road latitude at the end of this lane in the given driving direction.
    //!
    //! \return double
    double GetEndLatitude(Direction) const;

    //! Returns the signed distance from a to b in the given orientation.
    //!
    //! \tparam Orientation Forwards or Backwards
    template <Orientation = Orientation::Forwards, typename T = double, typename U = double>
    double GetDistanceBetween(const T &a, const U &b) const;

    //! Returns the signed distance from a to b in the given orientation.
    //!
    //! \param a
    //! \param b
    //! \return double
    template <typename T, typename U>
    double GetDistanceBetween(const T &a, const U &b, Orientation) const;
};
} // namespace detail

//! Returns the road-relative latitude at one of the ends of the given lane based on the passed direction and orientation
//!
//! \tparam D Downstream or Upstream
//! \tparam O Forwards or Backwards
//! \param lane
//! \return double
template <Direction D, Orientation O = Orientation::Forwards>
double GetLatitude(const detail::OrientedLane &lane);

//! Returns the road-relative latitude at one of the ends of the given lane based on the passed direction and orientation
//!
//! \tparam D Downstream or Upstream
//! \param lane
//! \param o Forwards or Backwards
//! \return double
template <Direction D>
double GetLatitude(const detail::OrientedLane &lane, Orientation o);

//! Returns the road-relative latitude at one of the ends of the given lane based on the passed direction and orientation
//!
//! \tparam O Forwards or Backwards
//! \param lane
//! \param d Downstream or Upstream
//! \return double
template <Orientation O>
double GetLatitude(const detail::OrientedLane &lane, Direction d);

//! Returns the road-relative latitude at one of the ends of the given lane based on the passed direction and orientation
//!
//! \param lane
//! \param d Downstream or Upstream
//! \param o Forwards or Backwards
//! \return double
double GetLatitude(const detail::OrientedLane &lane, Direction d, Orientation o);

//! Returns the road-relative latitude at the start of the given lane in the given orientation
//!
//! \tparam Orientation Forwards or Backwards
//! \param lane
//! \return double
template <Orientation O>
double GetLatitude(const detail::OrientedLane &lane);

//! Returns the road-relative latitude at the start of the given lane in the given driving direction
//!
//! \tparam D Downstream or Upstream
//! \param lane
//! \return double
template <Direction D>
double GetLatitude(const detail::OrientedLane *lane);
} // namespace osiql

namespace osiql {
namespace detail {
template <Orientation O>
Direction OrientedLane::GetDirection() const
{
    if constexpr (O == Orientation::Forwards)
    {
        assert(GetHandle().has_move_direction());
        return static_cast<Direction>(GetHandle().move_direction());
    }
    else
    {
        assert(GetHandle().has_move_direction());
        return !static_cast<Direction>(GetHandle().move_direction());
    }
}

template <Direction D>
double OrientedLane::GetLatitude(double distance) const
{
    if constexpr (D == Direction::Upstream)
    {
        return osiql::GetLatitude<D>(*this) - distance;
    }
    else if constexpr (D == Direction::Downstream)
    {
        return osiql::GetLatitude<D>(*this) + distance;
    }
    else
    {
        static_assert(D == Direction::Upstream, "Lane::GetLatitude called without valid direction. Must be either Upstream or Downstream.");
    }
}

template <Orientation O>
double OrientedLane::GetStartLatitude() const
{
    return osiql::GetLatitude(*this, GetDirection<O>());
}

template <Orientation O>
double OrientedLane::GetEndLatitude() const
{
    return osiql::GetLatitude(*this, !GetDirection<O>());
}

template <Direction D>
double OrientedLane::GetStartLatitude() const
{
    return osiql::GetLatitude<D>(*this);
}

template <Direction D>
double OrientedLane::GetEndLatitude() const
{
    return osiql::GetLatitude<!D>(*this);
}

template <Orientation O, typename T, typename U>
double OrientedLane::GetDistanceBetween(const T &a, const U &b) const
{
    if (GetDirection<O>() == Direction::Downstream)
    {
        return osiql::GetLatitude(b) - osiql::GetLatitude(a);
    }
    else
    {
        return osiql::GetLatitude<Direction::Upstream>(a) - osiql::GetLatitude<Direction::Upstream>(b);
    }
}

template <typename T, typename U>
double OrientedLane::GetDistanceBetween(const T &a, const U &b, Orientation o) const
{
    return o == Orientation::Backwards ? GetDistanceBetween<Orientation::Backwards>(a, b) : GetDistanceBetween(a, b);
}
} // namespace detail

template <Direction D, Orientation O>
double GetLatitude(const detail::OrientedLane &lane)
{
    if constexpr ((O == Orientation::Forwards) == (D == Direction::Upstream))
    {
        return lane.GetHandle().end_s();
    }
    else
    {
        return lane.GetHandle().start_s();
    }
}

template <Direction D>
double GetLatitude(const detail::OrientedLane &lane, Orientation o)
{
    return o == Orientation::Backwards ? GetLatitude<D, Orientation::Backwards>(lane) : GetLatitude<D, Orientation::Forwards>(lane);
}

template <Orientation O>
double GetLatitude(const detail::OrientedLane &lane, Direction d)
{
    return d == Direction::Upstream ? GetLatitude<Direction::Upstream, O>(lane) : GetLatitude<Direction::Downstream, O>(lane);
}

template <Orientation O>
double GetLatitude(const detail::OrientedLane &lane)
{
    return lane.GetDirection() == Direction::Upstream ? GetLatitude<Direction::Upstream, O>(lane) : GetLatitude<Direction::Downstream, O>(lane);
}

template <Direction D>
double GetLatitude(const detail::OrientedLane *lane)
{
    return GetLatitude<D>(*lane);
}
} // namespace osiql
