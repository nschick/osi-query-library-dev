/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/Lane/BoundaryEnclosure.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Direction.h"
#include "OsiQueryLibrary/Types/Orientation.h"

namespace osiql::detail {
template <typename Lane>
struct Connectable : BoundaryEnclosure
{
    using BoundaryEnclosure::BoundaryEnclosure;

    //! Returns the id of this lane's road.
    //!
    //! \return const RoadId&
    const RoadId &GetRoadId() const;

    //! Returns a container of all lanes connected to the end of this one
    //! in the driving direction of the given orientation
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \return const std::vector<Lane *>&
    template <Orientation = Orientation::Forwards>
    const std::vector<Lane *> &GetConnectedLanes() const;

    //! Returns a container of all lanes connected to the end of this one
    //! in the driving direction of the given orientation
    //!
    //! \param orientation Forwards or Backwards
    //! \return const std::vector<Lane *>&
    const std::vector<Lane *> &GetConnectedLanes(Orientation orientation);

    //! Returns the lane connected to the end of this one in the given direction
    //! that is part of the road with the given id or nullptr if no such lane exists.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \param id
    //! \return Lane*
    template <Orientation>
    const Lane *GetConnectedLane(const RoadId &) const;

    //! Returns the lane connected to the end of this one in the given direction
    //! that is part of the road with the given id or nullptr if no such lane exists.
    //!
    //! \tparam Orientation
    //! \return Lane*
    template <Orientation>
    Lane *GetConnectedLane(const RoadId &);

    //! Returns the lane connected to the end of this one in the given driving direction
    //! that is part of the road with the given id or nullptr if no such lane exists.
    //!
    //! \param orientation Forwards or Backwards
    //! \return const Lane*
    const Lane *GetConnectedLane(const RoadId &, Orientation orientation) const;

protected:
    template <Orientation>
    std::vector<Lane *> &GetConnectedLanes();

    Lane *GetConnectedLane(const RoadId &, Orientation);

    std::vector<Lane *> outgoingLanes;
    std::vector<Lane *> incomingLanes;
};
} // namespace osiql::detail

namespace osiql::detail {

template <typename Lane>
const RoadId &Connectable<Lane>::GetRoadId() const
{
    if (GetHandle().source_reference().empty())
    {
        std::cerr << "Lane::GetRoad(): Lane " << GetId() << " has no source reference. Unable to return road id.\n";
        return UNDEFINED_ROAD_ID;
    }
    else if (GetHandle().source_reference(0).identifier().empty())
    {
        std::cerr << "Lane::GetRoad(): Source reference of lane " << GetId() << " is empty. Unable to return road id.\n";
        return UNDEFINED_ROAD_ID;
    }
    return GetHandle().source_reference(0).identifier(0);
}

template <typename Lane>
template <Orientation O>
const std::vector<Lane *> &Connectable<Lane>::GetConnectedLanes() const
{
    if constexpr (O == Orientation::Forwards)
    {
        return outgoingLanes;
    }
    else
    {
        return incomingLanes;
    }
}

template <typename Lane>
template <Orientation O>
std::vector<Lane *> &Connectable<Lane>::GetConnectedLanes()
{
    return const_cast<std::vector<Lane *> &>(const_cast<const Connectable<Lane> *>(this)->GetConnectedLanes<O>());
}

template <typename Lane>
const std::vector<Lane *> &Connectable<Lane>::GetConnectedLanes(Orientation o)
{
    return o == Orientation::Forwards ? GetConnectedLanes<Orientation::Forwards>() : GetConnectedLanes<Orientation::Backwards>();
}

template <typename Lane>
template <Orientation O>
const Lane *Connectable<Lane>::GetConnectedLane(const RoadId &id) const
{
    const std::vector<Lane *> &lanes{GetConnectedLanes<O>()};
    const auto it{std::find_if(lanes.begin(), lanes.end(), [&id](const Lane *lane) { return id == lane->GetRoadId(); })};
    return it == lanes.end() ? nullptr : *it;
}

template <typename Lane>
template <Orientation O>
Lane *Connectable<Lane>::GetConnectedLane(const RoadId &id)
{
    return const_cast<Lane *>(const_cast<const Connectable<Lane> *>(this)->GetConnectedLane<O>(id));
}

template <typename Lane>
const Lane *Connectable<Lane>::GetConnectedLane(const RoadId &id, Orientation o) const
{
    return o == Orientation::Forwards ? GetConnectedLane<Orientation::Forwards>(id) : GetConnectedLane<Orientation::Backwards>(id);
}

template <typename Lane>
Lane *Connectable<Lane>::GetConnectedLane(const RoadId &id, Orientation o)
{
    return const_cast<Lane *>(const_cast<const Connectable<Lane> *>(this)->GetConnectedLane(id, o));
}
} // namespace osiql::detail
