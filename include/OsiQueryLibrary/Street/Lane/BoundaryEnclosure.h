/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Point/Vector2d.h"
#include "OsiQueryLibrary/Street/Boundary.h"
#include "OsiQueryLibrary/Street/Lane/OrientedLane.h"
#include "OsiQueryLibrary/Types/Direction.h"
#include "OsiQueryLibrary/Types/Orientation.h"
#include "OsiQueryLibrary/Types/Side.h"

namespace osiql::detail {
//! \brief A boundary enclosure is a component of a lane that defines getters to its
//! boundary points and logical partitions. These members are not public because
//! what constitutes the left and right boundary depends on the lane's intended
//! driving direction and the orientation in which it is being traversed.
struct BoundaryEnclosure : OrientedLane
{
    using OrientedLane::OrientedLane;

    //! Returns the boundary points on the given side of the given orientation relative to this
    //! lane's direction.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \return const Boundary::Points&
    template <Side, Orientation = Orientation::Forwards>
    const Boundary::Points &GetBoundaryPoints() const;

    //! Returns the boundary points on the given side of the given orientation relative to this
    //! lane's direction.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \return const Boundary::Points&
    template <Orientation = Orientation::Forwards>
    const Boundary::Points &GetBoundaryPoints(Side) const;

    //! Returns the boundary points on the given side of the given orientation relative to this
    //! lane's direction.
    //!
    //! \return const Boundary::Points&
    const Boundary::Points &GetBoundaryPoints(Side, Orientation) const;

    //! Returns the points of the boundary on the given side when driving in the given direction
    //!
    //! \tparam Side Left or Right relative to the driving direction
    //! \tparam Direction Downstream or Upstream relative to the point order of the boundaries
    //! \return const Boundary::Points&
    template <Side, Direction>
    const Boundary::Points &GetBoundaryPoints() const;

    //! Returns the boundary partitions on the given side of the given orientation relative to
    //! this lane's direction.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \return const Boundary::Partitions&
    template <Side, Orientation = Orientation::Forwards>
    const Boundary::Partitions &GetBoundaryPartitions() const;

    //! Returns the boundary partitions on the given side of the given orientation relative to
    //! this lane's direction.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \return const Boundary::Partitions&
    template <Orientation = Orientation::Forwards>
    const Boundary::Partitions &GetBoundaryPartitions(Side) const;

    //! Returns the boundary partitions on the given side of the given direction
    //!
    //! \tparam Side Left or Right
    //! \tparam Direction Downstream or Upstream
    //! \return const Boundary::Partitions&
    template <Side, Direction>
    const Boundary::Partitions &GetBoundaryPartitions() const;

    //! Returns whether this lane's boundary markings allow an object on it to pass to
    //! an adjacent lane on the given side at the given s-coordinate.
    //!
    //! \tparam Side Left of Right in driving direction
    //! \tparam Orientation Forwards or Backwards
    //! \param s s-coordinate contained by the boundary partitions whose passing rule will be evaluated. If this
    //! touches multiple partitions, the latter one in driving direction (based on orientation) is examined
    //! \return bool
    template <Side, Orientation = Orientation::Forwards>
    bool CanPass(double s) const;

    //! Converts a relative anchor point to local coordinates. Anchor::REAR_LEFT represents the left start
    //! of the lane in the given direction and Anchor::FRONT_RIGHT represents the right end of the lane.
    //!
    //! \tparam T
    //! \tparam Direction Downstream or Upstream
    //! \param anchor
    //! \return Coordinates<T>
    template <typename T, Direction>
    Coordinates<T> AnchorToCoords(const Vector2d &anchor) const;

    //! Converts a relative anchor point to local coordinates. Anchor::REAR_LEFT represents the left start
    //! of the lane in the given direction and Anchor::FRONT_RIGHT represents the right end of the lane.
    //!
    //! \tparam T
    //! \param anchor
    //! \param direction Downstream or Upstream
    //! \return Coordinates<T>
    template <typename T>
    Coordinates<T> AnchorToCoords(const Vector2d &anchor, Direction direction) const;

    //! Converts a relative anchor point to local coordinates. Anchor::REAR_LEFT represents the left start
    //! of the lane in the given orientation and Anchor::FRONT_RIGHT represents the right end of the lane.
    //!
    //! \tparam T
    //! \tparam O Forwards or Backwards
    //! \param anchor
    //! \return Coordinates<T>
    template <typename T, Orientation O = Orientation::Forwards>
    Coordinates<T> AnchorToCoords(const Vector2d &anchor) const;

    //! Converts a relative anchor point to local coordinates. Anchor::REAR_LEFT represents the left start
    //! of the lane in the given orientation and Anchor::FRONT_RIGHT represents the right end of the lane.
    //!
    //! \tparam T
    //! \param anchor
    //! \param orientation Forwards or Backwards
    //! \return Coordinates<T>
    template <typename T>
    Coordinates<T> AnchorToCoords(const Vector2d &anchor, Orientation orientation) const;

    //! Returns whether the given point lies inside or on the border of this lane.
    //!
    //! \param point
    //! \return bool
    bool Contains(const osiql::RoadPoint &point) const;

    //! Returns the angle of this lane's centerline at the given latitude in the given orientation.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \param latitude
    //! \return double
    template <Orientation = Orientation::Forwards>
    double GetAngle(double latitude) const;

    //! Returns the angle of this lane's centerline at the given latitude in the given orientation.
    //!
    //! \param latitude
    //! \param Orientation Forwards or Backwards
    //! \return double
    double GetAngle(double latitude, Orientation) const;

    //! Returns the angle of this lane's centerline at the given latitude in the given direction.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param latitude
    //! \return double
    template <Direction>
    double GetAngle(double latitude) const;

    //! Returns the angle of this lane's centerline at the given latitude in the given direction.
    //!
    //! \param latitude
    //! \param Direction Downstream or Upstream
    //! \return double
    double GetAngle(double latitude, Direction) const;

    //! Returns the average of the curvatures of this lane's left and right boundary at the given latitude.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \param latitude
    //! \return double
    template <Orientation = Orientation::Forwards>
    double GetCurvature(double latitude) const;

    //! Returns the average of the curvatures of this lane's left and right boundary at the given latitude.
    //!
    //! \param latitude
    //! \param Orientation Forwards or Backwards
    //! \return double
    double GetCurvature(double latitude, Orientation) const;

    //! Returns the average of the curvatures of this lane's left and right boundary at the given latitude.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param latitude
    //! \return double
    template <Direction>
    double GetCurvature(double latitude) const;

    //! Returns the average of the curvatures of this lane's left and right boundary at the given latitude.
    //!
    //! \param latitude
    //! \param Direction Downstream or Upstream
    //! \return double
    double GetCurvature(double latitude, Direction) const;

protected:
    template <Side, Direction>
    bool CanPass(double s) const;

    Boundary::Points leftBoundaryPoints;
    Boundary::Points rightBoundaryPoints;

    Boundary::Partitions leftBoundaryPartitions;
    Boundary::Partitions rightBoundaryPartitions;
};
} // namespace osiql::detail

namespace osiql::detail {
template <Side S, Direction D>
const Boundary::Points &BoundaryEnclosure::GetBoundaryPoints() const
{
    if constexpr ((S == Side::Left) == (D == Direction::Upstream))
    {
        return rightBoundaryPoints;
    }
    else
    {
        return leftBoundaryPoints;
    }
}

template <Side S, Orientation O>
const Boundary::Points &BoundaryEnclosure::GetBoundaryPoints() const
{
    return GetDirection<O>() == Direction::Upstream ? GetBoundaryPoints<S, Direction::Upstream>() : GetBoundaryPoints<S, Direction::Downstream>();
}

template <Orientation O>
const Boundary::Points &BoundaryEnclosure::GetBoundaryPoints(Side s) const
{
    return s == Side::Left ? GetBoundaryPoints<Side::Left, O>() : GetBoundaryPoints<Side::Right, O>();
}

template <Side S, Direction D>
const Boundary::Partitions &BoundaryEnclosure::GetBoundaryPartitions() const
{
    if constexpr ((S == Side::Left) == (D == Direction::Upstream))
    {
        return rightBoundaryPartitions;
    }
    else
    {
        return leftBoundaryPartitions;
    }
}

template <Orientation O>
const Boundary::Partitions &BoundaryEnclosure::GetBoundaryPartitions(Side s) const
{
    return s == Side::Left ? GetBoundaryPartitions<Side::Left, O>() : GetBoundaryPartitions<Side::Right, O>();
}

template <Side S, Orientation O>
const Boundary::Partitions &BoundaryEnclosure::GetBoundaryPartitions() const
{
    return GetDirection<O>() == Direction::Upstream ? GetBoundaryPartitions<S, Direction::Upstream>() : GetBoundaryPartitions<S, Direction::Downstream>();
}

template <Side S, Direction D>
bool BoundaryEnclosure::CanPass(double s) const
{
    const Boundary::Partitions &partitions{GetBoundaryPartitions<S>()};
    std::vector<Boundary::Partition>::const_iterator it{partitions.begin<D>(s)};
    return it->AllowsPassing<S, D>();
    // return GetBoundaryPartitions<S>().begin<D>(s)->AllowsPassing<S, D>();
}

template <Side S, Orientation O>
bool BoundaryEnclosure::CanPass(double s) const
{
    return GetDirection<O>() == Direction::Upstream ? CanPass<S, Direction::Upstream>(s) : CanPass<S, Direction::Downstream>(s);
}

template <typename T, Direction D>
Coordinates<T> BoundaryEnclosure::AnchorToCoords(const Vector2d &anchor) const
{
    if constexpr (D != Direction::Upstream && D != Direction::Downstream)
    {
        static_assert(always_false<T>, "Lane::AnchorToCoords: Direction must be either Upstream or Downstream");
    }
    else if constexpr (std::is_same_v<T, Road>)
    {
        const double startLatitude{osiql::GetLatitude<D>(*this)};
        const double endLatitude{osiql::GetLatitude<!D>(*this)};
        const double length{endLatitude - startLatitude};
        const double latitude{startLatitude + (anchor.x + 0.5) * length};
        const double leftLongitude{GetBoundaryPoints<Side::Left>().GetLongitude(latitude)};
        const double rightLongitude{GetBoundaryPoints<Side::Right>().GetLongitude(latitude)};
        const double width{rightLongitude - leftLongitude};
        const double longitude{leftLongitude + (anchor.y + 0.5) * width};
        return {latitude, longitude};
    }
    else if constexpr (std::is_same_v<T, Lane>)
    {
        const double startLatitude{osiql::GetLatitude<D>(*this)};
        const double endLatitude{osiql::GetLatitude<!D>(*this)};
        const double latitude{(anchor.x + 0.5) * (endLatitude - startLatitude)};
        const double leftLongitude{GetBoundaryPoints<Side::Left>().GetLongitude(latitude)};
        const double rightLongitude{GetBoundaryPoints<Side::Right>().GetLongitude(latitude)};
        const double longitude{(anchor.y + 0.5) * (rightLongitude - leftLongitude)};
        return {latitude, longitude};
    }
    else
    {
        static_assert(always_false<T>, "Lane::AnchorToCoords must be called with template type Road or Lane");
    }
}

template <typename T>
Coordinates<T> BoundaryEnclosure::AnchorToCoords(const Vector2d &anchor, Direction d) const
{
    return (d == Direction::Upstream) ? AnchorToCoords<T, Direction::Upstream>(anchor) : AnchorToCoords<T, Direction::Downstream>(anchor);
}

template <typename T, Orientation O>
Coordinates<T> BoundaryEnclosure::AnchorToCoords(const Vector2d &anchor) const
{
    return (GetDirection<O>() == Direction::Upstream) ? AnchorToCoords<T, Direction::Upstream>(anchor) : AnchorToCoords<T, Direction::Downstream>(anchor);
}

template <typename T>
Coordinates<T> BoundaryEnclosure::AnchorToCoords(const Vector2d &anchor, Orientation o) const
{
    return (o == Orientation::Forwards) ? AnchorToCoords<T, Orientation::Forwards>(anchor) : AnchorToCoords<T, Orientation::Backwards>(anchor);
}

template <Orientation O>
double BoundaryEnclosure::GetAngle(double s) const
{
    return GetDirection<O>() == Direction::Upstream ? GetAngle<Direction::Upstream>(s) : GetAngle<Direction::Downstream>(s);
}

template <Direction D>
double BoundaryEnclosure::GetAngle(double latitude) const
{
    const double leftLaneAngle{GetBoundaryPoints<osiql::Side::Left, D>().GetAngle(latitude)};
    const double rightLaneAngle{GetBoundaryPoints<osiql::Side::Right, D>().GetAngle(latitude)};
    return 0.5 * leftLaneAngle + 0.5 * rightLaneAngle;
}

template <Orientation O>
double BoundaryEnclosure::GetCurvature(double latitude) const
{
    if (GetDirection<O>() == Direction::Upstream)
    {
        return GetCurvature<Direction::Upstream>(latitude);
    }
    else
    {
        return GetCurvature<Direction::Downstream>(latitude);
    }
}

template <Direction D>
double BoundaryEnclosure::GetCurvature(double latitude) const
{
    const double leftCurvature{GetBoundaryPoints<osiql::Side::Left, D>().template GetCurvature<D>(latitude)};
    const double rightCurvature{GetBoundaryPoints<osiql::Side::Right, D>().template GetCurvature<D>(latitude)};
    return 0.5 * leftCurvature + 0.5 * rightCurvature;
}
} // namespace osiql::detail
