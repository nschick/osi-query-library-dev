/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/Lane/Connectable.h"
#include "OsiQueryLibrary/Types/Direction.h"
#include "OsiQueryLibrary/Types/Orientation.h"
#include "OsiQueryLibrary/Types/Side.h"

namespace osiql::detail {
template <typename Lane>
struct Adjoinable : Connectable<Lane>
{
    using Connectable<Lane>::Connectable;

    //! Returns all directly adjacent lanes to this one on the given side in regards to this
    //! lane's driving direction in the given orientation.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \return const std::vector<Lane *>&
    template <Side, Orientation = Orientation::Forwards>
    const std::vector<Lane *> &GetAdjacentLanes() const;

    //! Returns all directly adjacent lanes to this one on the given side in regards to this
    //! lane's driving direction in the given orientation.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \return std::vector<Lane *>&
    template <Side, Orientation = Orientation::Forwards>
    std::vector<Lane *> &GetAdjacentLanes();

    //! Returns all directly adjacent lanes to this one on the given side in regards to this
    //! the given driving direction.
    //!
    //! \tparam Side Left or Right
    //! \tparam Direction Downstream or Upstream
    //! \return const std::vector<Lane *>&
    template <Side, Direction>
    const std::vector<Lane *> &GetAdjacentLanes() const;

    //! Returns all directly adjacent lanes to this one on the given side in regards to this
    //! the given driving direction.
    //!
    //! \tparam Side Left or Right
    //! \tparam Direction Downstream or Upstream
    //! \return std::vector<Lane *>&
    template <Side, Direction>
    std::vector<Lane *> &GetAdjacentLanes();

    //! Returns the first adjacent lane on the given side relative to this lane's driving direction
    //! in the given orientation
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Downstream or Upstream
    //! \return const Lane*
    template <Side, Orientation = Orientation::Forwards>
    const Lane *GetAdjacentLane() const;

    //! Returns the first adjacent lane on the given side relative to this lane's driving direction
    //! in the given orientation
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Downstream or Upstream
    //! \return Lane*
    template <Side, Orientation = Orientation::Forwards>
    Lane *GetAdjacentLane();

    //! Returns the first adjacent lane on the given side relative to the given driving direction
    //!
    //! \tparam Side Left or Right
    //! \tparam Direction Downstream or Upstream
    //! \return const Lane*
    template <Side, Direction>
    const Lane *GetAdjacentLane() const;

    //! Returns the first adjacent lane on the given side relative to the given driving direction
    //!
    //! \tparam Side Left or Right
    //! \tparam Direction Downstream or Upstream
    //! \return Lane*
    template <Side, Direction>
    Lane *GetAdjacentLane();

    //! Returns the adjacent lane on the given side relative to this lane's driving direction in
    //! the given orientation that shares the given s-coordinate or nullptr if no such lane exists.
    //!
    //! \tparam Side Left or Right in driving direction
    //! \tparam Orientation Forwards or Backwards
    //! \param s s-coordinate contained by the returned lane
    //! \return const Lane*
    template <Side, Orientation = Orientation::Forwards>
    const Lane *GetAdjacentLane(double s) const;

    //! Returns the adjacent lane on the given side relative to this lane's driving direction in
    //! the given orientation that shares the given s-coordinate or nullptr if no such lane exists.
    //!
    //! \tparam Side Left or Right in driving direction
    //! \tparam Orientation Forwards or Backwards
    //! \param s s-coordinate contained by the returned lane
    //! \return const Lane*
    template <Side, Orientation = Orientation::Forwards>
    Lane *GetAdjacentLane(double s);

    //! Returns the adjacent lane on the given side relative to this lane's driving direction in
    //! the given orientation that shares the given s-coordinate or nullptr if no such lane exists.
    //!
    //! \tparam Orientation Forwards or Backwards - Defines what direction the given side should be relative to
    //! \param side Left or Right
    //! \param s s-coordinate contained by the returned lane
    //! \return const Lane*
    template <Orientation>
    const Lane *GetAdjacentLane(Side side, double s) const;

    //! Returns the adjacent lane on the given side relative to this lane's driving direction in
    //! the given orientation that shares the given s-coordinate or nullptr if no such lane exists.
    //!
    //! \tparam Orientation Forwards or Backwards - Defines what direction the given side should be relative to
    //! \param side Left or Right
    //! \param s s-coordinate contained by the returned lane
    //! \return Lane*
    template <Orientation>
    Lane *GetAdjacentLane(Side side, double s);

    //! Returns the adjacent lane on the given side relative to the given driving direction that
    //! contains the given s-coordinate or nullptr if no such lane exists.
    //!
    //! \tparam Side Left or Right in driving direction
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate contained by the returned lane
    //! \return const Lane*
    template <Side, Direction>
    const Lane *GetAdjacentLane(double s) const;

    //! Returns the adjacent lane on the given side relative to the given driving direction that
    //! contains the given s-coordinate or nullptr if no such lane exists.
    //!
    //! \tparam Side Left or Right in driving direction
    //! \tparam Direction Downstream or Upstream
    //! \param s s-coordinate contained by the returned lane
    //! \return Lane*
    template <Side, Direction>
    Lane *GetAdjacentLane(double s);

    //! Returns the next lane to be traversed in order to switch lanes on the given side according to the given orientation
    //! relative to this lane's direction after the given s coordinate. If lanes can not be switched on this lane without
    //! breaking the passing rules of adjacent boundaries, then a successor lane of the same road is returned. If no such
    //! lane exists, nullptr is returned.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \param s Earliest s-coordinate where the lane change may occur
    //! \return const Lane*
    template <Side, Orientation = Orientation::Forwards>
    const Lane *GetNextAdjacentLane(double &s) const;

    //! Returns the next lane to be traversed in order to switch lanes on the given side according to the given orientation
    //! relative to this lane's direction after the given s coordinate. If lanes can not be switched on this lane without
    //! breaking the passing rules of adjacent boundaries, then a successor lane of the same road is returned. If no such
    //! lane exists, nullptr is returned.
    //!
    //! \tparam Side Left or Right
    //! \tparam Orientation Forwards or Backwards
    //! \param s Earliest s-coordinate where the lane change may occur
    //! \return Lane*
    template <Side, Orientation = Orientation::Forwards>
    Lane *GetNextAdjacentLane(double &s);

    //! Returns the next lane to be traversed in order to switch lanes on the given side according to the given orientation
    //! relative to this lane's direction after the given s coordinate. If lanes can not be switched on this lane without
    //! breaking the passing rules of adjacent boundaries, then a successor lane of the same road is returned. If no such
    //! lane exists, nullptr is returned.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \param side Left or Right
    //! \param s Earliest s-coordinate where the lane change may occur
    //! \return const Lane*
    template <Orientation = Orientation::Forwards>
    const Lane *GetNextAdjacentLane(Side side, double &s) const;

    //! Returns the next lane to be traversed in order to switch lanes on the given side according to the given orientation
    //! relative to this lane's direction after the given s coordinate. If lanes can not be switched on this lane without
    //! breaking the passing rules of adjacent boundaries, then a successor lane of the same road is returned. If no such
    //! lane exists, nullptr is returned.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \param side Left or Right
    //! \param s Earliest s-coordinate where the lane change may occur
    //! \return Lane*
    template <Orientation = Orientation::Forwards>
    Lane *GetNextAdjacentLane(Side side, double &s);

protected:
    template <Side, Direction, Orientation>
    const Lane *GetNextAdjacentLane(double &s) const;

    std::vector<Lane *> leftAdjacentLanes;
    std::vector<Lane *> rightAdjacentLanes;
};
} // namespace osiql::detail

namespace osiql::detail {
template <typename Lane>
template <Side S, Direction D>
const std::vector<Lane *> &Adjoinable<Lane>::GetAdjacentLanes() const
{
    if constexpr ((S == Side::Left) == (D == Direction::Upstream))
    {
        return rightAdjacentLanes;
    }
    else
    {
        return leftAdjacentLanes;
    }
}
template <typename Lane>
template <Side S, Direction D>
std::vector<Lane *> &Adjoinable<Lane>::GetAdjacentLanes()
{
    return const_cast<std::vector<Lane *>>(const_cast<const Adjoinable<Lane> *>(this)->GetAdjacentLanes());
}

template <typename Lane>
template <Side S, Orientation O>
const std::vector<Lane *> &Adjoinable<Lane>::GetAdjacentLanes() const
{
    return this->template GetDirection<O>() == Direction::Upstream ? GetAdjacentLanes<S, Direction::Upstream>() : GetAdjacentLanes<S, Direction::Downstream>();
}

template <typename Lane>
template <Side S, Direction D>
const Lane *Adjoinable<Lane>::GetAdjacentLane() const
{
    if constexpr (D == Direction::Downstream)
    {
        return GetAdjacentLanes<S, D>().empty() ? nullptr : GetAdjacentLanes<S, D>().front();
    }
    else
    {
        return GetAdjacentLanes<S, D>().empty() ? nullptr : GetAdjacentLanes<S, D>().back();
    }
}

template <typename Lane>
template <Side S, Orientation O>
const Lane *Adjoinable<Lane>::GetAdjacentLane() const
{
    return this->template GetDirection<O>() == Direction::Upstream ? GetAdjacentLane<S, Direction::Upstream>() : GetAdjacentLane<S, Direction::Downstream>();
}

template <typename Lane>
template <Side S, Orientation O>
Lane *Adjoinable<Lane>::GetAdjacentLane()
{
    return const_cast<Lane *>(std::as_const(*this).template GetAdjacentLane<S, O>());
}

template <typename Lane>
template <Side S, Direction D>
const Lane *Adjoinable<Lane>::GetAdjacentLane(double s) const
{
    const auto it{std::upper_bound(GetAdjacentLanes<S, D>().begin(), std::prev(GetAdjacentLanes<S, D>().end()), s, [](double s, const Lane *lane) {
        return osiql::GetLatitude<!Direction::Downstream>(*lane) > s;
    })};
    return it == GetAdjacentLanes<S, D>().end() ? nullptr : *it;
}

template <typename Lane>
template <Side S, Orientation O>
const Lane *Adjoinable<Lane>::GetAdjacentLane(double s) const
{
    return this->template GetDirection<O>() == Direction::Upstream ? GetAdjacentLane<S, Direction::Upstream>(s) : GetAdjacentLane<S, Direction::Downstream>(s);
}

template <typename Lane>
template <Orientation O>
const Lane *Adjoinable<Lane>::GetAdjacentLane(Side side, double s) const
{
    if (side == Side::Left)
    {
        return GetAdjacentLane<Side::Left, O>(s);
    }
    else
    {
        return GetAdjacentLane<Side::Right, O>(s);
    }
}

template <typename Lane>
template <Side S, Direction D, Orientation O>
const Lane *Adjoinable<Lane>::GetNextAdjacentLane(double &localLatitude) const
{
    const Boundary::Partitions &partitions{this->template GetBoundaryPartitions<S, D>()};
    const typename Iterator<std::vector<Boundary::Partition>, D>::type end{partitions.end<D>(osiql::GetLatitude<!D>(*this))};
    const typename Iterator<std::vector<Boundary::Partition>, D>::type it{std::find_if(partitions.begin<D>(localLatitude), end, [](const Boundary::Partition &partition) {
        return partition.AllowsPassing<S>();
    })};
    if (it != end)
    {
        const typename Iterator<std::vector<BoundaryPoint>, D>::type pointIt{it->begin()};
        return GetAdjacentLane<S, D>(max<D>(osiql::GetLatitude<D>(*this), pointIt->latitude));
    }
    return this->template GetConnectedLane<O>(this->GetRoadId());
}

template <typename Lane>
template <Side S, Orientation O>
const Lane *Adjoinable<Lane>::GetNextAdjacentLane(double &s) const
{
    return this->template GetDirection<O>() == Direction::Upstream ? GetNextAdjacentLane<S, Direction::Upstream, O>(s) : GetNextAdjacentLane<S, Direction::Downstream, O>(s);
}

template <typename Lane>
template <Orientation O>
const Lane *Adjoinable<Lane>::GetNextAdjacentLane(Side side, double &s) const
{
    if (side == Side::Left)
    {
        return this->template GetDirection<O>() == Direction::Upstream          //
                   ? GetNextAdjacentLane<Side::Left, Direction::Upstream, O>(s) //
                   : GetNextAdjacentLane<Side::Left, Direction::Downstream, O>(s);
    }
    else
    {
        return this->template GetDirection<O>() == Direction::Upstream           //
                   ? GetNextAdjacentLane<Side::Right, Direction::Upstream, O>(s) //
                   : GetNextAdjacentLane<Side::Right, Direction::Downstream, O>(s);
    }
}
} // namespace osiql::detail
