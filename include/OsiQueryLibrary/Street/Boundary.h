/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Chain of osi3::LogicalLaneBoundaries that spans an entire road

#include <vector>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Component/Points.h"
#include "OsiQueryLibrary/Point/BoundaryPoint.h"
#include "OsiQueryLibrary/Street/LaneBoundary.h"
#include "OsiQueryLibrary/Street/LaneMarking.h"

namespace osiql {
//! \brief A Boundary is a road-length-spanning stitching of copied osi3::LogicalLaneBoundaries with possibly added points
//! at the start/end of lanes. This is done so that lane geometry can be triangulated directly from a lane's boundary points.
//! Stitching osi3::LogicalLaneBoundaries together has the benefit of making the entire road's points contiguously iterable,
//! allowing for easier lane-oriented driveline generation and exact distance queries.
struct Boundary : Points<std::vector<BoundaryPoint>>
{
    using Iterable<std::vector<BoundaryPoint>>::begin;
    typename std::vector<BoundaryPoint>::const_iterator begin() const override;

    using Iterable<std::vector<BoundaryPoint>>::end;
    typename std::vector<BoundaryPoint>::const_iterator end() const override;

    //! \brief A boundary partition is a portion of a lane boundary with a single passing rule
    //! and lets you access the corresponding physical lane markings along that boundary partition.
    struct Partition : osiql::Points<std::vector<BoundaryPoint>>
    {
        //! Constructs a boundary partition from its components
        //!
        //! \param boundary
        //! \param first
        //! \param pastLast
        Partition(const LaneBoundary &boundary,
                  std::vector<BoundaryPoint>::const_iterator first,
                  std::vector<BoundaryPoint>::const_iterator pastLast);

        using Iterable<std::vector<BoundaryPoint>>::begin;
        typename std::vector<BoundaryPoint>::const_iterator begin() const override;

        using Iterable<std::vector<BoundaryPoint>>::end;
        typename std::vector<BoundaryPoint>::const_iterator end() const override;

        //! Returns to which side an object may pass over this boundary in the given direction.
        //!
        //! \param direction
        //! \return Side
        Side GetPassingRule(Direction direction) const;

        //! Returns to which side an object may pass over this boundary in the given direction.
        //!
        //! \tparam Direction
        //! \return Side
        template <Direction = Direction::Downstream>
        Side GetPassingRule() const;

        //! Returns whether crossing this boundary onto the given side is legal.
        //!
        //! \param side
        //! \param direction
        //! \return bool
        bool AllowsPassing(Side side, Direction direction = Direction::Downstream) const;

        //! Returns whether crossing this boundary onto the given side is legal.
        //!
        //! \tparam Side
        //! \param direction
        //! \return bool
        template <Side>
        bool AllowsPassing(Direction direction) const;

        //! Returns whether crossing this boundary onto the given side is legal
        //!
        //! \tparam Side
        //! \tparam Direction
        //! \return bool
        template <Side, Direction = Direction::Downstream>
        bool AllowsPassing() const;

        //! \brief The wrapper of the OSI logical lane boundary of which this is a partition
        const LaneBoundary &boundary;

        //! \brief Container of parallel lane markings sorted right to left (downstream)
        std::vector<LaneMarking::Partition> markings;

    private:
        friend class osiql::World; // Needs to alter the iterators after construction

        std::vector<BoundaryPoint>::const_iterator a{};
        std::vector<BoundaryPoint>::const_iterator z{};
    };

    //! \brief A span/contiguous subset of boundary points
    struct Points : osiql::Points<std::vector<BoundaryPoint>>
    {
        Points() = default;

        //! Constructs a boundary point range from its components
        //!
        //! \param first
        //! \param pastLast
        Points(std::vector<BoundaryPoint>::const_iterator first, std::vector<BoundaryPoint>::const_iterator pastLast);

        using Iterable<std::vector<BoundaryPoint>>::begin;
        typename std::vector<BoundaryPoint>::const_iterator begin() const override;

        using Iterable<std::vector<BoundaryPoint>>::end;
        typename std::vector<BoundaryPoint>::const_iterator end() const override;

    private:
        std::vector<BoundaryPoint>::const_iterator a{};
        std::vector<BoundaryPoint>::const_iterator z{};
    };

    //! \brief A span/contiguous subset of boundary partitions, which are wrapped individual OSI logical lanes
    struct Partitions : Iterable<std::vector<Partition>>
    {
        Partitions() = default;

        //! Constructs a partition range from its components
        //!
        //! \param first
        //! \param pastLast
        Partitions(std::vector<Partition>::const_iterator first, std::vector<Partition>::const_iterator pastLast);

        using Iterable<std::vector<Partition>>::begin;
        typename std::vector<Partition>::const_iterator begin() const override;

        using Iterable<std::vector<Partition>>::end;
        typename std::vector<Partition>::const_iterator end() const override;

    private:
        std::vector<Partition>::const_iterator a{};
        std::vector<Partition>::const_iterator z{};
    };

    //! \brief The points of a boundary
    std::vector<BoundaryPoint> points;

    //! \brief Wrapper of an OSI logical lane boundary with an iterator range to the boundary's points
    std::vector<Partition> partitions;
};

std::ostream &operator<<(std::ostream &os, const BoundaryPoint &point);
std::ostream &operator<<(std::ostream &os, const Boundary::Points &points);
std::ostream &operator<<(std::ostream &os, const Boundary::Partition &partition);
std::ostream &operator<<(std::ostream &os, const Boundary &boundary);
} // namespace osiql

namespace osiql {
namespace {
template <Side Increasing, Side Decreasing>
Side GetPassingRule(const Boundary::Partition &partition)
{
    if (partition.boundary.GetHandle().has_passing_rule())
    {
        switch (partition.boundary.GetHandle().passing_rule())
        {
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_INCREASING_T:
            return Increasing;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_DECREASING_T:
            return Decreasing;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_NONE_ALLOWED:
            return Side::None;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_BOTH_ALLOWED:
            return Side::Both;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_OTHER:
            return Side::Other;
        case osi3::LogicalLaneBoundary_PassingRule_PASSING_RULE_UNKNOWN:
        default:
            return Side::Undefined;
        }
    }
    // else if (partition.boundary.GetHandle().physical_boundary_id().empty())
    // {
    //     // TODO: Try to derive passing rule from physical boundary
    // }
    else
    {
        return Side::Undefined;
    }
}
}

template <Direction D>
Side Boundary::Partition::GetPassingRule() const
{
    if constexpr (D == Direction::Downstream)
    {
        return osiql::GetPassingRule<Side::Left, Side::Right>(*this);
    }
    else if constexpr (D == Direction::Upstream)
    {
        return osiql::GetPassingRule<Side::Right, Side::Left>(*this);
    }
    else
    {
        return Side::Undefined;
    }
}

template <Side S, Direction D>
bool Boundary::Partition::AllowsPassing() const
{
    const Side passingRule{GetPassingRule<D>()};
    return (passingRule == Side::Both) || (passingRule == S);
}

template <Side S>
bool Boundary::Partition::AllowsPassing(Direction direction) const
{
    return direction == Direction::Upstream ? AllowsPassing<S, Direction::Upstream>() : AllowsPassing<S, Direction::Downstream>();
}
} // namespace osiql
