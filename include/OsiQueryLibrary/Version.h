/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <tuple>
//
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_version.pb.h>

namespace osiql {
using Version = std::tuple<uint32_t, uint32_t, uint32_t>;

Version GetVersion(const osi3::GroundTruth &);
Version GetVersion(const osi3::InterfaceVersion &);
} // namespace osiql
