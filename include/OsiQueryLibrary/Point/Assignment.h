/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Types/Direction.h"

namespace osiql {
class Lane;
class Road;
//! Wrapper of a osi3::LogicalLaneAssignment linked to an entity of an arbitrary type
//!
//! \tparam T  Arbitrary, but is usually either a osiql::Lane or a derived osiql::LaneAssignable
template <typename T>
struct Assignment
{
    //! Constructs an assignment of an entity to a road-relative pose
    //!
    //! \param entity
    //! \param handle
    Assignment(T &entity, const osi3::LogicalLaneAssignment &handle);

    //! Returns the yaw of the assignment relative to the angle of the edge of the
    //! reference line from which this assignment's point is laterally projected.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetLocalYaw() const;

    //! Returns the entity linked to this position assignment
    //!
    //! \return const T&
    const T &GetEntity() const;
    T &GetEntity();

    const osi3::LogicalLaneAssignment &GetHandle() const;

private:
    //! \brief An entity linked to the assignment's position.
    std::reference_wrapper<T> entity;

    //! \brief The OSI assignment that this assignment is a wrapper of.
    std::reference_wrapper<const osi3::LogicalLaneAssignment> handle;
};

//! Returns the s-coordinate of the given assignment
//!
//! \tparam Direction Direction is ignored since the s-coordinate is the same downstream and upstream
//! \tparam T
//! \param assignment
//! \return double
template <Direction = Direction::Downstream, typename T = Lane>
double GetLatitude(const Assignment<T> &assignment);

//! Returns the t-coordinate of the given assignment
//!
//! \tparam Direction
//! \tparam T
//! \param assignment
//! \return double
template <Direction = Direction::Downstream, typename T = Lane>
double GetLongitude(const Assignment<T> &assignment);

template <typename T, typename U>
bool operator<(const Assignment<T> &lhs, const Assignment<U> &rhs);

template <typename T>
std::ostream &operator<<(std::ostream &lhs, const Assignment<T> &);
} // namespace osiql

namespace osiql {
template <typename T>
Assignment<T>::Assignment(T &entity, const osi3::LogicalLaneAssignment &handle) :
    entity(entity), handle(handle)
{
}

template <typename T>
const T &Assignment<T>::GetEntity() const
{
    return entity;
}
template <typename T>
T &Assignment<T>::GetEntity()
{
    return entity;
}

template <typename T>
const osi3::LogicalLaneAssignment &Assignment<T>::GetHandle() const
{
    return handle;
}

template <Direction, typename T>
double GetLongitude(const Assignment<T> &assignment)
{
    return assignment.GetHandle().has_t_position() ? assignment.GetHandle().t_position() : std::numeric_limits<double>::quiet_NaN();
}

template <Direction, typename T>
double GetLatitude(const Assignment<T> &assignment)
{
    return assignment.GetHandle().has_s_position() ? assignment.GetHandle().s_position() : std::numeric_limits<double>::quiet_NaN();
}

template <typename T>
double Assignment<T>::GetLocalYaw() const
{
    return GetHandle().has_angle_to_lane() ? GetHandle().angle_to_lane() : std::numeric_limits<double>::quiet_NaN();
}

template <typename T, typename U>
bool operator<(const Assignment<T> &lhs, const Assignment<U> &rhs)
{
    // Can't use std::tie because some getters return values
    if (lhs.GetEntity().GetId() != rhs.GetEntity().GetId())
    {
        return lhs.GetEntity().GetId() < rhs.GetEntity().GetId();
    }
    if (GetLatitude(lhs) != GetLatitude(rhs))
    {
        return GetLatitude(lhs) < GetLatitude(rhs);
    }
    if (GetLongitude(lhs) != GetLongitude(rhs))
    {
        return GetLongitude(lhs) < GetLongitude(rhs);
    }
    return false;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Assignment<T> &a)
{
    return os << "[(" << GetLatitude(a) << ", " << GetLongitude(a) << "), " << a.GetEntity() << ']';
}
} // namespace osiql
