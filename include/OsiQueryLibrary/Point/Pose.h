/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived point that additionally stores an angle to the entity it is assigned to.

#include <osi3/osi_common.pb.h>

#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Point/LanePoint.h"
#include "OsiQueryLibrary/Point/RoadPoint.h"

namespace osiql {
//! \brief A Pose is an instance of Coordinates<Road> that additionally stores an angle relative to the entity it is assigned to.
template <typename Point = Coordinates<Road>>
struct Pose : Point
{
    //! \brief counter-clockwise ascending angle in radians within [-π, π]
    double angle;

    //! Returns the angle of this pose relative to its assigned object's road's reference line
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetAngle() const;

    // Constructors are defined explicitly since there is no emplacement support via aggregates prior to C++20

    //! Copy-constructs a pose from a point and an angle
    //!
    //! \param point
    //! \param angle
    Pose(const Point &point, double angle = 0.0);

    //! Move-constructs a pose from a point and an angle
    //!
    //! \param point
    //! \param angle
    Pose(Point &&point, double angle = 0.0);

    template <typename OtherPoint, std::enable_if_t<std::is_constructible_v<Point, const OtherPoint &>, bool> = true>
    Pose(const Pose<OtherPoint> &pose) :
        Point(pose), angle(pose.angle)
    {
    }

    template <typename OtherPoint, std::enable_if_t<std::is_constructible_v<Point, OtherPoint &>, bool> = true>
    Pose(Pose<OtherPoint> &pose) :
        Point(pose), angle(pose.angle)
    {
    }

    template <typename T, typename StoredLane>
    Pose(StoredLane &lane, const Pose<T> &pose) :
        Point(lane, pose), angle(pose.angle)
    {
    }

    template <typename T, typename StoredLane, std::enable_if_t<std::is_constructible_v<Point, StoredLane &, Coordinates<T>>, bool> = true>
    Pose(StoredLane &lane, Coordinates<T> &&coordinates, double angle = 0.0) :
        Point(lane, std::forward<Coordinates<T>>(coordinates)), angle(angle)
    {
    }
};

template <typename Point>
double GetAngle(const Pose<Point> &pose);

template <typename Point>
std::ostream &operator<<(std::ostream &os, const Pose<Point> &pose);

using RoadPose = Pose<RoadPoint>;
using LanePose = Pose<LanePoint>;
} // namespace osiql

namespace osiql {
template <typename Point>
Pose<Point>::Pose(const Point &point, double angle) :
    Point(point), angle(angle)
{
}

template <typename Point>
Pose<Point>::Pose(Point &&point, double angle) :
    Point(std::forward<Point>(point)), angle(angle)
{
}

template <typename Point>
double GetAngle(const Pose<Point> &pose)
{
    return pose.angle;
}

template <typename Point>
std::ostream &operator<<(std::ostream &os, const Pose<Point> &pose)
{
    if constexpr (std::is_same_v<Point, Coordinates<Road>>)
    {
        return os << "[(" << pose.latitude << ", " << pose.longitude << "), " << pose.angle << ']';
    }
    else
    {
        return os << '[' << pose.GetLane().GetId() << '(' << pose.latitude << ", " << pose.longitude << "), " << pose.angle << ']';
    }
}
} // namespace osiql
