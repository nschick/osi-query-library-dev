/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Two-dimensional global point that additionally supports queries like what side of a line it is on

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <osi3/osi_common.pb.h>        // osi3::Vector3d
#include <osi3/osi_lane.pb.h>          // osi3::LaneBoundary::BoundaryPoint
#include <osi3/osi_logicallane.pb.h>   // osi3::LogicalLaneBoundary::LogicalBoundaryPoint
#include <osi3/osi_referenceline.pb.h> // osi3::ReferenceLine::ReferenceLinePoint
#include <vector>

#include "OsiQueryLibrary/Point/Common.h" // GetLatitude & GetLongitude
#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Point/Vector.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Side.h"

namespace osiql {
template <typename T>
struct Coordinates;
class Road;

enum class Winding : bool
{
    Clockwise,
    CounterClockwise
};

//! \brief An x & y coordinate used to represent a planar vector or global point.
//! Provides transformation methods, conversions and containment queries.
struct Vector2d : Vector<double, 2>
{
    using Vector<double, 2>::Vector;

    //! Constructs a Vector2d from the x & y components of a given 3D osiql vector
    //!
    //! \param point
    Vector2d(const Vector<double, 3> &point);

    //! Constructs a Vector2d from an OSI Vector2d
    //!
    //! \param point
    Vector2d(const osi3::Vector2d &point);

    //! Constructs a Vector2d from an OSI Vector3d
    //!
    //! \param point
    Vector2d(const osi3::Vector3d &point);

    //! Constructs a Vector2d from an OSI LogicalLaneBoundaryPoint
    //!
    //! \param point
    Vector2d(const osi3::LogicalLaneBoundary::LogicalBoundaryPoint &point);

    //! Constructs a Vector2d from an OSI LaneBoundaryPoint
    //!
    //! \param point
    Vector2d(const osi3::LaneBoundary::BoundaryPoint &point);

    //! Constructs a Vector2d from an OSI ReferenceLinePoint
    //!
    //! \param point
    Vector2d(const osi3::ReferenceLine::ReferenceLinePoint &point);

    //! Returns this point rotated counter-clockwise by the given radians.
    //!
    //! \param angle
    //! \return Vector2d
    Vector2d Rotate(double angle) const noexcept;

    //! Computes the cross product between this point and the given one.
    //!
    //! \param point
    //! \return constexpr double
    constexpr double Cross(const Vector2d &point) const noexcept;

    //! Returns the angle of this vector relative to the x-axis.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double Angle() const noexcept;

    //! Returns this vector projected onto a line passing through the coordinate origin
    //! with the given angle relative to the x-axis.
    //!
    //! \param angle counter-clockwise angle in radians relative to the x-axis
    //! \return Vector2d
    Vector2d Projection(double angle) const;

    //! Returns a projection of this point onto the line defined by the given points.
    //!
    //! \param from Point on the line
    //! \param to Different point on the line
    //! \return Vector2d Point on the line defined by the given points
    Vector2d Projection(const Vector2d &from, const Vector2d &to) const;

    //! Returns the signed length of the projection of this point onto a lane passing
    //! through the coordinate vector with the given angle relative to the x-axis.
    //!
    //! \param angle
    //! \return double
    double ProjectionLength(double angle) const;

    //! Returns the component-wise multiplication of this vector with a given one
    //!
    //! \param other
    //! \return constexpr Vector2d
    constexpr Vector2d Times(const Vector2d &other) const;

    //! Return what side of the line defined by the given points this point lies on.
    //! Returns Side::None if the points are collinear
    //!
    //! \param a Point on the line
    //! \param b Different point on the line
    //! \return Side
    Side GetSide(const Vector2d &a, const Vector2d &b) const;

    //! Returns whether this vector is inside the simple polygon defined by the given points.
    //!
    //! \param points Points forming an open simple polygon in either clockwise or counter-clockwise orientation.
    //! \return bool Whether this vector is not outside the given polygon
    template <typename T>
    bool IsWithin(const T &points) const;

    //! Helper method for use in contexts where the edges between points forming a quadrilateral have already been calculated.
    //!
    //! \tparam T type that can be cast to Vector2d or from which a Vector2d can be constructed
    //! \tparam U type that can be cast to Vector2d or from which a Vector2d can be constructed
    //! \param points
    //! \param edges Corresponding edges between the given points.
    //! \return bool
    template <typename T, typename U>
    bool IsWithin(const T &points, const U &edges) const;

    //! Receives a range of points and returns an iterator to the point at the end of the edge closest to this point.
    //!
    //! \tparam It
    //! \param first
    //! \param pastLast
    //! \param distance Optional output parameter for the signed distance from the point
    //! \return It
    template <typename It>
    It GetEndOfClosestEdge(It first, It pastLast, double *distance = nullptr) const;

    //! Returns the shortest vector from the line segment defined by the given points to this point.
    //!
    //! \param a
    //! \param b
    //! \return Vector2d
    Vector2d GetPathTo(const Vector2d &a, const Vector2d &b) const;

    //! Returns the length of the shortest path to the edge defined by the given points.
    //! If this point is on the right side of the edge, the returned distance is negative.
    //!
    //! \param a
    //! \param b
    //! \return double
    double GetSignedDistanceTo(const Vector2d &a, const Vector2d &b) const;

    //! Finds the nearest edge in the given range of points and returns the signed distance of this point to it.
    //! If this point is on the right of said edge, the returned distance is negative.
    //!
    //! \tparam It
    //! \param first
    //! \param pastLast
    //! \return double
    template <typename It>
    double GetSignedDistanceToChain(It first, It pastLast) const;

    //! Converts this point's x- & y-coordinates to s- & t-coordinates relative to the line segment defined by the given vertices.
    //!
    //! \param a
    //! \param b
    //! \return Point
    template <typename T>
    Coordinates<Road> GetCoordinates(const T &a, const T &b) const;

    //! Returns this vector's clockwise or counter-clockwise normal, which is
    //! this vector rotated 90° in the given winding direction.
    //!
    //! \tparam Winding Clockwise or CounterClockwise
    //! \return constexpr Vector2d The normal of the input
    template <Winding>
    constexpr Vector2d GetPerpendicular() const;
};

//! Returns the given vector's clockwise or counter-clockwise normal, which is
//! said vector rotated 90° in the given winding direction.
//!
//! \tparam Winding Clockwise or CounterClockwise
//! \param const Vector2d& Global xy-coordinates whose normal will be returned
//! \return constexpr Vector2d The normal of the input
template <Winding>
constexpr Vector2d GetPerpendicular(const Vector2d &);

double GetX(const Vector2d &point);
double GetY(const Vector2d &point);

void PrintTo(const Vector2d &point, std::ostream *os);
std::ostream &operator<<(std::ostream &, Winding);
} // namespace osiql

// Makes Vector2d usable as a boost point type
BOOST_GEOMETRY_REGISTER_POINT_2D(osiql::Vector2d, double, cs::cartesian, x, y)

// ----- Function Definitions

namespace osiql {
// Arithmetic operations are already defined generically in Vector.h, however, overloading them specifically
// for Vector2d allows swizzles that implicitly cast to Vector2d to use these operators without explicit casting.

constexpr Vector2d operator+(const Vector2d &lhs, const Vector2d &rhs)
{
    return {lhs.values[0] + rhs.values[0], lhs.values[1] + rhs.values[1]};
}
constexpr Vector2d operator-(const Vector2d &rhs)
{
    return {-rhs.values[0], -rhs.values[1]};
}
constexpr Vector2d operator-(const Vector2d &lhs, const Vector2d &rhs)
{
    return {lhs.values[0] - rhs.values[0], lhs.values[1] - rhs.values[1]};
}
constexpr Vector2d operator*(const Vector2d &vector, double factor)
{
    return {vector.values[0] * factor, vector.values[1] * factor};
}
constexpr Vector2d operator*(double factor, const Vector2d &vector)
{
    return vector * factor;
}
constexpr Vector2d operator/(const Vector2d &vector, double divisor)
{
    return {vector.values[0] / divisor, vector.values[1] / divisor};
}

constexpr double Vector2d::Cross(const Vector2d &point) const noexcept
{
    return values[0] * point.values[1] - values[1] * point.values[0];
}

constexpr Vector2d Vector2d::Times(const Vector2d &other) const
{
    return {values[0] * other.values[0], values[1] * other.values[1]};
}

template <typename It>
It Vector2d::GetEndOfClosestEdge(It it, It pastLast, double *distance) const
{
    // Note: C++20 ranges would be nice here
    if (it == pastLast)
    {
        return pastLast;
    }
    ++it;
    It result{it};
    double min{std::numeric_limits<double>::max()};
    while (it != pastLast)
    {
        const double d{this->GetPathTo(*std::prev(it), *it).SquaredLength()};
        if (d < min)
        {
            min = d;
            result = it;
        }
        ++it;
    }
    if (distance)
    {
        *distance = this->GetSide(*std::prev(result), *result) == Side::Left ? -std::sqrt(min) : std::sqrt(min);
    }
    return result;
}

template <typename It>
double Vector2d::GetSignedDistanceToChain(It first, It pastLast) const
{
    if (first == pastLast)
    {
        throw std::runtime_error("Vector2d::GetSignedDistanceToChain: Chain of points may not be empty");
    }
    double result{std::numeric_limits<double>::quiet_NaN()};
    GetEndOfClosestEdge(first, pastLast, &result);
    return result;
}

template <typename T>
Coordinates<Road> Vector2d::GetCoordinates(const T &a, const T &b) const
{
    // Project the point onto the line through start and end
    // t is the distance of the point to its projection
    const Vector2d path{static_cast<Vector2d>(b) - static_cast<Vector2d>(a)};
    const Vector2d projection{Projection(a, b)};
    double longitude{GetSide(a, b) == Side::Right ? -(*this - projection).Length() : (*this - projection).Length()};
    // Calculate the s-coordinate
    const double length{GetLatitude(b) - GetLatitude(a)};
    assert(path.Length());
    double ratio{(projection - a).Length() / path.Length()};
    // Flip sign of ratio if the projection begins before the line segment
    if (GetX(a) != GetX(b))
    {
        if ((projection.x < GetX(a)) == (GetX(a) < GetX(b)))
        {
            ratio = -ratio;
        }
    }
    else if ((projection.y < GetY(a)) == (GetY(a) < GetY(b)))
    {
        ratio = -ratio;
    }
    const double latitude{GetLatitude(a) + ratio * length};
    // Offset the t-coordinate
    if (ratio >= 1.0)
    {
        longitude += GetLongitude(b);
    }
    else if (ratio <= 0.0)
    {
        longitude += GetLongitude(a);
    }
    else
    {
        longitude += GetLongitude(a) * (1.0 - ratio) + GetLongitude(b) * ratio;
    }
    return {latitude, longitude};
}

template <typename T>
bool Vector2d::IsWithin(const T &polygon) const
{
    double determinant{(*this - polygon.back()).Cross(polygon.front() - polygon.back())};
    size_t i{1u};
    while (std::abs(determinant) < detail::EPSILON)
    {
        determinant = (*this - polygon[i - 1]).Cross(polygon[i] - polygon[i - 1]);
        ++i;
    }
    const bool base{determinant >= 0.0};
    for (; i < polygon.size(); ++i)
    {
        const double orientation{(*this - polygon[i - 1]).Cross(polygon[i] - polygon[i - 1])};
        if (std::abs(orientation) < detail::EPSILON)
        {
            continue;
        }
        if ((orientation >= 0.0) != base)
        {
            return false;
        }
    }
    return true;
}

template <typename T, typename U>
bool Vector2d::IsWithin(const T &polygon, const U &edges) const
{
    assert(polygon.size() == edges.size());
    double determinant{(*this - polygon.back()).Cross(edges.back())};
    size_t i{1u};
    while (std::abs(determinant) < detail::EPSILON && i < polygon.size())
    {
        determinant = (*this - polygon[i - 1]).Cross(edges[i - 1]);
        ++i;
    }
    const bool base{determinant >= 0.0};
    for (; i < polygon.size(); ++i)
    {
        const double orientation{(*this - polygon[i - 1]).Cross(edges[i - 1])};
        if (std::abs(orientation) < detail::EPSILON)
        {
            continue;
        }
        if ((orientation >= 0.0) != base)
        {
            return false;
        }
    }
    return true;
}

template <Winding W>
constexpr Vector2d Vector2d::GetPerpendicular() const
{
    if constexpr (W == Winding::Clockwise)
    {
        return {y, -x};
    }
    else
    {
        return {-y, x};
    }
}

template <Winding W>
constexpr Vector2d GetPerpendicular(const Vector2d &point)
{
    return point.template GetPerpendicular<W>();
}

namespace detail {
constexpr const std::array<std::string_view, 2> windingToString{
    "Clockwise", "Counter-clockwise" //
};
}
} // namespace osiql
