/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base class of any local point, containing its latitude and longitude

#include <iostream>
#include <type_traits>
//
#include <osi3/osi_logicallane.pb.h>
#include <osi3/osi_referenceline.pb.h>

#include "OsiQueryLibrary/Point/Assignment.h"
#include "OsiQueryLibrary/Types/Direction.h"
#include "OsiQueryLibrary/Types/Swizzle.h"

namespace osiql {
class Lane;
class Road;

//! Local coordinates with a latitude and longitude relative to the template type
//!
//! \tparam T Either Lane or Road
template <typename T>
struct Coordinates;

template <>
struct Coordinates<Road>
{
    Coordinates(double latitude, double longitude);

    //! Constructs a local st-coordinate point from an OSI logical lane boundary point
    //!
    //! \param point
    Coordinates(const osi3::LogicalLaneBoundary::LogicalBoundaryPoint &point);

    //! Constructs a local st-coordinate point from an OSI reference line point
    //!
    //! \param point
    Coordinates(const osi3::ReferenceLine::ReferenceLinePoint &point);

    //! Constructs local coordinates from a logical lane assignment
    //!
    //! \param assignment
    template <typename U>
    Coordinates(const Assignment<U> &assignment);

    double latitude; //!< \brief Signed distance along the road's reference from the start of the reference line
    //! \brief Signed distance to the nearest point on the reference line. Negative if on the right side of the reference line
    double longitude;
};

template <>
struct Coordinates<Lane>
{
    double latitude; //!< \brief Signed distance along the lane from the start of the lane in its driving direction
    //! \brief Signed distance to the nearest point on the reference line of this lane's road minus the same distance of the centerline
    //! at the same latitude from the reference line.
    double longitude;
};

// Free getter functions that allow Coordinates<T> and Assignment<T> to be used
// interchangably without forcing them to inherit a common base class

template <Direction = Direction::Downstream, typename T = Road>
double GetLatitude(const Coordinates<T> &point)
{
    return point.latitude;
}

template <Direction = Direction::Downstream, typename T = Road>
double GetLongitude(const Coordinates<T> &point)
{
    return point.longitude;
}

template <Direction D, typename T>
Coordinates<Road> GetCoordinates(const T &item)
{
    return {GetLatitude<D>(item), GetLongitude<D>(item)};
}

template <typename T>
Coordinates<Road> GetCoordinates(const T &item, Direction d)
{
    return d == Direction::Upstream ? GetCoordinates<Direction::Upstream>(item) : GetCoordinates<Direction::Downstream>(item);
}

template <typename T>
constexpr bool operator==(const Coordinates<T> &lhs, const Coordinates<T> &rhs);

template <typename T>
constexpr bool operator==(const Coordinates<T> &lhs, const Coordinates<T> &rhs);

template <typename T>
std::ostream &operator<<(std::ostream &os, const Coordinates<T> &rhs);

namespace detail {
template <typename T, size_t I>
struct GlobalCoordinates
{
    std::array<T, I> values;
};

template <typename T>
struct GlobalCoordinates<T, 2>
{
    union
    {
        std::array<T, 2> values;
        Swizzle<T, 0> x;
        Swizzle<T, 1> y;
        Swizzle<T, 0, 1> xy;
        Swizzle<T, 1, 0> yx;
    };
};

template <typename T>
struct GlobalCoordinates<T, 3>
{
    union
    {
        std::array<T, 3> values;
        Swizzle<T, 0> x;
        Swizzle<T, 1> y;
        Swizzle<T, 2> z;

        Swizzle<T, 0, 1> xy;
        Swizzle<T, 0, 2> xz;
        Swizzle<T, 1, 0> yx;
        Swizzle<T, 1, 2> yz;
        Swizzle<T, 2, 0> zx;
        Swizzle<T, 2, 1> zy;

        Swizzle<T, 0, 1, 2> xyz;
        Swizzle<T, 0, 2, 1> xzy;
        Swizzle<T, 1, 0, 2> yxz;
        Swizzle<T, 1, 2, 0> yzx;
        Swizzle<T, 2, 0, 1> zxy;
        Swizzle<T, 2, 1, 0> zyx;
    };
};

template <typename T, size_t I>
constexpr bool operator==(const GlobalCoordinates<T, I> &lhs, const GlobalCoordinates<T, I> &rhs);

template <typename T, size_t I>
constexpr bool operator!=(const GlobalCoordinates<T, I> &lhs, const GlobalCoordinates<T, I> &rhs);
} // namespace detail

template <typename T, size_t I>
std::ostream &operator<<(std::ostream &os, const detail::GlobalCoordinates<T, I> &rhs);
} // namespace osiql

namespace osiql {
template <typename U>
Coordinates<Road>::Coordinates(const Assignment<U> &assignment) :
    latitude(GetLatitude(assignment)), longitude(GetLongitude(assignment))
{
}

template <typename T>
constexpr bool operator==(const Coordinates<T> &lhs, const Coordinates<T> &rhs)
{
    return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude;
}

template <typename T>
constexpr bool operator!=(const Coordinates<T> &lhs, const Coordinates<T> &rhs)
{
    return !(lhs == rhs);
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Coordinates<T> &rhs)
{
    return os << '(' << rhs.latitude << ", " << rhs.longitude << ')';
}

namespace detail {
template <typename T, size_t I, size_t... Is>
std::ostream &print(std::ostream &os, const GlobalCoordinates<T, sizeof...(Is) + 1> &coordinates, std::index_sequence<I, Is...>)
{
    if constexpr (sizeof...(Is) == 0)
    {
        return os << coordinates.values[I];
    }
    else
    {
        os << '(' << coordinates.values[I];
        ((os << ", " << coordinates.values[Is]), ...);
        return os << ')';
    }
}
} // namespace detail

template <typename T, size_t I>
std::ostream &operator<<(std::ostream &os, const detail::GlobalCoordinates<T, I> &coordinates)
{
    return detail::print(os, coordinates, std::make_index_sequence<I>());
}
} // namespace osiql
