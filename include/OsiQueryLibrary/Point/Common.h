/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Common functions for specific objects, mostly primitive getters

#include <osi3/osi_logicallane.pb.h>
#include <osi3/osi_referenceline.pb.h>

#include "OsiQueryLibrary/Types/Direction.h"

namespace osiql {
template <Direction = Direction::Downstream>
double GetLatitude(const osi3::ReferenceLine::ReferenceLinePoint &point)
{
    return point.has_s_position() ? point.s_position() : std::numeric_limits<double>::quiet_NaN();
}

template <Direction = Direction::Downstream>
double GetLatitude(const osi3::LogicalLaneBoundary::LogicalBoundaryPoint &point)
{
    return point.has_s_position() ? point.s_position() : std::numeric_limits<double>::quiet_NaN();
}
template <Direction = Direction::Downstream>
double GetLongitude(const osi3::LogicalLaneBoundary::LogicalBoundaryPoint &point)
{
    return point.has_t_position() ? point.t_position() : std::numeric_limits<double>::quiet_NaN();
}

template <Direction = Direction::Downstream>
double GetLongitude(const osi3::ReferenceLine::ReferenceLinePoint &point)
{
    std::ignore = point;
    return 0.0;
}

double GetX(const osi3::ReferenceLine::ReferenceLinePoint &);
double GetY(const osi3::ReferenceLine::ReferenceLinePoint &);

std::ostream &operator<<(std::ostream &, const osi3::ReferenceLine::ReferenceLinePoint &);
} // namespace osiql
