/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Point/Vector2d.h"

namespace osiql {
//! \brief Pre-defined global x,y-coordinate point used by Locatable::GetXY
//! to receive the position of certain points relative to the locatable's dimensions.
struct Anchor
{
    static constexpr Vector2d CENTER = {0.0, 0.0};         //!< The center of an object
    static constexpr Vector2d FRONT = {0.5, 0.0};          //!< The middle of the front bumper of a vehicle or equivalent for other objects
    static constexpr Vector2d LEFT = {0.0, 0.5};           //!< The middle of the left side of an object
    static constexpr Vector2d RIGHT = -LEFT;               //!< The middle of the right side of an object
    static constexpr Vector2d REAR = -FRONT;               //!< The middle of the rear bumper of a vehicle or equivalent for other objects
    static constexpr Vector2d FRONT_LEFT = FRONT + LEFT;   //!< The left end of the front bumper of a vehicle or equivalent for other objects
    static constexpr Vector2d FRONT_RIGHT = FRONT + RIGHT; //!< The right end of the front bumper of a vehicle or equivalent for other objects
    static constexpr Vector2d REAR_LEFT = REAR + LEFT;     //!< The left end of the rear bumper of a vehicle or equivalent for other objects
    static constexpr Vector2d REAR_RIGHT = REAR + RIGHT;   //!< The right end of the rear bumper of a vehicle or equivalent for other objects
    Anchor() = delete;
};
} // namespace osiql
