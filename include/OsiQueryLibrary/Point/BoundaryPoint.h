/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Point/Vector2d.h"

namespace osiql {
//! A BoundaryPoint holds its global xy-coordinates & st-coordinates (which represent its projection onto a reference line).
struct BoundaryPoint : Vector2d, Coordinates<Road>
{
    //! Constructs a boundary point from xy-coordinates and st-coordinates
    //!
    //! \param coordinates global xy-coordinates
    //! \param point road-relative st-coordinates
    BoundaryPoint(Vector2d &&coordinates, Coordinates<Road> &&point);

    //! Constructs a boundary point from  an OSI boundary point.
    //!
    //! \param point
    BoundaryPoint(const osi3::LogicalLaneBoundary::LogicalBoundaryPoint &point);
};
} // namespace osiql
