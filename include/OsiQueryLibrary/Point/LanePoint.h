/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Point/Coordinates.h"

namespace osiql {
class Lane;
struct RoadPoint;
namespace detail {
struct RoadPoint;
}
//! \brief Lane-relative latitude and longitude associated with a given lane (unlike Coordinates<Lane>, which has no associated lane).
//! Latitude represents the distance along the reference line in driving direction from the start of the lane projected
//! onto the road's reference line to this point projected onto the road's reference line.
//! Longitude represents the signed distance from the centerline of the lane (leftward-positive distance in the lane's driving direction).
//! The distance is of a line perpendicular to the reference line if unambiguous, otherwise it's of the shortest path.
struct LanePoint : Coordinates<Lane>
{
    //! Copy-constructs a lane point from a lane and coordinates
    //!
    //! \param lane
    //! \param coordinates
    LanePoint(const Lane &lane, const Coordinates<Lane> &coordinates);

    //! Move-constructs a lane point from a lane and coordinates
    //!
    //! \param lane
    //! \param coordinates
    LanePoint(const Lane &lane, Coordinates<Lane> &&coordinates);

    //! Constructs a lane point from a road point by performing coordinate conversion
    //!
    //! \param point
    LanePoint(const detail::RoadPoint &point);

    //! Constructs a lane point from a road point by performing coordinate conversion
    //!
    //! \param point
    LanePoint(const RoadPoint &point);

    //! Creates a lane point from a wrapped logical lane assignment
    //!
    //! \param assignment
    LanePoint(const Assignment<Lane> &assignment);

    //! Converts this point's coordinates to road coordinates with the same global xy-position
    //!
    //! \return Coordinates<Road>
    operator Coordinates<Road>() const;

    //! Returns the lane associated with this point
    //!
    //! \return const Lane&
    const Lane &GetLane() const;

    //! Returns the road associated with this point
    //!
    //! \return const Road&
    const Road &GetRoad() const;

    //! Return the latitude of this point relative to the start the road it is a part of.
    //!
    //! \return double
    double GetRoadLatitude() const;

    //! Returns the global position of this point
    //!
    //! \return Vector2d
    Vector2d GetXY() const;

private:
    std::reference_wrapper<const Lane> lane;
};

const Lane &GetLane(const LanePoint &point);

namespace detail {
//! \brief A detail::LanePoint is a LanePoint whose Lane is not const-qualified. This class is only used internally
//! to allow modification of the lane's state (such as removing associated moving objects) via access to a point.
struct LanePoint : Coordinates<Lane>
{
    //! Copy-constructs a lane point from a lane and coordinates
    //!
    //! \param lane
    //! \param coordinates
    LanePoint(Lane &lane, const Coordinates<Lane> &coordinates);

    //! Move-constructs a lane point from a lane and coordinates
    //!
    //! \param lane
    //! \param coordinates
    LanePoint(Lane &lane, Coordinates<Lane> &&coordinates);

    //! Creates a lane point from a road point by converting its coordinates
    //!
    //! \param point
    LanePoint(RoadPoint &point);

    //! Creates a lane point from a wrapped logical lane assignment
    //!
    //! \param assignment
    LanePoint(Assignment<Lane> &assignment);

    //! Converts this point's coordinates to road coordinates with the same global xy-position
    //!
    //! \return Coordinates<Road>
    operator Coordinates<Road>() const;

    //! Returns the lane associated with this point
    //!
    //! \return const Lane&
    const Lane &GetLane() const;

    //! Returns the road associated with this point
    //!
    //! \return const Road&
    const Road &GetRoad() const;

    //! Returns the road associated with this point
    //!
    //! \return const Road&
    Road &GetRoad();

    //! Returns the lane associated with this point
    //!
    //! \return Lane&
    Lane &GetLane();

    //! Return the latitude of this point relative to the start the road it is a part of.
    //!
    //! \return double
    double GetRoadLatitude() const;

    //! Returns the global position of this point
    //!
    //! \return Vector2d
    Vector2d GetXY() const;

private:
    std::reference_wrapper<Lane> lane;
};
} // namespace detail

const Lane &GetLane(const detail::LanePoint &point);
Lane &GetLane(detail::LanePoint &point);

bool operator==(const LanePoint &lhs, const LanePoint &rhs);
bool operator==(const detail::LanePoint &lhs, const LanePoint &rhs);
bool operator!=(const LanePoint &lhs, const LanePoint &rhs);
bool operator!=(const detail::LanePoint &lhs, const LanePoint &rhs);

bool operator==(const LanePoint &lhs, const detail::LanePoint &rhs);
bool operator==(const detail::LanePoint &lhs, const detail::LanePoint &rhs);
bool operator!=(const LanePoint &lhs, const detail::LanePoint &rhs);
bool operator!=(const detail::LanePoint &lhs, const detail::LanePoint &rhs);

bool operator==(const LanePoint &lhs, const Assignment<Lane> &rhs);
bool operator==(const detail::LanePoint &lhs, const Assignment<Lane> &rhs);
bool operator!=(const LanePoint &lhs, const Assignment<Lane> &rhs);
bool operator!=(const detail::LanePoint &lhs, const Assignment<Lane> &rhs);

bool operator==(const Assignment<Lane> &lhs, const LanePoint &rhs);
bool operator==(const Assignment<Lane> &lhs, const detail::LanePoint &rhs);
bool operator!=(const Assignment<Lane> &lhs, const LanePoint &rhs);
bool operator!=(const Assignment<Lane> &lhs, const detail::LanePoint &rhs);

bool operator==(const LanePoint &lhs, const RoadPoint &rhs);
bool operator==(const detail::LanePoint &lhs, const RoadPoint &rhs);
bool operator!=(const LanePoint &lhs, const RoadPoint &rhs);
bool operator!=(const detail::LanePoint &lhs, const RoadPoint &rhs);

bool operator==(const LanePoint &lhs, const detail::RoadPoint &rhs);
bool operator==(const detail::LanePoint &lhs, const detail::RoadPoint &rhs);
bool operator!=(const LanePoint &lhs, const detail::RoadPoint &rhs);
bool operator!=(const detail::LanePoint &lhs, const detail::RoadPoint &rhs);

std::ostream &operator<<(std::ostream &os, const LanePoint &rhs);
std::ostream &operator<<(std::ostream &os, const detail::LanePoint &rhs);
} // namespace osiql
