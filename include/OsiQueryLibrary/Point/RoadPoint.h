/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Trait/Common.h"
#include "OsiQueryLibrary/Types/Direction.h"
#include "OsiQueryLibrary/Types/Orientation.h"

namespace osiql {
class Lane;
class Road;
struct LanePoint;
namespace detail {
struct RoadPoint;
struct LanePoint;
} // namespace detail

struct RoadPoint : Coordinates<Road>
{
    //! Copy-constructs a road road point from a lane and road coordinates
    //!
    //! \param lane
    //! \param coordinates
    RoadPoint(const Lane &lane, const osiql::Coordinates<Road> &coordinates);

    //! Move-constructs a road point from a lane and road coordinates
    //!
    //! \param lane
    //! \param coordinates
    RoadPoint(const Lane &lane, osiql::Coordinates<Road> &&coordinates);

    //! Constructs a road point from a lane and lane coordinates by performing coordinate conversion
    //!
    //! \param lane
    //! \param coordinates
    RoadPoint(const Lane &lane, const osiql::Coordinates<Lane> &coordinates);

    //! Constructs a road point from another road point
    //!
    //! \param point
    RoadPoint(const detail::RoadPoint &point);

    //! Constructs a road point from a lane point by performing coordinate conversion
    //!
    //! \param point
    RoadPoint(const LanePoint &point);

    //! Constructs a road point from a lane point by performing coordinate conversion
    //!
    //! \param point
    RoadPoint(const detail::LanePoint &point);

    //! Constructs a local point from a wrapped logical lane assignment
    //!
    //! \param assignment
    RoadPoint(const Assignment<Lane> &assignment);

    //! Converts this road point's coordinates to lane coordinates with the same global xy-position
    //!
    //! \return Coordinates<Lane>
    operator Coordinates<Lane>() const;

    //! Returns the lane associated with this point
    //!
    //! \return const Lane&
    const Lane &GetLane() const;

    //! Returns the road associated with this point
    //!
    //! \return const Road&
    const Road &GetRoad() const;

    //! Returns the global position of this point
    //!
    //! \return Vector2d
    Vector2d GetXY() const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \tparam D Downstream or Upstream
    //! \param destination
    //! \param maxRange
    //! \return double
    template <Direction D>
    double GetDistanceTo(const RoadPoint &destination, double maxRange) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \param destination
    //! \param maxRange
    //! \param direction Downstream or Upstream
    //! \return double
    double GetDistanceTo(const RoadPoint &destination, double maxRange, Direction direction) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \tparam O Forwards or Backwards
    //! \param destination
    //! \param maxRange
    //! \return double
    template <Orientation O = Orientation::Forwards>
    double GetDistanceTo(const RoadPoint &destination, double maxRange) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \param destination
    //! \param maxRange
    //! \return double
    double GetDistanceTo(const RoadPoint &destination, double maxRange) const;

    //! Returns the difference in latitude between this point and the given latitude.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \param latitude
    //! \return double
    template <Orientation = Orientation::Forwards>
    double GetDistanceTo(double latitude) const;

    //! Returns the difference in latitude between this point and the given latitude.
    //!
    //! \param latitude
    //! \param orientation Forwards or Backwards
    //! \return double
    double GetDistanceTo(double latitude, Orientation orientation) const;

    //! Returns the difference in latitude between this point and the given latitude.
    //!
    //! \tparam D Downstream or Upstream
    //! \param latitude
    //! \return double
    template <Direction>
    double GetDistanceTo(double latitude) const;

    //! Returns the difference in latitude between this point and the given latitude.
    //!
    //! \param latitude
    //! \param direction Downstream or Upstream
    //! \return double
    double GetDistanceTo(double latitude, Direction direction) const;

private:
    std::reference_wrapper<const Lane> lane;
};

//! Returns the lane of any object that has one
//!
//! \tparam T
//! \return const Lane&
template <typename T>
const Lane &GetLane(const T &);

//! Returns the road of any object that has a lane
//!
//! \tparam T
//! \return const Road&
template <typename T>
const Road &GetRoad(const T &);

//! Returns the latitude of the given point relative to the lane it is associated with
//!
//! \return double
template <typename Point>
double GetLaneLatitude(const Point &);

//! Returns the longitude of the given point relative to the lane it is associated with
//!
//! \return double
template <typename Point>
double GetLaneLongitude(const Point &);

namespace detail {
struct RoadPoint : Coordinates<Road>
{
    RoadPoint(Lane &lane, const Coordinates<Road> &coordinates);

    RoadPoint(Lane &lane, Coordinates<Road> &&coordinates);

    RoadPoint(Lane &lane, const Coordinates<Lane> &coordinates);

    RoadPoint(Assignment<Lane> &assignment);

    //! Constructs a road point from a lane point by performing coordinate conversion
    //!
    //! \param point
    RoadPoint(detail::LanePoint &point);

    //! Converts this road point's coordinates to lane coordinates with the same global xy-position
    //!
    //! \return Coordinates<Lane>
    operator Coordinates<Lane>() const;

    //! Returns the lane associated with this point
    //!
    //! \return const Lane&
    const Lane &GetLane() const;

    //! Returns the lane associated with this point
    //!
    //! \return Lane&
    Lane &GetLane();

    //! Returns the road associated with this point
    //!
    //! \return const Road&
    const Road &GetRoad() const;

    //! Returns the road associated with this point
    //!
    //! \return const Road&
    Road &GetRoad();

    //! Returns the global position of this point
    //!
    //! \return Vector2d
    Vector2d GetXY() const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \tparam D Downstream or Upstream
    //! \param destination
    //! \param maxRange
    //! \return bool
    template <Direction D>
    bool GetDistanceTo(const RoadPoint &destination, double maxRange) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \param destination
    //! \param maxRange
    //! \param direction Downstream or Upstream
    //! \return bool
    bool GetDistanceTo(const RoadPoint &destination, double maxRange, Direction direction) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \tparam O Forwards or Backwards
    //! \param destination
    //! \param maxRange
    //! \return bool
    template <Orientation O = Orientation::Forwards>
    bool GetDistanceTo(const RoadPoint &destination, double maxRange) const;

    //! Returns the distance to the given destination or infinity if beyond the given maximum search range
    //!
    //! \param destination
    //! \param maxRange
    //! \return bool
    bool GetDistanceTo(const RoadPoint &destination, double maxRange) const;

private:
    std::reference_wrapper<Lane> lane;
};
} // namespace detail

bool operator==(const RoadPoint &lhs, const RoadPoint &rhs);
bool operator==(const detail::RoadPoint &lhs, const RoadPoint &rhs);
bool operator==(const RoadPoint &lhs, const detail::RoadPoint &rhs);
bool operator==(const detail::RoadPoint &lhs, const detail::RoadPoint &rhs);
bool operator==(const RoadPoint &lhs, const Assignment<Lane> &rhs);
bool operator==(const detail::RoadPoint &lhs, const Assignment<Lane> &rhs);
bool operator==(const Assignment<Lane> &lhs, const RoadPoint &rhs);
bool operator==(const Assignment<Lane> &lhs, const detail::RoadPoint &rhs);
bool operator==(const RoadPoint &lhs, const LanePoint &rhs);
bool operator==(const detail::RoadPoint &lhs, const LanePoint &rhs);
bool operator==(const RoadPoint &lhs, const detail::LanePoint &rhs);
bool operator==(const detail::RoadPoint &lhs, const detail::LanePoint &rhs);

bool operator!=(const RoadPoint &lhs, const RoadPoint &rhs);
bool operator!=(const detail::RoadPoint &lhs, const RoadPoint &rhs);
bool operator!=(const RoadPoint &lhs, const detail::RoadPoint &rhs);
bool operator!=(const detail::RoadPoint &lhs, const detail::RoadPoint &rhs);
bool operator!=(const RoadPoint &lhs, const Assignment<Lane> &rhs);
bool operator!=(const detail::RoadPoint &lhs, const Assignment<Lane> &rhs);
bool operator!=(const Assignment<Lane> &lhs, const RoadPoint &rhs);
bool operator!=(const Assignment<Lane> &lhs, const detail::RoadPoint &rhs);
bool operator!=(const RoadPoint &lhs, const LanePoint &rhs);
bool operator!=(const detail::RoadPoint &lhs, const LanePoint &rhs);
bool operator!=(const RoadPoint &lhs, const detail::LanePoint &rhs);
bool operator!=(const detail::RoadPoint &lhs, const detail::LanePoint &rhs);

std::ostream &operator<<(std::ostream &os, const RoadPoint &rhs);
std::ostream &operator<<(std::ostream &os, const detail::RoadPoint &rhs);
} // namespace osiql

namespace osiql {

template <typename T>
const Lane &GetLane(const T &point)
{
    if constexpr (HAS_MEMBER(T, GetLane()))
    {
        return point.GetLane();
    }
    else if constexpr (HAS_MEMBER(T, GetEntity()))
    {
        return point.GetEntity();
    }
    else if constexpr (HAS_MEMBER(T, lane))
    {
        return point.lane;
    }
    else
    {
        static_assert(always_false<T>, "GetLane: Type not supported");
    }
}
template <typename T>
const Road &GetRoad(const T &point)
{
    return GetLane(point).GetRoad();
}

template <typename Point>
double GetLaneLatitude(const Point &p)
{
    if constexpr (std::is_same_v<Point, LanePoint> || std::is_same_v<Point, detail::LanePoint>)
    {
        return GetLatitude(p);
    }
    else
    {
        if (GetLane(p).GetDirection() == Direction::Upstream)
        {
            return GetLatitude<Direction::Upstream>(GetLane(p)) - GetLatitude(p);
        }
        else
        {
            return GetLatitude(p) - GetLatitude<Direction::Downstream>(GetLane(p));
        }
    }
}

template <typename Point>
double GetLaneLongitude(const Point &p)
{
    if constexpr (std::is_same_v<Point, LanePoint> || std::is_same_v<Point, detail::LanePoint>)
    {
        return GetLongitude(p);
    }
    else
    {
        const double leftLongitude{GetLane(p).template GetBoundaryPoints<Side::Left>().GetLongitude(GetLatitude(p))};
        const double rightLongitude{GetLane(p).template GetBoundaryPoints<Side::Right>().GetLongitude(GetLatitude(p))};
        return GetLongitude(p) - (leftLongitude * 0.5 + rightLongitude * 0.5);
    }
}
} // namespace osiql
