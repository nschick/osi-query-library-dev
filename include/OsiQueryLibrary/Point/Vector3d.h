/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Three-dimensional global point that additionally supports construction from OSI point types

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <osi3/osi_common.pb.h>        // osi3::Vector3d
#include <osi3/osi_lane.pb.h>          // osi3::LaneBoundary::BoundaryPoint
#include <osi3/osi_logicallane.pb.h>   // osi3::LogicalLaneBoundary::LogicalBoundaryPoint
#include <osi3/osi_referenceline.pb.h> // osi3::ReferenceLine::ReferenceLinePoint
#include <vector>

#include "OsiQueryLibrary/Point/Vector.h"
#include "OsiQueryLibrary/Types/Common.h"

namespace osiql {

struct Vector3d : Vector<double, 3>
{
    using Vector<double, 3>::Vector;

    //! Constructs a Vector3d from an OSI Vector2d and an optional z component
    //!
    //! \param point
    //! \param z
    Vector3d(const osi3::Vector2d &point, double z = 0.0);

    //! Constructs a Vector3d from an OSI Vector3d
    //!
    //! \param point
    Vector3d(const osi3::Vector3d &point);

    //! Constructs a Vector3d from an OSI LogicalLaneBoundaryPoint
    //!
    //! \param point
    explicit Vector3d(const osi3::LogicalLaneBoundary::LogicalBoundaryPoint &point);

    //! Constructs a Vector3d from an OSI LaneBoundaryPoint
    //!
    //! \param point
    explicit Vector3d(const osi3::LaneBoundary::BoundaryPoint &point);

    //! Constructs a Vector3d from an OSI ReferenceLinePoint
    //!
    //! \param point
    explicit Vector3d(const osi3::ReferenceLine::ReferenceLinePoint &point);

    //! Computes the cross product between this point and the given one.
    //!
    //! \param point
    //! \return constexpr double
    constexpr Vector3d Cross(const Vector<double, 3> &point) const noexcept;
};

void PrintTo(const Vector3d &point, std::ostream *os);
} // namespace osiql

// Makes Vector3d usable as a boost point type
BOOST_GEOMETRY_REGISTER_POINT_3D(osiql::Vector3d, double, cs::cartesian, x, y, z)

namespace osiql {
constexpr Vector3d operator+(const Vector3d &lhs, const Vector3d &rhs)
{
    return {lhs.values[0] + rhs.values[0], lhs.values[1] + rhs.values[1], lhs.values[2] + rhs.values[2]};
}
constexpr Vector3d operator-(const Vector3d &rhs)
{
    return {-rhs.values[0], -rhs.values[1], -rhs.values[2]};
}
constexpr Vector3d operator-(const Vector3d &lhs, const Vector3d &rhs)
{
    return {lhs.values[0] - rhs.values[0], lhs.values[1] - rhs.values[1], lhs.values[2] - rhs.values[2]};
}
constexpr Vector3d operator*(const Vector3d &lhs, double rhs)
{
    return {lhs.values[0] * rhs, lhs.values[1] * rhs, lhs.values[2] * rhs};
}
constexpr Vector3d operator*(double lhs, const Vector3d &rhs)
{
    return rhs * lhs;
}
constexpr Vector3d operator/(const Vector3d &lhs, double rhs)
{
    return {lhs.values[0] / rhs, lhs.values[1] / rhs, lhs.values[2] / rhs};
}

constexpr Vector3d Vector3d::Cross(const Vector<double, 3> &point) const noexcept
{
    return {
        values[1] * point[2] - values[2] * point[1],
        values[2] * point[0] - values[0] * point[2],
        values[0] * point[1] - values[1] * point[0] //
    };
}
} // namespace osiql
