/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Global point that supports various operators

#include <algorithm>
#include <cmath>
#include <cstddef>

#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Trait/Common.h"

namespace osiql {
namespace detail {
//! \brief Tolerance for rounding errors
static double constexpr EPSILON = 1e-9;
} // namespace detail

template <typename T, size_t N>
struct Vector : detail::GlobalCoordinates<T, N>
{
    constexpr static const size_t Dimensions = N;
    using Type = T;

    constexpr Vector() = default;

    constexpr Vector(const Vector<T, N - 1> &point, const T &coordinate = T(0));

    template <typename... Args, std::enable_if_t<(sizeof...(Args) == N) && (std::conjunction_v<std::is_convertible<unswizzle_t<Args>, unswizzle_t<T>>...>), bool> = true>
    constexpr Vector(Args &&...coordinates) :
        detail::GlobalCoordinates<T, N>{std::array<T, N>{static_cast<T>(coordinates)...}}
    {
    }

    constexpr T &operator[](size_t i);
    constexpr const T &operator[](size_t i) const;

    //! Returns whether this vector has a non-zero component
    //!
    //! \return bool
    operator bool() const noexcept; // C++20 std::any_of constexpr would be nice here

    //! Adds the coordinate components of the right-hand side to those of the left-hand side
    //!
    //! \param point
    constexpr void operator+=(const Vector<T, N> &point) noexcept;

    //! Subtracts the coordinate components of the right-hand side from those of the left-hand side
    //!
    //! \param point
    constexpr void operator-=(const Vector<T, N> &point) noexcept;

    //! Multiplies each component of the left-hand side by the given factor
    //!
    //! \param scale
    constexpr void operator*=(const T &factor) noexcept;

    //! Divides each component of the left-hand side by the given divisor
    //!
    //! \param scale
    constexpr void operator/=(const T &divisor);

    //! Computes the dot product between this point and the given one, which is the chain product
    //! of their Euclidean magnitudes and the cosine of the angle between them.
    //!
    //! \param point
    //! \return constexpr T
    constexpr T Dot(const Vector<T, N> &point) const noexcept;

    //! Returns the squared length of this vector.
    //!
    //! \return double
    constexpr double SquaredLength() const noexcept;

    //! Returns the numerically robust length of this vector.
    //!
    //! \return double
    double Length() const;

private:
    template <size_t... Is>
    constexpr Vector(const Vector<T, N - 1> &point, const T &coordinate, std::index_sequence<Is...>) :
        detail::GlobalCoordinates<T, N>{std::array<T, N>{point[Is]..., coordinate}}
    {
    }
};

template <typename T, size_t N>
constexpr bool operator==(const Vector<T, N> &lhs, const Vector<T, N> &rhs); // C++20 std::any_of constexpr would be nice here

template <typename T, size_t N>
constexpr bool operator!=(const Vector<T, N> &lhs, const Vector<T, N> &rhs); // C++20 std::any_of constexpr would be nice here

//! Returns the given vector scaled down by its magnitude or the coordinate origin if the point is near the origin.
//!
//! \tparam Point
//! \param vector
//! \return constexpr Point
template <typename Point, std::enable_if_t<detail::is_vector<Point>::value, bool> = true>
constexpr Point Norm(const Point &vector)
{
    const double length{vector.Length()};
    return (std::abs(length) < detail::EPSILON) ? Point() : (vector / length);
}

namespace {
template <typename Point, size_t... Is>
constexpr Point Add(const Point &lhs, const Point &rhs, std::index_sequence<Is...>)
{
    return {(lhs[Is] + rhs[Is])...};
}
template <typename Point, size_t... Is>
constexpr Point Subtract(const Point &lhs, const Point &rhs, std::index_sequence<Is...>)
{
    return {(lhs[Is] - rhs[Is])...};
}
template <typename Point, size_t... Is>
constexpr Point Invert(const Point &rhs, std::index_sequence<Is...>)
{
    return {-rhs[Is]...};
}
template <typename Point, typename T, size_t... Is>
constexpr Point MultiplyPointWithScalar(const Point &lhs, const T &factor, std::index_sequence<Is...>)
{
    return {(lhs[Is] * factor)...};
}
template <typename Point, typename T, size_t... Is>
constexpr Point Divide(const Point &lhs, const T &divisor, std::index_sequence<Is...>)
{
    return {(lhs[Is] / divisor)...};
}
} // namespace

template <typename Point, std::enable_if_t<detail::is_vector<Point>::value, bool> = true>
constexpr Point operator+(const Point &lhs, const Point &rhs)
{
    return Add(lhs, rhs, std::make_index_sequence<Point::Dimensions>());
}
template <typename Point, std::enable_if_t<detail::is_vector<Point>::value, bool> = true>
constexpr Point operator-(const Point &rhs)
{
    return Invert(rhs, std::make_index_sequence<Point::Dimensions>());
}

template <typename Point, std::enable_if_t<detail::is_vector<Point>::value, bool> = true>
constexpr Point operator-(const Point &lhs, const Point &rhs)
{
    return Subtract(lhs, rhs, std::make_index_sequence<Point::Dimensions>());
}

template <typename Point, typename T, std::enable_if_t<detail::is_vector<Point>::value, bool> = true>
constexpr Point operator*(const Point &vector, T factor)
{
    return MultiplyPointWithScalar(vector, factor, std::make_index_sequence<Point::Dimensions>());
}

template <typename Point, typename T, std::enable_if_t<detail::is_vector<Point>::value, bool> = true>
constexpr Point operator*(T factor, const Point &vector)
{
    return vector * factor;
}

template <typename Point, typename T, std::enable_if_t<detail::is_vector<Point>::value, bool> = true>
constexpr Point operator/(const Point &vector, T divisor)
{
    return Divide(vector, divisor, std::make_index_sequence<Point::Dimensions>());
}
} // namespace osiql

namespace osiql {
template <typename T, size_t N>
constexpr Vector<T, N>::Vector(const Vector<T, N - 1> &point, const T &coordinate) :
    Vector(point, coordinate, std::make_index_sequence<N - 1>())
{
}

template <typename T, size_t N>
constexpr T &Vector<T, N>::operator[](size_t i)
{
    return this->values[i];
}

template <typename T, size_t N>
constexpr const T &Vector<T, N>::operator[](size_t i) const
{
    return this->values[i];
}

template <typename T, size_t N>
Vector<T, N>::operator bool() const noexcept
{
    return std::any_of(this->values.begin(), this->values.end(), [](const T &value) {
        return value != T(0);
    });
}

template <typename T, size_t N>
constexpr bool operator==(const Vector<T, N> &lhs, const Vector<T, N> &rhs)
{
    for (size_t i{0u}; i < N; ++i)
    {
        if (static_cast<double>(std::abs(lhs.values[i] - rhs.values[i])) > detail::EPSILON)
        {
            return false;
        }
    }
    return true;
}

template <typename T, size_t N>
constexpr bool operator!=(const Vector<T, N> &lhs, const Vector<T, N> &rhs)
{
    return !(lhs == rhs);
}

template <typename T, size_t N>
constexpr void Vector<T, N>::operator+=(const Vector<T, N> &point) noexcept
{
    constexpr_for<0, N>([&](size_t i) {
        this->values[i] += point.values[i];
    });
}

template <typename T, size_t N>
constexpr void Vector<T, N>::operator-=(const Vector<T, N> &point) noexcept
{
    constexpr_for<0, N>([&](size_t i) {
        this->values[i] -= point.values[i];
    });
}

template <typename T, size_t N>
constexpr void Vector<T, N>::operator*=(const T &factor) noexcept
{
    constexpr_for<0, N>([&](size_t i) {
        this->values[i] *= factor;
    });
}

template <typename T, size_t N>
constexpr void Vector<T, N>::operator/=(const T &divisor)
{
    constexpr_for<0, N>([&](size_t i) {
        this->values[i] /= divisor;
    });
}

namespace {
template <typename T, size_t... Is>
constexpr T Dot(const Vector<T, sizeof...(Is)> &lhs, const Vector<T, sizeof...(Is)> &rhs, std::index_sequence<Is...>)
{
    return ((lhs[Is] * rhs[Is]) + ...);
}
} // namespace

template <typename T, size_t N>
constexpr T Vector<T, N>::Dot(const Vector<T, N> &point) const noexcept
{
    return osiql::Dot(*this, point, std::make_index_sequence<N>());
}

template <typename T, size_t N>
constexpr double Vector<T, N>::SquaredLength() const noexcept
{
    return Dot(*this);
}

template <typename T, size_t N>
double Vector<T, N>::Length() const
{
    if constexpr (N == 2)
    {
        const auto x{static_cast<double>(this->values[0])};
        const auto y{static_cast<double>(this->values[1])};
        // Borges, Carlos F. "An Improved Algorithm for hypot (a, b)." arXiv preprint arXiv:1904.09481 (2019).
        if (x == 0.0)
        {
            return std::abs(y);
        }

        if (y == 0.0)
        {
            return std::abs(x);
        }
        const auto h{std::sqrt(std::fma(x, x, y * y))};
        const auto h_sq{h * h};
        const auto x_sq{x * x};
        const auto l{std::fma(-y, y, h_sq - x_sq) + std::fma(h, h, -h_sq) - std::fma(x, x, -x_sq)};
        return h - l / (2 * h);
    }
    else
    {
        return std::sqrt(SquaredLength());
    }
}
} // namespace osiql
