/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/NumberGenerator.h"
#include "OsiQueryLibrary/Object/Object.h"
#include "OsiQueryLibrary/Point/Anchor.h"
#include "OsiQueryLibrary/Routing/Pathfinder.h"
#include "OsiQueryLibrary/Routing/Route.h"
#include "OsiQueryLibrary/Routing/Stream.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Types/Orientation.h"

namespace osiql {
class Query;

struct Distances
{
    double left;
    double right;
};
//! \brief A MovingObject is an osiql::Object that has velocity and acceleration and can generate routes from its location
struct MovingObject : Object<osi3::MovingObject>
{
    using Object<osi3::MovingObject>::Object;

    ~MovingObject();

    //! Copies the given handle to this object.
    //!
    //! \param handle
    void operator=(const osi3::MovingObject &handle);

    //! Returns whether this object is moving in the specified orientation.
    //!
    template <Orientation O = Orientation::Any>
    bool IsMoving() const;

    //! Returns the global directional velocity of this object or a zero vector if not assigned
    //!
    //! \return Vector3d
    Vector3d GetVelocity() const;

    //! Returns the change in position of the given local object point after traveling one second
    //! going only by the object's current velocity and angular velocity.
    //!
    //! \param localPoint Point relative to this object's origin
    //! \return Vector3d
    Vector3d GetVelocity(const Vector3d &localPoint) const;

    //! Returns the directional acceleration of this object or a zero vector if not assigned.
    //!
    //! \return Vector3d
    Vector3d GetAcceleration() const;

    //! Returns the spin, aka angular velocity of this object. Each component represents the change of
    //! rotation per second around the corresponding axis parallel passing through the object's origin.
    //!
    //! \return Vector3d counter-clockwise ascending change in angle around each axis in radians per second
    Vector3d GetAngularVelocity() const;

    //! Returns the change in roll (rotation around x-axis in OSI) per second of this object.
    //!
    //! \return double counter-clockwise ascending angle in radians per second
    double GetRollVelocity() const;

    //! Returns the change in pitch (rotation around y-axis in OSI) per second of this object.
    //!
    //! \return double counter-clockwise ascending angle in radians per second
    double GetPitchVelocity() const;

    //! Returns the change in yaw (rotation around z-axis in OSI) per second of this object.
    //!
    //! \return double counter-clockwise ascending angle in radians per second
    double GetYawVelocity() const;

    //! Returns a pseudovector representing the product of the object's rotational inertia
    //! and rotational velocity (in radians/sec) at the given point relative to the object
    //! center. In layman's terms, this is the distance the point would travel per second
    //! based on its spinning speed if it were not forced to rotate around the object center.
    //!
    //! \param localPoint
    //! \return Vector3d Current spin direction and intensity.
    Vector3d GetAngularMomentum(const Vector3d &localPoint) const;

    //! Returns the spin angular acceleration of this object. Each component represents the change in change of
    //! rotation per second around the corresponding axis parallel passing through the object's origin.
    //!
    //! \return Vector3d
    Vector3d GetAngularAcceleration() const;

    //! Returns this object's change in change in roll (rotation around x-axis in OSI) per second.
    //!
    //! \return double counter-clockwise ascending angle in radians per second within [-π, π]
    double GetRollAcceleration() const;

    //! Returns this object's change in change in pitch (rotation around y-axis in OSI) per second.
    //!
    //! \return double counter-clockwise ascending angle in radians per second within [-π, π]
    double GetPitchAcceleration() const;

    //! Returns this object's change in change in yaw (rotation around z-axis in OSI) per second.
    //!
    //! \return double counter-clockwise ascending angle in radians per second within [-π, π]
    double GetYawAcceleration() const;

    //! \brief Type of a moving object, describing whether it's an animal, pedestrian, vehicle or something else
    enum class Type : std::underlying_type_t<osi3::MovingObject_Type>
    {
        Unknown = osi3::MovingObject_Type_TYPE_UNKNOWN,
        Other = osi3::MovingObject_Type_TYPE_OTHER,
        Vehicle = osi3::MovingObject_Type_TYPE_VEHICLE,
        Pedestrian = osi3::MovingObject_Type_TYPE_PEDESTRIAN,
        Animal = osi3::MovingObject_Type_TYPE_ANIMAL
    };
    //! Returns the type of moving object this object is, which may affect which lanes it moves on and in what manner it navigates them.
    //!
    //! \return Type
    Type GetType() const;

    //! Returns this object's angular velocity as an antimetric matrix. This represents the tangential rotational force
    //! of any given local point at the current time.
    //!
    //! \return const Matrix<3>&
    const Matrix<3> &GetSpinMatrix() const;

    //! Returns a transformation matrix that converts a local point relative to this object's origin to where it will
    //! be in one second with the current linear velocity (ignoring spin & acceleration) in global xyz-coordinates.
    //!
    //! \return const Matrix<4>&
    const Matrix<4> &GetVelocityMatrix() const;

    //! Returns the combined spin and velocity matrix.
    //!
    //! \return const Matrix<4>&
    const Matrix<4> &GetLocalMovementMatrix() const;

    //! Returns the combined spin, rotation and velocity matrix.
    //!
    //! \return const Matrix<4>&
    const Matrix<4> &GetGlobalMovementMatrix() const;

    //! Returns the combined spin and rotation matrix
    //!
    //! \return const Matrix<3>&
    const Matrix<3> &GetSpinAndRotationMatrix() const;

    //! Returns the shortest route from this object's position to the nearest of the given destinations.
    //!
    //! \tparam Orientation Forwards, Backwards or Any (both)
    //! \tparam T Representation of a point that can be implicitly cast to a RoadPoint
    //! \param destinations
    //! \param distance Optional output parameter which will be overwritten with the length of the shortest route
    //! \return Route
    template <Orientation = Orientation::Any, typename T = RoadPoint>
    Route GetShortestRoute(const std::vector<T> &destinations, double *distance = nullptr) const;

    //! Returns a random route by randomly selecting connected lanes using the given callable.
    //! The route ends once there are no more connected lanes or the maximum distance has been reached.
    //!
    //! \tparam RNG osiql::NumberGenerator by default
    //! \param rng Nullary callable returning values in the range [0, 1)
    //! \param maxDistance s-coordinate distance in meters along the route
    //! \return Route
    template <typename RNG = NumberGenerator>
    Route GetRandomRoute(RNG &rng, double maxDistance = MAX_SEARCH_DISTANCE) const;

    //! Returns the leftward ascending distance to the given left boundary and
    //! the rightward ascending distance to the right boundary.
    //!
    //! \tparam Direction How the boundaries are traversed: Downstream (front to back) or Upstream (back to front)
    //! \tparam T Iterable chain of points
    //! \param left
    //! \param right
    //! \return Distances
    template <Direction, typename T>
    Distances GetDistancesToBoundaries(const T &left, const T &right) const;

    //! Returns the leftward ascending distance to the given left boundary and
    //! the rightward ascending distance to the right boundary.
    //!
    //! \tparam T Iterable chain of points
    //! \param left Left chain of points
    //! \param right Left chain of points
    //! \param direction How the boundaries are traversed: Downstream (front to back) or Upstream (back to front)
    //! \return Distances
    template <typename T>
    Distances GetDistancesToBoundaries(const T &left, const T &right, Direction direction) const;

    //! Returns the shortest distance from this object to the left and right boundary of the given lane.
    //!
    //! \tparam Direction How the boundaries are traversed: Downstream (front to back) or Upstream (back to front)
    //! \param lane
    //! \return Distances
    template <Direction>
    Distances GetDistancesToBoundaries(const Lane &lane) const;

    //! Returns the shortest distance from this object to the left and right boundary of the given lane.
    //!
    //! \tparam O Forwards or Backwards
    //! \param lane
    //! \return Distances First is distance to the left boundary, second is distance to the right boundary
    template <Orientation O = Orientation::Forwards>
    Distances GetDistancesToBoundaries(const Lane &lane) const;

    //! Returns the shortest distance from this object to the left and right boundary of the given road
    //!
    //! \tparam Direction Upstream or Downstream
    //! \tparam Side Left, Right or Both
    //! \param road
    //! \return Distances
    template <Direction>
    Distances GetDistancesToBoundaries(const Road &road) const;

    //! Returns the shortest distance from this object to the left and right boundary of the given road
    //!
    //! \param road
    //! \param direction How the boundaries are traversed: Downstream (front to back) or Upstream (back to front)
    //! \return Distances
    Distances GetDistancesToBoundaries(const Road &road, Direction direction) const;

    //! Returns the difference in longitude between this object and the given lane's left and right boundary.
    //! Note: The implementation only checks the longitude differences at the object's greatest and smallest
    //! latitude.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \return Distances
    template <Orientation = Orientation::Forwards>
    Distances GetApproximateBoundsDistancesToBoundaries(const Lane &) const;

    //! Returns an iterator to the bounds of this object clamped to the latitudes and longitudes
    //! of the given lane relative to the given lane's reference line.
    //!
    //! \param lane
    //! \return std::vector<LocalBounds<Lane>>::const_iterator
    std::vector<LocalBounds<Lane>>::const_iterator GetLocalIntersectionBounds(const Lane &) const;

    //! Returns an iterator to the bounds of this object clamped to the latitudes and longitudes
    //! of the given lane relative to the given lane's reference line.
    //!
    //! \param lane
    //! \return std::vector<LocalBounds<Lane>>::iterator
    std::vector<LocalBounds<Lane>>::iterator GetLocalIntersectionBounds(Lane &);

    //! \brief Container of lane-specific bounds of this object, describing the intersection of this object's shape and the respective lane.
    std::vector<LocalBounds<Lane>> intersectionBounds;

protected:
    void OutdateRotation();

    void OutdateSpin();

    void OutdateVelocity();

    //! \brief Returns the patfinder from the lane assignment of this object with the shortest path to the given destination.
    //!
    //! \tparam Orientation What way to search in: Forwards, Backwards or Any (either/both)
    //! \param destination
    //! \return std::shared_ptr<detail::Pathfinder>  Pathfinder starting on the most favorable lane assignment.
    template <Orientation = Orientation::Forwards>
    std::shared_ptr<detail::Pathfinder> GetPreferredPathfinder(const RoadPoint &destination) const;

private:
    mutable std::unique_ptr<Matrix<3>> spinMatrix{};
    mutable std::unique_ptr<Matrix<3>> spinAndRotationMatrix{};
    mutable std::unique_ptr<Matrix<4>> velocityMatrix{};
    mutable std::unique_ptr<Matrix<4>> localMovementMatrix{};
    mutable std::unique_ptr<Matrix<4>> globalMovementMatrix{};
};

namespace detail {
constexpr const std::array<std::string_view, 5> movingObjectTypeToString{
    "Unknown",
    "Other",
    "Vehicle",
    "Pedestrian",
    "Animal",
};
}

std::ostream &operator<<(std::ostream &os, const MovingObject &object);
std::ostream &operator<<(std::ostream &os, const MovingObject::Type type);
} // namespace osiql

namespace osiql {
template <Orientation O>
bool MovingObject::IsMoving() const
{
    if constexpr (O == Orientation::Any)
    {
        return GetBase().has_velocity() && ((GetBase().velocity().has_x() && GetBase().velocity().x() != 0.0) ||
                                            (GetBase().velocity().has_y() && GetBase().velocity().y() != 0.0) ||
                                            (GetBase().velocity().has_z() && GetBase().velocity().z() != 0.0));
    }
    else if constexpr (O == Orientation::Backwards)
    {
        const double angle{GetYaw() + M_PI_2};
        // TODO: Implement GetSide generically and perform this check more efficiently
        return Vector2d{GetVelocity()}.GetSide(Vector2d{}, Vector2d{std::cos(angle), std::sin(angle)}) == Side::Left;
    }
    else if constexpr (O == Orientation::Forwards)
    {
        const double angle{GetYaw() + M_PI_2};
        return Vector2d{GetVelocity()}.GetSide(Vector2d{}, Vector2d{std::cos(angle), std::sin(angle)}) == Side::Right;
    }
    else
    {
        static_assert(always_false<decltype(O)>, "MovingObject::IsMoving only support orientations 'Any', 'Forwards' and 'Backwards'");
    }
}

template <typename RNG>
Route MovingObject::GetRandomRoute(RNG &rng, double maxDistance) const
{
    if (this->positions.empty())
    {
        return {};
    }
    if (maxDistance == 0.0)
    {
        return Route(this->positions.front());
    }
    Stream stream(this->positions.front(), maxDistance, 0.0, LaneChangeBehavior::WithLaneChangesForwards);
    const std::vector<const Node *> path{stream.GetRandomPath(rng)};
    std::vector<RoadPoint> result(std::max(path.size(), static_cast<size_t>(2)), stream.origin);
    const double remainingDistance{std::abs(maxDistance) - path.back()->distance};

    std::transform(std::next(path.begin()), path.end(), std::next(result.begin()), [](const Node *node) {
        return RoadPoint(node->lane, node->lane.AnchorToCoords<Road>(Anchor::REAR, node->lane.GetDirection()));
    });
    if (remainingDistance > 0.0)
    {
        const Lane &lane{path.back()->lane};
        const Direction d{lane.GetDirection(stream.orientation)};
        double latitude;
        if (d == Direction::Upstream)
        {
            latitude = osiql::min<Direction::Upstream>(lane.GetLatitude<Direction::Upstream>(remainingDistance), lane.GetEndLatitude<Direction::Upstream>());
        }
        else
        {
            latitude = osiql::min<Direction::Downstream>(lane.GetLatitude<Direction::Downstream>(remainingDistance), lane.GetEndLatitude<Direction::Downstream>());
        }
        const double leftLongitude{lane.GetBoundaryPoints<Side::Left>().GetLongitude(latitude)};
        const double rightLongitude{lane.GetBoundaryPoints<Side::Right>().GetLongitude(latitude)};
        result.back() = RoadPoint{lane, Coordinates<Road>{latitude, leftLongitude + (rightLongitude - leftLongitude) * 0.5}};
    }
    return Route(std::move(result), stream.orientation);
}

template <Orientation O>
std::shared_ptr<detail::Pathfinder> MovingObject::GetPreferredPathfinder(const RoadPoint &destination) const
{
    if constexpr (O == Orientation::Any)
    {
        std::shared_ptr<detail::Pathfinder> forward{GetPreferredPathfinder<Orientation::Forwards>(destination)};
        std::shared_ptr<detail::Pathfinder> backward{GetPreferredPathfinder<Orientation::Backwards>(destination)};
        if (forward->GetSolutionCost() < backward->GetSolutionCost())
        {
            return forward;
        }
        backward->goal.distanceFromOrigin = -backward->goal.distanceFromOrigin;
        return backward;
    }
    else
    {
        std::vector<std::shared_ptr<detail::Pathfinder>> pathfinders;
        pathfinders.reserve(this->positions.size());
        for (const auto &origin : this->positions)
        {
            pathfinders.push_back(std::make_shared<detail::Pathfinder>(origin, destination));
            pathfinders.back()->CalculateSolution<Road, O>();
        }
        return *std::min_element(pathfinders.begin(), pathfinders.end(), [](const auto &lhs, const auto &rhs) {
            return lhs->GetSolutionCost() < rhs->GetSolutionCost();
        });
    }
}

template <Orientation O, typename T>
Route MovingObject::GetShortestRoute(const std::vector<T> &destinations, double *distance) const
{
    if (positions.empty())
    {
        return Route();
    }
    std::vector<std::shared_ptr<detail::Pathfinder>> pathfinders;
    pathfinders.reserve(destinations.size());
    for (const auto &destination : destinations)
    {
        pathfinders.push_back(GetPreferredPathfinder<O>(destination));
    }
    const auto it{std::min_element(pathfinders.begin(), pathfinders.end(), [](const auto &lhs, const auto &rhs) {
        return std::abs(lhs->GetSolutionCost()) < std::abs(rhs->GetSolutionCost());
    })};
    if (distance)
    {
        *distance = (*it)->GetSolutionCost();
    }
    if (O == Orientation::Any)
    {
        if ((*it)->GetSolutionCost() < 0.0)
        {
            return (*it)->template GetSolution<Orientation::Backwards>();
        }
        else if ((*it)->GetSolutionCost() != std::numeric_limits<double>::max())
        {
            return (*it)->template GetSolution<Orientation::Forwards>();
        }
    }
    return (*it)->template GetSolution<O>();
}

template <Direction D, typename T>
Distances MovingObject::GetDistancesToBoundaries(const T &left, const T &right) const
{
    const auto [leftSideToLeftBoundary, rightSideToLeftBoundary]{GetSignedDistancesTo(left.template begin<D>(), left.template end<D>())};
    const auto [leftSideToRightBoundary, rightSideToRightBoundary]{GetSignedDistancesTo(right.template begin<D>(), right.template end<D>())};
    return {leftSideToLeftBoundary, -rightSideToRightBoundary};
}

template <typename T>
Distances MovingObject::GetDistancesToBoundaries(const T &left, const T &right, Direction d) const
{
    if (d == Direction::Upstream)
    {
        return GetDistancesToBoundaries<Direction::Upstream>(left, right);
    }
    return GetDistancesToBoundaries<Direction::Downstream>(left, right);
}

template <Direction D>
Distances MovingObject::GetDistancesToBoundaries(const Lane &lane) const
{
    return GetDistancesToBoundaries<D>(lane.GetBoundaryPoints<osiql::Side::Left, D>(), lane.GetBoundaryPoints<osiql::Side::Right, D>());
}

template <Orientation O>
Distances MovingObject::GetDistancesToBoundaries(const Lane &lane) const
{
    if (lane.GetDirection<O>() == Direction::Upstream)
    {
        return GetDistancesToBoundaries<Direction::Upstream>(lane);
    }
    return GetDistancesToBoundaries<Direction::Downstream>(lane);
}

template <Direction D>
Distances MovingObject::GetDistancesToBoundaries(const Road &road) const
{
    return GetDistancesToBoundaries<D>(osiql::back<D>(road.boundaries), osiql::front<D>(road.boundaries));
}

template <Orientation O>
Distances MovingObject::GetApproximateBoundsDistancesToBoundaries(const Lane &lane) const
{
    const auto bounds{GetLocalBounds(lane)};
    if (lane.GetDirection<O>() == Direction::Upstream)
    {
        const auto &left{lane.GetBoundaryPoints<osiql::Side::Left, Direction::Upstream>()};
        const double leftLongitude{std::max(left.GetLongitude(bounds.min.latitude), left.GetLongitude(bounds.max.latitude))};

        const auto &right{lane.GetBoundaryPoints<osiql::Side::Right, Direction::Upstream>()};
        const double rightLongitude{std::min(right.GetLongitude(bounds.min.latitude), right.GetLongitude(bounds.max.latitude))};

        return {rightLongitude - bounds.min.longitude, bounds.max.longitude - leftLongitude};
    }
    else
    {
        const auto &left{lane.GetBoundaryPoints<osiql::Side::Left, Direction::Downstream>()};
        const double leftLongitude{std::min(left.GetLongitude(bounds.min.latitude), left.GetLongitude(bounds.max.latitude))};

        const auto &right{lane.GetBoundaryPoints<osiql::Side::Right, Direction::Downstream>()};
        const double rightLongitude{std::max(right.GetLongitude(bounds.min.latitude), right.GetLongitude(bounds.max.latitude))};

        return {leftLongitude - bounds.max.longitude, bounds.min.longitude - rightLongitude};
    }
}
} // namespace osiql
