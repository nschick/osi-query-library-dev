/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived Object which consists of one or more light bulbs (osi3::TrafficLight)

#include <osi3/osi_trafficlight.pb.h>

#include "OsiQueryLibrary/Object/Object.h"
#include "OsiQueryLibrary/Types/Common.h"

namespace osiql {
//! \brief Class that represents common traffic light types(e.g. three lights, one-light bicycle, etc.) and state(off, green, etc).
struct TrafficLight : Object<osi3::TrafficLight>
{
    //! Constructs a TrafficLight from a set of bulbs
    //!
    //! \param bulbs
    TrafficLight(std::vector<const osi3::TrafficLight *> &&bulbs);

    // TODO: Type and State

    //! \brief The individual light bulbs that make up a traffic light
    std::vector<const osi3::TrafficLight *> bulbs;
};
} // namespace osiql
