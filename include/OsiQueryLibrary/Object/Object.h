/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Derived LaneAssignable which additionally provides distance and obstruction getters

#include <osi3/osi_object.pb.h>

#include "OsiQueryLibrary/Component/LaneAssignable.h"
#include "OsiQueryLibrary/Component/Locatable.h"
#include "OsiQueryLibrary/Point/Pose.h"
#include "OsiQueryLibrary/Routing/Pathfinder.h"
#include "OsiQueryLibrary/Routing/Route.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Types/Orientation.h"

namespace osiql {
template <typename T>
bool operator==(const Interval<T> &, const Interval<T> &);
template <typename T>
std::ostream &operator<<(std::ostream &, const Interval<T> &);

//! An Object is an entity with a global shape that can perform distance queries from its position
//!
//! \tparam Handle Underlying OSI class that this object is a wrapper of.
template <typename Handle>
struct Object : LaneAssignable<Locatable<Handle>>
{
    using LaneAssignable<Locatable<Handle>>::LaneAssignable;

    //! Returns the smallest and largest signed Euclidean distance from this object to the chain of points specified
    //! by the given iterators. The first value represents how far the leftmost point of this object is to the right
    //! of the nearest edge (relative to that edge's orientation) while the second value represents the same for the
    //! rightmost point.
    //!
    //! \tparam It
    //! \param first
    //! \param pastLast
    //! \return Interval<double>
    template <typename It>
    Interval<double> GetSignedDistancesTo(It first, It pastLast) const;

    //! Returns the shortest driving distance along the road network in the specified direction
    //! such that the s-coordinates of the boundaries of this and the given object touch/overlap.
    //! A negative result indicates that the objects already overlap along the road and
    //! represents the inverse amount this object would need to drive backwards to cease intersecting.
    //!
    //! \tparam Orientation Forwards, Backwards or Any (Both)
    //! \tparam U Object type (MovingObject, Object, TrafficSign, TrafficLight, RoadMarking)
    //! \param other the object to which the shortest collision distance gets determined
    //! \return double
    template <Orientation = Orientation::Any, typename OtherHandle = Handle>
    double GetDistanceTo(const Object<OtherHandle> &other) const;

    //! Returns the shortest driving distance along the road network such that the center of this object
    //! shares the same s-coordinate as the given location.
    //!
    //!
    //! \tparam Orientation Forwards, Backwards or Any (Both)
    //! \param destination destination to which the distance is measured
    //! \return double positive distance or max if no route was found
    template <Orientation = Orientation::Any>
    double GetDistanceTo(const RoadPoint &destination) const;

    //! Returns the smallest lateral distance from the given route to the furthest left and right point of this object.
    //! The first element in the pair is the distance to this object's right side, the second is to its left side
    //! relative to the orientation of the route.
    //!
    //! \param route
    //! \return Interval<double>
    Interval<double> GetDeviation(const Route &route) const;

    //! Returns the signed lateral movement this object would have to move leftward relative to the direction
    //! of the given route in order to pass by the given target entity without collision. The min value is
    //! the distance to pass by the target's right side, the max is that to pass by its left side.
    //!
    //! \tparam Entity Either an object of the same type as the caller, an iterable collection of road points,
    //! or a single point convertible to a road point.
    //! \param target Object, iterable collection of road point or a road point convertible type
    //! \param route Route along which the obstruction is determined. Obstruction is the difference in deviations
    //! of two entities from their respective lanes's center line.
    //! \return Interval<double> Minimum and maximum distance to barely avoid the target
    template <typename Entity>
    Interval<double> GetObstruction(const Entity &target, const Route &route) const;

    //! Returns a pair of distances along the given route. The first is the distance this object needs to travel along this
    //! route to be right behind the earliest of the given points along the route. The second is the distance for this object
    //! to be just ahead of the furthest among the given points along the route.
    //!
    //! \param points
    //! \param route
    //! \return Interval<double>
    Interval<double> GetLongitudinalObstruction(const std::vector<RoadPoint> &points, const Route &route) const;

    //! Returns whether the object is on a given route.
    //!
    //! \tparam LocalPoint Type that is or inherits from RoadPoint
    //! \param route
    //! \return bool
    template <typename LocalPoint>
    bool IsOnRoute(const std::vector<LocalPoint> &route) const;
};

//! \brief A stationary object that will never move.
struct StaticObject : Object<osi3::StationaryObject>
{
    using Object<osi3::StationaryObject>::Object;
};
} // namespace osiql

namespace osiql {
template <typename Handle>
template <typename It>
Interval<double> Object<Handle>::GetSignedDistancesTo(It first, It pastLast) const
{
    assert(first != pastLast);
    const Polygon &shape{this->GetShape()};
    std::vector<double> distances(shape.size());
    std::transform(shape.begin(), shape.end(), distances.begin(), [a = first, z = pastLast](const Vector2d &point) {
        return point.GetSignedDistanceToChain(a, z);
    });
    const auto [min, max]{std::minmax_element(distances.begin(), distances.end())};
    return {*min, *max};
}

template <typename Handle>
template <Orientation O, typename OtherHandle>
double Object<Handle>::GetDistanceTo(const Object<OtherHandle> &other) const
{
    if constexpr (O == Orientation::Any)
    { // TODO: Interlaced bidirectional search
        const double forwardDistance{GetDistanceTo<Orientation::Forwards>(other)};
        const double backwardDistance{GetDistanceTo<Orientation::Backwards>(other)};
        if (forwardDistance < 0.0 && backwardDistance < 0.0)
        {
            return 0.0;
        }
        return (forwardDistance <= std::max(backwardDistance, 0.0)) ? forwardDistance : -backwardDistance;
    }
    else
    {
        double result{std::numeric_limits<double>::max()};
        const double range{this->GetSize().SquaredLength()};
        const double otherRange{other.GetSize().SquaredLength()};
        for (const auto &origin : this->positions)
        {
            const RoadPoint rearStart{this->template GetFurthestRoadPoint<!O>(GetLane(origin))};
            const RoadPoint frontStart{this->template GetFurthestRoadPoint<O>(GetLane(origin))};
            const double startOffset{rearStart.GetDistanceTo<O>(frontStart, range)};
            for (const auto &destination : other.positions)
            {
                const RoadPoint rearEnd{other.template GetFurthestRoadPoint<!O>(GetLane(destination))};
                const RoadPoint frontEnd{other.template GetFurthestRoadPoint<O>(GetLane(destination))};
                const double endOffset{rearEnd.GetDistanceTo<O>(frontEnd, otherRange * otherRange)};
                // TODO: Restrict by maximum search distance!
                detail::Pathfinder pathfinder(rearStart, frontEnd);
                pathfinder.CalculateSolution<Road, O>();
                const double cost{pathfinder.GetSolutionCost()};
                const double totalOffset{startOffset + endOffset};
                if (cost <= totalOffset)
                {
                    return cost - totalOffset;
                }
                result = std::min(result, cost - totalOffset);
            }
        }
        return result;
    }
}

template <typename Handle>
template <Orientation O>
double Object<Handle>::GetDistanceTo(const RoadPoint &destination) const
{
    if constexpr (O == Orientation::Any)
    {
        const double forwardDistance{GetDistanceTo<Orientation::Forwards>(destination)};
        const double backwardDistance{GetDistanceTo<Orientation::Backwards>(destination)};
        return (forwardDistance <= backwardDistance) ? forwardDistance : -backwardDistance;
    }
    else
    {
        double result{std::numeric_limits<double>::max()};
        for (const auto &origin : this->positions)
        {
            detail::Pathfinder pathfinder(RoadPoint{origin}, destination);
            pathfinder.CalculateSolution<Road, O>();
            result = std::min(result, pathfinder.GetSolutionCost());
        }
        return result;
    }
}

template <typename Handle>
Interval<double> Object<Handle>::GetDeviation(const Route &route) const
{
    // Iterator to the route point whose lane's road contains this object
    const auto it{std::find_if(this->positions.begin(), this->positions.end(), [&](const auto &position) { // clang-format off
        return std::find_if(route.begin(), route.end(), [&id = GetLane(position).GetRoadId()](const RoadPoint &point) {
            return point.GetLane().GetRoadId() == id;
        }) != route.end();
    })}; // clang-format on
    if (it == this->positions.end())
    {
        std::cerr << "Object is not on the given route. Returning pair of NaNs.\n";
        return {std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};
    }
    const std::vector<RoadPoint> shape{this->GetLocalShape(GetLane(*it))};
    std::vector<double> selfDeviations(shape.size());
    std::transform(shape.begin(), shape.end(), selfDeviations.begin(), [&](const RoadPoint &point) {
        return route.GetDeviation(point);
    });
    const auto [minA, maxA]{std::minmax_element(selfDeviations.begin(), selfDeviations.end())};
    return {*minA, *maxA};
}

template <typename Handle>
template <typename Entity>
Interval<double> Object<Handle>::GetObstruction(const Entity &target, const Route &route) const
{
    if constexpr (std::is_base_of_v<LaneAssignable<Locatable<Handle>>, Entity>)
    {
        const Interval<double> selfDeviations{GetDeviation(route)};
        const Interval<double> targetDeviations{target.GetDeviation(route)};
        return {targetDeviations.min - selfDeviations.max, targetDeviations.max - selfDeviations.min};
    }
    else if constexpr (HAS_MEMBER(Entity, begin()))
    {
        const Interval<double> selfDeviations{GetDeviation(route)};
        std::vector<double> targetDeviations(target.size());
        std::transform(target.begin(), target.end(), targetDeviations.begin(), [&](const RoadPoint &point) {
            return route.GetDeviation(point);
        });
        const auto [minB, maxB]{std::minmax_element(targetDeviations.begin(), targetDeviations.end())};
        return {*minB - selfDeviations.max, *maxB - selfDeviations.min};
    }
    else if constexpr (std::is_convertible_v<Entity, RoadPoint>)
    {
        Interval<double> selfDeviations{GetDeviation(route)};
        const double targetDeviation{route.GetDeviation(target)};
        return {targetDeviation - selfDeviations.max, targetDeviation - selfDeviations.min};
    }
    else
    {
        static_assert(always_false<Entity>, "Object::GetObstruction: First argument's type must be either an object of the same type, convertible to a osiql::RoadPoint or an iterable collection of road points.");
    }
}

template <typename Handle>
Interval<double> Object<Handle>::GetLongitudinalObstruction(const std::vector<RoadPoint> &points, const Route &route) const
{
    // Iterator to the route point whose lane's road contains this object
    const auto it{std::find_if(this->positions.begin(), this->positions.end(), [&](const auto &position) {
        return std::find_if(route.begin(), route.end(), [&id = GetLane(position).GetRoadId()](const RoadPoint &point) {
                   return point.GetLane().GetRoadId() == id;
               }) != route.end();
    })};
    if (it == this->positions.end())
    {
        std::cerr << "GetObstruction: Object is not on the given route. Returning pair of NaNs.\n";
        return {std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};
    }
    const std::vector<RoadPoint> shape{this->GetLocalShape(GetLane(*it))};
    std::vector<double> selfDistances(shape.size());
    std::transform(shape.begin(), shape.end(), selfDistances.begin(), [&](const RoadPoint &point) {
        return route.GetDistance(point);
    });
    std::vector<double> targetDistances(points.size());
    std::transform(points.begin(), points.end(), targetDistances.begin(), [&](const RoadPoint &point) {
        return route.GetDistance(point);
    });
    const auto [minA, maxA]{std::minmax_element(selfDistances.begin(), selfDistances.end())};
    const auto [minB, maxB]{std::minmax_element(selfDistances.begin(), selfDistances.end())};
    return {*minB - *maxA, *maxB - *minA};
}

template <typename Handle>
template <typename LocalPoint>
bool Object<Handle>::IsOnRoute(const std::vector<LocalPoint> &route) const
{
    // TODO: Return false if past last point
    return std::any_of(route.begin(), route.end(), [&](const LocalPoint &location) {
        return std::any_of(this->positions.begin(), this->positions.end(), [&road = location.GetLane().GetRoad()](const auto &position) {
            return GetLane(position).GetRoad() == road;
        });
    });
}

std::ostream &operator<<(std::ostream &, const StaticObject &);
} // namespace osiql
