/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/CommonSign.h"
//
#include <array>
#include <iostream>
#include <string_view>

#include "OsiQueryLibrary/Object/SupplementarySign.h"
#include "OsiQueryLibrary/Object/TrafficSign.h"

namespace osiql {
template <typename T>
Value CommonSign<T>::GetValue() const
{
    if constexpr (std::is_same_v<T, TrafficSign>)
    {
        const auto &handle{static_cast<const TrafficSign *>(this)->GetHandle()};
        if (handle.has_main_sign() && handle.main_sign().has_classification() && handle.main_sign().classification().has_value())
        {
            return Value(handle.main_sign().classification().value());
        }
        for (const osi3::TrafficSign_SupplementarySign &sign : handle.supplementary_sign())
        {
            // TODO: Should all values of a supplementary sign be checked or is the first enough?
            if (sign.has_classification() && !sign.classification().value().empty())
            {
                return sign.classification().value(0);
            }
        }
        return {};
    }
    else if constexpr (std::is_same_v<T, SupplementarySign>)
    {
        const auto &handle{static_cast<const SupplementarySign *>(this)->GetHandle()};
        if (!handle.has_classification() || handle.classification().value_size() == 0)
        {
            return {};
        }
        return handle.classification().value(0);
    }
    else
    {
        static_assert(always_false<T>, "Traffic Sign must be a wrapper of osi3::TrafficSign or osi3::TrafficSign_SupplementarySign");
    }
}

template <typename T>
Variability CommonSign<T>::GetVariability() const
{
    if constexpr (std::is_same_v<T, osi3::TrafficSign>)
    {
        const auto &handle{this->GetHandle()};
        if (!handle.has_main_sign() || !handle.main_sign().has_classification() || !handle.main_sign().classification().has_variability())
        {
            return Variability::Unknown;
        }
        return static_cast<Variability>(handle.main_sign().classification().variability());
    }
    else if constexpr (std::is_same_v<T, osi3::TrafficSign_SupplementarySign>)
    {
        const auto &handle{this->GetHandle()};
        if (!handle.has_classification() || !handle.classification().has_variability())
        {
            return Variability::Unknown;
        }
        return handle.classification().variability();
    }
    else
    {
        static_assert(always_false<T>, "Traffic Sign must be a wrapper of osi3::TrafficSign or osi3::TrafficSign_SupplementarySign");
    }
}

namespace detail {
constexpr const std::array<std::string_view, 7> variabilityToString{
    "Unknown",
    "Other",
    "Fixed",
    "Variable",
    "Movable",
    "Mutable",
    "Movable & mutable",
};
}
} // namespace osiql
