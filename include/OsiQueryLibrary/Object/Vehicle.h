/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Object/MovingObject.h"

//! \file
//! \brief Derived MovingObject with additional getters for vehicle lights, steering wheel angle and other data

namespace osiql {
//! \brief A vehicle's axle
enum class Axle
{
    Front,
    Rear
};

//! \brief A moving object that contains vehicle specific information, e.g. vehicle type, brake-light states, etc.
struct Vehicle : MovingObject
{
    using MovingObject::MovingObject;

    //! Returns whether the given axle is present and defined in this vehicle
    //!
    //! \tparam A Front or Rear
    //! \return bool
    template <Axle A>
    bool HasAxle() const;

    //! Returns the distance from the vehicle's center to the specified axle before any transformations are applied,
    //! meaning the x-coordinate represents the offset in the direction the vehicle is facing and the y-coordinate
    //! represents the offset towards its left side.
    //!
    //! \tparam A Front or Rear
    //! \return Vector3d
    template <Axle A>
    Vector3d GetAxleOffset() const;

    //! Returns the global coordinates of the specified axle of this vehicle.
    //!
    //! \tparam A Front or Rear
    //! \return Vector2d
    template <Axle A>
    Vector2d GetAxleXY() const;

    //! Returns the position of the specified axle localized to each road this vehicle touches.
    //!
    //! \tparam A Front or Rear
    //! \return std::vector<RoadPoint>
    template <Axle A>
    std::vector<RoadPoint> GetLocalAxlePoints() const;

    //! \brief State of the brake light. Can be statically cast to the corresponding OSI light state
    enum class BrakeLightState : std::underlying_type_t<osi3::MovingObject_VehicleClassification_LightState_BrakeLightState>
    {
        Unknown = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OTHER,
        Off = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_OFF,
        Normal = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_NORMAL,
        Strong = osi3::MovingObject_VehicleClassification_LightState_BrakeLightState_BRAKE_LIGHT_STATE_STRONG
    };
    //! Returns the state of this vehicle's brake lights
    //!
    //! \return BrakeLightState
    BrakeLightState GetBrakeLightState() const;

    //! \brief State of a vehicle light. Can be statically cast to the corresponding OSI light state
    enum class GenericLightState : std::underlying_type_t<osi3::MovingObject_VehicleClassification_LightState_GenericLightState>
    {
        Unknown = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OTHER,
        Off = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_OFF,
        On = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_ON,
        FlashingBlue = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_FLASHING_BLUE,
        FlashingBlueAndRed = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_FLASHING_BLUE_AND_RED,
        FlashingAmber = osi3::MovingObject_VehicleClassification_LightState_GenericLightState_GENERIC_LIGHT_STATE_FLASHING_AMBER,
    };
    //! Returns the state of this vehicle's front fog light
    //!
    //! \return GenericLightState
    GenericLightState GetFrontFogLightState() const;

    //! Returns the state of this vehicle's rear fog light
    //!
    //! \return GenericLightState
    GenericLightState GetRearFogLightState() const;

    //! Returns the state of this vehicle's head light
    //!
    //! \return GenericLightState
    GenericLightState GetHeadLightState() const;

    //! Returns the state of this vehicle's high beam light
    //!
    //! \return GenericLightState
    GenericLightState GetHighBeamState() const;

    //! Returns the state of this vehicle's reversing light
    //!
    //! \return GenericLightState
    GenericLightState GetReversingLightState() const;

    //! Returns the state of this vehicle's license plate illumination
    //!
    //! \return GenericLightState
    GenericLightState GetRearLicensePlateIlluminationState() const;

    //! Returns the state of this vehicle's emergency light, which is present in
    //! ambulances, police cars, etc.
    //!
    //! \return GenericLightState
    GenericLightState GetEmergencyVehicleIlluminationState() const;

    //! Returns the state of this vehicle's service light, which is present in snow
    //! removal vehicles, garbage trucks, towing trucks, etc.
    //!
    //! \return GenericLightState
    GenericLightState GetServiceVehicleIlluminationState() const;

    //! \brief State of a vehicle's turn or hazard warning indicator light. Can be statically cast to the corresponding OSI light state
    enum class IndicatorState : std::underlying_type_t<osi3::MovingObject_VehicleClassification_LightState_IndicatorState>
    {
        Unknown = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OTHER,
        Off = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OFF,
        Left = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_LEFT,
        Right = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_RIGHT,
        Warning = osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_WARNING
    };
    //! Returns the state of this vehicle's turn or hazard warning indicator light
    //!
    //! \return IndicatorState
    IndicatorState GetIndicatorState() const;

    //! \brief Type of a vehicle. Can be statically cast to the corresponding OSI type
    enum class VehicleType : std::underlying_type_t<osi3::MovingObject_VehicleClassification_Type>
    {
        Unknown = osi3::MovingObject_VehicleClassification_Type_TYPE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_Type_TYPE_OTHER,
        SmallCar = osi3::MovingObject_VehicleClassification_Type_TYPE_SMALL_CAR,
        CompactCar = osi3::MovingObject_VehicleClassification_Type_TYPE_COMPACT_CAR,
        MediumCar = osi3::MovingObject_VehicleClassification_Type_TYPE_MEDIUM_CAR,
        LuxuryCar = osi3::MovingObject_VehicleClassification_Type_TYPE_LUXURY_CAR,
        DeliveryVan = osi3::MovingObject_VehicleClassification_Type_TYPE_DELIVERY_VAN,
        HeavyTruck = osi3::MovingObject_VehicleClassification_Type_TYPE_HEAVY_TRUCK,
        SemiTractor = osi3::MovingObject_VehicleClassification_Type_TYPE_SEMITRACTOR,
        SemiTrailer = osi3::MovingObject_VehicleClassification_Type_TYPE_SEMITRAILER,
        Trailer = osi3::MovingObject_VehicleClassification_Type_TYPE_TRAILER,
        Motorbike = osi3::MovingObject_VehicleClassification_Type_TYPE_MOTORBIKE,
        Bicycle = osi3::MovingObject_VehicleClassification_Type_TYPE_BICYCLE,
        Bus = osi3::MovingObject_VehicleClassification_Type_TYPE_BUS,
        Tram = osi3::MovingObject_VehicleClassification_Type_TYPE_TRAM,
        Train = osi3::MovingObject_VehicleClassification_Type_TYPE_TRAIN,
        Wheelchair = osi3::MovingObject_VehicleClassification_Type_TYPE_WHEELCHAIR
    };
    //! Returns the type of this vehicle
    //!
    //! \return VehicleType
    VehicleType GetVehicleType() const;

    //! \brief The perceived role of a vehicle, ergo the role it appears to have in the eyes of other traffic participants.
    //! Can be statically cast to the corresponding OSI role
    enum class Role : std::underlying_type_t<osi3::MovingObject_VehicleClassification_Role>
    {
        Unknown = osi3::MovingObject_VehicleClassification_Role_ROLE_UNKNOWN,
        Other = osi3::MovingObject_VehicleClassification_Role_ROLE_OTHER,
        Civil = osi3::MovingObject_VehicleClassification_Role_ROLE_CIVIL,
        Ambulance = osi3::MovingObject_VehicleClassification_Role_ROLE_AMBULANCE,
        Fire = osi3::MovingObject_VehicleClassification_Role_ROLE_FIRE,
        Police = osi3::MovingObject_VehicleClassification_Role_ROLE_POLICE,
        PublicTransport = osi3::MovingObject_VehicleClassification_Role_ROLE_PUBLIC_TRANSPORT,
        RoadAssistance = osi3::MovingObject_VehicleClassification_Role_ROLE_ROAD_ASSISTANCE,
        GarbageCollection = osi3::MovingObject_VehicleClassification_Role_ROLE_GARBAGE_COLLECTION,
        RoadConstruction = osi3::MovingObject_VehicleClassification_Role_ROLE_ROAD_CONSTRUCTION,
        Military = osi3::MovingObject_VehicleClassification_Role_ROLE_MILITARY,
    };
    //! Returns the perceived role of this vehicle
    //!
    //! \return Role
    Role GetRole() const;

    //! Steering wheel angle relative to that of this vehicle.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetSteeringWheelAngle() const;

    //! \brief Trailer of this vehicle, which is nullptr if the vehicle has no attached trailer
    MovingObject *trailer{nullptr};
};

std::ostream &operator<<(std::ostream &, Axle);
std::ostream &operator<<(std::ostream &, typename Vehicle::BrakeLightState);
std::ostream &operator<<(std::ostream &, typename Vehicle::GenericLightState);
std::ostream &operator<<(std::ostream &, typename Vehicle::IndicatorState);
std::ostream &operator<<(std::ostream &, typename Vehicle::VehicleType);
std::ostream &operator<<(std::ostream &, typename Vehicle::Role);
} // namespace osiql

namespace osiql {
template <Axle A>
bool Vehicle::HasAxle() const
{
    if constexpr (A == Axle::Front)
    {
        return GetHandle().has_vehicle_attributes() && GetHandle().vehicle_attributes().has_bbcenter_to_front();
    }
    else // Axle::Back
    {
        return GetHandle().has_vehicle_attributes() && GetHandle().vehicle_attributes().has_bbcenter_to_rear();
    }
}

template <Axle A>
Vector3d Vehicle::GetAxleOffset() const
{
    if constexpr (A == Axle::Front)
    {
        return GetHandle().vehicle_attributes().bbcenter_to_front();
    }
    else // A == Axle::Back
    {
        return GetHandle().vehicle_attributes().bbcenter_to_rear();
    }
}

template <Axle A>
Vector2d Vehicle::GetAxleXY() const
{
    return GetTransformationMatrix() * GetAxleOffset<A>();
}

template <Axle A>
std::vector<RoadPoint> Vehicle::GetLocalAxlePoints() const
{
    const Vector2d axle{GetAxleXY<A>()};
    std::vector<RoadPoint> result;
    result.reserve(positions.size());
    std::transform(positions.begin(), positions.end(), std::back_insert_iterator(result), [&](const Pose<detail::RoadPoint> &position) {
        return position.GetLane().FindPointNearby(axle);
    });
    return result;
}

namespace detail {
constexpr const std::array<std::string_view, 2> axleToString{"Front", "Rear"};
constexpr const std::array<std::string_view, 5> brakeLightStateToString{
    "Unknown",
    "Other",
    "Off",
    "Normal",
    "Strong",
};
constexpr const std::array<std::string_view, 7> genericLightStateToString{
    "Unknown",
    "Other",
    "Off",
    "On",
    "Flashing blue",
    "Flashing blue & red",
    "Flashing amber",
};
constexpr const std::array<std::string_view, 6> indicatorStateToString{
    "Unknown",
    "Other",
    "Off",
    "Left",
    "Right",
    "Warning",
};
constexpr const std::array<std::string_view, 17> vehicleTypeToString{
    "Unknown",
    "Other",
    "Small car",
    "Compact car",
    "Medium car",
    "Luxury car",
    "Delivery van",
    "Heavy Truck",
    "Semi-tractor",
    "Semi-trailer",
    "Trailer",
    "Motorbike",
    "Bicycle",
    "Bus",
    "Tram",
    "Train",
    "Wheelchair",
};
constexpr const std::array<std::string_view, 11> vehicleRoleToString{
    "Unknown",
    "Other",
    "Civil",
    "Ambulance",
    "Fire",
    "Police",
    "Public transport",
    "Road assistance",
    "Garbage collection",
    "Road construction",
    "Military",
};
} // namespace detail
} // namespace osiql
