/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Representation of an osi3::GroundTruth

#include <map>
#include <osi3/osi_groundtruth.pb.h>
#include <set>
#include <vector>

#include "OsiQueryLibrary/Object/MovingObject.h"
#include "OsiQueryLibrary/Object/Object.h"
#include "OsiQueryLibrary/Object/RoadMarking.h"
#include "OsiQueryLibrary/Object/TrafficLight.h"
#include "OsiQueryLibrary/Object/TrafficSign.h"
#include "OsiQueryLibrary/Object/Vehicle.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/LaneBoundary.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Trait/Collection.h"
#include "OsiQueryLibrary/Trait/Id.h"
#include "OsiQueryLibrary/Version.h"

namespace osiql {
class Query;

//! \brief Encapsulates osi3::GroundTruth which provides information about the objects in the simulated world.
//! This class is public to allow for full access of OSI, but should be used interfacing through an osiql::Query,
//! as it is needed to update the world's moving objects between timesteps.
class World
{
public:
    //! Constructs a World from the given osi3::GroundTruth
    //!
    //! \param world
    World(const osi3::GroundTruth &world);

    ~World();

    // Generic Getters for accessing any object with an Id

    //! Returns the object of the given type with the given id or nullptr if no such object exists.
    //!
    //! \tparam T
    //! \param id
    //! \return const T*
    template <typename T>
    const T *Get(id_t<T> id) const;

    //! Returns all objects of the given type.
    //!
    //! \tparam T
    //! \return const Collection<T>&
    template <typename T>
    const Collection<T> &GetAll() const;

    //! Returns the host vehicle (aka ego)
    //!
    //! \return const MovingObject&
    const MovingObject &GetHostVehicle() const;

    //! Takes a vector of osi3::MovingObjects and returns a set of shared_ptr<osiql::MovingObject>.
    //! Each object in the set owns a copy of its handle.
    //!
    //! \param container
    //! \return Set<std::shared_ptr<MovingObject>>
    static Set<std::shared_ptr<MovingObject>> FetchMovingObjects(const Container<typename MovingObject::Handle> &container);

    //! \brief Underlying OSI object that holds all world information
    const osi3::GroundTruth &handle;

    //! \brief The interface version of this world's ground truth.
    const Version version;

private:
    friend class osiql::Query;
    friend std::ostream &operator<<(std::ostream &, const World &);

    void LinkAdjacentLanes(Lane &lane);
    void LinkSuccessorLanes(Lane &lane);
    void LinkPredecessorLanes(Lane &lane);
    void LinkBoundariesAndMarkings();

    Boundary CreateBoundary(std::vector<Lane *> &leftRow, std::vector<Lane *> &rightRow);
    // Helper methods for Create Boundary
    Boundary CreateLeftBoundary(std::vector<Lane *> &row);
    Boundary CreateRightBoundary(std::vector<Lane *> &row);
    // Helper method for CreateLeftBoundary & CreateRightBoundary
    Boundary CreateSideBoundary(std::vector<Lane *> &row,
                                std::string side,
                                const Container<osi3::Identifier> &(*getBoundaryIds)(const Lane *),
                                Boundary::Points &(*getPoints)(Lane *),
                                Boundary::Partitions &(*getPartitions)(Lane *));
    // Helper method for CreateSideBoundary
    std::vector<LaneBoundary *> GetLaneBoundaries(std::vector<Lane *> &row,
                                                  std::string side,
                                                  const Container<osi3::Identifier> &(*getBoundaryIds)(const Lane *));

    void CreateBoundaries(Road &road);
    void CreateRoads();
    void PopulateBoundaries(Road &road);

    template <typename T>
    std::vector<T> fetchAll(const Container<typename T::Handle> &container);

    //! Returns all objects of the given type.
    //!
    //! \tparam T
    //! \return Collection<T>&
    template <typename T>
    Collection<T> &GetAll();

    //! Returns the object of the given type with the given id.
    //!
    //! \tparam T
    //! \param id
    //! \return T&
    template <typename T>
    T &Find(id_t<T> id);

    template <typename T>
    void LinkToLanes(std::vector<T> &entities);

    void CreateAndLinkTrafficLights();

    const Vehicle *ego{nullptr};

    Collection<LaneBoundary> laneBoundaries;
    Collection<LaneMarking> laneMarkings;
    Collection<Lane> lanes;
    Collection<ReferenceLine> referenceLines;
    Collection<Road> roads;

    Collection<MovingObject> movingObjects;
    Collection<RoadMarking> roadMarkings;
    Collection<StaticObject> staticObjects;
    Collection<LightBulb> lightBulbs;
    Collection<TrafficLight> trafficLights;
    Collection<TrafficSign> trafficSigns;
};

std::ostream &operator<<(std::ostream &os, const World &world);
} // namespace osiql

// ---------- Template definitions ----------

namespace osiql {
template <typename T>
Collection<T> &World::GetAll()
{
    return const_cast<Collection<T> &>(std::as_const(*this).GetAll<T>());
}

template <typename T>
const T *World::Get(id_t<T> id) const
{
    return osiql::Get<T>(GetAll<T>(), id);
}

template <typename T>
T &World::Find(id_t<T> id)
{
    return osiql::Find<T>(this->GetAll<T>(), id);
}

template <typename T>
std::vector<T> World::fetchAll(const Container<typename T::Handle> &container)
{
    if constexpr (std::is_same_v<T, ReferenceLine>)
    {
        std::vector<T> result;
        result.reserve(static_cast<size_t>(container.size()));
        for (const typename T::Handle &entity : container)
        {
            result.emplace_back(entity, version);
        }
        std::sort(result.begin(), result.end());
        return result;
    }
    else
    {
        std::vector<T> result;
        result.reserve(static_cast<size_t>(container.size()));
        for (const typename T::Handle &entity : container)
        {
            result.emplace_back(entity);
        }
        std::sort(result.begin(), result.end());
        return result;
    }
}

template <typename T>
void World::LinkToLanes(std::vector<T> &entities)
{
    for (T &entity : entities)
    {
        for (const osi3::LogicalLaneAssignment &assignment : entity.GetLaneAssignments())
        {
            Lane &lane{Find<Lane>(assignment.assigned_lane_id().value())};
            lane.GetAll<T>().emplace_back(entity, assignment);
            entity.positions.emplace_back(lane, assignment);
        }
    }
}
} // namespace osiql
