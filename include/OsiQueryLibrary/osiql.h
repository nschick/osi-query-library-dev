/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief File including all others

#include <boost/iterator/function_output_iterator.hpp>
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_trafficcommand.pb.h>

#include "OsiQueryLibrary/NumberGenerator.h"
#include "OsiQueryLibrary/Object/SupplementarySign.h"
#include "OsiQueryLibrary/Object/Vehicle.h"
#include "OsiQueryLibrary/Routing/Stream.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/LaneBoundary.h"
#include "OsiQueryLibrary/Street/LaneSegment.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/World.h"
//
#include "OsiQueryLibrary/Object/CommonSign.tpp"

namespace osiql {
//! \brief A Query offers location query methods, xy- <-> st-coordinate conversion and bidirectional road to lane to object mappings.
class Query
{
public:
    //! Initialize the query with a fully defined ground truth.
    //!
    //! \param groundTruth  ground truth with static world data
    Query(const osi3::GroundTruth &groundTruth);

    //! Updates moving objects and lane assignments of moving objects.
    //! Objects not present in the given container are deleted while new ones are added.
    //! No previously existing object is moved, meaning pointers/references to it will remain valid.
    //!
    //! \param allMovingObjects
    void Update(const Container<osi3::MovingObject> &allMovingObjects);

    //! Returns the world this query is querying
    //!
    //! \return const World&
    const World &GetWorld() const;

    //! Returns the lane with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const Lane*
    const Lane *GetLane(Id id) const;

    //! Returns the lane boundary with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const LaneBoundary*
    const LaneBoundary *GetLaneBoundary(Id id) const;

    //! Returns the reference line with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const ReferenceLine*
    const ReferenceLine *GetReferenceLine(Id id) const;

    //! Returns the moving object with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const MovingObject*
    const MovingObject *GetMovingObject(Id id) const;

    //! Returns the road marking with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const RoadMarking*
    const RoadMarking *GetRoadMarking(Id id) const;

    //! Returns the stationary object with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const Object*
    const StaticObject *GetStaticObject(Id id) const;

    //! Returns the traffic light with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const TrafficLight*
    const TrafficLight *GetTrafficLight(Id id) const;

    //! Returns the traffic sign with the given id or nullptr if none exists
    //!
    //! \param id
    //! \return const TrafficSign*
    const TrafficSign *GetTrafficSign(Id id) const;

    //! Returns the host vehicle of the simulation (aka the ego).
    //!
    //! \return const Vehicle*
    const Vehicle *GetHostVehicle() const;

    //! Assigns a host vehicle that serves as the origin of this query's route generation methods.
    //!
    //! \param id
    void SetHostVehicle(Id id);

    //! Returns the shortest route from the host vehicle to the action's position.
    //! If the host vehicle is already at the specified position, returns a route with only that point.
    //!
    //! \tparam Orientation Forwards, Backwards or Any (both)
    //! \param action
    //! \return Route Empty if the host vehicle is not on any lane or the action is invalid
    template <Orientation = Orientation::Any>
    Route GetRoute(const osi3::TrafficAction::AcquireGlobalPositionAction &action) const;

    //! Returns a vector of local points starting at the host vehicle's current position and passing through the
    //! ordered collection of global points specified in the given action while ignoring timestamps and orientation.
    //! Local points may be added in-between such that no lane is skipped from one point to the next. A partial result
    //! is returned as soon as an off-road point is encountered.
    //!
    //! \tparam Orientation Forwards, Backwards or Any (both)
    //! \param action The TrafficAction containing the ordered set of global points
    //! \return Route
    template <Orientation = Orientation::Any>
    Route GetRoute(const osi3::TrafficAction::FollowPathAction &action) const;

    //! Returns the s-coordinate distance of the shortest route from the origin to the destination.
    //!
    //! \tparam Orientation Forwards, Backwards or Any (both). If the orientation specifies a search in both directions
    //! and the shortest distance is found to travel backwards, then the returned distance will be negative.
    //! \param origin The point from which the distance is measured
    //! \param destination The destination to be reached
    //! \return double
    template <Orientation = Orientation::Any>
    double GetDistanceBetween(const RoadPoint &origin, const RoadPoint &destination) const;

    //! Returns the shortest route from the origin to the destination.
    //!
    //! \tparam Orientation Forwards, Backwards or Any (both)
    //! \param origin The starting point of the returned route
    //! \param destination The local point that will be the last entry in the returned container if a route is found
    //! \param distance Optional output parameter that will be overwritten with the length of the returned route
    //! \return Route Container of local points, one per road traversed on said route
    template <Orientation = Orientation::Any>
    Route GetShortestRoute(const RoadPoint &origin, const RoadPoint &destination, double *distance = nullptr) const;

    //! Returns the shortest route from the origin to the nearest of the destinations.
    //!
    //! \tparam Orientation Forwards, Backwards or Any (both)
    //! \param origin The starting point of the returned route
    //! \param destinations The local points of which one will be the last entry in the returned container if a route is found
    //! \param distance Optional output parameter that will be overwritten with the length of the returned route
    //! \return Route Container of local points, one per road traversed on said route
    template <Orientation = Orientation::Any>
    Route GetShortestRoute(const RoadPoint &origin, const std::vector<RoadPoint> &destinations, double *distance = nullptr) const;

    //! Returns a random route from the host vehicle up until a given maximum distance, either in driving direction
    //! if given a positive distance or against driving direction if given a negative distance.
    //!
    //! \param maxDistance
    //! \return Route
    Route GetRandomRoute(double maxDistance = MAX_SEARCH_DISTANCE) const;

    //! Calculates the local ST-coordinates representing a given point for each lane covering the point.
    //!
    //! \param point    global XY-coordinate
    //! \return std::vector<RoadPoint>
    std::vector<RoadPoint> FindPoints(const Vector2d &point) const;

    //! Calculates the local ST-coordinates and angles representing a given point and angle for each lane covering the point.
    //!
    //! \param point global XY-coordinate
    //! \param angle global yaw of the point
    //! \return std::vector<Pose<RoadPoint>>
    std::vector<Pose<RoadPoint>> FindPoses(const Vector2d &point, double angle) const;

    //! Returns the relative bounds (minimum and maximum s and t values) of the intersections
    //! of the given global bounding box with any road geometry it touches in the world.
    //! Note: If an object intersects the same piece of geometry in two places, only one intersection will be returned.
    //!
    //! \tparam T Either Lane or Road
    //! \param bounds  axis-aligned rectangular bounding box of global points
    //! \return std::vector<LocalBounds<T>>
    template <typename T = Lane>
    std::vector<LocalBounds<const T>> FindLocalIntersectionBounds(const Box &bounds) const;

    //! Returns the relative bounds (minimum and maximum s and t values) of the intersections
    //! of the given polygon with any road geometry it touches in the world.
    //! Note: If an object intersects the same piece of geometry in two places, only one intersection will be returned.
    //!
    //! \tparam T Either Lane or Road
    //! \param polygon an open boost-geometry ring (i.e. simple polygon) with points in counter-clockwise order.
    //! \return std::vector<LocalBounds<T>>
    template <typename T = Lane>
    std::vector<LocalBounds<const T>> FindLocalIntersectionBounds(const Polygon &polygon) const;

    //! Debug output listing the state of the world.
    //!
    //! \param os
    //! \param query
    //! \return std::ostream&
    friend std::ostream &operator<<(std::ostream &os, const Query &query);

    //! \brief The query's random number generator
    mutable NumberGenerator rng;

private:
    //! Returns the relative bounds (minimum and maximum s and t values) of the intersections
    //! of the given global bounding box with any road geometry it touches in the world.
    //! Note: If an object intersects the same piece of geometry in two places, only one intersection will be returned.
    //!
    //! \tparam T Either Lane or Road
    //! \param bounds  axis-aligned rectangular bounding box of global points
    //! \return std::vector<LocalBounds<T>>
    template <typename T = Lane>
    std::vector<LocalBounds<T>> FindInternalIntersectionBounds(const Box &bounds);

    //! Returns the relative bounds (minimum and maximum s and t values) of the intersections
    //! of the given polygon with any road geometry it touches in the world.
    //! Note: If an object intersects the same piece of geometry in two places, only one intersection will be returned.
    //!
    //! \tparam T Either Lane or Road
    //! \param polygon an open boost-geometry ring (i.e. simple polygon) with points in counter-clockwise order.
    //! \return std::vector<LocalBounds<T>>
    template <typename T = Lane>
    std::vector<LocalBounds<T>> FindInternalIntersectionBounds(const Polygon &polygon);

    //! Returns a vector of Lane segments
    //!
    //! \param point is a 2D vector containing the x,y coordinates
    //! \return std::vector<std::pair<Box, LaneSegment>>
    std::vector<std::pair<Box, LaneSegment>> GetSegmentsContaining(const Vector2d &point) const;

    //! Triangulates the given lane and adds the lane triangulation to the query's rTree
    //!
    //! \param lane
    void IndexSegments(Lane &lane);

    //! Updates the global shape, local positions and lane intersection bounds of the given object.
    //!
    //! \param object
    void Update(MovingObject &object);

    std::unique_ptr<World> world;

    using RTree = boost::geometry::index::rtree<std::pair<Box, LaneSegment>, boost::geometry::index::quadratic<8, 4>>;
    // Tree of rectangles for fast location queries
    RTree rTree;
};

namespace detail {
//! Returns a container of the intersection points of two polygons
//!
//! \param polygonA
//! \param polygonB
//! \return const std::vector<Vector2d>
template <typename T, typename U>
std::vector<Vector2d> GetIntersections(const T &polygonA, const U &polygonB);

//! Returns whether two polygons touch
//!
//! \param polygonA
//! \param polygonB
//! \return const std::vector<Vector2d>
template <typename T, typename U>
bool HasIntersections(const T &polygonA, const U &polygonB);
} // namespace detail

std::ostream &operator<<(std::ostream &os, const Query &query);
} // namespace osiql

// ---------- Template definitions ----------

namespace osiql {

template <Orientation O>
double Query::GetDistanceBetween(const RoadPoint &origin, const RoadPoint &destination) const
{
    if constexpr (O == Orientation::Any)
    { // TODO: Interlaced bidirectional search
        const double forwardDistance{GetDistanceBetween<Orientation::Forwards>(origin, destination)};
        const double backwardDistance{GetDistanceBetween<Orientation::Backwards>(origin, destination)};
        return (forwardDistance <= backwardDistance) ? forwardDistance : -backwardDistance;
    }
    else
    {
        detail::Pathfinder pathfinder(origin, destination);
        pathfinder.CalculateSolution<Road, O>();
        return pathfinder.GetSolutionCost();
    }
}

template <Orientation O>
Route Query::GetRoute(const osi3::TrafficAction::AcquireGlobalPositionAction &action) const
{
    if (!GetHostVehicle())
    {
        std::cerr << "Unable to generate route from host vehicle because the GroundTruth has no assigned host vehicle. Use SetHostVehicle to assign a host vehicle. Returning an empty route.\n";
        return {};
    }
    if (action.has_position())
    {
        const std::vector<RoadPoint> locations{FindPoints({action.position().x(), action.position().y()})};
        if (locations.empty())
        {
            std::cerr << "Query::GetRoute: Point specified in AcquireGlobalPositionAction does not lie on any road. Returning an empty route instead.\n";
            return {};
        }
        Route result{GetHostVehicle()->GetShortestRoute<O>(locations)};
        if (!result.empty())
        {
            return result;
        }
        std::cerr << "Query::GetRoute: Point specified in AcquireGlobalPositionAction can not be reached from the current position. Returning an empty route.\n";
    }
    else
    {
        std::cerr << "Query::GetRoute: AcquireGlobalPositionAction has no assigned position. Returning an empty route.\n";
    }
    return {};
}

template <Orientation O>
Route Query::GetRoute(const osi3::TrafficAction::FollowPathAction &action) const
{
    if (!world->ego)
    {
        std::cerr << "Unable to generate route from host vehicle because the GroundTruth has no assigned host vehicle. Use SetHostVehicle to assign a host vehicle. Returning an empty route.\n";
        return {};
    }
    Route result{};
    if (action.path_point().empty())
    {
        return {};
    };
    if (!action.path_point(0).has_position())
    {
        std::cerr << "osiql::GetRoute: FollowPathAction's initial point has no assigned position.\n";
        return {};
    }
    const auto locations{FindPoints({action.path_point(0).position().x(), action.path_point(0).position().y()})};
    if (locations.empty())
    {
        std::cout << "osiql::GetRoute: Initial Pose specified in FollowPathAction does not lie on any road.\n";
        return {};
    }
    auto partialResult{GetHostVehicle()->GetShortestRoute<O>(locations)};
    if (partialResult.GetPoints().empty())
    {
        return {};
    }
    result.GetPoints().insert(result.end(), partialResult.begin(), partialResult.end());

    for (auto i{1}; i < action.path_point_size(); ++i)
    {
        if (!action.path_point(i).has_position())
        {
            std::cerr << "osiql::GetRoute: FollowPathAction contains a point without assigned position.\n";
            break;
        }
        const auto locations{FindPoints({action.path_point(i).position().x(), action.path_point(i).position().y()})};
        if (locations.empty())
        {
            std::cout << "osiql::GetRoute: Pose specified in FollowPathAction does not lie on any road. Returning partial route.\n";
            break;
        }

        partialResult = GetShortestRoute<O>(result.back(), locations);
        if (partialResult.GetPoints().empty())
        {
            break;
        }
        result.GetPoints().insert(result.end(), std::next(partialResult.begin()), partialResult.end());
    }
    return result;
}

template <Orientation O>
Route Query::GetShortestRoute(const RoadPoint &origin, const RoadPoint &destination, double *distance) const
{
    if constexpr (O == Orientation::Any)
    {
        double forwardDistance;
        Route forwardResult{GetShortestRoute<Orientation::Forwards>(origin, destination, &forwardDistance)};
        double backwardDistance;
        Route backwardResult{GetShortestRoute<Orientation::Backwards>(origin, destination, &backwardDistance)};

        if (forwardDistance < backwardDistance)
        {
            if (distance)
            {
                *distance = forwardDistance;
            }
            return forwardResult;
        }
        else if (distance)
        {
            *distance = -backwardDistance;
        }
        return backwardResult;
    }
    else
    {
        detail::Pathfinder pathfinder{origin, destination};
        pathfinder.CalculateSolution<Road, O>();
        Route result{pathfinder.GetSolution<O>()};
        if (distance)
        {
            *distance = pathfinder.GetSolutionCost();
        }
        return result;
    }
}

template <Orientation O>
Route Query::GetShortestRoute(const RoadPoint &origin, const std::vector<RoadPoint> &destinations, double *distance) const
{
    if constexpr (O == Orientation::Any)
    {
        double forwardDistance;
        Route forwardResult{GetShortestRoute<Orientation::Forwards>(origin, destinations, &forwardDistance)};
        double backwardDistance;
        Route backwardResult{GetShortestRoute<Orientation::Backwards>(origin, destinations, &backwardDistance)};

        if (forwardDistance < backwardDistance)
        {
            if (distance)
            {
                *distance = forwardDistance;
            }
            return forwardResult;
        }
        else
        {
            if (distance)
            {
                *distance = -backwardDistance;
            }
            return backwardResult;
        }
    }
    else
    {
        std::shared_ptr<detail::Pathfinder> bestPathfinder{nullptr};
        double cost{std::numeric_limits<double>::max()};
        for (const RoadPoint &destination : destinations)
        {
            auto pathfinder{std::make_shared<detail::Pathfinder>(origin, destination)};
            pathfinder->CalculateSolution<Road, O>();
            if (pathfinder->GetSolutionCost() < cost)
            {
                cost = pathfinder->GetSolutionCost();
                bestPathfinder = pathfinder;
            }
        }
        if (distance)
        {
            *distance = cost;
        }
        if (bestPathfinder)
        {
            return bestPathfinder->GetSolution();
        }
        return {};
    }
}

namespace detail {
template <typename T>
struct IntersectionInserter
{
    std::vector<LocalBounds<T>> &container;
    const Polygon &shape;
    std::set<raw_id_t<T>> visited{};

    // NOTE: The IntersectionInserter is used as the target boost function output iterators in boost rTree queries.
    // Boost always passes const-qualified parameters to such functions, thus a const-cast may be required.

    void operator()(const std::pair<Box, LaneSegment> &entry)
    {
        auto &segment{GetSegment(entry.second)};
        if (visited.find(this->GetId(segment.GetLane())) != visited.end())
        {
            return;
        }
        const std::vector<Vector2d> intersection{detail::GetIntersections(segment.triangle, shape)};
        if (intersection.size() >= 3u)
        {
            visited.insert(GetId(segment.GetLane()));
            container.emplace_back(segment.GetLane());
            for (const Vector2d &point : intersection)
            {
                const Lane &lane{segment.GetLane()};
                const Road &road{lane.GetRoad()};
                container.back().Add(road.referenceLine.GetCoordinates(point)); // TODO: Hint s-coordinate
            }
        }
    }

private:
    id_t<T> GetId(const Lane &lane) const;

    std::conditional_t<std::is_const_v<T>, const LaneSegment, LaneSegment> &GetSegment(const LaneSegment &laneSegment) const;
};

template <typename T>
id_t<T> IntersectionInserter<T>::GetId(const Lane &lane) const
{
    if constexpr (std::is_same_v<std::remove_const_t<T>, Lane>)
    {
        return lane.GetId();
    }
    else
    {
        return lane.GetRoadId();
    }
}

template <typename T>
std::conditional_t<std::is_const_v<T>, const LaneSegment, LaneSegment> &IntersectionInserter<T>::GetSegment(const LaneSegment &laneSegment) const
{
    if constexpr (std::is_const_v<T>)
    {
        return laneSegment;
    }
    else
    {
        return const_cast<LaneSegment &>(laneSegment);
    }
}
} // namespace detail

template <typename T>
std::vector<LocalBounds<const T>> Query::FindLocalIntersectionBounds(const Polygon &shape) const
{
    std::vector<LocalBounds<const T>> bounds;
    typename detail::IntersectionInserter<const T> inserter{bounds, shape};

    rTree.query(
        boost::geometry::index::satisfies([&shape](const std::pair<Box, LaneSegment> &value) { return boost::geometry::intersects(value.first, shape); }),
        boost::make_function_output_iterator(inserter) //
    );
    return bounds;
}

template <typename T>
std::vector<LocalBounds<T>> Query::FindInternalIntersectionBounds(const Polygon &shape)
{
    std::vector<LocalBounds<T>> bounds;
    typename detail::IntersectionInserter<T> inserter{bounds, shape};

    rTree.query(
        boost::geometry::index::satisfies([&shape](const std::pair<Box, LaneSegment> &value) { return boost::geometry::intersects(value.first, shape); }),
        boost::make_function_output_iterator(inserter) //
    );
    return bounds;
}

namespace {
struct GetId
{
    template <typename T>
    Id operator()(const T &object) const
    {
        return object.GetId();
    }
};
} // namespace

namespace detail {
template <typename T, typename U>
std::vector<Vector2d> GetIntersections(const T &polygonA, const U &polygonB)
{
    std::vector<Vector2d> intersections{};
    intersections.reserve(8u);

    std::vector<Vector2d> edgesA(polygonA.size());
    for (size_t i{1}; i < polygonA.size(); ++i)
    {
        edgesA[i - 1] = polygonA[i] - polygonA[i - 1];
    }
    edgesA.back() = polygonA.front() - polygonA.back();

    std::vector<Vector2d> edgesB(polygonB.size());
    for (size_t i{1}; i < polygonB.size(); ++i)
    {
        edgesB[i - 1] = polygonB[i] - polygonB[i - 1];
    }
    edgesB.back() = polygonB.front() - polygonB.back();

    for (size_t i{0}; i < edgesB.size(); ++i)
    {
        for (size_t k{0}; k < edgesA.size(); ++k)
        {
            const double det{edgesB[i].Cross(edgesA[k])};
            const double lambda{(edgesA[k].Cross(polygonB[i]) + polygonA[k].Cross(edgesA[k])) / det};
            const double kappa{(edgesB[i].Cross(polygonB[i]) + polygonA[k].Cross(edgesB[i])) / det};
            if (lambda >= 0.0 && lambda <= 1.0 && kappa >= 0.0 && kappa <= 1.0)
            {
                const double x{polygonB[i].x + lambda * edgesB[i].x};
                const double y{polygonB[i].y + lambda * edgesB[i].y};
                if (intersections.empty() || intersections.back().x != x || intersections.back().y != y)
                {
                    intersections.emplace_back(x, y);
                }
            }
        }
    }

    std::copy_if(polygonA.begin(), polygonA.end(), std::back_inserter(intersections), [&](const Vector2d &a) {
        return (std::find(intersections.begin(), intersections.end(), a) == intersections.end()) && a.IsWithin(polygonB, edgesB);
    });
    std::copy_if(polygonB.begin(), polygonB.end(), std::back_inserter(intersections), [&](const Vector2d &b) {
        return (std::find(intersections.begin(), intersections.end(), b) == intersections.end()) && b.IsWithin(polygonA, edgesA);
    });
    return intersections;
}

template <typename T, typename U>
bool HasIntersections(const T &polygonA, const U &polygonB)
{
    std::vector<Vector2d> edgesA(polygonA.size());
    for (size_t i{1}; i < polygonA.size(); ++i)
    {
        edgesA[i - 1] = polygonA[i] - polygonA[i - 1];
    }
    edgesA.back() = polygonA.front() - polygonA.back();

    std::vector<Vector2d> edgesB(polygonB.size());
    for (size_t i{1}; i < polygonB.size(); ++i)
    {
        edgesB[i - 1] = polygonB[i] - polygonB[i - 1];
    }
    edgesB.back() = polygonB.front() - polygonB.back();

    for (size_t i{0}; i < edgesB.size(); ++i)
    {
        for (size_t k{0}; k < edgesA.size(); ++k)
        {
            const double det{edgesB[i].Cross(edgesA[k])};
            const double lambda{(edgesA[k].Cross(polygonB[i]) + polygonA[k].Cross(edgesA[k])) / det};
            const double kappa{(edgesB[i].Cross(polygonB[i]) + polygonA[k].Cross(edgesB[i])) / det};
            if (lambda >= 0.0 && lambda <= 1.0 && kappa >= 0.0 && kappa <= 1.0)
            {
                return true;
            }
        }
    }

    return std::any_of(polygonA.begin(), polygonA.end(), [&](const Vector2d &a) { return a.IsWithin(polygonB, edgesB); }) //
           || std::any_of(polygonB.begin(), polygonB.end(), [&](const Vector2d &b) { return b.IsWithin(polygonA, edgesA); });
}
} // namespace detail
} // namespace osiql
