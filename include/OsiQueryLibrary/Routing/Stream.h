/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Point/RoadPoint.h"
#include "OsiQueryLibrary/Routing/Node.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
class Stream;
//! \brief A tree of nodes representing the start of a lane in the stream's direction.
//! Each node points to a lane that is a direct successor of its parent node's lane.
//! Provides various search and query methods. If a backward distance is provided,
//! each node constructs an additional tree in the Stream's opposing direction,
//! which allows for separate queries covering lanes merging into the Stream as well.
class Stream
{
public:
    //! Constructs a stream from the given location up to the given distance
    //!
    //! \param origin location from which to construct the stream. Must have a valid assigned lane
    //! \param distance the length of the stream. If negative, the stream is constructed
    //! opposing the driving direction of the origin's lane
    //! \param backwardDistance the distance to search backwards on each node
    //! \param LaneChangeBehavior
    template <typename Point, typename StoredLane = const Lane>
    Stream(const Point &origin, double distance, double backwardDistance = 0.0, LaneChangeBehavior = LaneChangeBehavior::WithoutLaneChanges);

    //! \brief Paired with an entity, the Distance stores the node on which the entity was found
    //! and the exact distance of the entity from the stream origin.
    struct Distance
    {
        //! \brief Constructs a default Distance
        Distance() = default;

        //! Constructs a Distance from its components
        //!
        //! \param node
        //! \param distance
        Distance(const Node *node, double distance);

        //! \brief The node containing the value of this distance
        const Node *node{nullptr};

        //! \brief The distance from the stream origin
        double value{0.0};
    };

    //! \brief The starting point of the stream
    const RoadPoint origin;

    //! \brief The stream's orientation relative to the origin lane's driving direction
    const Orientation orientation;

    //! \brief The maximum distance of any leaf node in the stream
    const double range;

    //! \brief The maximum distance of any peripheral leaf node in the stream
    const double backwardRange;

    //! \brief Initial node of the stream. It's distance is from the origin location to the start of the lane in stream direction
    Node root;

    //! Returns a container of objects of the given type mapped to their shortest distance within this stream.
    //!
    //! \tparam T
    //! \param LaneChangeBehavior
    //! \return std::vector<std::pair<const T *, Distance>>
    template <typename T>
    std::vector<std::pair<const T *, Distance>> Find(LaneChangeBehavior = LaneChangeBehavior::WithoutLaneChanges) const;

    //! Returns a container of objects of the given type mapped to their shortest distance within the periphery of this stream.
    //!
    //! \tparam T
    //! \param LaneChangeBehavior
    //! \return std::vector<std::pair<const T *, Distance>>
    template <typename T>
    std::vector<std::pair<const T *, Distance>> FindPeripheral(LaneChangeBehavior = LaneChangeBehavior::WithoutLaneChanges) const;

    //! Returns a container of all objects of the given type mapped to their shortest distance within this stream including its periphery.
    //!
    //! \tparam T
    //! \param LaneChangeBehavior
    //! \return std::vector<std::pair<const T *, Distance>>
    template <typename T>
    std::vector<std::pair<const T *, Distance>> FindAll(LaneChangeBehavior = LaneChangeBehavior::WithoutLaneChanges) const;

    //! Returns the distances from the stream origin to all nodes whose lanes have one of the given types
    //! while none of their successors do, sorted by ascending distance.
    //!
    //! \param types
    //! \return std::vector<Distance>
    std::vector<Distance> GetDistancesToLaneEnds(const std::vector<Lane::Type> &types = Lane::defaultTypes) const;

    //! Returns an ordered linear path from the given start node to the given end node.
    //! If the given start node is not an ancestor of the end node, a path from the end node's
    //! stream root to said end node is returned instead.
    //!
    //! \param start
    //! \param end
    //! \return std::vector<const Node *>
    std::vector<const Node *> GetLinearPath(const Node &start, const Node &end) const;

    //! Returns a chain of nodes starting with the stream root followed by a random child until the last node has no more children.
    //!
    //! \param rng A number generator that returns a double in the range [0.0, 1.0) when invoked
    //! \return Ordered nodes where front() is the stream root and back() is a leaf.
    template <typename RNG>
    std::vector<const Node *> GetRandomPath(RNG &rng) const;

    //! Returns a chain of nodes starting with the stream root followed by a random child
    //! until the given UnaryFunction returns nullptr or the previous node has no more children.
    //!
    //! \param selector A unary function that takes a const Node* and returns a Node*, usually pointing to one of its children.
    //! The function is never called on a node without children.
    //! \return Ordered nodes where front() is the stream root and back() is a leaf.
    template <typename UnaryFunction>
    std::vector<const Node *> GetCustomPath(const UnaryFunction &selector) const;

    //! Returns the terminal nodes of all paths from the root to the first occurrence of a node that satisfies the given predicate
    //!
    //! \tparam UnaryPred
    //! \param pred
    //! \return std::vector<const Node *>
    template <typename UnaryPred>
    std::vector<const Node *> GetEarliestMatches(const UnaryPred &&pred) const;

    //! Returns all nodes satisfying the given predicate where none of their children satisfy the predicate.
    //!
    //! \tparam UnaryPred
    //! \param pred
    //! \return std::vector<const Node *>
    template <typename UnaryPred>
    std::vector<const Node *> GetLastMatches(const UnaryPred &&pred) const;

    //! Returns the nearest node in terms of distance along lanes that contains the given location.
    //!
    //! \param location
    //! \return const Node*
    const Node *GetClosestNodeContaining(const RoadPoint &location) const;

    //! Returns the first node of lowest depth with the given road id or nullptr if none exist in this stream.
    //!
    //! \param id
    //! \return const Node*
    const Node *FindNode(const RoadId &id) const;

    //! Returns the first node in this stream's periphery of lowest depth with the given road id
    //! or nullptr if none exist in its periphery.
    //!
    //! \param id
    //! \return const Node*
    const Node *FindPeripheralNode(const RoadId &id) const;

    //! Returns the first node of lowest depth satisfying the given predicate or nullptr if none exist in this stream.
    //!
    //! \tparam UnaryPred
    //! \param pred
    //! \return const Node*
    template <typename UnaryPred>
    const Node *FindNode(const UnaryPred &&pred) const;

private:
    // Helper method for Find
    template <typename T>
    void Find_(const Node &node, const Lane &lane, std::set<Id> &pool, std::vector<std::pair<const T *, Distance>> &result) const;

    template <typename T, Orientation>
    std::vector<std::pair<const T *, Distance>> FindPeripheral(LaneChangeBehavior) const;
    template <Orientation, typename T>
    void FindPeripheral_(const Node &root, const Node &node, const Lane &lane, std::set<Id> &pool, std::vector<std::pair<const T *, Distance>> &result) const;
};

std::ostream &operator<<(std::ostream &, const Stream &);
std::ostream &operator<<(std::ostream &, const Stream::Distance &);

//! \brief A StreamPoint is a RoadPoint represented in its distance along a stream from that stream's origin.
//! The lane of its stored node may not correspond to the RoadPoint's lane, but is always part of the same road.
struct StreamPoint
{
    //! Constructs a StreamPoint representing the given localPoint relative to the given stream's origin.
    //!
    //! \param point
    //! \param stream
    StreamPoint(const RoadPoint &point, const Stream &stream);

    //! Longitudinal/protensive distance along the stream from its origin
    double s{0.0};

    //! \brief Signed lateral distance from the reference line of the road of the lane of the stream point's node.
    //! A negative value means the point is to the right of the reference line.
    double t{0.0};

    //! \brief The stream node containing this point
    const Node *node{nullptr};
};
} // namespace osiql

// ---------- Template definitions ----------

namespace osiql {
template <typename Point, typename StoredLane>
Stream::Stream(const Point &location, double distance, double backwardDistance, LaneChangeBehavior laneChangeBehavior) :
    origin(location),
    orientation(std::signbit(distance) ? Orientation::Backwards : Orientation::Forwards),
    range(std::abs(distance)),
    backwardRange(-std::abs(backwardDistance)),
    root(*this, range, laneChangeBehavior)
{
    if (backwardRange < root.distance)
    {
        root.constructPeripheralNodes(*this, root, backwardRange - root.distance, laneChangeBehavior);
    }
    if (backwardRange < 0.0)
    {
        for (Node &child : root.children)
        {
            child.TraverseDepthFirst([&](Node &node) {
                node.constructPeripheralNodes(*this, node, backwardRange, laneChangeBehavior);
                return false;
            });
        }
    }
}

template <typename UnaryPred>
const Node *Stream::FindNode(const UnaryPred &&pred) const
{
    return root.SearchBreadthFirst(std::forward<const UnaryPred>(pred));
}

template <typename UnaryPred>
std::vector<const Node *> Stream::GetEarliestMatches(const UnaryPred &&pred) const
{
    if (pred(root))
    {
        return {&root};
    }
    std::vector<const Node *> matches{};
    std::vector<const std::vector<Node> *> queue{&root.children};
    std::vector<const std::vector<Node> *> nextQueue{};
    while (!queue.empty())
    {
        for (const std::vector<Node> *children : queue)
        {
            for (const Node &child : *children)
            {
                if (pred(child))
                {
                    matches.push_back(&child);
                }
                else if (!child.children.empty())
                {
                    nextQueue.push_back(&child.children);
                }
            }
        }
        std::swap(queue, nextQueue);
        nextQueue.clear();
    }
    return matches;
}

template <typename UnaryPred>
std::vector<const Node *> Stream::GetLastMatches(const UnaryPred &&pred) const
{
    std::vector<const Node *> matches;
    root.FindLastMatches(std::forward<const UnaryPred>(pred), matches);
    return matches;
}

template <typename T>
void Stream::Find_(const Node &node, const Lane &lane, std::set<Id> &pool, std::vector<std::pair<const T *, Distance>> &result) const
{
    for (const auto &assignment : lane.GetAll<T>())
    {
        double distance{node.distance};
        if (lane.GetDirection(orientation) == Direction::Upstream)
        {
            distance += GetLatitude<Direction::Upstream>(lane) - GetLatitude<Direction::Upstream>(assignment);
        }
        else
        {
            distance += GetLatitude<Direction::Downstream>(assignment);
        }
        if (distance < std::abs(range) && (distance + Length(assignment) > backwardRange))
        {
            if (pool.insert(assignment.GetEntity().GetId()).second)
            {
                result.push_back(std::make_pair(&assignment.GetEntity(), Distance(&node, distance)));
            }
        }
    }
}

template <Orientation O, typename T>
void Stream::FindPeripheral_(const Node &root, const Node &node, const Lane &lane, std::set<Id> &pool, std::vector<std::pair<const T *, Distance>> &result) const
{
    for (const auto &assignment : lane.GetAll<T>())
    {
        const RoadPoint point(node.lane, GetCoordinates(assignment, !node.lane.GetDirection()));
        const double distance{node.distance - point.GetLane().GetDistanceBetween<O>(point.latitude, GetLatitude<!O>(point.GetLane()))};
        if (distance >= root.distance + backwardRange)
        {
            if (pool.insert(assignment.GetEntity().GetId()).second)
            {
                result.push_back(std::make_pair(&assignment.GetEntity(), Distance(&node, distance)));
            }
        }
    }
}

template <typename T>
std::vector<std::pair<const T *, Stream::Distance>> Stream::Find(LaneChangeBehavior laneChangeBehavior) const
{
    std::set<Id> pool;
    std::vector<std::pair<const T *, Distance>> result;
    if (laneChangeBehavior == LaneChangeBehavior::WithLaneChanges || laneChangeBehavior == LaneChangeBehavior::WithLaneChangesForwards)
    {
        root.SearchBreadthFirst([&](const Node &node) {
            const Road &road{node.lane.GetRoad()};
            const Side sideOfRoad{GetSideOfRoad(node.lane.GetDirection())};
            for (auto row{road.laneRows.begin(sideOfRoad)}; row != road.laneRows.end(sideOfRoad); ++row)
            {
                for (const Lane *lane : *row)
                {
                    Find_(node, *lane, pool, result);
                }
            }
            return false;
        });
    }
    else
    {
        root.SearchBreadthFirst([&](const Node &node) {
            const Road &road{node.lane.GetRoad()};
            for (const Lane *lane : road.laneRows.at(GetSideOfRoad(node.lane), node.lane.index))
            {
                Find_(node, *lane, pool, result);
            }
            return false;
        });
    }

    std::sort(result.begin(), result.end(), [&](const auto &lhs, const auto &rhs) { return lhs.second.value < rhs.second.value; });
    return result;
}

template <typename T>
std::vector<std::pair<const T *, Stream::Distance>> Stream::FindPeripheral(LaneChangeBehavior s) const
{
    return orientation == Orientation::Forwards ? FindPeripheral<T, Orientation::Forwards>(s) : FindPeripheral<T, Orientation::Backwards>(s);
}

template <typename T, Orientation O>
std::vector<std::pair<const T *, Stream::Distance>> Stream::FindPeripheral(LaneChangeBehavior laneChangeBehavior) const
{
    std::set<Id> pool;
    std::vector<std::pair<const T *, Distance>> result;
    if (laneChangeBehavior == LaneChangeBehavior::WithLaneChanges || laneChangeBehavior == LaneChangeBehavior::WithLaneChangesBackwards)
    {
        root.SearchBreadthFirst([&](const Node &peripheralRoot) {
            peripheralRoot.SearchPeripheryBreadthFirst([&](const Node &node) {
                const Road &road{node.lane.GetRoad()};
                const Side sideOfRoad{GetSideOfRoad(node.lane.GetDirection<O>())};
                for (auto row{road.laneRows.begin(sideOfRoad)}; row != road.laneRows.end(sideOfRoad); ++row)
                {
                    for (const Lane *lane : *row)
                    {
                        FindPeripheral_<!O>(peripheralRoot, node, *lane, pool, result);
                    }
                }
                return false;
            });
            return false;
        });
    }
    else
    {
        root.SearchBreadthFirst([&](const Node &peripheralRoot) {
            peripheralRoot.SearchPeripheryBreadthFirst([&](const Node &node) {
                const Road &road{node.lane.GetRoad()};
                for (const Lane *lane : road.laneRows.at(GetSideOfRoad(node.lane.GetDirection<O>()), node.lane.index))
                {
                    FindPeripheral_<!O>(peripheralRoot, node, *lane, pool, result);
                }
                return false;
            });
            return false;
        });
    }
    std::sort(result.begin(), result.end(), [&](const auto &lhs, const auto &rhs) {
        return lhs.second.value < rhs.second.value;
    });
    return result;
}

template <typename T>
std::vector<std::pair<const T *, Stream::Distance>> Stream::FindAll(LaneChangeBehavior laneChangeBehavior) const
{
    auto objectsAhead{Find<T>(laneChangeBehavior)};
    auto objectsAround{FindPeripheral<T>(laneChangeBehavior)};
    std::vector<std::pair<const T *, Distance>> allObjects;
    allObjects.reserve(objectsAhead.size() + objectsAround.size());
    std::set_union(objectsAhead.begin(), objectsAhead.end(), objectsAround.begin(), objectsAround.end(), std::back_insert_iterator(allObjects), [](const std::pair<const T *, Distance> &lhs, const std::pair<const T *, Distance> &rhs) {
        return (lhs.first->GetId() != rhs.first->GetId()) && (lhs.second.value < rhs.second.value);
    });
    return allObjects;
}

template <typename RNG>
std::vector<const Node *> Stream::GetRandomPath(RNG &rng) const
{
    std::vector<const Node *> result{&root};
    while (!result.back()->children.empty())
    {
        const auto i{static_cast<size_t>(rng() * static_cast<double>(result.back()->children.size()))};
        result.push_back(&result.back()->children[i]);
    }
    return result;
}

template <typename UnaryFunction>
std::vector<const Node *> Stream::GetCustomPath(const UnaryFunction &selector) const
{
    std::vector<const Node *> result{&root};
    while (!result.back()->children.empty())
    {
        const Node *node{selector(result.back())};
        if (!node)
        {
            break;
        }
        result.push_back(node);
    }
    return result;
}
} // namespace osiql
