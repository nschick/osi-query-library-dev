/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Node of a road network search graph

#include <algorithm>
#include <iostream>
#include <vector>

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
class Lane;
class Stream;
//! \brief The behavior with which connected nodes are chosen. Differentiates between allowing
//! and disallowing lane changes while separating stream nodes from peripheral nodes
enum class LaneChangeBehavior
{
    WithLaneChanges,         //!< +lane change, +peripheral lane change
    WithoutLaneChanges,      //!< -lane change, -peripheral lane change
    WithLaneChangesForwards, //!< +lane change, -peripheral lane change
    WithLaneChangesBackwards //!< -lane change, +peripheral lane change
};

std::ostream &operator<<(std::ostream &os, LaneChangeBehavior);

//! \brief A tree node representing the start of a lane in stream direction that stores
//! its distance from the root node, its children, a pointer to its parent node and optionally
//! a set of other ancestors. Its members and properties can be traversed and queried.
struct Node
{
    //! Constructs the initial node of a stream. Recursively constructs its children.
    //!
    //! \param stream
    //! \param range
    //! \param LaneChangeBehavior
    Node(Stream &stream, double range, LaneChangeBehavior);

    //! Constructs a child node of a stream. Recursively constructs its children.
    //!
    //! \param stream
    //! \param parent
    //! \param lane
    //! \param range
    //! \param LaneChangeBehavior
    Node(Stream &stream, const Node &parent, const Lane &lane, double range, LaneChangeBehavior);

    //! Constructs a peripheral node of a stream. Recursively constructs its children.
    //!
    //! \param stream
    //! \param root
    //! \param parent
    //! \param lane
    //! \param backwardRange
    //! \param LaneChangeBehavior
    Node(Stream &stream, const Node &root, const Node &parent, const Lane &lane, double backwardRange, LaneChangeBehavior);

    //! Returns the children of this node
    //!
    //! \return const std::vector<Node>&
    const std::vector<Node> &GetChildren() const;

    //! Returns the peripherally connected nodes of this node aside from its stream parent
    //!
    //! \return const std::vector<Node>&
    const std::vector<Node> &GetPeriphery() const;

    //! Returns this node's number of ancestors
    //!
    //! \return size_t
    size_t GetDepth() const;

    //! Return whether the given node is an ancestor of this node
    //!
    //! \param node
    //! \return whether the given node is an ancestor of this node
    bool HasAncestor(const Node &node) const;

    //! Returns the ancestor with the given difference in node depth.
    //!
    //! \param distance
    //! \return const Node* This node if distance is 0, nullptr if distance > GetDepth()
    const Node *GetAncestor(size_t distance) const;

    //! Performs a breadth-first search starting on this node, terminating on the first node
    //! for which the given predicate returns true and returning said node.
    //!
    //! \tparam UnaryPred
    //! \param pred
    //! \return const Node*
    template <typename UnaryPred>
    const Node *SearchBreadthFirst(const UnaryPred &&pred) const;

    //! Performs a breadth-first search on the periphery of this node (which does not include itself),
    //! returning the first node for which the given predicate returns true.
    //!
    //! \tparam UnaryPred
    //! \param pred Callable unary predicate taking a const Node& and returning a bool
    //! \return const Node* The first node for which the predicate returned true or nullptr if there is none.
    template <typename UnaryPred>
    const Node *SearchPeripheryBreadthFirst(const UnaryPred &&pred) const;

    //! Performs a depth-first search starting on this node, terminating on the first node
    //! for which the given predicate returns true and returning said node.
    //!
    //! \tparam UnaryPred
    //! \param pred Callable unary predicate taking a const Node& and returning a bool
    //! \return const Node*
    template <typename UnaryPred>
    const Node *SearchDepthFirst(const UnaryPred &&pred) const;

    //! Performs a depth-first traversal starting on this node, terminating on the first node for which
    //! the given predicate returns true. Unlike SearchDepthFirst, the traversed nodes are modifiable.
    //!
    //! \tparam UnaryPred
    //! \param pred Callable unary predicate taking a Node& and returning a bool
    //! \return bool Whether the traversal terminated early
    template <typename UnaryPred>
    bool TraverseDepthFirst(const UnaryPred &&pred);

    //! Returns whether this node is the last one continuously matching the given predicate. In other
    //! words, returns whether this node satisfies the given predicate while none of its children do.
    //! Any last match found among this node or any of its descendants is added to the given
    //! container of last matches.
    //!
    //! \tparam UnaryPred
    //! \param pred
    //! \param matches
    //! \return bool Whether
    template <typename UnaryPred>
    bool FindLastMatches(const UnaryPred &&pred, std::vector<const Node *> &matches) const;

    //! \brief The lane of this node
    const Lane &lane;
    //! \brief distance from the stream's origin to the start (in stream direction) of this node's lane
    //! If this node is a peripheral node or the stream root, this might be negative.
    double distance{0.0};
    //! \brief This node's preceding node (relative to the stream direction).
    //! If this node is peripheral, the parent is the successor in stream direction.
    const Node *parent{nullptr};

private:
    friend class Stream;

    void constructPeripheralNodes(Stream &, const Node &root, double backwardRange, LaneChangeBehavior);
    void constructChildNodes(Stream &, double range, LaneChangeBehavior);

    std::vector<Lane *> GetSuccessorLanes(Stream &, LaneChangeBehavior) const;
    std::vector<Lane *> GetPeripheralSuccessorLanes(Stream &, LaneChangeBehavior) const;

    template <Orientation, Direction>
    std::vector<Lane *> GetNarrowPeripheralSuccessorLanes() const;

    //! \brief Successors in stream direction. If this node is peripheral, this container is always empty
    //! and its only successor in stream direction is stored as its parent.
    std::vector<Node> children{};

    //! \brief Container of ancestors (relative to the stream direction) of this node excluding the parent,
    //! allowing the stream to "look behind"
    std::vector<Node> periphery{};
};

std::ostream &operator<<(std::ostream &, const Node &);
} // namespace osiql

// ----- Template definitions

namespace osiql {
template <typename UnaryPred>
const Node *Node::SearchBreadthFirst(const UnaryPred &&pred) const
{
    if (pred(*this))
    {
        return this;
    }
    std::vector<const std::vector<Node> *> queue{&children}, nextQueue{};
    while (!queue.empty())
    {
        for (const std::vector<Node> *children : queue)
        {
            for (const Node &child : *children)
            {
                if (pred(child))
                {
                    return &child;
                }
                else if (!child.children.empty())
                {
                    nextQueue.push_back(&child.children);
                }
            }
        }
        std::swap(queue, nextQueue);
        nextQueue.clear();
    }
    return nullptr;
}

template <typename UnaryPred>
const Node *Node::SearchPeripheryBreadthFirst(const UnaryPred &&pred) const
{
    std::vector<const std::vector<Node> *> queue{&periphery}, nextQueue{};
    while (!queue.empty())
    {
        for (const std::vector<Node> *peripherals : queue)
        {
            for (const Node &peripheral : *peripherals)
            {
                if (pred(peripheral))
                {
                    return &peripheral;
                }
                else if (!peripheral.periphery.empty())
                {
                    nextQueue.push_back(&peripheral.periphery);
                }
            }
        }
        std::swap(queue, nextQueue);
        nextQueue.clear();
    }
    return nullptr;
}

template <typename UnaryPred>
const Node *Node::SearchDepthFirst(const UnaryPred &&pred) const
{
    if (pred(*this))
    {
        return this;
    }
    for (const Node &child : children)
    {
        const Node *result{child.SearchDepthFirst(std::forward<const UnaryPred>(pred))};
        if (result)
        {
            return result;
        }
    }
    return nullptr;
}

template <typename UnaryPred>
bool Node::TraverseDepthFirst(const UnaryPred &&pred)
{
    if (pred(*this))
    {
        return true;
    }

    for (Node &child : children)
    {
        if (child.TraverseDepthFirst(std::forward<const UnaryPred>(pred)))
        {
            return true;
        }
    }
    return false;
}

template <typename UnaryPred>
bool Node::FindLastMatches(const UnaryPred &&pred, std::vector<const Node *> &matches) const
{
    if (pred(*this))
    {
        if (std::none_of(children.begin(), children.end(), [&](const Node &node) { return pred(node); }))
        {
            matches.push_back(this);
        }
        else
        {
            for (const Node &child : children)
            {
                child.FindLastMatches(std::forward<const UnaryPred>(pred), matches);
            }
        }
        return true;
    }
    return false;
}

template <Orientation O, Direction D>
std::vector<Lane *> Node::GetNarrowPeripheralSuccessorLanes() const
{
    const LaneRow &row{lane.GetRoad().laneRows.at<GetSideOfRoad<D>()>(lane.index)};
    const auto endIt{std::upper_bound(std::next(row.begin<D>()), row.end<D>(), GetLatitude<!D>(lane), [](double s, const Lane *lane) {
        return greater<D>(GetLatitude<D>(*lane), s);
    })};
    const std::vector<Lane *> &initialLanes{std::as_const(**row.begin<D>()).template GetConnectedLanes<!O>()};
    std::vector<Lane *> result{initialLanes.begin(), initialLanes.end()};
    for (auto it{std::next(row.begin<D>())}; it != endIt; ++it)
    {
        const std::vector<Lane *> &lanes{std::as_const(**it).template GetConnectedLanes<!O>()};
        result.insert(result.end(), lanes.begin(), lanes.end());
    }
    std::sort(result.begin(), result.end(), [](const Lane *lhs, const Lane *rhs) {
        return lhs->GetRoad() < rhs->GetRoad();
    });
    result.erase(
        std::unique(result.begin(), result.end(), [](const Lane *lhs, const Lane *rhs) {
            return lhs->GetRoad() == rhs->GetRoad();
        }),
        result.end() //
    );
    const auto it{std::find_if(result.begin(), result.end(), [&lane = lane](const Lane *result) {
        return result->GetRoad() == lane.GetRoad();
    })};
    if (it != result.end())
    {
        result.erase(it);
    }
    return result;
}
} // namespace osiql
