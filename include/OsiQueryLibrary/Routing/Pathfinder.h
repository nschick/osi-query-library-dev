/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief A* pathfinder implementation used to find the shortest routes

#include <algorithm>
#include <vector>

#include "OsiQueryLibrary/Point/RoadPoint.h"
#include "OsiQueryLibrary/Point/Vector2d.h"
#include "OsiQueryLibrary/Routing/Route.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
namespace detail {
struct Pathfinder;

//! \brief A* search algorithm implementation
struct Pathfinder
{
    //! Constructs a pathfinder that when queried will try to generate the shortest route from the given
    //! start point to the given end point
    //!
    //! \param start
    //! \param end
    Pathfinder(const osiql::RoadPoint &start, const osiql::RoadPoint &end);

    //! \brief Point storing its local and global representation, as well as its state within a pathfinder
    struct Node : osiql::RoadPoint
    {
        Vector2d xy;
        //! Constructs an incomplete Node
        //!
        //! \param point
        Node(const osiql::RoadPoint &point);

        //! \brief The node preceding this one in the route
        const Node *parent{nullptr};

        //! \brief Exact minimum cost of travelling from the pathfinder origin to this node
        double distanceFromOrigin{0.0};

        //! \brief Euclidean distance estimate to the goal
        double distanceToGoal{0.0};

        //! \brief Estimated distance from the origin to the goal through this node
        double routeDistanceEstimate{distanceFromOrigin + distanceToGoal};

        //! Returns the distance from this node to the given successor with the assumption
        //! that the successor lies on this node's road or a directly neighboring road.
        //!
        //! \tparam Orientation
        //! \param successor
        //! \return double
        template <Orientation = Orientation::Forwards>
        double GetDistanceTo(const Node &successor) const;
    };

    //! \brief The end goal of the pathfinder.
    mutable Node goal;

    //! \brief The starting point of the pathfinder. If the goal is the same as the origin, it was reached without
    //! having a parent and the solution should return it, but if the goal was not reached it will also have no
    //! parent, in which case an empty route should be returned, thus the origin needs to be stored to compare it
    //! against the goal when returning the solution.
    osiql::RoadPoint origin;

    //! \brief Set of nodes to be evaluated
    std::vector<std::unique_ptr<Node>> openNodes;

    //! \brief Set of fully evaluated nodes
    std::vector<std::unique_ptr<Node>> closedNodes;

    //! \brief Highest number of parents of any explored node
    size_t depth{0u};

    //! Performs search steps until a solution has been found
    //!
    //! \tparam T Search scope: Road to include adjacent lanes and their successors, Lane for lane-wide search
    //! \tparam Orientation Forwards or Backwards. NOTE: Orientation "Any" is not allowed.
    template <typename T, Orientation = Orientation::Forwards>
    void CalculateSolution();

    //! Returns the shortest route to the destination or an empty route if none was found.
    //!
    //! \return Route
    template <Orientation = Orientation::Forwards>
    Route GetSolution() const;

    //! Advances search by one step if possible.
    //!
    //! \tparam T Lane or Road. "Lane" will traverse the succesors of the lane of pathfinder nodes
    //! whereas "Road" will traverse the successors of all adjacent lanes on the pathfinder node's side of the road.
    //! \tparam Orientation Forwards or Backwards - Whether the successors are in driving direction or not.
    template <typename T, Orientation = Orientation::Forwards>
    void SearchStep();

    //! Returns all successors of the given node.
    //!
    //! \tparam T Lane or Road. "Lane" will include only the succesors of the given node's lane
    //! whereas "Road" will include the successors of all its adjacent lanes on the node's side of the road.
    //! \tparam Orientation Forwards or Backwards - Whether the successors are in driving direction or not.
    //! \param node
    //! \return std::vector<std::unique_ptr<Node>>
    template <typename T, Orientation = Orientation::Forwards>
    std::vector<std::unique_ptr<Node>> GetSuccessors(const Node &node) const;

    //! Returns the distance of the shortest route to the solution or max if none was found
    //!
    //! \return double
    double GetSolutionCost() const;

private:
    //! Returns all successors of the given lane in this pathfinder's search direction.
    //!
    //! \param lane
    //! \return std::vector<Lane*>
    template <typename T, Orientation = Orientation::Forwards>
    const std::vector<Lane *> &GetSuccessorLanes(const Lane &lane) const;

    //! Removes and returns the node with the smallest goal distance estimate.
    //!
    //! \return std::unique_ptr<Node>
    std::unique_ptr<Node> PopBestNode();

    //! Returns whether the given node has already been processed by the pathfinder.
    //!
    //! \param node
    //! \return bool
    bool IsClosedNode(const Node &node) const;

    //! Inserts the given node to the list of open nodes and connects it to the given parent
    //!
    //! \tparam Orientation
    //! \param node
    //! \param parent
    template <Orientation>
    void AddOpenNode(std::unique_ptr<Node> &&node, const Node &parent);

    //! Updates the given node's distances by connecting it to the given parent node
    //!
    //! \tparam Orientation
    //! \param node
    //! \param parent
    template <Orientation>
    void UpdateOpenNode(Node &node, const Node &parent) const;
};
} // namespace detail

std::ostream &operator<<(std::ostream &os, const detail::Pathfinder::Node &node);

std::ostream &operator<<(std::ostream &os, const detail::Pathfinder &pathfinder);
} // namespace osiql

// ----- template definitions

namespace osiql {
namespace detail {
template <typename T, Orientation O>
const std::vector<Lane *> &Pathfinder::GetSuccessorLanes(const Lane &lane) const
{
    if constexpr (std::is_same_v<T, Lane>)
    {
        return lane.GetConnectedLanes<O>();
    }
    else if constexpr (std::is_same_v<T, Road>)
    {
        return lane.GetRoad().GetConnectedLanes<O>(GetSideOfRoad(lane.GetDirection()));
    }
}

template <typename T, Orientation O>
std::vector<std::unique_ptr<Pathfinder::Node>> Pathfinder::GetSuccessors(const Node &node) const
{
    std::vector<std::unique_ptr<Node>> successors;
    if ((node.GetRoad().GetStreetId() == goal.GetRoad().GetStreetId()) && (GetSideOfRoad(node.GetLane()) == GetSideOfRoad(goal.GetLane())))
    {
        const double distance{GetLatitudeDifference(node, goal, goal.GetLane().GetDirection<O>())};
        if (node.distanceFromOrigin + distance >= 0.0)
        {
            goal.parent = &node;
            goal.distanceFromOrigin = node.distanceFromOrigin + distance;
            return successors;
        }
    }
    for (const Lane *successor : GetSuccessorLanes<T, O>(node.GetLane()))
    {
        successors.push_back(std::make_unique<Node>(osiql::RoadPoint(*successor, Coordinates<Road>{GetLatitude<O>(*successor), 0.0})));
    }
    return successors;
}

template <Orientation O>
double Pathfinder::Node::GetDistanceTo(const Node &successor) const
{
    if (successor.GetLane().GetRoad() == GetLane().GetRoad())
    {
        return std::abs(successor.latitude - latitude);
    }
    const double distanceToNext{GetLane().GetDistanceBetween<O>(latitude, GetLatitude<!O>(GetLane()))};
    const double distanceFromPrev{successor.GetLane().GetDistanceBetween<O>(GetLatitude<O>(successor.GetLane()), successor.latitude)};
    return distanceToNext + distanceFromPrev;
}

template <Orientation O>
void Pathfinder::AddOpenNode(std::unique_ptr<Node> &&node, const Node &parent)
{
    node->parent = &parent;
    node->distanceFromOrigin = parent.distanceFromOrigin + parent.GetDistanceTo<O>(*node);
    node->distanceToGoal = (node->xy - goal.xy).SquaredLength();
    node->routeDistanceEstimate = node->distanceFromOrigin + node->distanceToGoal;
    openNodes.emplace_back(std::move(node));
} // namespace util

template <Orientation O>
void Pathfinder::UpdateOpenNode(Node &node, const Node &parent) const
{
    const double distanceFromOrigin{parent.distanceFromOrigin + parent.GetDistanceTo<O>(node)};
    if (distanceFromOrigin < node.distanceFromOrigin)
    {
        node.distanceFromOrigin = distanceFromOrigin;
        node.routeDistanceEstimate = distanceFromOrigin + node.distanceToGoal;
        node.parent = &parent;
    }
}

template <typename T, Orientation O>
void Pathfinder::SearchStep()
{
    ++depth;

    std::unique_ptr<Node> current{PopBestNode()};

    for (std::unique_ptr<Node> &next : GetSuccessors<T, O>(*current))
    {
        if (IsClosedNode(*next))
        {
            continue;
        }

        auto it{std::find_if(openNodes.begin(), openNodes.end(), [&next](const std::unique_ptr<Node> &node) {
            return node->GetLane().GetRoad() == next->GetLane().GetRoad();
        })};
        if (it == openNodes.end())
        {
            AddOpenNode<O>(std::move(next), *current);
        }
        else
        {
            UpdateOpenNode<O>(**it, *current);
        }
    }
    closedNodes.emplace_back(std::move(current));
}

template <typename T, Orientation O>
void Pathfinder::CalculateSolution()
{
    while (!openNodes.empty() && !goal.parent)
    {
        SearchStep<T, O>();
    }
}

template <Orientation O>
Route Pathfinder::GetSolution() const
{
    if (!goal.parent)
    {
        // Either CalculateSolution was not called
        // or no route to the goal was found
        // or the goal is also the starting point
        return goal == origin ? Route(goal) : Route{};
    }
    const Node *node{&goal};
    std::vector<osiql::RoadPoint> result;
    result.reserve(depth);
    result.push_back(*node);
    if (goal.parent->parent && (goal.GetLane().GetRoad() == goal.parent->GetLane().GetRoad()))
    {
        // The last road has two locations now: The start of the road and the destination.
        // To only return one location per road, skip the second to last node:
        node = node->parent;
    }
    else
    {
        return goal == origin ? Route(goal) : Route({origin, goal}, O);
    }
    while ((node = node->parent))
    {
        result.push_back(*node);
    }
    std::reverse(result.begin(), result.end());
    return Route(std::move(result), O);
}
} // namespace detail

} // namespace osiql
