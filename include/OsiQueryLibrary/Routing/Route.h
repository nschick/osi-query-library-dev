/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Trait/Placement.h"
#include "OsiQueryLibrary/Types/Ordering.h"

namespace osiql {
//! \brief A chain of points, one for each traversed road
class Route : public Iterable<std::vector<Waypoint>>
{ // TODO: Introduce route point for which GetLatitude returns distance
public:
    //! \brief Constructs a default route
    Route() = default;

    //! Creates a route of length zero
    //!
    //! \param point
    //! \param orientation
    Route(const RoadPoint &point, Orientation orientation = Orientation::Any);

    //! Creates a route of length zero
    //!
    //! \param point
    //! \param orientation
    Route(const detail::RoadPoint &point, Orientation orientation = Orientation::Any);

    //! Creates a route passing through the given points in the given orientation
    //!
    //! \param points
    //! \param orientation Forwards or Backwards
    Route(std::vector<RoadPoint> &&points, Orientation orientation);

    //! Returns an immutable reference to the set of points of this route
    //!
    //! \return const std::vector<Waypoint>&
    const std::vector<Waypoint> &GetPoints() const;

    //! Returns a mutable reference to the set of points of this route
    //!
    //! \return std::vector<Waypoint>&
    std::vector<Waypoint> &GetPoints();

    using Iterable<std::vector<Waypoint>>::begin;
    typename std::vector<Waypoint>::const_iterator begin() const override;
    typename std::vector<Waypoint>::iterator begin();

    using Iterable<std::vector<Waypoint>>::end;
    typename std::vector<Waypoint>::const_iterator end() const override;
    typename std::vector<Waypoint>::iterator end();

    //! Returns the total longitudinal distance between this route's first and last point along this route.
    //!
    //! \return double
    double GetLength() const;

    //! Returns a local point at the given distance from this route's starting point. The returned point
    //! has the same longitude as the route point on the same road.
    //!
    //! \tparam Orientation Forwards or backwards
    //! \param distance
    template <Orientation = Orientation::Forwards>
    RoadPoint GetRoadPoint(double distance) const;

    //! Returns a local point at the given distance from this route's starting point. The returned point
    //! has the same longitude as the route point on the same road.
    //!
    //! \param distance
    //! \return RoadPoint
    RoadPoint GetRoadPoint(double distance, Orientation) const;

    //! Returns the s-coordinate distance from the start of this route to the given point. If the point does not lie
    //! on any road of this route, returns NaN instead.
    //!
    //! \param point
    //! \return double
    double GetDistance(const RoadPoint &point) const;

    //! Returns the difference in latitude from the polyline of this
    //! path to the given local point.
    //!
    //! \param location
    //! \return double
    double GetDeviation(const RoadPoint &location) const;

    //! Returns an iterator pointing to the road point of this
    //! route on the road closest to the given global point.
    //!
    //! \param globalPoint
    //! \return std::vector<Waypoint>::const_iterator
    std::vector<Waypoint>::const_iterator GetClosestWaypoint(const Vector2d &globalPoint) const;

    //! Returns an iterator to the closest waypoint of this route
    //! to the given distance from the start of this route.
    //!
    //! \param distance
    //! \return std::vector<Waypoint>::const_iterator
    std::vector<Waypoint>::const_iterator GetClosestWaypoint(double distance) const;

    //! Returns a local point of the closest road on this route representing the given global point.
    //!
    //! \param globalPoint
    //! \return RoadPoint
    RoadPoint GetRoadPoint(const Vector2d &globalPoint) const;

    //! Returns a local point and angle of the closest road on this route representing the given global point.
    //!
    //! \param globalPoint
    //! \param globalYaw
    //! \return RoadPose
    RoadPose GetRoadPose(const Vector2d &globalPoint, double globalYaw) const;

    //! \brief Information on the first entry point of a lane of a route
    struct LaneEntry
    {
        //! Constructs lane entry information from its components
        //!
        //! \param lane
        //! \param s
        //! \param distance
        LaneEntry(const Lane &lane, double latitude, double distance);

        //! Returns the lane being entered
        //!
        //! \return const Lane&
        const Lane &GetLane() const;

        double latitude; //!< \brief Road s-coordinate of the entry point of the lane
        double distance; //!< \brief Distance from start of route
    private:
        std::reference_wrapper<const Lane> lane; //!< lane being entered
    };

    //! Returns all lanes traversed in order along this route.
    //!
    //! \return const std::vector<LaneEntry>&
    const std::vector<LaneEntry> &GetLaneEntries() const;

    //! Returns an iterator pointing to the point of this route that lies on the road containing the given distance.
    //! If that distance is at the end of one road and the start of another, an iterator to the latter is returned.
    //! If the distance lies outside the range of the route, an iterator to the end of the route is returned.
    //!
    //! \param distance
    //! \return std::vector<Waypoint>::const_iterator
    std::vector<Waypoint>::const_iterator GetRoutePoint(double distance) const;

    //! Returns all objects of the specified type assigned to any lane traversed on this route sorted by distance
    //! along this route. In the case of TrafficSigns and RoadMarkings the last entity prior to the route is returned
    //! as well to allow for reading traffic rules along the entire route.
    //!
    //! \tparam T
    //! \return std::vector<std::pair<const T *, double>>
    template <typename T>
    std::vector<std::pair<const T *, double>> FindAll() const;

    //! Returns all objects of the specified type along this route that satisfies the given predicate
    //!
    //! \tparam T
    //! \tparam Pred
    //! \return std::vector<std::pair<const T *, double>>
    template <typename T, typename Pred>
    std::vector<std::pair<const T *, double>> FindAll(Pred &&) const;

    //! Returns the curvature of the reference line at the given distance from this
    //! route's origin. Return NaN if the distance is outside of the route's range.
    //!
    //! \param distance
    //! \return double
    double GetCurvature(double distance) const;

    std::vector<Waypoint>::const_iterator FindWaypoint(const Lane &) const;

    //! Returns the chain of successor and predecessor lanes from the given lane along this route.
    //! Returns an empty vector if the given lane does not lie on the route.
    //!
    //! \return std::vector<const Lane*>
    std::vector<const Lane *> GetLaneChain(const Lane &) const;

    template <typename Pred>
    std::vector<Waypoint>::const_iterator TraverseUntil(Pred &&) const;

    template <typename Pred>
    std::vector<Waypoint>::const_iterator TraverseWhile(Pred &&) const;

    template <typename Pred>
    Interval<std::vector<Waypoint>::const_iterator> GetMatchingRange(Pred &&) const;

    //! Returns the given point projected along its lane chain of this route by the given distance.
    //!
    //! \param distance
    //! \return RoadPoint Point with the same absolute lateral distance from the (possibly endlessly extended)
    //! centerline of the closest lane of the input point's lane chain
    RoadPoint ProjectPoint(const RoadPoint &, double distance) const;

    //! \brief The direction of this route relative to the driving direction of the lanes it passes over.
    Orientation orientation{Orientation::Any};

private:
    mutable std::vector<Waypoint> points;
};

std::vector<Waypoint>::const_iterator GetClosestWaypoint(std::vector<Waypoint>::const_iterator first,
                                                         std::vector<Waypoint>::const_iterator pastLast,
                                                         const Vector2d &globalPoint);

std::vector<Waypoint>::const_iterator GetClosestWaypoint(std::vector<Waypoint>::const_iterator first,
                                                         std::vector<Waypoint>::const_iterator pastLast,
                                                         double distance);

template <typename WaypointIt>
double GetDistance(WaypointIt first,
                   WaypointIt pastLast,
                   const RoadPoint &point,
                   Orientation o);

template <typename WaypointIt>
RoadPoint GetRoadPoint(WaypointIt first,
                       WaypointIt pastLast,
                       const Vector2d &globalPoint);

template <typename WaypointIt>
RoadPose GetRoadPose(WaypointIt first,
                     WaypointIt pastLast,
                     const Vector2d &globalPoint,
                     double globalYaw);

std::ostream &operator<<(std::ostream &os, const Route &route);
} // namespace osiql

namespace osiql {
template <Orientation O>
RoadPoint Route::GetRoadPoint(double targetDistance) const
{
    constexpr const Direction D{O == Orientation::Backwards ? Direction::Upstream : Direction::Downstream};
    const auto it{end<D>(targetDistance)};
    if (it == osiql::end<O>(points))
    {
        return osiql::back<O>(points);
    }
    if (it == osiql::begin<O>(points))
    {
        return osiql::front<O>(points);
    }
    const RoadPoint &point{*it};
    if (point.GetLane().GetDirection(orientation) == Direction::Downstream)
    {
        return {point.GetLane(), Coordinates<Road>{point.latitude + (targetDistance - it->distance), point.longitude}};
    }
    return {point.GetLane(), Coordinates<Road>{point.latitude - (targetDistance - it->distance), point.longitude}};
}

template <typename T>
std::vector<std::pair<const T *, double>> Route::FindAll() const
{
    std::set<raw_id_t<T>> pool;
    std::vector<std::pair<const T *, double>> result;
    for (const Waypoint &point : points)
    {
        const std::vector<Road::Relation<T>> &relations{point.GetRoad().GetAll<T>(point.GetLane().GetSideOfRoad())};
        for (const Road::Relation<T> &relation : relations)
        {
            if (pool.insert(relation.GetEntity().GetId()).second)
            {
                const double distance{point.GetLane().GetDistanceBetween(point, relation.GetPlacement(), orientation)};
                result.emplace_back(&relation.GetEntity(), point.distance + distance);
            }
        }
    }
    std::sort(result.begin(), result.end(), [](const auto &lhs, const auto &rhs) { return lhs.second < rhs.second; });
    return result;
}

template <typename T, typename Pred>
std::vector<std::pair<const T *, double>> Route::FindAll(Pred &&predicate) const
{
    std::set<raw_id_t<T>> pool;
    std::vector<std::pair<const T *, double>> result;
    for (const Waypoint &point : points)
    {
        const std::vector<Road::Relation<T>> &relations{point.GetRoad().GetAll<T>(point.GetLane().GetSideOfRoad())};
        for (const Road::Relation<T> &relation : relations)
        {
            if (pool.insert(relation.GetEntity().GetId()).second)
            {
                const double distance{point.distance + point.GetLane().GetDistanceBetween(point, relation.GetPlacement(), orientation)};
                if (predicate(relation.GetEntity(), distance))
                {
                    result.emplace_back(&relation.GetEntity(), distance);
                }
            }
        }
    }
    std::sort(result.begin(), result.end(), [](const auto &lhs, const auto &rhs) { return lhs.second < rhs.second; });
    return result;
}

template <typename WaypointIt>
double GetDistance(WaypointIt first,
                   WaypointIt pastLast,
                   const RoadPoint &point,
                   Orientation o)
{
    const auto it{std::find_if(first, pastLast, [&point](const RoadPoint &node) {
        return node.GetRoad() == point.GetRoad();
    })};
    if (it == pastLast)
    {
        return std::numeric_limits<double>::quiet_NaN();
    }
    return it->GetDistance() + it->GetLane().GetDistanceBetween(it->latitude, point.latitude, o);
}

template <typename WaypointIt>
RoadPoint GetRoadPoint(WaypointIt first,
                       WaypointIt pastLast,
                       const Vector2d &globalPoint)
{
    const Waypoint &waypoint{*GetClosestWaypoint(first, pastLast, globalPoint)};
    return waypoint.GetRoad().GetPoint(globalPoint, waypoint.GetLane().GetSideOfRoad());
}

template <typename WaypointIt>
RoadPose GetRoadPose(WaypointIt first,
                     WaypointIt pastLast,
                     const Vector2d &globalPoint,
                     double globalYaw)
{
    const Waypoint &waypoint{*GetClosestWaypoint(first, pastLast, globalPoint)};
    return waypoint.GetRoad().GetPose(globalPoint, globalYaw, waypoint.GetLane().GetSideOfRoad());
}

template <typename Pred>
std::vector<Waypoint>::const_iterator Route::TraverseUntil(Pred &&pred) const
{
    return std::find_if(begin(), end(), std::forward<Pred>(pred));
}

template <typename It, typename Pred>
It TraverseWhile(It current, It pastLast, Pred &&pred)
{
    return current == pastLast ? current : std::prev(std::find_if(std::next(current), pastLast, [&pred](const auto &_) { return !pred(_); }));
}
template <typename Pred>
std::vector<Waypoint>::const_iterator Route::TraverseWhile(Pred &&pred) const
{
    return osiql::TraverseWhile(TraverseUntil(std::forward<Pred>(pred)), end(), std::forward<Pred>(pred));
}

template <typename Pred>
Interval<std::vector<Waypoint>::const_iterator> Route::GetMatchingRange(Pred &&pred) const
{
    const auto it{TraverseUntil(std::forward<Pred>(pred))};
    return {it, osiql::TraverseWhile(it, end(), std::forward<Pred>(pred))};
}
} // namespace osiql
