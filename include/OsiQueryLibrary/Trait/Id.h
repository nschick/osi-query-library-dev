/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait for the type of an object's id based on the type of that object

#include <google/protobuf/port.h> // uint64
#include <limits>
#include <string>

namespace osiql {
using Id = google::protobuf::uint64;
static inline const Id UNDEFINED_ID{std::numeric_limits<Id>::max()};

//! Identifier type of the given template type
//!
//! \tparam T
template <typename T>
struct Identifier
{
    using raw_type = Id;
    using type = Id;
};

class Road;
using RoadId = std::string;
static inline const RoadId UNDEFINED_ROAD_ID{"UNDEFINED_ROAD_ID"};

//! Identifier type of road
//!
//! \tparam Road
template <>
struct Identifier<Road>
{
    using raw_type = RoadId;
    using type = const RoadId &;
};

template <typename T>
using id_t = typename Identifier<T>::type;
template <typename T>
using raw_id_t = typename Identifier<T>::raw_type;
} // namespace osiql
