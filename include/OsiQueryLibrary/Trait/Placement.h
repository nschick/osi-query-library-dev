/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait for how an object's position on a lane is stored as part of that lane based on that object's type

namespace osiql {
template <typename T>
struct Assignment;
template <typename T>
struct LocalBounds;
struct MovingObject;

namespace trait {
//! Type trait for lane-relative object placements
//!
//! \tparam T Type of the object
template <typename T>
struct Placement
{
    //! \brief lane-relative object placement
    using type = Assignment<std::remove_const_t<T>>;
};

//! Type trait for lane-relative object placements of moving objects
//!
//! \tparam MovingObject
template <>
struct Placement<MovingObject>
{
    //! \brief lane-relative placement of moving objects
    using type = LocalBounds<MovingObject>;
};
template <>
struct Placement<const MovingObject>
{
    //! \brief lane-relative placement of moving objects
    using type = LocalBounds<const MovingObject>;
};
} // namespace trait

template <typename T>
using Placement = typename trait::Placement<T>::type;
} // namespace osiql
