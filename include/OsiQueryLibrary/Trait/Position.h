/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait for how an object's position on a lane is stored as part of that object based on that object's type

#include <osi3/osi_object.pb.h>

namespace osiql {
template <typename T>
struct Assignment;
class Lane;
template <typename T>
struct Locatable;
template <typename Point>
struct Pose;
class Road;
namespace detail {
struct RoadPoint;
}

namespace trait {
//! The type in which an object stores its positions.
//! For static objects this is an Assignment<Lane>, which is a wrapper of an osi3::LogicalLaneAssignment.
//!
//! \tparam T
template <typename T>
struct Position
{
    //! \brief How an object stores its positions
    using type = Assignment<Lane>;
};

//! The type in which a moving object stores its positions.
//!
//! \tparam osi3::MovingObject
template <>
struct Position<Locatable<osi3::MovingObject>>
{
    //! \brief How a MovingObject stores its positions
    using type = Pose<detail::RoadPoint>;
};
} // namespace trait

//! Pose or Assignment<Lane>
//!
//! \tparam T Wrapper of an OSI object
template <typename T>
using Position = typename trait::Position<T>::type;

template <typename T>
struct Locatable;
} // namespace osiql
