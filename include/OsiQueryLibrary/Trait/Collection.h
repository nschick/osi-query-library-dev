/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Type trait to specify different containers for different OSI objects

#include <google/protobuf/repeated_ptr_field.h>
#include <map>
#include <set>
#include <vector>

#include "OsiQueryLibrary/Trait/Common.h" // always_false<T>
#include "OsiQueryLibrary/Trait/Id.h"
#include "OsiQueryLibrary/Types/Container.h"

namespace osiql {
struct LightBulb;
template <typename T>
struct LocalBounds;
struct MovingObject;
class Road;
struct TrafficLight;

namespace trait {
//! Type trait determining how objects are stored in osiql::World
//!
//! \tparam T
template <typename T>
struct Collection
{
    //! \brief How objects are stored in osiql::World
    using type = std::vector<T>;
};

//! Type trait specifying how objects are stored in osiql::World
//!
//! \tparam
template <>
struct Collection<MovingObject>
{
    //! \brief How moving objects are stored in osiql::World
    using type = Set<std::shared_ptr<MovingObject>>;
};

//! Type trait specifying how moving objects are stored in a lane
//!
//! \tparam
template <>
struct Collection<LocalBounds<MovingObject>>
{
    //! \brief How moving objects are stored in osiql::World
    using type = std::unordered_set<LocalBounds<const MovingObject>, Hash<Id>, Equal>;
};

//! Type trait specifying how roads are stored in osiql::World
//!
//! \tparam Road
// template <>
// struct Collection<Road>
// {
//     //! \brief How roads are stored in osiql::World
//     using type = Set<Road>;
// };

//! Type trait specifying how light bulbs are stored in osiql::World
//!
//! \tparam Road
template <>
struct Collection<LightBulb>
{
    //! \brief How light bulbs are stored in osiql::World
    using type = std::map<Id, TrafficLight *>;
};
} // namespace trait

//! How objects are stored in osiql::World
//!
//! \tparam T
template <typename T>
using Collection = typename trait::Collection<T>::type;

//! Creates and returns a collection of wrapper objects from the given container of OSI objects
//!
//! \tparam T
//! \param container
//! \return Collection<T>::type
template <typename T>
Collection<T> Collect(const Container<typename T::Handle> &container);

//! Returns a pointer to the item with the given id in the collection or nullptr if there is none.
//!
//! \tparam T
//! \param iterator
//! \return const T*
template <typename T>
const T *Get(const Collection<T> &collection, id_t<T> id);

//! Emplaces an object into the given collection
//!
//! \tparam T
//! \tparam Args
//! \param collection
//! \param args
template <typename T, typename... Args>
T &Emplace(Collection<T> &collection, Args &&...args);

//! Returns the element an iterator points to
//!
//! \tparam T
//! \param iterator
//! \return const T&
template <typename T>
T &Unwrap(typename Collection<T>::iterator iterator);

//! Returns the element an iterator points to
//!
//! \tparam T
//! \param iterator
//! \return const T&
template <typename T>
const T &Unpack(const typename Collection<T>::const_iterator iterator);

//! Searches for an object within the collection and returns it if found, otherwise returns nullptr.
//!
//! \tparam T
//! \param collection
//! \param id
//! \return const T*
template <typename T>
typename Collection<T>::const_iterator Search(const Collection<T> &collection, id_t<T> id);

//! Searches for an object within the collection and returns it if found, otherwise returns nullptr.
//!
//! \tparam T
//! \param collection
//! \param id
//! \return Collection<T>::iterator
template <typename T>
typename Collection<T>::iterator Search(Collection<T> &collection, id_t<T> id);

//! Finds and returns an object in the collection by its id.
//!
//! \tparam T
//! \param collection
//! \param id
//! \return T&
template <typename T>
T &Find(Collection<T> &collection, id_t<T> id);

//! Finds and returns an object in the collection by its id.
//!
//! \tparam T
//! \param collection
//! \param id
//! \return const T&
template <typename T>
const T &Find(const Collection<T> &collection, id_t<T> id);
} // namespace osiql

namespace osiql {
template <typename T>
Collection<T> From(const Container<typename T::Handle> &container)
{
    if constexpr (std::is_same_v<Collection<T>, Set<T>> || std::is_same_v<Collection<T>, Set<std::shared_ptr<T>>>)
    {
        Set<T> result;
        for (const typename T::Handle &entity : container)
        {
            result.emplace(entity);
        }
        return result;
    }
    else if constexpr (std::is_same_v<Collection<T>, std::vector<T>>)
    {
        std::vector<T> result;
        result.reserve(static_cast<size_t>(container.size()));
        for (const typename T::Handle &entity : container)
        {
            result.emplace_back(entity);
        }
        std::sort(result.begin(), result.end());
        return result;
    }
    else
    {
        static_assert(always_false<T>, "The specialized collection can not be constructed directly from an OSI object container");
    }
}

template <typename T, typename... Args>
T &Emplace(Collection<T> &collection, Args &&...args)
{
    if constexpr (std::is_same_v<Collection<T>, std::vector<T>>)
    {
        return collection.emplace_back(std::forward<Args>(args)...);
    }
    else
    {
        return Unwrap<T>(collection.emplace(std::forward<Args>(args)...).first);
    }
}

template <typename T>
T &Unwrap(typename Collection<T>::iterator iterator)
{
    return const_cast<T &>(Unpack<T>(typename Collection<T>::const_iterator(iterator)));
}

template <typename T>
const T &Unpack(typename Collection<T>::const_iterator iterator)
{
    if constexpr (std::is_same_v<Collection<T>, std::vector<T>> || std::is_same_v<Collection<T>, Set<T>>)
    {
        return *iterator;
    }
    else if constexpr (std::is_same_v<Collection<T>, Set<std::shared_ptr<T>>>)
    {
        return **iterator;
    }
    else if constexpr (std::is_same_v<Collection<T>, std::map<Id, T *>>)
    {
        return *iterator->second;
    }
}

template <typename T>
const T *Get(const Collection<T> &collection, id_t<T> id)
{
    const auto it{osiql::Search<T>(collection, id)};
    return (it == collection.end()) ? nullptr : &Unpack<T>(it);
}

template <typename T>
typename Collection<T>::const_iterator Search(const Collection<T> &collection, id_t<T> id)
{
    if constexpr (std::is_same_v<Collection<T>, std::vector<T>>)
    {
        auto it{std::lower_bound(collection.cbegin(), collection.cend(), id)};
        return (it == collection.cend() || it->GetId() != id) ? collection.cend() : it;
    }
    else
    {
        return collection.find(id);
    }
}

template <typename T>
typename Collection<T>::iterator Search(Collection<T> &collection, id_t<T> id)
{
    if constexpr (std::is_same_v<Collection<T>, std::vector<T>>)
    {
        auto it{std::lower_bound(collection.begin(), collection.end(), id)};
        return (it == collection.end() || it->GetId() != id) ? collection.end() : it;
    }
    else
    {
        return collection.find(id);
    }
}

template <typename T>
T &Find(Collection<T> &collection, id_t<T> id)
{
    return const_cast<T &>(Find<T>(std::as_const(collection), id));
}

template <typename T>
const T &Find(const Collection<T> &collection, id_t<T> id)
{
    if constexpr (std::is_same_v<Collection<T>, std::vector<T>>)
    {
        return Unpack<T>(std::lower_bound(collection.begin(), collection.end(), id));
    }
    else
    {
        return Unpack<T>(collection.find(id));
    }
}
} // namespace osiql
