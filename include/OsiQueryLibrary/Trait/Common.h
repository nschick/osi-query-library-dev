/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Common type traits and macros

#include <cstddef>
#include <experimental/type_traits>
#include <type_traits>
#include <utility>

namespace osiql {
template <class... T>
constexpr bool always_false = false;

template <auto Start, auto End, class F>
constexpr void constexpr_for(F &&f)
{
    if constexpr (Start < End)
    {
        f(std::integral_constant<decltype(Start), Start>());
        constexpr_for<Start + 1, End>(f);
    }
}

template <typename T, typename F>
constexpr auto has_member(F &&f) -> decltype(f(std::declval<T>()), true)
{
    return true;
}

template <typename>
constexpr bool has_member(...)
{
    return false;
}

#define HAS_MEMBER(T, EXPR) has_member<T>([](auto &&obj) -> decltype(obj.EXPR) {})

namespace detail {
template <typename T, size_t... Is>
struct Swizzle;
template <typename T>
struct Unswizzle
{
    using type = std::decay_t<T>;
};
template <typename T, size_t... Is>
struct Unswizzle<Swizzle<T, Is...>>
{
    using type = T;
};
} // namespace detail

template <typename T>
using unswizzle_t = typename detail::Unswizzle<T>::type;

namespace {
template <typename T, T N>
constexpr T max_helper(std::integer_sequence<T, N>)
{
    return N;
}

template <typename T, T N1, T N2, T... Rest>
constexpr T max_helper(std::integer_sequence<T, N1, N2, Rest...>)
{
    if constexpr (N1 > N2)
    {
        return max_helper(std::integer_sequence<T, N1, Rest...>{});
    }
    else
    {
        return max_helper(std::integer_sequence<T, N2, Rest...>{});
    }
}
} // namespace

template <typename T, T... Ints>
constexpr T max(std::integer_sequence<T, Ints...>)
{
    return max_helper(std::integer_sequence<T, Ints...>{});
}

namespace {
template <size_t Skip, size_t N, size_t I, size_t... Is>
constexpr auto skip_sequence_helper(std::index_sequence<I, Is...>)
{
    if constexpr (N == 0 && Skip == I)
    {
        return std::index_sequence<Is...>{};
    }
    else if constexpr (N == 0)
    {
        return std::index_sequence<Is..., I>{};
    }
    else if constexpr (Skip == I)
    {
        return skip_sequence_helper<Skip, N - 1>(std::index_sequence<Is...>{});
    }
    else
    {
        return skip_sequence_helper<Skip, N - 1>(std::index_sequence<Is..., I>{});
    }
}
} // namespace

template <size_t Skip, size_t To>
constexpr auto make_skip_sequence()
{
    return skip_sequence_helper<Skip, To - 1>(std::make_index_sequence<To>());
}

template <typename T, size_t N>
struct Vector;
struct Vector2d;
struct Vector3d;
namespace detail {
template <typename T>
struct is_vector : std::false_type
{
};

template <typename T>
struct is_vector<osiql::Vector<T, 0>> : std::false_type
{
};
template <typename T>
struct is_vector<osiql::Vector<T, 1>> : std::false_type
{
};
template <typename T, size_t N>
struct is_vector<osiql::Vector<T, N>> : std::true_type
{
};
template <>
struct is_vector<osiql::Vector2d> : std::true_type
{
};

template <>
struct is_vector<osiql::Vector3d> : std::true_type
{
};
} // namespace detail
} // namespace osiql
