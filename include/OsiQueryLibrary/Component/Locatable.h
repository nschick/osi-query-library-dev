/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base class for objects that have a shape and global position

#include <algorithm>
#include <limits>
#include <osi3/osi_common.pb.h>
#include <osi3/osi_object.pb.h>
#include <osi3/osi_trafficsign.pb.h>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Point/LanePoint.h"
#include "OsiQueryLibrary/Point/RoadPoint.h"
#include "OsiQueryLibrary/Point/Vector2d.h"
#include "OsiQueryLibrary/Point/Vector3d.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Trait/Base.h"
#include "OsiQueryLibrary/Types/Bounds.h"
#include "OsiQueryLibrary/Types/Matrix.h"

namespace osiql {
//! A Locatable is a wrapper of an OSI object that possesses a base, which allows it to returns its
//! shape, size and global position and rotation. Additionally, a Locatable stores its global shape.
//!
//! \tparam Handle
template <typename Handle>
struct Locatable : Identifiable<Handle>
{
    using Identifiable<Handle>::Identifiable;

    //! Returns the global x-coordinate of this object's bounding box center or NaN if not assigned
    //!
    //! \return double
    double x() const;

    //! Returns the global y-coordinate of this object's bounding box center or NaN if not assigned
    //!
    //! \return double
    double y() const;

    //! Returns the global z-coordinate of this object's bounding box center or NaN if not assigned
    //!
    //! \return double
    double z() const;

    //! Returns the global xy-coordinates of this object's bounding box center
    //!
    //! \return Vector2d
    Vector2d GetXY() const;

    //! Returns the global xyz-coordinates of this object's bounding box center
    //!
    //! \return Vector3d
    Vector3d GetXYZ() const;

    //! Returns the global xy-coordinates of the point at the given local point.
    //!
    //! \param localPoint Local object-relative coordinates. In OSI, x represents
    //! forwards in the direction the object is facing and y represents leftwards
    //! \return Vector2d
    Vector2d GetXY(const Vector2d &localPoint) const;

    //! Returns the global xyz-coordinates of the point at the given local point.
    //!
    //! \param localPoint Local object-relative coordinates. In OSI, x represents
    //! forwards in the direction the object is facing, y represents leftwards and z upwards
    //! \return Vector3d
    Vector3d GetXYZ(const Vector3d &localPoint) const;

    //! Returns the width of this object's axis-aligned bounding box prior to any transformation.
    //!
    //! \return double
    double GetWidth() const;

    //! Returns the length of this object's axis-aligned bounding box prior to any transformation.
    //!
    //! \return double
    double GetLength() const;

    //! Returns the height of this object's axis-aligned bounding box prior to any transformation
    //! or NaN if not assigned.
    //!
    //! \return double
    double GetHeight() const;

    //! Returns a vector whose x-coordinate represents this object's assigned length
    //! and whose y-coordinate represents this object's assigned width. The returned
    //! coordinates are NaN if not assigned.
    //!
    //! \return Vector2d
    Vector2d GetSize() const;

    //! Returns the global pitch of this object, which is its up-/downward tilt, like on an incline.
    //! In OSI, this is the rotation around the y-axis.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetPitch() const;

    //! Returns the global roll of this object, which is its sideway tilt.
    //! In OSI, this is the rotation around the x-axis.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetRoll() const;

    //! Returns the global yaw of this object, which is its rotation on a ground surface.
    //! In OSI, this is the rotation around the z-axis.
    //!
    //! \return double counter-clockwise ascending angle in radians within [-π, π]
    double GetYaw() const;

    //! Returns the rotation of this object. Each component returns the the change
    //! in angle around that component's corresponding axis.
    //!
    //! \return Vector3d
    Vector3d GetRotation() const;

    //! Returns this object's global shape localized to the st-coordinate system defined by the
    //! reference line of the road of the given lane
    //!
    //! \param lane
    //! \return std::vector<RoadPoint>
    std::vector<RoadPoint> GetLocalShape(const Lane &lane) const;

    //! Returns a chain of points describing the global contour of this object. The contour
    //! before any transformation are applied can be retrieved via GetBase().base_polygon()
    //!
    //! \return const Polygon&
    const Polygon &GetShape() const;

    //! Returns the st-coordinate bounds of this object's shape in the given lane or road's coordinate system
    //!
    //! \tparam T (const) Lane or (const) Road
    //! \param lane
    //! \return LocalBounds<T>
    template <typename T>
    LocalBounds<T> GetLocalBounds(T &) const;

    //! Returns the st-coordinate bounds of this object's shape overlapping the given lane in its coordinate system
    //!
    //! \tparam T Lane or const Lane
    //! \param lane
    //! \return LocalBounds<T>
    template <typename T>
    LocalBounds<T> GetLocalIntersectionBounds(T &lane) const;

    //! Returns this object's underlying OSI base
    //!
    //! \return const base_t<Handle>& Either osi3::BaseMoving or osi3::BaseStationary
    const base_t<Handle> &GetBase() const;

    //! Returns a transformation matrix representing the rotation of this object.
    //!
    //! \return const Matrix<3>&
    const Matrix<3> &GetRotationMatrix() const;

    //! Returns a transformation matrix representing the translation of this object.
    //!
    //! \return const Matrix<4>&
    const Matrix<4> &GetTranslationMatrix() const;

    //! Returns the transformation matrix of this object
    //!
    //! \return const Matrix<4>&
    const Matrix<4> &GetTransformationMatrix() const;

    //! Returns the RoadPoint relative to the given lane of the corner of this
    //! object furthest along said lane in the given orientation.
    //!
    //! \tparam Orientation Forwards or Backwards
    //! \param lane
    //! \return RoadPoint
    template <Orientation = Orientation::Forwards>
    RoadPoint GetFurthestRoadPoint(const Lane &) const;

    //! Returns the RoadPoint relative to the given lane of the corner of this
    //! object furthest along said lane in the given orientation.
    //!
    //! \param lane
    //! \param o Forwards or Backwards
    //! \return RoadPoint
    RoadPoint GetFurthestRoadPoint(const Lane &lane, Orientation o) const;

protected:
    void OutdateTranslation();

    void OutdateRotation();

private:
    mutable std::unique_ptr<Matrix<3>> rotationMatrix{};
    mutable std::unique_ptr<Matrix<4>> translationMatrix{};
    mutable std::unique_ptr<Matrix<4>> transformationMatrix{};
    //! \brief A chain of points describing the global contour of this object. The contour
    //! before any transformation are applied can be retrieved via GetBase().base_polygon()
    mutable Polygon shape;
};
} // namespace osiql

// ---------- Template definitions ----------

namespace osiql {
template <typename Handle>
double Locatable<Handle>::x() const
{
    return (GetBase().has_position() && GetBase().position().has_x()) ? GetBase().position().x() : std::numeric_limits<double>::quiet_NaN();
}

template <typename Handle>
double Locatable<Handle>::y() const
{
    return (GetBase().has_position() && GetBase().position().has_y()) ? GetBase().position().y() : std::numeric_limits<double>::quiet_NaN();
}

template <typename Handle>
double Locatable<Handle>::z() const
{
    return (GetBase().has_position() && GetBase().position().has_z()) ? GetBase().position().z() : std::numeric_limits<double>::quiet_NaN();
}

template <typename Handle>
Vector2d Locatable<Handle>::GetXY() const
{
    return {x(), y()};
}

template <typename Handle>
Vector3d Locatable<Handle>::GetXYZ() const
{
    return {x(), y(), z()};
}

template <typename Handle>
Vector2d Locatable<Handle>::GetXY(const Vector2d &localPoint) const
{
    return GetTransformationMatrix() * localPoint;
}

template <typename Handle>
Vector3d Locatable<Handle>::GetXYZ(const Vector3d &localPoint) const
{
    return GetTransformationMatrix() * localPoint;
}

template <typename Handle>
double Locatable<Handle>::GetWidth() const
{
    if (GetBase().has_dimension() && GetBase().dimension().has_width())
    {
        return GetBase().dimension().width();
    }
    const auto [min, max]{std::minmax_element(GetBase().base_polygon().begin(), GetBase().base_polygon().end(), [](const osi3::Vector2d &lhs, const osi3::Vector2d &rhs) {
        return lhs.y() < rhs.y();
    })};
    return max->y() - min->y();
}

template <typename Handle>
double Locatable<Handle>::GetLength() const
{
    if (GetBase().has_dimension() && GetBase().dimension().has_length())
    {
        return GetBase().dimension().length();
    }
    const auto [min, max]{std::minmax_element(GetBase().base_polygon().begin(), GetBase().base_polygon().end(), [](const osi3::Vector2d &lhs, const osi3::Vector2d &rhs) {
        return lhs.x() < rhs.x();
    })};
    return max->x() - min->x();
}

template <typename Handle>
double Locatable<Handle>::GetHeight() const
{
    return (GetBase().has_dimension() && GetBase().dimension().has_height()) ? GetBase().dimension().height() : std::numeric_limits<double>::quiet_NaN();
}

template <typename Handle>
Vector2d Locatable<Handle>::GetSize() const
{
    return {GetLength(), GetWidth()};
}

template <typename Handle>
Vector3d Locatable<Handle>::GetRotation() const
{
    return {GetRoll(), GetPitch(), GetYaw()};
}

template <typename Handle>
double Locatable<Handle>::GetPitch() const
{
    return (GetBase().has_orientation() && GetBase().orientation().has_pitch()) ? GetBase().orientation().pitch() : 0.0;
}

template <typename Handle>
double Locatable<Handle>::GetRoll() const
{
    return (GetBase().has_orientation() && GetBase().orientation().has_roll()) ? GetBase().orientation().roll() : 0.0;
}

template <typename Handle>
double Locatable<Handle>::GetYaw() const
{
    return (GetBase().has_orientation() && GetBase().orientation().has_yaw()) ? GetBase().orientation().yaw() : 0.0;
}

template <typename Handle>
const Polygon &Locatable<Handle>::GetShape() const
{
    if (shape.empty())
    {
        const Matrix<4> &transform{GetTransformationMatrix()};
        if (!GetBase().base_polygon().empty())
        {
            shape.reserve(static_cast<size_t>(GetBase().base_polygon().size()));
            for (const auto &point : GetBase().base_polygon())
            {
                shape.emplace_back(transform * Vector3d{point});
            }
        }
        else // No base polygon provided: Use width, length and center to create a rectangular shape
        {
            const double halfLength{GetLength() * 0.5};
            const double halfWidth{GetWidth() * 0.5};
            // Counter-clockwise winding so that external distances to it are positive:
            shape.emplace_back(transform * Vector3d{-halfLength, -halfWidth, 0.0}); // Rear right
            shape.emplace_back(transform * Vector3d{halfLength, -halfWidth, 0.0});  // Front right
            shape.emplace_back(transform * Vector3d{halfLength, halfWidth, 0.0});   // Front left
            shape.emplace_back(transform * Vector3d{-halfLength, halfWidth, 0.0});  // Rear left
        }
    }
    return shape;
}

template <typename Handle>
std::vector<RoadPoint> Locatable<Handle>::GetLocalShape(const Lane &lane) const
{
    std::vector<RoadPoint> localShape;
    localShape.reserve(GetShape().size());
    std::transform(shape.begin(), shape.end(), std::back_inserter(localShape), [&](const Vector2d &point) {
        return lane.GetPoint(point);
    });
    return localShape;
}

template <typename Handle>
template <typename T>
LocalBounds<T> Locatable<Handle>::GetLocalBounds(T &street) const
{
    if constexpr (std::is_same_v<std::remove_const_t<T>, Lane> || std::is_same_v<std::remove_const_t<T>, Road>)
    {
        LocalBounds<T> result(street);
        for (const Vector2d &point : GetShape())
        {
            result.Add(street.GetPoint(point));
        }
        return result;
    }
    else
    {
        static_assert(always_false<T>, "GetLocalBounds must be passed a Lane or Road");
    }
}

namespace detail {
template <typename T>
void ConstrainedAdd(LocalBounds<T> &bounds, const Lane &lane, const Vector2d &point)
{
    const double minLatitude{lane.GetHandle().start_s()};
    const double maxLatitude{lane.GetHandle().end_s()};

    const osiql::RoadPoint roadPoint{lane.GetPoint(point)};
    if (roadPoint.latitude <= minLatitude)
    {
        const double minLongitude{lane.GetBoundaryPoints<Side::Right, Direction::Downstream>().front().longitude};
        const double maxLongitude{lane.GetBoundaryPoints<Side::Left, Direction::Downstream>().front().longitude};
        if (roadPoint.longitude <= minLongitude)
        {
            bounds.Add({minLatitude, minLongitude});
        }
        else if (roadPoint.longitude >= maxLongitude)
        {
            bounds.Add({minLatitude, maxLongitude});
        }
        else
        {
            bounds.Add({minLatitude, roadPoint.longitude});
        }
    }
    else if (roadPoint.latitude >= maxLatitude)
    {
        const double minLongitude{lane.GetBoundaryPoints<Side::Right, Direction::Downstream>().back().longitude};
        const double maxLongitude{lane.GetBoundaryPoints<Side::Left, Direction::Downstream>().back().longitude};
        if (roadPoint.longitude <= minLongitude)
        {
            bounds.Add({maxLatitude, minLongitude});
        }
        else if (roadPoint.longitude >= maxLongitude)
        {
            bounds.Add({maxLatitude, maxLongitude});
        }
        else
        {
            bounds.Add({maxLatitude, roadPoint.longitude});
        }
    }
}
} // namespace detail

template <typename Handle>
template <typename T>
LocalBounds<T> Locatable<Handle>::GetLocalIntersectionBounds(T &lane) const
{
    // TODO: Support T = Road
    if constexpr (std::is_same_v<std::remove_const_t<T>, Lane>)
    {
        LocalBounds<T> result(lane);
        for (const Vector2d &point : GetShape())
        {
            detail::ConstrainedAdd(result, lane, point);
        }
        return result;
    }
    else
    {
        static_assert(always_false<T>, "Template type must be Lane or const Lane");
    }
}

template <typename Handle>
const typename Base<Handle>::type &Locatable<Handle>::GetBase() const
{
    if constexpr (std::is_same_v<Handle, osi3::TrafficSign>)
    {
        return this->GetHandle().main_sign().base();
    }
    else
    {
        return this->GetHandle().base();
    }
}

template <typename Handle>
const Matrix<3> &Locatable<Handle>::GetRotationMatrix() const
{
    if (!rotationMatrix)
    {
        rotationMatrix = std::make_unique<Matrix<3>>(RotationMatrix(GetRotation()));
    }
    return *rotationMatrix;
}

template <typename Handle>
const Matrix<4> &Locatable<Handle>::GetTranslationMatrix() const
{
    if (!translationMatrix)
    {
        const auto &contour{GetBase().base_polygon()};
        Vector3d displacement{GetXY()};
        // Double check that the object's center is (0, 0) and move it there if it is not.
        // NOTE: This can be removed in the future (or be made debug-only)
        if (!contour.empty())
        {
            const auto [minX, maxX]{std::minmax_element(contour.begin(), contour.end(), [](const auto &lhs, const auto &rhs) { return GetX(lhs) < GetX(rhs); })};
            const auto [minY, maxY]{std::minmax_element(contour.begin(), contour.end(), [](const auto &lhs, const auto &rhs) { return GetY(lhs) < GetY(rhs); })};
            const Vector3d offset{GetX(*minX) + GetX(*maxX), GetY(*minY) + GetY(*maxY), 0.0};
            if (offset)
            {
                std::cerr << "Bounding box of shape lies on " << offset << " instead of the required (0, 0). Adjusting transformation matrix.\n";
            }
            displacement -= offset;
        }
        translationMatrix = std::make_unique<Matrix<4>>(TranslationMatrix(displacement));
    }
    return *translationMatrix;
}

template <typename Handle>
const Matrix<4> &Locatable<Handle>::GetTransformationMatrix() const
{
    if (!transformationMatrix)
    {
        transformationMatrix = std::make_unique<Matrix<4>>(GetTranslationMatrix() * GetRotationMatrix());
    }
    return *transformationMatrix;
}

template <typename Handle>
template <Orientation O>
RoadPoint Locatable<Handle>::GetFurthestRoadPoint(const Lane &lane) const
{
    const auto localShape{GetLocalShape(lane)};
    if (lane.GetDirection<O>() == Direction::Upstream)
    {
        return *std::max_element(localShape.begin(), localShape.end(), less<Direction::Upstream, RoadPoint, RoadPoint>);
    }
    else
    {
        return *std::max_element(localShape.begin(), localShape.end(), less<Direction::Downstream, RoadPoint, RoadPoint>);
    }
}

template <typename Handle>
RoadPoint Locatable<Handle>::GetFurthestRoadPoint(const Lane &lane, Orientation o) const
{
    return (o == Orientation::Backwards) ? GetFurthestRoadPoint<Orientation::Backwards>(lane) : GetFurthestRoadPoint<Orientation::Forwards>(lane);
}

template <typename Handle>
void Locatable<Handle>::OutdateTranslation()
{
    translationMatrix.reset();
    transformationMatrix.reset();
    shape.clear();
}

template <typename Handle>
void Locatable<Handle>::OutdateRotation()
{
    rotationMatrix.reset();
    transformationMatrix.reset();
    shape.clear();
}
} // namespace osiql
