/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OsiQueryLibrary/Component/Iterable.h"
#include "OsiQueryLibrary/Point/Common.h"
#include "OsiQueryLibrary/Point/Pose.h"
#include "OsiQueryLibrary/Point/Vector2d.h"
#include "OsiQueryLibrary/Types/Geometry.h"

namespace osiql {
//! Points are an Iterable wrapper of an ordered container of points. Inheriting classes must publicly define:
//! T::const_iterator begin() const and T::const_iterator end() const. Public using declarations for Iterable<T>::begin
//! and end are highly recommended.
//!
//! \tparam T Container
template <typename T>
struct Points : Iterable<T>
{
    using Iterable<T>::begin;
    using Iterable<T>::end;

    //! Returns an iterator to the start of the nearest edge to the given point
    //!
    //! \tparam D Downstream or Upstream
    //! \param point
    //! \return ConstIterator<T, D>
    template <Direction D = Direction::Downstream>
    ConstIterator<T, D> begin(const Vector2d &point) const;

    //! Returns an iterator to the point past the start of the nearest edge to the given point
    //!
    //! \tparam D Downstream or Upstream
    //! \param point
    //! \return ConstIterator<T, D>
    template <Direction D = Direction::Downstream>
    ConstIterator<T, D> end(const Vector2d &point) const;

    //! Returns an iterator to the end of the nearest edge. If distance is at
    //! the end of one and the start of another edge, the latter is returned.
    //!
    //! \tparam D
    //! \param distanceFromStart
    //! \return ConstIterator<T, D>
    template <Direction D = Direction::Downstream>
    ConstIterator<T, D> GetNearestEdge(double distanceFromStart) const;
    template <Direction D = Direction::Downstream>
    Iterator<T, D> GetNearestEdge(double distanceFromStart);

    //! Computes and returns the sum of Euclidean distances between all neighboring points in this point chain.
    //!
    //! \return double
    double GetLength() const;

    //! Returns the interpolated point on this Points with the given s-coordinate
    //!
    //! \param latitude
    //! \return Vector2d
    Vector2d GetXY(double latitude) const;

    //! Returns the global point of any given s- & t-coordinate relative to this Points. This implementation is independent from
    //! other points comprising a lane and thus may produce results deviating from OpenDRIVE specification.
    //! The point is interpolated perpendicular to the (possibly extended) points and is thus ambiguous near the start/end of line segments.
    //!
    //! \param point
    //! \return Vector2d
    Vector2d GetXY(const Coordinates<Road> &point) const;

    //! Returns a st-coordinate representation of the given xy-coordinates localized to this point set
    //!
    //! \param point
    //! \return Point
    Coordinates<Road> GetCoordinates(const Vector2d &point) const;

    //! Returns the interpolated longitude (t-coordinate) of the point on the line with the given latitude (s-coordinate). If
    //! the latitude (s-coordinate) is outside the chain of points, the longitude (t-coordinate) of the nearest point is returned
    //!
    //! \param latitude s-coordinate
    //! \return double Negative if to the right of the nearest edge, otherwise positive
    double GetLongitude(double latitude) const;

    //! Takes a global point and angle and returns a pose localized to the nearest edge of this set of points.
    //!
    //! \param point
    //! \param angle counter-clockwise ascending angle in radians within [-π, π] from the x-axis
    //! \return Pose<Coordinates<Road>>
    Pose<Coordinates<Road>> GetPose(const Vector2d &point, double angle) const;

    //! Returns the global angle of the point set's edge at the given value in the template direction.
    //! If the value hits a corner, the angle of the latter edge in the template direction is returned.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param latitude s-coordinate
    //! \return double counter-clockwise ascending angle in radians within [-π, π] from the x-axis
    template <Direction = Direction::Downstream>
    double GetAngle(double latitude) const;

    //! Returns the global angle of the point set's edge at the given value in the given direction.
    //! If the value hits a corner, the angle of the latter edge in the given direction is returned.
    //!
    //! \param latitude s-coordinate
    //! \param d Downstream or Upstream
    //! \return double counter-clockwise ascending angle in radians within [-π, π] from the x-axis
    double GetAngle(double latitude, Direction d) const;

    //! Treats this chain of points as though it were a smooth curve and
    //! returns the change in angle per unit of length at the given latitude.
    //!
    //! \tparam Direction Downstream or Upstream
    //! \param s
    //! \return double
    template <Direction = Direction::Downstream>
    double GetCurvature(double s) const;

    //! Treats this chain of points as though it were a smooth curve and
    //! returns the change in angle per unit of length at the given latitude.
    //!
    //! \param s
    //! \param Direction Downstream or Upstream
    //! \return double
    double GetCurvature(double s, Direction) const;
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const Points<T> &line);
} // namespace osiql

namespace osiql {
template <typename T>
template <Direction D>
ConstIterator<T, D> Points<T>::begin(const Vector2d &point) const
{
    double min{point.GetPathTo(*begin<D>(), *std::next(begin<D>())).Length()};
    auto result{begin<D>()};
    for (auto it{std::next(begin<D>(), 2)}; it != end<D>(); ++it)
    {
        const double distance{point.GetPathTo(*std::prev(it), *it).Length()};
        if (distance < min)
        {
            result = std::prev(it);
            min = distance;
        }
    }
    return result;
}

template <typename T>
template <Direction D>
ConstIterator<T, D> Points<T>::end(const Vector2d &point) const
{
    double min{point.GetPathTo(*begin<D>(), *std::next(begin<D>())).Length()};
    auto result{std::next(begin<D>())};
    for (auto it{std::next(begin<D>(), 2)}; it != end<D>(); ++it)
    {
        const double distance{point.GetPathTo(*std::prev(it), *it).Length()};
        if (distance < min)
        {
            result = it;
            min = distance;
        }
    }
    return result;
}

template <typename T>
template <Direction D>
ConstIterator<T, D> Points<T>::GetNearestEdge(double latitude) const
{
    auto it{end<D>(latitude)};
    return (it == begin<D>()) ? ++it : ((it == end<D>()) ? --it : it);
}
template <typename T>
template <Direction D>
Iterator<T, D> Points<T>::GetNearestEdge(double latitude)
{
    auto it{end<D>(latitude)};
    return (it == begin<D>()) ? ++it : ((it == end<D>()) ? --it : it);
}

template <typename T>
double Points<T>::GetLength() const
{
    double length{0.0};
    for (auto it{std::next(begin())}; it != end(); ++it)
    {
        length += (Vector2d(*it) - Vector2d(*std::prev(it))).Length();
    }
    return length;
}

template <typename T>
template <Direction D>
double Points<T>::GetAngle(double s) const
{
    auto edgeEnd{GetNearestEdge<D>(s)};
    return (*edgeEnd - *std::prev(edgeEnd)).Angle();
}
template <typename T>
double Points<T>::GetAngle(double latitude, Direction d) const
{
    return d == Direction::Upstream ? GetAngle<Direction::Upstream>(latitude) : GetAngle<Direction::Upstream>(latitude);
}

template <typename T>
template <Direction D>
double Points<T>::GetCurvature(double s) const
{
    // TODO Performance: Lazy-evaluate and memoize curvatures at each point

    const auto edgeEnd{GetNearestEdge<D>(s)};
    const auto edgeStart{std::prev(edgeEnd)};
    if (less<D>(s, GetLatitude(*edgeStart)) || greater<D>(s, GetLatitude(*edgeEnd)))
    {
        return std::numeric_limits<double>::quiet_NaN();
    }
    const Vector2d edge{*edgeEnd - *edgeStart};

    const double edgeCenterLatitude{GetLatitude(*edgeStart) * 0.5 + GetLatitude(*edgeEnd) * 0.5};
    double startCurvature{0.0};
    if (edgeStart != begin<D>())
    {
        const Vector2d priorEdge{*edgeStart - *std::prev(edgeStart)};
        const double priorEdgeCenterLatitude{GetLatitude(*std::prev(edgeStart)) * 0.5 + GetLatitude(*edgeStart) * 0.5};
        const double length{std::abs(edgeCenterLatitude - priorEdgeCenterLatitude)};
        if (length)
        {
            const double deltaAngle{std::atan2(priorEdge.Cross(edge), priorEdge.Dot(edge))};
            startCurvature = deltaAngle / length;
        }
    }

    double endCurvature{0.0};
    if (std::next(edgeEnd) != end<D>())
    {
        const Vector2d nextEdge{Norm(*std::next(edgeEnd) - *edgeEnd)};
        const double nextEdgeCenterLatitude{GetLatitude(*edgeEnd) * 0.5 + GetLatitude(*std::next(edgeEnd)) * 0.5};
        const double length{std::abs(nextEdgeCenterLatitude - edgeCenterLatitude)};
        if (length)
        {
            const double deltaAngle{std::atan2(edge.Cross(nextEdge), edge.Dot(nextEdge))};
            endCurvature = deltaAngle / length;
        }
    }

    const double startLatitude{GetLatitude(*edgeStart)};
    const double endLatitude{GetLatitude(*edgeEnd)};
    if (startLatitude == endLatitude)
    {
        return 0.0;
    }
    const double ratio{(s - startLatitude) / (endLatitude - startLatitude)};
    return startCurvature * (1.0 - ratio) + endCurvature * ratio;
}

template <typename T>
double Points<T>::GetCurvature(double s, Direction d) const
{
    return d == Direction::Upstream ? GetCurvature<Direction::Upstream>(s) : GetCurvature<Direction::Downstream>(s);
}

template <typename T>
double Points<T>::GetLongitude(double latitude) const
{
    const typename T::const_iterator it{begin(latitude)};
    if (std::next(it) == end() || GetLatitude(*it) == GetLatitude(*std::next(it)))
    {
        return osiql::GetLongitude(*it);
    }
    if (GetLatitude(*std::next(it)) == GetLatitude(*it))
    {
        return osiql::GetLongitude(*it);
    }
    const double ratio{(latitude - GetLatitude(*it)) / (GetLatitude(*std::next(it)) - GetLatitude(*it))};
    return osiql::GetLongitude(*it) * (1.0 - ratio) + osiql::GetLongitude(*std::next(it)) * ratio;
}

template <typename T>
Vector2d Points<T>::GetXY(double s) const
{
    const auto it{GetNearestEdge(s)};
    const double prevS{GetLatitude(*std::prev(it))};
    const double currS{GetLatitude(*it)};
    if (prevS == currS)
    {
        return Vector2d{*it};
    }
    // within [0,1]
    const double ratio{(s - prevS) / (currS - prevS)};
    return Vector2d(*std::prev(it)) * (1.0 - ratio) + Vector2d(*it) * ratio;
}

template <typename T>
Vector2d Points<T>::GetXY(const Coordinates<Road> &coordinates) const
{
    const auto it{GetNearestEdge(coordinates.latitude)};
    const Vector2d edgeStart{*std::prev(it)};
    const Vector2d edgeEnd{*it};
    const Vector2d edge{edgeEnd - edgeStart};
    const Vector2d normal{Norm(Vector2d{-edge.y, edge.x})};

    const double startDistance{std::prev(it)->s_position()};
    const double endDistance{it->s_position()};
    if (startDistance == endDistance)
    {
        return edgeEnd + normal * coordinates.longitude;
    }
    // within [0,1]
    const double ratio{(coordinates.latitude - startDistance) / (endDistance - startDistance)};
    const Vector2d pointOnEdge{edgeStart * (1.0 - ratio) + edgeEnd * ratio};

    return pointOnEdge + normal * coordinates.longitude;
}

template <typename T>
Coordinates<Road> Points<T>::GetCoordinates(const Vector2d &globalPoint) const
{
    auto it{end(globalPoint)};
    if (it == begin())
    {
        ++it;
    }
    return globalPoint.GetCoordinates(*std::prev(it), *it);
}

template <typename T>
Pose<Coordinates<Road>> Points<T>::GetPose(const Vector2d &point, double angle) const
{
    auto it{end(point)};
    if (it == begin())
    {
        ++it;
    }
    return {
        point.GetCoordinates(*std::prev(it), *it),
        detail::SetAngleToValidRange(angle - (Vector2d{*it} - *std::prev(it)).Angle()) //
    };
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Points<T> &line)
{
    if (!line.empty())
    {
        os << line.front();
        for (auto it{std::next(line.begin())}; it != line.end(); ++it)
        {
            os << ", " << *it;
        }
    }
    return os;
}
} // namespace osiql
