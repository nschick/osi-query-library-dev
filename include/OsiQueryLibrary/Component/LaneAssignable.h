/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base class for objects that store their assigned positions on or relative to specified lanes.

#include <map>
#include <osi3/osi_object.pb.h>
#include <osi3/osi_trafficsign.pb.h>

#include "OsiQueryLibrary/Routing/Route.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"
#include "OsiQueryLibrary/Trait/Position.h"

namespace osiql {
//! \brief A LaneAssignable is an object that can be assigned to a lane, which is either a
//! MovingObject, StaticObject, RoadMarking, TrafficLight or TrafficSign.
//! MovingObjects store their assignments as a Pose while the other objects use Assignment<Lane>.
template <typename T>
struct LaneAssignable : T
{
    using T::T;

    //! Returns the position assigned to the lane with the given id. Throws an exception if no such lane exists.
    //!
    //! \param id Id of the lane of the returned position.
    //! \return const Position<T::Handle>::type&
    const Position<T> &GetLocalPosition(Id id) const;

    //! Returns the local yaw of this object relative to that of the given lane at the object's position. If this object
    //! has no assigned position on the given lane, NaN is returned.
    //!
    //! \param lane Lane to which this object is assigned
    //! \return double counter-clockwise ascending angle in radians within [-π, π] relative to th lane's reference line
    //! in definition direction (which may not be the same as the lane's driving direction)
    double GetLocalYaw(const Lane &lane) const; // TODO: Backwards

    //! Returns the internal OSI representation of this object's lane assignments.
    //!
    //! \return const Container<osi3::LogicalLaneAssignment>&
    const Container<osi3::LogicalLaneAssignment> &GetLaneAssignments() const;

    //! Returns the first position encountered along the given route.
    //! If no such position exists, a nullptr is returned.
    //!
    //! \param route
    //! \return const Position<T> *
    const Position<T> *GetFirstPositionOnRoute(const Route &route) const;

    //! Returns the earliest continuous range of position encountered on the given route.
    //! If no such range exists, an empty vector is returned.
    //!
    //! \param route
    //! \return std::vector<const Position<T> *>
    std::vector<const Position<T> *> GetPositionsOnRoute(const Route &route) const;

    //! \brief Road positions of the center of this object. Only one position is stored per touched road.
    std::vector<Position<T>> positions;
};
} // namespace osiql

// ----- Template definitions:

namespace osiql {
template <typename T>
const Position<T> &LaneAssignable<T>::GetLocalPosition(const Id id) const
{
    const auto it{std::find_if(positions.begin(), positions.end(), [&id](const Position<T> &position) {
        return position.entity.GetId() == id;
    })};
    if (it == positions.end())
    {
        std::ostringstream stream;
        stream << "Object has no assigned position to a lane with the given id.\n";
        throw std::runtime_error(stream.str());
    }
    return *it;
}

template <typename T>
double LaneAssignable<T>::GetLocalYaw(const Lane &lane) const
{
    const auto it{std::find_if(positions.begin(), positions.end(), [&](const Position<T> &position) {
        return position.entity.GetRoad() == lane.GetRoad();
    })};
    return it != positions.end() ? it->GetLocalYaw() : std::numeric_limits<double>::quiet_NaN();
}

template <typename T>
const Container<osi3::LogicalLaneAssignment> &LaneAssignable<T>::GetLaneAssignments() const
{
    if constexpr (std::is_same_v<typename T::Handle, osi3::MovingObject>)
    {
        return T::GetHandle().moving_object_classification().logical_lane_assignment();
    }
    else if constexpr (std::is_same_v<typename T::Handle, osi3::TrafficSign>)
    {
        return T::GetHandle().main_sign().classification().logical_lane_assignment();
    }
    else
    {
        return T::GetHandle().classification().logical_lane_assignment();
    }
}

template <typename T>
const Position<T> *LaneAssignable<T>::GetFirstPositionOnRoute(const Route &route) const
{
    auto it{positions.end()};
    auto pred = [&](const Waypoint &point) {
        it = std::find_if(positions.begin(), positions.end(), [&road = point.GetRoad()](const auto &position) {
            return GetRoad(position) == road;
        });
        return it != positions.end();
    };
    auto firstMatch{route.TraverseUntil(pred)};
    return it == positions.end() ? nullptr : &*it;
}

template <typename T>
std::vector<const Position<T> *> LaneAssignable<T>::GetPositionsOnRoute(const Route &route) const
{
    std::vector<const Position<T> *> result{};
    auto pred = [&](const Waypoint &point) {
        auto it = std::find_if(positions.begin(), positions.end(), [&road = point.GetRoad()](const auto &position) {
            return GetRoad(position) == road;
        });
        if (it != positions.end())
        {
            result.push_back(&*it);
            return true;
        }
        return false;
    };
    TraverseWhile(route.TraverseUntil(pred), route.end(), pred);
    return result;
}
} // namespace osiql
