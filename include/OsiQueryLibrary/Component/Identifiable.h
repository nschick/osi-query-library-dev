/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Base of most osi-object wrappers, which holds the handle to said object,
//! provides a getter for its id, and supports comparison operators via that id.

#include <osi3/osi_object.pb.h>

#include "OsiQueryLibrary/Trait/Common.h"
#include "OsiQueryLibrary/Trait/Handle.h"
#include "OsiQueryLibrary/Trait/Id.h"

namespace osiql {
//!  A wrapper of an osi3 object
//!
//! \tparam T The underlying handle, ergo said osi3 object
template <typename T>
struct Wrapper
{
    //! \brief The OSI object that this object wraps around
    using Handle = T;

    //! Constructs a wrapper from the given osi3 handle
    //!
    //! \param T Underlying osi3 handle
    Wrapper(const T &);

    //! Returns the underlying OSI object handle of this object.
    //!
    //! \return const Handle&
    const Handle &GetHandle() const;

protected:
    //! \brief Underlying OSI handle of this object
    handle_t<T> handle;
};

//! An Identifiable is a wrapper for osi3 objects that have a unique osi3::Identifier.
//! It offers a public handle to the underlying osi3 object
//!
//! \tparam T
template <typename T>
struct Identifiable : Wrapper<T>
{
    using Wrapper<T>::Wrapper;

    //! Returns the id of this object. The id is unique among all osi3 objects of any type
    //!
    //! \return Id
    Id GetId() const;

    //! Compares two objects of the same type by their id
    //!
    //! \param other
    //! \return whether this object is the same as the other
    bool operator==(const Identifiable<T> &other) const;

    //! Compares two objects of different types and returns false.
    //!
    //! \tparam U
    //! \param other
    //! \return false
    template <typename U>
    bool operator==(const Identifiable<U> &other) const;
};

//! Compares the objects by their id
//!
//! \tparam T osi3 object that can access its osi3 identifier
//! \param lhs term on the left side of the operator
//! \param rhs term on the right side of the operator
//! \return whether the left term is less that the right term
template <typename T>
bool operator<(const Identifiable<T> &lhs, const Identifiable<T> &rhs);

//! Returns whether the object on the left side has a smaller id than the id on the right side
//!
//! \tparam T
//! \param lhs
//! \param rhs
//! \return bool
template <typename T>
bool operator<(const Identifiable<T> &lhs, id_t<T> rhs);

//! Returns whether the id on the left sid is smaller than the id of the object on the right side
//!
//! \tparam T
//! \param lhs
//! \param rhs
//! \return bool
template <typename T>
bool operator<(id_t<T> lhs, const Identifiable<T> &rhs);
} // namespace osiql

// ---------- Template definitions ----------

namespace osiql {

template <typename T>
Wrapper<T>::Wrapper(const T &handle) :
    handle(handle)
{
}

template <typename T>
const T &Wrapper<T>::GetHandle() const
{
    return handle;
}

template <typename T>
Id Identifiable<T>::GetId() const
{
    return this->GetHandle().has_id() ? this->GetHandle().id().value() : UNDEFINED_ID;
}

template <typename T>
bool Identifiable<T>::operator==(const Identifiable<T> &other) const
{
    return GetId() == other.GetId();
}

template <typename T>
template <typename U>
bool Identifiable<T>::operator==(const Identifiable<U> &other) const
{
    std::ignore = other;
    return false;
}

template <typename T>
bool operator<(const Identifiable<T> &lhs, const Identifiable<T> &rhs)
{
    return lhs.GetId() < rhs.GetId();
}
template <typename T>
bool operator<(const Identifiable<T> &lhs, id_t<T> rhs)
{
    return lhs.GetId() < rhs;
}
template <typename T>
bool operator<(id_t<T> lhs, const Identifiable<T> &rhs)
{
    return lhs < rhs.GetId();
}
} // namespace osiql
