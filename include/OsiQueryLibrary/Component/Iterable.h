/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Roads are defined in one direction but can be driven on in the opposite direction. The Iterable header
//! provides direction-based iterator abstractions to allow for uniform traversal no matter the direction.

#include "OsiQueryLibrary/Component/Comparable.h"
#include "OsiQueryLibrary/Types/Direction.h"
#include "OsiQueryLibrary/Types/Orientation.h"

namespace osiql {

namespace trait {
//! Iterator type trait whose type depends on the template direction
//!
//! \tparam T
//! \tparam Direction
template <typename T, Direction>
struct Iterable
{
    //! \brief forward iterator
    using const_iterator = typename T::const_iterator;
    using iterator = typename T::iterator;
};
//! Type trait for an iterator whose forward traversal is upstream,
//! which is in reverse order of a boundary's points
//!
//! \tparam T
template <typename T>
struct Iterable<T, Direction::Upstream>
{
    //! \brief forward iterator
    using const_iterator = typename T::const_reverse_iterator;
    using iterator = typename T::reverse_iterator;
};

//! Type trait for an iterator whose forward traversal is upstream,
//! which is in reverse order of a boundary's points
//!
//! \tparam T
template <typename T, Orientation>
struct OrientedIterable
{
    //! \brief forward iterator
    using const_iterator = typename T::const_iterator;
    using iterator = typename T::iterator;
};
template <typename T>
struct OrientedIterable<T, Orientation::Backwards>
{
    //! \brief forward iterator
    using const_iterator = typename T::const_reverse_iterator;
    using iterator = typename T::reverse_iterator;
};
} // namespace trait

template <typename T, Direction D>
using ConstIterator = typename trait::Iterable<T, D>::const_iterator;
template <typename T, Direction D>
using Iterator = typename trait::Iterable<T, D>::iterator;

template <typename T, Orientation O>
using OrientedConstIterator = typename trait::OrientedIterable<T, O>::const_iterator;
template <typename T, Orientation O>
using OrientedIterator = typename trait::OrientedIterable<T, O>::iterator;

//! An Iterable is an abstract class that provides accessor methods to a random-access-iterable container or span of elements
//! that can be ordered by s-coordinate. It provides osiql::Direction-based begin, end, at, front and back methods and should
//! thus only be inherited by classes where this makes semantic sense. Inheriting classes must publicly define:
//! T::const_iterator begin() const and T::const_iterator end() const. Public using declarations for Iterable<T>::begin
//! and end are highly recommended.
//!
//! \tparam T
template <typename T>
struct Iterable
{
    virtual ~Iterable() = default;

    //! Returns an iterator to the first element of this span
    //!
    //! \return T::const_iterator
    virtual typename T::const_iterator begin() const = 0;

    //! Returns an iterator past the last element of this span
    //!
    //! \return T::const_iterator
    virtual typename T::const_iterator end() const = 0;

    //! Returns an iterator to the first element of this span in the given direction
    //!
    //! \tparam D
    //! \return ConstIterator<T, D> const_reverse_iterator if Upstream, otherwise const_iterator
    template <Direction D>
    ConstIterator<T, D> begin() const;

    //! Returns an iterator to the last element of this span in the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam D The direction in which the span should be traversed
    //! \param s Value before or at which the returned iterator's element should begin
    //! \return ConstIterator<T, D> const_reverse_iterator if Upstream, otherwise const_iterator
    template <Direction D = Direction::Downstream>
    ConstIterator<T, D> begin(double s) const;

    //! Returns an iterator past the last element of this span in the given direction
    //!
    //! \tparam D
    //! \return ConstIterator<T, D> const_reverse_iterator if Upstream, otherwise const_iterator
    template <Direction D>
    ConstIterator<T, D> end() const;

    //! Returns an iterator past the last element of this span in the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam D The direction in which the span should be traversed
    //! \param s Value after which the returned iterator's element should begin
    //! \return ConstIterator<T, D>
    template <Direction D = Direction::Downstream>
    ConstIterator<T, D> end(double s) const;

    //! Returns an iterator to the first element of this span in the opposite of the given direction
    //!
    //! \tparam D The direction in which the span should be traversed
    //! \return Iterator
    template <Direction D = Direction::Downstream>
    ConstIterator<T, !D> rbegin() const;

    //! Returns an iterator to the last element of this span in the opposite of the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam D The opposite of the direction in which the span should be traversed
    //! \param s Value before or at which the returned iterator's element should begin
    //! \return Iterator
    template <Direction D = Direction::Downstream>
    ConstIterator<T, !D> rbegin(double s) const;

    //! Return an iterator past the last element of this span in the opposite of the given direction
    //!
    //! \tparam D The opposite of the direction in which the span should be traversed
    //! \return Iterator
    template <Direction D = Direction::Downstream>
    ConstIterator<T, !D> rend() const;

    //! Returns an iterator past the last element of this span in the opposite of the given direction
    //! with a starting s-coordinate less or equal to the given value. If all elements
    //! start after the given value, an iterator to the span's first element is returned.
    //!
    //! \tparam D The opposite of the direction in which the span should be traversed
    //! \param s Value after which the returned iterator's element should begin
    //! \return Iterator
    template <Direction D = Direction::Downstream>
    ConstIterator<T, !D> rend(double s) const;

    //! Returns whether the s-coordinate interval of the given element is a subset of this span's s-coordinate interval
    //!
    //! \tparam U Type representing an s-coordinate interval
    //! \param u s-coordinate interval
    //! \return bool Whether the given object begins at or after and ends before or with this object
    template <typename U>
    bool ContainsS(const U &u) const;

    //! Returns a reference to the first element of this span in the given direction.
    //!
    //! \tparam D The direction in which the span should be traversed
    //! \return ConstIterator<T, D>::reference
    template <Direction D = Direction::Downstream>
    typename ConstIterator<T, D>::reference front() const;
    typename ConstIterator<T, Direction::Downstream>::reference front(Direction) const;

    //! Returns a reference to the last element of this span in the given direction
    //!
    //! \tparam D The direction in which the span should be traversed
    //! \return ConstIterator<T, D>::reference
    template <Direction D = Direction::Downstream>
    typename ConstIterator<T, D>::reference back() const;
    typename ConstIterator<T, Direction::Downstream>::reference back(Direction) const;

    //! Returns the number of elements in this span
    //!
    //! \return size_t
    size_t size() const;

    //! Returns whether this object contains no elements
    //!
    //! \return bool
    bool empty() const;

    //! Returns the (n-1)-th element in the given direction of this span
    //!
    //! \tparam D The direction in which the span should be traversed
    //! \param i
    //! \return ConstIterator<T, D>::reference
    template <Direction D = Direction::Downstream>
    typename ConstIterator<T, D>::reference at(size_t i) const;
};

template <typename T>
template <Direction D>
ConstIterator<T, D> Iterable<T>::begin() const
{
    if constexpr (D == Direction::Upstream)
    {
        return std::make_reverse_iterator(end());
    }
    else
    {
        return begin();
    }
}

template <typename T>
template <Direction D>
ConstIterator<T, D> Iterable<T>::begin(double s) const
{
    auto it{end<D>(s)};
    return it != begin<D>() ? --it : it;
}

template <typename T>
template <Direction D>
ConstIterator<T, D> Iterable<T>::end() const
{
    if constexpr (D == Direction::Upstream)
    {
        return std::make_reverse_iterator(begin());
    }
    else
    {
        return end();
    }
}

template <typename T>
template <Direction D>
ConstIterator<T, D> Iterable<T>::end(double s) const
{
    return std::upper_bound(begin<D>(), end<D>(), s, lessEqual<D, double, typename T::value_type>);
}

template <typename T>
template <Direction D>
ConstIterator<T, !D> Iterable<T>::rbegin() const
{
    return begin<!D>();
}

template <typename T>
template <Direction D>
ConstIterator<T, !D> Iterable<T>::rbegin(double s) const
{
    return begin<!D>(s);
}

template <typename T>
template <Direction D>
ConstIterator<T, !D> Iterable<T>::rend() const
{
    return end<!D>();
}

template <typename T>
template <Direction D>
ConstIterator<T, !D> Iterable<T>::rend(double s) const
{
    return end<!D>(s);
}

template <typename T>
template <typename U>
bool Iterable<T>::ContainsS(const U &element) const
{
    return greaterEqual<Direction::Downstream>(element, front()) && greaterEqual<Direction::Upstream>(element, back());
}

template <typename T>
template <Direction D>
typename ConstIterator<T, D>::reference Iterable<T>::front() const
{
    return *begin<D>();
}
template <typename T>
typename ConstIterator<T, Direction::Downstream>::reference Iterable<T>::front(Direction d) const
{
    return d == Direction::Upstream ? front<Direction::Upstream>() : front<Direction::Downstream>();
}

template <typename T>
template <Direction D>
typename ConstIterator<T, D>::reference Iterable<T>::back() const
{
    return *std::prev(end<D>());
}
template <typename T>
typename ConstIterator<T, Direction::Downstream>::reference Iterable<T>::back(Direction d) const
{
    return d == Direction::Upstream ? back<Direction::Upstream>() : back<Direction::Downstream>();
}

template <typename T>
size_t Iterable<T>::size() const
{
    return static_cast<size_t>(std::distance(begin(), end()));
}

template <typename T>
bool Iterable<T>::empty() const
{
    return begin() == end();
}

template <typename T>
template <Direction D>
typename ConstIterator<T, D>::reference Iterable<T>::at(size_t i) const
{
    return *std::next(begin<D>(), static_cast<typename ConstIterator<T, D>::difference_type>(i));
}

template <Direction D, typename T>
double GetLatitude(const Iterable<T> &iterable)
{
    return GetLatitude<D>(iterable.template front<D>());
}

// OrientationalIterator

template <Direction D, typename T>
ConstIterator<T, D> begin(const T &container)
{
    if constexpr (D == Direction::Upstream)
    {
        return container.rbegin();
    }
    else
    {
        return container.begin();
    }
}
template <Direction D, typename T>
Iterator<T, D> begin(T &container)
{
    if constexpr (D == Direction::Upstream)
    {
        return container.rbegin();
    }
    else
    {
        return container.begin();
    }
}

template <Orientation O, typename T>
OrientedConstIterator<T, O> begin(const T &container)
{
    if constexpr (O == Orientation::Forwards)
    {
        return container.cbegin();
    }
    else
    {
        return container.crbegin();
    }
}
template <Orientation O, typename T>
OrientedIterator<T, O> begin(T &container)
{
    if constexpr (O == Orientation::Forwards)
    {
        return container.begin();
    }
    else
    {
        return container.rbegin();
    }
}

template <Direction D, typename T>
ConstIterator<T, D> end(const T &container)
{
    if constexpr (D == Direction::Upstream)
    {
        return container.rend();
    }
    else
    {
        return container.end();
    }
}
template <Direction D, typename T>
Iterator<T, D> end(T &container)
{
    if constexpr (D == Direction::Upstream)
    {
        return container.rend();
    }
    else
    {
        return container.end();
    }
}

template <Orientation O, typename T>
OrientedConstIterator<T, O> end(const T &container)
{
    if constexpr (O == Orientation::Forwards)
    {
        return container.cend();
    }
    else
    {
        return container.crend();
    }
}
template <Orientation O, typename T>
OrientedIterator<T, O> end(T &container)
{
    if constexpr (O == Orientation::Forwards)
    {
        return container.end();
    }
    else
    {
        return container.rend();
    }
}

template <Direction D, typename T>
typename ConstIterator<T, D>::reference front(const T &container)
{
    return *begin<D>(container);
}
template <Direction D, typename T>
typename Iterator<T, D>::reference front(T &container)
{
    return *begin<D>(container);
}

template <Orientation O, typename T>
typename OrientedConstIterator<T, O>::reference front(const T &container)
{
    return *begin<O>(container);
}
template <Orientation O, typename T>
typename OrientedIterator<T, O>::reference front(T &container)
{
    return *begin<O>(container);
}

template <Direction D, typename T>
typename ConstIterator<T, D>::reference back(const T &container)
{
    return *std::prev(end<D>(container));
}
template <Direction D, typename T>
typename Iterator<T, D>::reference back(T &container)
{
    return *std::prev(end<D>(container));
}

template <Orientation O, typename T>
typename OrientedConstIterator<T, O>::reference back(const T &container)
{
    return *std::prev(end<O>(container));
}
template <Orientation O, typename T>
typename OrientedIterator<T, O>::reference back(T &container)
{
    return *std::prev(end<O>(container));
}
} // namespace osiql
