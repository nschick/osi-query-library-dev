/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Free functions for comparing types that cover a longitudinal interval depending on
//! a given traversal direction. These types include osiql::Lane, osiql::LaneRow, osiql::Boundary,
//! osiql::LocalBounds, osiql::Point & double.

#include "OsiQueryLibrary/Point/Common.h"
#include "OsiQueryLibrary/Types/Orientation.h"

namespace osiql {
//! Returns the earliest latitude of the element that is encountered first when traveling along the given template direction
//!
//! \tparam D Downstream or Upstream
//! \tparam T Type that is less-than comparable with U and that can be implicitly converted to a double
//! \tparam U Type that is less-than comparable with T and that can be implicitly converted to a double
//! \param a entity of type T
//! \param b entity of type U
//! \return double Earliest latitude in the given template direction
template <Direction D, typename T, typename U>
double min(const T &a, const U &b)
{
    // TODO: Turn this function into one that calls a type trait class function that extracts the directional latitude from an entity.
    if constexpr (D == Direction::Upstream)
    {
        return std::max(a, b);
    }
    else
    {
        return std::min(a, b);
    }
}

//! Returns the earliest latitude of the element that is encountered first when traveling along the given template orientation
//!
//! \tparam O Forwards or Backwards
//! \tparam T Type that is less-than comparable with U and that can be implicitly converted to a double
//! \tparam U Type that is less-than comparable with T and that can be implicitly converted to a double
//! \param a entity of type T
//! \param b entity of type U
//! \return double Earliest latitude in the given template orientation
template <Orientation O, typename T, typename U>
double min(const T &a, const U &b)
{
    if constexpr (O == Orientation::Backwards)
    {
        return std::max(a, b);
    }
    else
    {
        return std::min(a, b);
    }
}

//! Returns the earliest latitude of the element that is encountered last when traveling along the given template direction
//!
//! \tparam D Downstream or Upstream
//! \tparam T Type that is less-than comparable with U and that can be implicitly converted to a double
//! \tparam U Type that is less-than comparable with T and that can be implicitly converted to a double
//! \param a entity of type T
//! \param b entity of type U
//! \return double Earliest latitude in the given template direction of the last encountered entity
template <Direction D, typename T, typename U>
double max(const T &a, const U &b)
{
    if constexpr (D == Direction::Upstream)
    {
        return std::min(a, b);
    }
    else
    {
        return std::max(a, b);
    }
}

//! Returns the earliest latitude of the element that is encountered last when traveling along the given template orientation
//!
//! \tparam O Forwards or Backwards
//! \tparam T Type that is less-than comparable with U and that can be implicitly converted to a double
//! \tparam U Type that is less-than comparable with T and that can be implicitly converted to a double
//! \param a entity of type T
//! \param b entity of type U
//! \return double Earliest latitude in the given template orientation of the last encountered entity
template <Orientation O, typename T, typename U>
double max(const T &a, const U &b)
{
    if constexpr (O == Orientation::Backwards)
    {
        return std::min(GetLatitude<O>(a), GetLatitude<O>(b));
    }
    else
    {
        return std::max(GetLatitude<O>(a), GetLatitude<O>(b));
    }
}

//! Returns the given value. This function is overloaded for other input types.
//!
//! \tparam Direction Ignored for this overload
//! \param value A value representing a distance
//! \return double The value that was passed
template <Direction = Direction::Downstream>
double GetLatitude(const double &value)
{
    return value;
}

//! Returns the starting latitude of the given object in the given direction.
//!
//! \tparam T Type for which the overload double GetLatitude<Direction>(const T&) is defined
//! \param object An object spanning an latitude interval
//! \param d Direction along which the starting direction is determined. The start is the largest
//! value touched by the object if the direction is Upstream and the smallest value if it's not.
//! \return double Earliest latitude of the given object in the given direction
template <typename T>
double GetLatitude(const T &object, Direction d)
{
    return d == Direction::Upstream ? GetLatitude<Direction::Upstream>(object) : GetLatitude<Direction::Downstream>(object);
}

//! Returns the given value. This function is overloaded for other types
//!
//! \tparam Orientation Ignored for this overload
//! \param value A value representing a distance
//! \return double The value that was passed
template <Orientation>
double GetLatitude(const double &value)
{
    return value;
}

//! Returns the starting latitude of the given object in the given orientation.
//!
//! \tparam T Type for which the overload double GetLatitude<Orientation>(const T&) is defined
//! \param object An object spanning an latitude interval
//! \param d orientation along which the starting orientation is determined. The start is the largest
//! value touched by the object if the orientation is Upstream and the smallest value if it's not.
//! \return double Earliest latitude of the given object in the given orientation
template <typename T>
double GetLatitude(const T &object, Orientation o)
{
    return o == Orientation::Backwards ? GetLatitude<Orientation::Backwards>(object) : GetLatitude<Orientation::Forwards>(object);
}

//! Returns the difference between an object's maximum and minimum latitude
//!
//! \tparam T Type for which the overload double GetLatitude<Direction>(const T&) is defined
//! \param object Entity of type T
//! \return double Longitudinal length of the given entity
template <typename T>
double Length(const T &object)
{
    return GetLatitude<Direction::Upstream>(object) - GetLatitude<Direction::Downstream>(object);
}

//! Returns whether the given latitude interval of the contained object is a
//! subset of the container object's latitude interval.
//!
//! \tparam T Type spanning a longitudinal interval
//! \tparam U Type spanning a longitudinal interval
//! \param container Object to be tested for whether it contains the other object
//! \param contained Object to be tested for whether it is contained by the other object
//! \return bool Whether the latitude interval of the contained object lies on or inside the
//! latitude interval of the container object.
template <typename T, typename U>
bool Contains(const T &container, const U &contained)
{
    return GetLatitude<Direction::Downstream>(container) <= GetLatitude<Direction::Downstream>(contained) && //
           GetLatitude<Direction::Upstream>(container) >= GetLatitude<Direction::Upstream>(contained);
}

//! Returns whether the first object begins before the second in the given direction.
//!
//! \tparam D Downstream or Upstream
//! \tparam T Type for which double GetLatitude<D>(const T&) is defined
//! \tparam U Type for which double GetLatitude<D>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \return bool Whether "a" begins before "b" in the given direction
template <Direction D, typename T, typename U>
bool less(const T &a, const U &b)
{
    if constexpr (D == Direction::Upstream)
    {
        return GetLatitude<D>(a) > GetLatitude<D>(b);
    }
    else
    {
        return GetLatitude<D>(a) < GetLatitude<D>(b);
    }
}

//! Returns whether the first object begins before the second in the given direction.
//!
//! \tparam T Type for which double GetLatitude<D>(const T&) is defined
//! \tparam U Type for which double GetLatitude<D>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \param d Downstream or Upstream
//! \return bool Whether "a" begins before "b" in the given direction
template <typename T, typename U>
bool less(const T &a, const U &b, Direction d)
{
    return d == Direction::Upstream ? less<Direction::Upstream>(a, b) : less<Direction::Downstream>(a, b);
}

//! Returns whether the the first object begins before the second in the given orientation.
//!
//! \tparam O Forwards or Backwards
//! \tparam T Type for which double GetLatitude<O>(const T&) is defined
//! \tparam U Type for which double GetLatitude<O>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \return bool Whether "a" begins before "b" in the given orientation
template <Orientation O, typename T, typename U>
bool less(const T &a, const U &b)
{
    if constexpr (O == Orientation::Backwards)
    {
        return GetLatitude<O>(a) > GetLatitude<O>(b);
    }
    else
    {
        return GetLatitude<O>(a) < GetLatitude<O>(b);
    }
}

//! Returns whether the the first object begins before the second in the given orientation.
//!
//! \tparam T Type for which double GetLatitude<Orientation>(const T&) is defined
//! \tparam U Type for which double GetLatitude<Orientation>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \param o Forwards or Backwards
//! \return bool Whether "a" begins before "b" in the given orientation
template <typename T, typename U>
bool less(const T &a, const U &b, Orientation o)
{
    return o == Orientation::Backwards ? less<Orientation::Backwards>(a, b) : less<Orientation::Forwards>(a, b);
}

//! Returns whether the first object begins after the second in the given direction.
//!
//! \tparam D Downstream or Upstream
//! \tparam T Type for which double GetLatitude<D>(const T&) is defined
//! \tparam U Type for which double GetLatitude<D>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \return bool Whether "a" begins after "b" in the given direction
template <Direction D, typename T, typename U>
bool greater(const T &a, const U &b)
{
    if constexpr (D == Direction::Upstream)
    {
        return GetLatitude<D>(b) > GetLatitude<D>(a);
    }
    else
    {
        return GetLatitude<D>(a) > GetLatitude<D>(b);
    }
}

//! Returns whether the first object begins after the second in the given direction.
//!
//! \tparam T Type for which double GetLatitude<Direction>(const T&) is defined
//! \tparam U Type for which double GetLatitude<Direction>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \param d Downstream or Upstream
//! \return bool Whether "a" begins after "b" in the given direction
template <typename T, typename U>
bool greater(const T &a, const U &b, Direction d)
{
    return d == Direction::Upstream ? greater<Direction::Upstream>(a, b) : greater<Direction::Downstream>(a, b);
}

//! Returns whether the first object begins after the second in the given orientation.
//!
//! \tparam O Forwards or Backwards
//! \tparam T Type for which double GetLatitude<O>(const T&) is defined
//! \tparam U Type for which double GetLatitude<O>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \return bool Whether "a" begins after "b" in the given orientation
template <Orientation O, typename T, typename U>
bool greater(const T &a, const U &b)
{
    if constexpr (O == Orientation::Backwards)
    {
        return GetLatitude<O>(b) > GetLatitude<O>(a);
    }
    else
    {
        return GetLatitude<O>(a) > GetLatitude<O>(b);
    }
}

//! Returns whether the first object begins after the second in the given orientation.
//!
//! \tparam T Type for which double GetLatitude<O>(const T&) is defined
//! \tparam U Type for which double GetLatitude<O>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \param o Forwards or Backwards
//! \return bool Whether "a" begins after "b" in the given orientation
template <typename T, typename U>
bool greater(const T &a, const U &b, Orientation o)
{
    return o == Orientation::Backwards ? greater<Orientation::Backwards>(a, b) : greater<Orientation::Forwards>(a, b);
}

//! Returns whether the first object does not begin after the second in the given direction.
//!
//! \tparam D Downstream or Upstream
//! \tparam T Type for which double GetLatitude<D>(const T&) is defined
//! \tparam U Type for which double GetLatitude<D>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \return bool Whether "a" does not begin after "b" in the given direction
template <Direction D, typename T, typename U>
bool lessEqual(const T &a, const U &b)
{
    return !greater<D>(a, b);
}

//! Returns whether the first object does not begin after the second in the given direction.
//!
//! \tparam T Type for which double GetLatitude<D>(const T&) is defined
//! \tparam U Type for which double GetLatitude<D>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \param d Downstream or Upstream
//! \return bool Whether "a" does not begin after "b" in the given direction
template <typename T, typename U>
bool lessEqual(const T &a, const U &b, Direction d)
{
    return d == Direction::Upstream ? lessEqual<Direction::Upstream>(a, b) : lessEqual<Direction::Downstream>(a, b);
}

//! Returns whether the first object does not begin after the second in the given orientation.
//!
//! \tparam O Forwards or Backwards
//! \tparam T Type for which double GetLatitude<O>(const T&) is defined
//! \tparam U Type for which double GetLatitude<O>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \return bool Whether "a" does not begin after "b" in the given orientation
template <Orientation O, typename T, typename U>
bool lessEqual(const T &a, const U &b)
{
    return !greater<O>(a, b);
}

//! Returns whether the first object does not begin after the second in the given orientation.
//!
//! \tparam T Type for which double GetLatitude<Orientation>(const T&) is defined
//! \tparam U Type for which double GetLatitude<Orientation>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \param o Forwards or Backwards
//! \return bool Whether "a" does not begin after "b" in the given orientation
template <typename T, typename U>
bool lessEqual(const T &a, const U &b, Orientation o)
{
    return o == Orientation::Backwards ? lessEqual<Orientation::Backwards>(a, b) : lessEqual<Orientation::Forwards>(a, b);
}

//! Returns whether the first object does not begin before the second in the given direction.
//!
//! \tparam D Downstream or Upstream
//! \tparam T Type for which double GetLatitude<D>(const T&) is defined
//! \tparam U Type for which double GetLatitude<D>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \return bool Whether "a" does not begin before "b" in the given direction
template <Direction D, typename T, typename U>
bool greaterEqual(const T &a, const U &b)
{
    return !less<D>(a, b);
}

//! Returns whether the first object does not begin before the second in the given direction.
//!
//! \tparam T Type for which double GetLatitude<Direction>(const T&) is defined
//! \tparam U Type for which double GetLatitude<Direction>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \param d Downstream or Upstream
//! \return bool Whether "a" does not begin before "b" in the given direction
template <typename T, typename U>
bool greaterEqual(const T &a, const U &b, Direction d)
{
    return d == Direction::Upstream ? greaterEqual<Direction::Upstream>(a, b) : greaterEqual<Direction::Downstream>(a, b);
}

//! Returns whether the first object does not begin before the second in the given orientation.
//!
//! \tparam O Forwards or Backwards
//! \tparam T Type for which double GetLatitude<O>(const T&) is defined
//! \tparam U Type for which double GetLatitude<O>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \return bool Whether "a" does not begin before "b" in the given orientation
template <Orientation O, typename T, typename U>
bool greaterEqual(const T &a, const U &b)
{
    return !less<O>(a, b);
}

//! Returns whether the first object does not begin before the second in the given orientation.
//!
//! \tparam T Type for which double GetLatitude<Orientation>(const T&) is defined
//! \tparam U Type for which double GetLatitude<Orientation>(const U&) is defined
//! \param a Entity of type T
//! \param b Entity of type U
//! \param o Forwards or Backwards
//! \return bool Whether "a" does not begin before "b" in the given orientation
template <typename T, typename U>
bool greaterEqual(const T &a, const U &b, Orientation o)
{
    return o == Orientation::Backwards ? greaterEqual<Orientation::Backwards>(a, b) : greaterEqual<Orientation::Forwards>(a, b);
}

//! Returns the signed distance from the furthest latitude of the first argument to the nearest latitude
//! of the second argument according to the given direction.
//!
//! \tparam D Downstream or Upstream
//! \tparam T
//! \tparam U
//! \param from
//! \param to
//! \return constexpr double
template <Direction D, typename T, typename U>
constexpr double GetLatitudeDifference(const T &from, const U &to)
{
    if constexpr (D == Direction::Upstream)
    {
        return GetLatitude<Direction::Downstream>(from) - GetLatitude<Direction::Upstream>(to);
    }
    else
    {
        return GetLatitude<Direction::Downstream>(to) - GetLatitude<Direction::Upstream>(from);
    }
}

//! Returns the signed distance from the furthest latitude of the first argument to the nearest latitude
//! of the second argument according to the given direction.
//!
//! \tparam T
//! \tparam U
//! \param from
//! \param to
//! \param d Downstream or Upstream
//! \return constexpr double
template <typename T, typename U>
constexpr double GetLatitudeDifference(const T &from, const U &to, Direction d)
{
    return d == Direction::Upstream ? GetLatitudeDifference<Direction::Upstream>(from, to) : GetLatitudeDifference<Direction::Downstream>(from, to);
}

//! Increases the given latitude in the given direction by the given amount (value reduction if Upstream)
//!
//! \tparam D Downstream or Upstream
//! \param latitude
//! \param amount
//! \return constexpr double
template <Direction D>
constexpr double IncreaseLatitude(double latitude, double amount)
{
    if constexpr (D == Direction::Upstream)
    {
        return latitude - amount;
    }
    else
    {
        return latitude + amount;
    }
}

//! Increases the given latitude in the given direction by the given amount (value reduction if Upstream)
//!
//! \param latitude
//! \param amount
//! \param d Downstream or Upstream
//! \return constexpr double
constexpr double IncreaseLatitude(double l, double a, Direction d)
{
    return d == Direction::Upstream ? IncreaseLatitude<Direction::Upstream>(l, a) : IncreaseLatitude<Direction::Downstream>(l, a);
}
} // namespace osiql
