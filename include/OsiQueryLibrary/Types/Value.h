/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Value of a specific SI unit

#include <osi3/osi_trafficsign.pb.h>

namespace osiql {
//! \brief SI units supported by osiql::TrafficSign and osiql::RoadMarking
class Value
{
public:
    Value() = default;

    //! Constructs an SI Value from the given osi3::TrafficSignValue
    //!
    //! \param value
    Value(const osi3::TrafficSignValue &value);

    //! implicit cast from Value to its numerical component
    //!
    //! \return double
    operator double() const;

    //! \brief The raw numerical component of this value
    double value{std::numeric_limits<double>::quiet_NaN()};

    //! \brief The unit or type of measurement of a value
    enum class Unit
    {
        None = 0,
        Kilogram,
        MeterPerSecond,
        Meter,
        Percentage,
        Second
    };
    //! \brief The unit or type of measurement of this value
    Unit unit{Unit::None};

private:
    //! \brief Used to convert various osi3 traffic sign values to SI units
    static const std::map<osi3::TrafficSignValue_Unit, std::pair<double, Unit>> units;
};

std::ostream &operator<<(std::ostream &, Value::Unit);

namespace detail {
constexpr const std::array<std::string_view, 6> unitToString{
    "None",
    "Kilogram",
    "MeterPerSecond",
    "Meter",
    "Percentage",
    "Second" //
};
}
} // namespace osiql
