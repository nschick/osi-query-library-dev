/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Rearranged member accessor of global coordinates

#include <array>
#include <cstddef>
#include <iostream>

#include "OsiQueryLibrary/Trait/Common.h"

namespace osiql {
template <typename T, size_t Dimensions>
struct Vector;

namespace detail {
template <typename T, size_t Dimensions>
struct Vector
{
    using type = osiql::Vector<T, Dimensions>;
};

template <typename T>
struct Vector<T, 1>
{
    using type = T;
};

template <typename T, size_t Dimensions>
using vector_t = typename Vector<T, Dimensions>::type;

template <typename T, size_t... Is>
struct Swizzle
{
    vector_t<T, sizeof...(Is)> &operator=(const vector_t<T, sizeof...(Is)> &rhs);

    constexpr operator vector_t<T, sizeof...(Is)>() const;

    template <size_t... Js>
    friend constexpr bool operator==(const Swizzle<T, Is...> &lhs, const Swizzle<T, Js...> &rhs)
    {
        return ((lhs.v[Is] == rhs.v[Js]) && ...);
    }

    constexpr const T &operator[](size_t i) const;

private:
    T v[max(std::index_sequence<Is...>{}) + 1];
};

template <typename T, size_t I, size_t... Is>
std::ostream &operator<<(std::ostream &os, const Swizzle<T, I, Is...> &swizzle);
} // namespace detail
} // namespace osiql

namespace osiql::detail {
template <typename T, size_t... Is>
vector_t<T, sizeof...(Is)> &Swizzle<T, Is...>::operator=(const vector_t<T, sizeof...(Is)> &rhs)
{
    if constexpr (sizeof...(Is) == 1)
    {
        constexpr size_t indices[] = {Is...};
        v[indices[0]] = rhs;
        return *reinterpret_cast<vector_t<T, sizeof...(Is)> *>(this);
    }
    else
    {
        constexpr size_t indices[] = {Is...};
        for (size_t i{0u}; i < sizeof...(Is); ++i)
        {
            v[indices[i]] = rhs[i];
        }
        return *reinterpret_cast<vector_t<T, sizeof...(Is)> *>(this);
    }
}

template <typename T, size_t... Is>
constexpr const T &Swizzle<T, Is...>::operator[](size_t i) const
{
    return v[i];
}

template <typename T, size_t... Is>
constexpr Swizzle<T, Is...>::operator vector_t<T, sizeof...(Is)>() const
{
    return vector_t<T, sizeof...(Is)>(v[Is]...);
}

template <typename T, size_t I, size_t... Is>
std::ostream &operator<<(std::ostream &os, const Swizzle<T, I, Is...> &swizzle)
{
    if constexpr (sizeof...(Is) == 0)
    {
        return os << swizzle[I];
    }
    else
    {
        os << '(' << swizzle[I];
        ((os << ", " << swizzle[Is]), ...);
        return os << ')';
    }
}
} // namespace osiql::detail
