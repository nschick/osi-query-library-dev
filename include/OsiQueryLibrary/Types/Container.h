/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Aliases for standard library container types with custom comparators

#include <type_traits> // is_transparent

#include "OsiQueryLibrary/Trait/Id.h"

namespace osiql {

template <typename T>
using Container = google::protobuf::RepeatedPtrField<T>;

template <typename T>
struct is_smart_ptr : std::false_type
{
};
template <typename T>
struct is_smart_ptr<std::shared_ptr<T>> : std::true_type
{
};
template <typename T>
struct is_smart_ptr<std::unique_ptr<T>> : std::true_type
{
};

//! \brief Custom comparator of a given comparable type or pointers of it
struct Comparator
{
    using is_transparent = std::true_type;

    //! Returns whether the id of the object pointed to by the left-hand side is less than right-hand side
    //!
    //! \tparam T
    //! \param lhs
    //! \param rhs
    //! \return bool
    template <typename T>
    bool operator()(const T &lhs, id_t<T> rhs) const
    {
        if constexpr (std::is_pointer_v<T> || is_smart_ptr<T>::value)
        {
            return lhs->GetId() < rhs;
        }
        else
        {
            return lhs.GetId() < rhs;
        }
    }

    //! Returns whether the left-hand side is less than the id of the object pointed to by the right-hand side
    //!
    //! \tparam T
    //! \param lhs
    //! \param rhs
    //! \return bool
    template <typename T>
    bool operator()(id_t<T> lhs, const T &rhs) const
    {
        if constexpr (std::is_pointer_v<T> || is_smart_ptr<T>::value)
        {
            return lhs < rhs->GetId();
        }
        else
        {
            return lhs < rhs.GetId();
        }
    }
    //! Returns whether the object pointed to by the left-hand side is less than
    //! the object pointed to by the right-hand side.
    //!
    //! \tparam T Comparable type
    //! \param lhs
    //! \param rhs
    //! \return bool
    template <typename T>
    bool operator()(const T &lhs, const T &rhs) const
    {
        if constexpr (std::is_pointer_v<T> || is_smart_ptr<T>::value)
        {
            return *lhs < *rhs;
        }
        else
        {
            return lhs < rhs;
        }
    }
};

//! A std::set of shared pointers of the given type
//!
//! \tparam T Pointer of a type that supports the less-than operator (<) and has the member function id_t<T> GetId()
template <typename T>
using Set = std::set<T, Comparator>;

//! \brief Generic equality comparator for unordered containers
struct Equal
{
    using is_transparent = std::true_type;

    template <typename T, typename U>
    bool operator()(const T &lhs, const U &rhs) const
    {
        if constexpr (HAS_MEMBER(T, GetId()) && HAS_MEMBER(U, GetId()))
        {
            return lhs.GetId() == rhs.GetId();
        }
        else if constexpr (HAS_MEMBER(T, GetId()) && HAS_MEMBER(U, GetEntity()))
        {
            return lhs.GetId() == rhs.GetEntity().GetId();
        }
        else if constexpr (HAS_MEMBER(T, GetEntity()) && HAS_MEMBER(U, GetEntity()))
        {
            return lhs.GetEntity().GetId() == rhs.GetEntity().GetId();
        }
        else if constexpr (HAS_MEMBER(T, GetEntity()) && HAS_MEMBER(U, GetId()))
        {
            return lhs.GetEntity().GetId() == rhs.GetId();
        }
        else if constexpr (HAS_MEMBER(T, GetEntity()))
        {
            return lhs.GetEntity().GetId() == rhs;
        }
        else if constexpr (HAS_MEMBER(U, GetId()))
        {
            return lhs == rhs.GetId();
        }
        else if constexpr (HAS_MEMBER(U, GetEntity()))
        {
            return lhs == rhs.GetEntity().GetId();
        }
        else
        {
            return lhs == rhs;
        }
    }
};

//! \brief Generic hash
template <typename Identifier>
struct Hash
{
    using transparent_key_equal = Equal;

    template <typename T>
    Identifier operator()(const T &object) const
    {
        if constexpr (HAS_MEMBER(T, GetId()))
        {
            return object.GetId();
        }
        else if constexpr (HAS_MEMBER(T, GetEntity()))
        {
            return object.GetEntity().GetId();
        }
        else
        {
            return object;
        }
    }
};
} // namespace osiql
