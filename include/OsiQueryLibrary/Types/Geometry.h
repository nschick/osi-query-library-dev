/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Alias and output operator for geometrical types (box & polygon)

#include <iostream>
#include <optional>
//
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/ring.hpp>

#include "OsiQueryLibrary/Point/Vector2d.h"

namespace osiql {
//! \brief Infinite 2D line defined by two points
struct Line
{
    Vector2d start;
    Vector2d end;

    //! Returns the intersection of this line with the given line or a nullopt if they are parallel.
    //!
    //! \return std::optional<Vector2d>
    std::optional<Vector2d> GetIntersection(const Line &) const;
};

std::ostream &operator<<(std::ostream &, const Line &);

// axis-aligned rectangle used as a bounding box of lane segments in a osiql::Query's RTree
using Box = boost::geometry::model::box<Vector2d>;

std::ostream &operator<<(std::ostream &, const Box &);

// Open, counter-clockwise simple polygon
using Polygon = boost::geometry::model::ring<Vector2d, false, false>;

std::ostream &operator<<(std::ostream &, const Polygon &);

namespace detail {
//! Returns the equivalent to the given angle within the range (-pi, pi]
//!
//! \param angle
//! \return double
double SetAngleToValidRange(const double angle);
} // namespace detail
} // namespace osiql
