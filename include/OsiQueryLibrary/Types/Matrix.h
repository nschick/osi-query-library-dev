/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief 2D-Grid of values that represents a linear transformation

#include <algorithm>
#include <array>
#include <tuple> // std::ignore

#include "OsiQueryLibrary/Point/Vector.h"
#include "OsiQueryLibrary/Trait/Common.h"

// NOTE: C++23 multidimensional subscript would be nice here
namespace osiql {
template <size_t N>
struct Matrix;

template <size_t N>
[[nodiscard]] bool operator==(const Matrix<N> &lhs, const Matrix<N> &rhs); // C++20 std::array constexpr operator== would be nice

template <size_t N>
using Row = std::array<double, N>;

//! Square matrix (nxn) used for transformations.
//!
//! \tparam N
template <size_t N>
struct Matrix
{
    constexpr Matrix();
    constexpr Matrix(std::array<Row<N>, N> &&values);

    constexpr Matrix(const Matrix<N - 1> &submatrix);

    constexpr Row<N> &operator[](size_t i);
    constexpr const Row<N> &operator[](size_t i) const;

    friend bool operator==<>(const Matrix<N> &lhs, const Matrix<N> &rhs);

    constexpr void operator+=(const Matrix<N> &rhs);
    constexpr void operator-=(const Matrix<N> &rhs);
    constexpr void operator*=(const Matrix<N> &rhs);
    constexpr void operator*=(double rhs);
    constexpr void operator/=(double rhs);

    constexpr void Translate(const Vector<double, N - 1> &displacement);

    //! Combines this matrix with a rotation matrix generated from the given arguments.
    //! See RotationMatrix<N>(Args...) overloads.
    //!
    //! \tparam Args
    //! \param args
    template <typename... Args>
    constexpr void Rotate(Args... args);

private:
    std::array<Row<N>, N> m;
};

//! Returns the cofactor matrix of the given matrix
//!
//! \tparam N
//! \param matrix
//! \return constexpr Matrix<N>
template <size_t N>
[[nodiscard]] constexpr Matrix<N> Cof(const Matrix<N> &matrix);

//! Returns the determinant of the given matrix
//!
//! \tparam N
//! \param matrix
//! \return constexpr double
template <size_t N>
[[nodiscard]] constexpr double Det(const Matrix<N> &matrix);

//! Returns the inverse of the given matrix
//!
//! \tparam N
//! \param matrix
//! \return constexpr Matrix<N>
template <size_t N>
[[nodiscard]] constexpr Matrix<N> Inv(const Matrix<N> &matrix);

//! Returns the cofactor matrix of the given matrix
//!
//! \tparam N
//! \param matrix
//! \return constexpr Matrix<N>
template <size_t Row, size_t Col, size_t N>
[[nodiscard]] constexpr Matrix<N - 1> Submatrix(const Matrix<N> &matrix);

template <size_t N>
[[nodiscard]] constexpr Matrix<N> Transpose(const Matrix<N> &matrix);

template <size_t N>
[[nodiscard]] bool operator!=(const Matrix<N> &lhs, const Matrix<N> &rhs);

template <size_t N>
[[nodiscard]] constexpr Matrix<N> operator+(const Matrix<N> &lhs, const Matrix<N> &rhs);

template <size_t N>
[[nodiscard]] constexpr Matrix<N> operator-(const Matrix<N> &lhs, const Matrix<N> &rhs);

template <size_t N>
[[nodiscard]] constexpr Matrix<N> operator*(const Matrix<N> &lhs, const Matrix<N> &rhs);

template <size_t N>
[[nodiscard]] constexpr Matrix<N + 1> operator*(const Matrix<N + 1> &lhs, const Matrix<N> &rhs);

template <size_t N>
[[nodiscard]] constexpr Matrix<N + 1> operator*(const Matrix<N> &lhs, const Matrix<N + 1> &rhs);

template <size_t N>
[[nodiscard]] constexpr Matrix<N> operator*(const Matrix<N> &lhs, double rhs);

template <size_t N>
constexpr Matrix<N + 1> TranslationMatrix(const Vector<double, N> &displacement);

//! Returns a transformation matrix that rotates points around the coordinate origin by the given angle
//!
//! \param angle
//! \return Matrix<2>
Matrix<2> RotationMatrix(double angle);

//! Returns a transformation matrix that rotates and then translates any point
//! it is multiplied with by the given angle and origin.
//!
//! \param angle counter-clockwise right-handed ascending angle in radians within [-π, π]
//! around the z-axis parallel passing through the given origin
//! \param origin The point around which the rotation will occur
//! \return Matrix<3>
Matrix<3> RotationMatrix(double angle, const Vector<double, 2> &origin);

//! Returns a transformation matrix that applies rotations around the z-axis (yaw in OSI),
//! then the y-axis (pitch in OSI) and then the x-axis (roll in OSI).
//!
//! \param rotation counter-clockwise right-handed ascending changes in radians within [-π, π] around the respective axis
//! \return Matrix<3>
Matrix<3> RotationMatrix(const Vector<double, 3> &rotation);

//! Returns a transformation matrix that rotates by the given angle around the given axis
//!
//! \param angle
//! \param axis
//! \return Matrix<3>
Matrix<3> RotationMatrix(double angle, const Vector<double, 3> &axis);

//! Returns an antimetric matrix of a given vector v representing the cross product with v.
//!
//! \param v
//! \return Matrix<3>
//!
//! \param v Components in radians
//! \return Matrix<3>
Matrix<3> AntimetricMatrix(const Vector<double, 3> &v);

template <size_t N>
std::ostream &operator<<(std::ostream &os, const Matrix<N> &matrix);
} // namespace osiql

namespace osiql {

namespace detail { // clang-format off
template <size_t Row, size_t Column>
constexpr double GetIdentityCell()
{
    if constexpr(Row == Column) {
        return 1.0;
    } else {
        return 0.0;
    }
}

template <size_t I, size_t... Is>
constexpr Row<sizeof...(Is)> GetIdentityRow() {
    return Row<sizeof...(Is)>{GetIdentityCell<I, Is>()...};
}

template <size_t... Is>
constexpr std::array<Row<sizeof...(Is)>, sizeof...(Is)> GetIdentityMatrix(std::index_sequence<Is...>)
{
    return {GetIdentityRow<Is, Is...>()...};
}

template <size_t... Is>
const std::array<Row<sizeof...(Is)+1>, sizeof...(Is)+1> Promote(const Matrix<sizeof...(Is)>& submatrix, std::index_sequence<Is...>)
{
    return {[&m = submatrix](auto row) -> Row<sizeof...(Is)+1> {
        return {(m[row][Is])..., 0.0};
    }(Is)..., {(std::ignore = Is, 0.0)..., 1.0}};
} 

template <size_t Row, size_t Column, size_t... Is>
constexpr double GetMultipliedCell(const Matrix<sizeof...(Is)> &lhs, const Matrix<sizeof...(Is)> &rhs)
{
    return ((lhs[Row][Is] * rhs[Is][Column]) + ...);
}

template <size_t Row, size_t Column, size_t... Is>
constexpr double GetMultipliedCell(const Matrix<sizeof...(Is) + 1> &lhs, const Matrix<sizeof...(Is)> &rhs)
{
    return ((lhs[Row][Is] * rhs[Is][Column]) + ...);
}

template <size_t Row, size_t Column, size_t... Is>
constexpr double GetMultipliedCell(const Matrix<sizeof...(Is)> &lhs, const Matrix<sizeof...(Is) + 1> &rhs)
{
    return ((lhs[Row][Is] * rhs[Is][Column]) + ...);
}

template <size_t I, size_t... Is>
constexpr Row<sizeof...(Is)> GetMultipliedRow(const Matrix<sizeof...(Is)>& lhs, const Matrix<sizeof...(Is)>& rhs)
{
    return Row<sizeof...(Is)>{(GetMultipliedCell<I, Is, Is...>(lhs, rhs))...};
}

template <size_t I, size_t... Is>
constexpr Row<sizeof...(Is)+1> GetMultipliedRow(const Matrix<sizeof...(Is)+1>& lhs, const Matrix<sizeof...(Is)>& rhs)
{
    return Row<sizeof...(Is)+1>{(GetMultipliedCell<I, Is, Is...>(lhs, rhs))..., lhs[I][sizeof...(Is)]};
}

template <size_t I, size_t... Is>
constexpr Row<sizeof...(Is)+1> GetMultipliedRow(const Matrix<sizeof...(Is)>& lhs, const Matrix<sizeof...(Is)+1>& rhs)
{
    return Row<sizeof...(Is)+1>{(GetMultipliedCell<I, Is, Is...>(lhs, rhs))..., rhs[I][sizeof...(Is)]};
}

template <size_t... Is>
constexpr std::array<Row<sizeof...(Is)>, sizeof...(Is)> Multiply(const Matrix<sizeof...(Is)> &lhs, const Matrix<sizeof...(Is)> &rhs, std::index_sequence<Is...>)
{
    return {GetMultipliedRow<Is, Is...>(lhs, rhs)...};
}

template <size_t... Is>
constexpr std::array<Row<sizeof...(Is) + 1>, sizeof...(Is) + 1> Multiply(const Matrix<sizeof...(Is) + 1> &lhs, const Matrix<sizeof...(Is)> &rhs, std::index_sequence<Is...>)
{
    constexpr const size_t N{sizeof...(Is)};
    return {GetMultipliedRow<Is, Is...>(lhs, rhs)..., Row<N + 1>{(lhs[N][Is])..., lhs[N][N]}};
}

template <size_t... Is>
constexpr std::array<Row<sizeof...(Is) + 1>, sizeof...(Is) + 1> Multiply(const Matrix<sizeof...(Is)> &lhs, const Matrix<sizeof...(Is)+1> &rhs, std::index_sequence<Is...>)
{
    constexpr const size_t N{sizeof...(Is)};
    return {GetMultipliedRow<Is, Is...>(lhs, rhs)..., Row<N + 1>{(rhs[N][Is])..., rhs[N][N]}};
}

template <typename Point, size_t N, size_t... Is>
constexpr Point Multiply(const Matrix<N> &matrix, const Point &vector, std::index_sequence<Is...>)
{
    if constexpr(N > sizeof...(Is)) {
        return {[&](auto I) {
            return ((vector[Is] * matrix[I][Is]) + ...) + matrix[I].back();
        }(Is)...};
    } else {
        return {[&](auto I) {
            return ((vector[Is] * matrix[I][Is]) + ...);
        }(Is)...};
    }
}

template <size_t Row, size_t Column, size_t N>
constexpr double GetCofactorCell(const Matrix<N>& m)
{
    if constexpr(Row % 2 == Column % 2) {
        return Det(Submatrix<Row, Column>(m));
    } else {
        return -Det(Submatrix<Row, Column>(m));
    }
}

template <size_t I, size_t... Columns>
constexpr Row<sizeof...(Columns)> GetCofactorRow(const Matrix<sizeof...(Columns)>& m)
{
    return {GetCofactorCell<I+1, Columns+1>(m)...};
}

template <size_t... Is>
constexpr std::array<Row<sizeof...(Is)>, sizeof...(Is)> GetCofactorMatrix(const Matrix<sizeof...(Is)>& m, std::index_sequence<Is...>)
{
    return {GetCofactorRow<Is, Is...>(m)...};
}

template <size_t... Rows, size_t... Cols, std::enable_if_t<sizeof...(Rows) == sizeof...(Cols), bool> = true>
constexpr std::array<Row<sizeof...(Cols)>, sizeof...(Rows)> GetSubmatrix(const Matrix<sizeof...(Rows)+1>& m, std::index_sequence<Rows...>, std::index_sequence<Cols...>)
{
    return {[&](auto row) -> Row<sizeof...(Cols)> {
        return {m[row][Cols]...};
    }(Rows)...};
}

template <size_t I, size_t N>
constexpr double GetDeterminantComponent(const Matrix<N>&m)
{
    if constexpr(I % 2)
    {
        return -m[0][I];
    } else {
        return m[0][I];
    }
}

template <size_t... Is>
constexpr double GetDeterminant(const Matrix<sizeof...(Is)> &m, std::index_sequence<Is...>)
{
    return ((GetDeterminantComponent<Is>(m) * Det(Submatrix<1, Is + 1>(m))) + ...);
}

template <size_t... Is>
std::array<Row<sizeof...(Is)>, sizeof...(Is)> GetTransposed(const Matrix<sizeof...(Is)> &m, std::index_sequence<Is...>)
{
    return {[&m](auto i) -> Row<sizeof...(Is)> {
        return {m[Is][i]...};
    }(Is)...};
}

template <size_t I, size_t...Is>
constexpr Row<sizeof...(Is)+1> GetTranslationRow(const osiql::Vector<double, sizeof...(Is)>& displacement)
{
    return Row<sizeof...(Is)+1>{GetIdentityCell<I, Is>()..., displacement[I]};
}

template <size_t... Is>
constexpr std::array<Row<sizeof...(Is)+1>, sizeof...(Is)+1> GetTranslationMatrix(const osiql::Vector<double, sizeof...(Is)> &displacement, std::index_sequence<Is...>)
{
    return {GetTranslationRow<Is, Is...>(displacement)..., Row<sizeof...(Is)+1>{(std::ignore = Is, 0.0)..., 1.0}};
}
// clang-format on
} // namespace detail

template <size_t N>
constexpr Matrix<N>::Matrix() :
    m(detail::GetIdentityMatrix(std::make_index_sequence<N>()))
{
}

template <size_t N>
constexpr Matrix<N>::Matrix(std::array<Row<N>, N> &&values) :
    m(std::forward<std::array<Row<N>, N>>(values))
{
}

template <size_t N>
constexpr Matrix<N>::Matrix(const Matrix<N - 1> &submatrix) :
    m(detail::Promote(submatrix, std::make_index_sequence<N - 1>()))
{
}

template <size_t N>
constexpr Row<N> &Matrix<N>::operator[](size_t i)
{
    return m[i];
}

template <size_t N>
constexpr const Row<N> &Matrix<N>::operator[](size_t i) const
{
    return m[i];
}

template <size_t N>
constexpr void Matrix<N>::operator+=(const Matrix<N> &rhs)
{
    constexpr_for<0, N>([&](size_t row) {
        constexpr_for<0, N>([&, row](size_t column) {
            m[row][column] += rhs[row][column];
        });
    });
}

template <size_t N>
constexpr void Matrix<N>::operator-=(const Matrix<N> &rhs)
{
    constexpr_for<0, N>([&](size_t row){
        constexpr_for<0, N>([&, row](size_t column){
            m[row][column] -= rhs[row][column];
        });
    });
}

template <size_t N>
constexpr void Matrix<N>::operator*=(const Matrix<N> &rhs)
{
    *this = *this * rhs;
}

template <size_t N>
constexpr void Matrix<N>::operator*=(double rhs)
{
    constexpr_for<0, N>([&](auto row) {
        constexpr_for<0, N>([&](auto col) {
            m[row][col] *= rhs;
        });
    });
}

template <size_t N>
constexpr void Matrix<N>::operator/=(double rhs)
{
    constexpr_for<0, N>([&](auto row) {
        constexpr_for<0, N>([&](auto col) {
            m[row][col] /= rhs;
        });
    });
}

template <size_t N>
constexpr Matrix<N + 1> TranslationMatrix(const Vector<double, N> &displacement)
{
    return detail::GetTranslationMatrix(displacement, std::make_index_sequence<N>());
}

template <size_t N>
constexpr void Matrix<N>::Translate(const Vector<double, N - 1> &displacement)
{
    *this *= TranslationMatrix(displacement);
    // TODO: Optimize to avoid predictable zero multiplication
}

template <size_t N>
template <typename... Args>
constexpr void Matrix<N>::Rotate(Args... args)
{
    *this *= RotationMatrix(std::forward<Args>(args)...);
    // TODO: Optimize to avoid predictable zero multiplication
}

template <size_t N>
constexpr Matrix<N> Cof(const Matrix<N> &matrix)
{
    return detail::GetCofactorMatrix(matrix, std::make_index_sequence<N>());
}

template <size_t Row, size_t Column, size_t N>
constexpr Matrix<N - 1> Submatrix(const Matrix<N> &m)
{
    return detail::GetSubmatrix(m, make_skip_sequence<Row - 1, N>(), make_skip_sequence<Column - 1, N>());
}

template <size_t N>
constexpr double Det(const Matrix<N> &m)
{
    if constexpr (N == 2)
    {
        return m[0][0]*m[1][1] - m[0][1]*m[1][0];
    } else {
        // TODO: PERFORMANCE! Replace Laplace expansions with Gaussian elimination (Gauss-Jordan)!!!
        return detail::GetDeterminant(m, std::make_index_sequence<N>());
    }
}

template <size_t N>
constexpr Matrix<N> Inv(const Matrix<N> &m)
{
    return Transpose(Cof(m)) / Det(m);
}

template <size_t N>
constexpr Matrix<N> Transpose(const Matrix<N> &m)
{
    return detail::GetTransposed(m, std::make_index_sequence<N>());
}

template <size_t N>
bool operator==(const Matrix<N> &lhs, const Matrix<N> &rhs)
{
    return lhs.m == rhs.m;
}

template <size_t N>
bool operator!=(const Matrix<N> &lhs, const Matrix<N> &rhs)
{
    return !(lhs == rhs);
}

template <size_t N>
constexpr Matrix<N> operator+(const Matrix<N> &lhs, const Matrix<N> &rhs)
{
    Matrix<N> matrix(lhs);
    matrix += rhs;
    return matrix;
}

template <size_t N>
constexpr Matrix<N> operator-(const Matrix<N> &lhs, const Matrix<N> &rhs)
{
    Matrix<N> matrix(lhs);
    matrix -= rhs;
    return matrix;
}

template <size_t N>
constexpr Matrix<N> operator*(const Matrix<N> &lhs, const Matrix<N> &rhs)
{
    return detail::Multiply(lhs, rhs, std::make_index_sequence<N>());
}

template <size_t N>
constexpr Matrix<N + 1> operator*(const Matrix<N + 1> &lhs, const Matrix<N> &rhs)
{
    return detail::Multiply(lhs, rhs, std::make_index_sequence<N>());
}

template <size_t N>
constexpr Matrix<N + 1> operator*(const Matrix<N> &lhs, const Matrix<N + 1> &rhs)
{
    return detail::Multiply(lhs, rhs, std::make_index_sequence<N>());
}

template <typename Point, size_t N, std::enable_if_t<detail::is_vector<Point>::value && std::is_base_of_v<Vector<double, N - 2>, Point>, bool> = true>
constexpr Point operator*(const Matrix<N> &lhs, const Point &rhs)
{
    return Multiply(lhs, rhs, std::make_index_sequence<N - 2>());
}

template <typename Point, size_t N, std::enable_if_t<detail::is_vector<Point>::value && std::is_base_of_v<Vector<double, N - 1>, Point>, bool> = true>
constexpr Point operator*(const Matrix<N> &lhs, const Point &rhs)
{
    return Multiply(lhs, rhs, std::make_index_sequence<N - 1>());
}

template <typename Point, size_t N, std::enable_if_t<detail::is_vector<Point>::value && std::is_base_of_v<Vector<double, N>, Point>, bool> = true>
constexpr Point operator*(const Matrix<N> &lhs, const Point &rhs)
{
    return Multiply(lhs, rhs, std::make_index_sequence<N>());
}

template <size_t N>
constexpr Matrix<N> operator*(const Matrix<N> &lhs, double rhs)
{
    Matrix<N> result(lhs);
    result *= rhs;
    return result;
}

template <size_t N>
constexpr Matrix<N> operator/(const Matrix<N> &lhs, double rhs)
{
    Matrix<N> result(lhs);
    result /= rhs;
    return result;
}

template <size_t N>
std::ostream &operator<<(std::ostream &os, const Matrix<N> &matrix)
{
    os << "{\n";
    constexpr_for<0, N>([&](auto row) {
        os << '[' << matrix[row][0];
        constexpr_for<1, N>([&](auto col) {
            os << ", " << matrix[row][col];
        });
        os << "]\n";
    });
    return os << '}';
}
} // namespace osiql
