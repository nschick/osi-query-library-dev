/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Interval from a minimum up to and including a maximum value

#include <iostream>

namespace osiql {
static inline constexpr double MAX_SEARCH_DISTANCE{5000.0};

template <typename T>
struct Interval
{
    T min;
    T max;
};

template <typename T>
std::ostream &operator==(std::ostream &, const Interval<T> &);

template <typename T>
std::ostream &operator<<(std::ostream &, const Interval<T> &);
} // namespace osiql

namespace osiql {
template <typename T>
bool operator==(const Interval<T> &lhs, const Interval<T> &rhs)
{
    return lhs.min == rhs.min && lhs.max == rhs.max;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Interval<T> &interval)
{
    return os << '[' << interval.min << ", " << interval.max << ']';
}
} // namespace osiql
