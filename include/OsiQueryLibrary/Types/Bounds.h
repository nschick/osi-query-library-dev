/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Local bounds of an object

#include <limits>

#include "OsiQueryLibrary/Component/Identifiable.h"
#include "OsiQueryLibrary/Point/Coordinates.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Direction.h"

namespace osiql {
//! s,t-coordinate bounds linked to an arbitrary entity
//!
//! \tparam T Can be any type but is most commonly either osiql::Lane or osiql::Road
template <typename T>
struct LocalBounds
{
    //! Constructs fully defined LocalBounds from its components
    //!
    //! \param min
    //! \param max
    //! \param entity
    LocalBounds(Coordinates<Road> min, Coordinates<Road> max, T &entity);

    //! Constructs undefined bounds but with a given entity
    //!
    //! \param entity
    LocalBounds(T &entity);

    //! Constructs a copy of the given bounds but with a different entity
    //!
    //! \tparam U
    //! \param bounds
    //! \param entity
    template <typename U>
    LocalBounds(const LocalBounds<U> &bounds, T &entity);

    //! Returns the entity linked to these local bounds
    //!
    //! \return const T&

    std::add_const_t<T> &GetEntity() const;

    //! Minimally widens the minimum and maximum s- and t-coordinates of these bounds
    //! such that the given s- and t-coordinate lies inside them.
    //!
    //! \param localCoordinates
    void Add(const Coordinates<Road> &localCoordinates);

    //! Minimally widens the minimum and maximum s- and t-coordinates of these bounds
    //! such that the given bounds lie on/inside them.
    //!
    //! \param bounds
    template <typename U>
    void Add(const LocalBounds<U> &bounds);

    //! \brief Smallest s- & t-coordinate touched by the bounds
    Coordinates<Road> min{std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity()};

    //! \brief Largest s- & t-coordinate touched by the bounds
    Coordinates<Road> max{-std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity()};

private:
    //! \brief The entity linked to the local bounds
    std::reference_wrapper<T> entity;

public:
    template <typename = std::enable_if<!std::is_const_v<T>>>
    T &GetEntity()
    {
        return entity;
    }
};

const Lane &GetLane(const LocalBounds<Lane> &bounds);
const Lane &GetLane(LocalBounds<const Lane> &bounds);
Lane &GetLane(LocalBounds<Lane> &bounds);

template <typename T>
bool operator<(const LocalBounds<T> &lhs, const LocalBounds<T> &rhs);
template <typename T>
bool operator<(const LocalBounds<T> &lhs, const T &rhs);
template <typename T>
bool operator<(const T &lhs, const LocalBounds<T> &rhs);
template <typename T>
bool operator<(const LocalBounds<T> &lhs, const id_t<T> &rhs);
template <typename T>
bool operator<(const id_t<T> &lhs, const LocalBounds<T> &rhs);

template <typename T, typename U>
bool operator==(const LocalBounds<T> &lhs, const LocalBounds<U> &rhs);
template <typename T, typename U>
bool operator!=(const LocalBounds<T> &lhs, const LocalBounds<U> &rhs);

template <typename T>
std::ostream &operator<<(std::ostream &os, const LocalBounds<T> &bounds);
} // namespace osiql

// ----- Template definitions:

namespace osiql {
template <typename T>
LocalBounds<T>::LocalBounds(Coordinates<Road> min, Coordinates<Road> max, T &entity) :
    min(min), max(max), entity(entity)
{
}

template <typename T>
LocalBounds<T>::LocalBounds(T &entity) :
    entity(entity)
{
}

template <typename T>
template <typename U>
LocalBounds<T>::LocalBounds(const LocalBounds<U> &bounds, T &entity) :
    min(bounds.min), max(bounds.max), entity(entity)
{
}

template <typename T>
std::add_const_t<T> &LocalBounds<T>::GetEntity() const
{
    return entity;
}

template <typename T>
void LocalBounds<T>::Add(const Coordinates<Road> &point)
{
    min.latitude = std::min(min.latitude, point.latitude);
    max.latitude = std::max(max.latitude, point.latitude);
    min.longitude = std::min(min.longitude, point.longitude);
    max.longitude = std::max(max.longitude, point.longitude);
}

template <typename T>
template <typename U>
void LocalBounds<T>::Add(const LocalBounds<U> &bounds)
{
    min.latitude = std::min(min.latitude, bounds.min.latitude);
    max.latitude = std::max(max.latitude, bounds.max.latitude);
    min.longitude = std::min(min.longitude, bounds.min.longitude);
    max.longitude = std::max(max.longitude, bounds.max.longitude);
}

template <Direction D, typename T>
double GetLatitude(const LocalBounds<T> &bounds)
{
    if constexpr (D == Direction::Upstream)
    {
        return bounds.max.latitude;
    }
    else
    {
        return bounds.min.latitude;
    }
}

template <Direction D, typename T>
double GetLongitude(const LocalBounds<T> &bounds)
{
    if constexpr (D == Direction::Upstream)
    {
            return bounds.max.longitude;
    }
    else
    {
        return bounds.min.longitude;
    }
}

template <typename T>
bool operator<(const LocalBounds<T> &lhs, const LocalBounds<T> &rhs)
{
    return lhs.GetEntity() < rhs.GetEntity();
}
template <typename T>
bool operator<(const LocalBounds<T> &lhs, const T &rhs)
{
    return lhs.GetEntity() < rhs;
}
template <typename T>
bool operator<(const T &lhs, const LocalBounds<T> &rhs)
{
    return lhs < rhs.GetEntity();
}
template <typename T>
bool operator<(const LocalBounds<T> &lhs, const id_t<T> &rhs)
{
    return lhs.GetEntity() < rhs;
}
template <typename T>
bool operator<(const id_t<T> &lhs, const LocalBounds<T> &rhs)
{
    return lhs < rhs.GetEntity();
}

template <typename T, typename U>
bool operator==(const LocalBounds<T> &lhs, const LocalBounds<U> &rhs)
{
    return lhs.min == rhs.min && lhs.max == rhs.max;
}
template <typename T, typename U>
bool operator!=(const LocalBounds<T> &lhs, const LocalBounds<U> &rhs)
{
    return !(lhs == rhs);
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const LocalBounds<T> &bounds)
{
    return os << "[s: (" << bounds.min.latitude << ", " << bounds.max.latitude
              << "), t: (" << bounds.min.longitude << ", " << bounds.max.longitude
              << "), id: " << bounds.GetEntity().GetId() << ']';
}
} // namespace osiql
