/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Traversal direction relative to the a direction relative to a reference line

#include <array>
#include <iostream>
#include <string_view>

namespace osiql {
//! \brief Orientation is used to tell queries whether to respect (Forwards),
//! oppose (Backwards) or ignore (Any) the driving direction of traversed lanes.
//!
enum class Orientation : char
{
    Any,
    Forwards,
    Backwards
};

//! Returns the combined orientation
//!
//! \param lhs
//! \param rhs
//! \return constexpr Orientation
constexpr Orientation operator*(Orientation lhs, Orientation rhs)
{
    if (lhs == Orientation::Any)
    {
        return rhs;
    }
    if (rhs == Orientation::Any)
    {
        return lhs;
    }
    return (lhs == rhs) ? Orientation::Forwards : Orientation::Backwards;
}

//! Inverts the given orientation
//!
//! \param orientation
//! \return constexpr Orientation
constexpr Orientation operator!(Orientation orientation)
{
    if (orientation == Orientation::Forwards)
        return Orientation::Backwards;
    if (orientation == Orientation::Backwards)
        return Orientation::Forwards;
    return Orientation::Any;
}

std::ostream &operator<<(std::ostream &os, Orientation orientation);
} // namespace osiql

namespace osiql::detail {
constexpr const std::array<std::string_view, 3> orientationToString{"Any", "Forwards", "Backwards"};
}
