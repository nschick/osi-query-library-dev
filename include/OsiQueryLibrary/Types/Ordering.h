/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Extension of any type such that a set of it can be ordered

#include <functional>

#include "OsiQueryLibrary/Point/RoadPoint.h"

namespace osiql {
template <typename T>
struct Ordering;

class Lane;
template <>
struct Ordering<Lane>
{
    const Lane &GetLane() const;
    double GetDistance() const;

private:
    std::reference_wrapper<const Lane> lane;
    double distance;
};

class Route;
template <>
struct Ordering<RoadPoint> : RoadPoint
{
    using RoadPoint::RoadPoint;

    double GetDistance() const;

private:
    friend class Route;

    double distance{0.0};
};
using Waypoint = Ordering<RoadPoint>;

bool operator<(const Waypoint &lhs, const Waypoint &rhs);
bool operator<(double lhs, const Waypoint &rhs);
bool operator<(const Waypoint &lhs, double rhs);

std::ostream &operator<<(std::ostream &os, const Waypoint &rhs);
} // namespace osiql
