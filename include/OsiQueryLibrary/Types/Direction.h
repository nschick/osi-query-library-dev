/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

//! \file
//! \brief Traversal direction relative to a reference line

#include <iostream>
#include <osi3/osi_logicallane.pb.h>

#include "OsiQueryLibrary/Types/Side.h"

namespace osiql {
//! \brief A wrapper of osi3::LogicalLane::MoveDirection that describes the driving direction of a lane
//! relative to the order in which the points of its boundaries are defined. It is also used to specify
//! how queries ought to traverse road geometry. If Direction is Downstream, it follows the boundary in
//! the order it was defined, ergo with a steadily increasing s-coordinate. If Direction is Upstream,
//! the boundary is traversed in reverse, thus with a decreasing s-coordinate.
//!
enum class Direction : std::underlying_type_t<osi3::LogicalLane::MoveDirection>
{
    Unknown = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_UNKNOWN,
    Other = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_OTHER,
    Downstream = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S,
    Upstream = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S,
    Bidirectional = osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_BOTH_ALLOWED,
};

//! Inverts the given direction
//!
//! \param direction
//! \return constexpr Direction
constexpr Direction operator!(Direction direction)
{
    if (direction == Direction::Downstream)
        return Direction::Upstream;
    if (direction == Direction::Upstream)
        return Direction::Downstream;
    return direction;
}

std::ostream &operator<<(std::ostream &os, Direction direction);

//! Returns the driving direction of lanes on the given side of a road.
//!
//! \tparam Side Side of the road
//! \return constexpr Direction
template <Side S>
constexpr Direction GetDirection()
{
    if constexpr (S == Side::Left)
    {
        return Direction::Upstream;
    }
    else
    {
        return Direction::Downstream;
    }
}
//! Returns the driving direction of lanes on the given side of a road.
//!
//! \return Direction
Direction GetDirection(Side);

//! Returns what side of the road a lane with the given driving direction is on.
//!
//! \tparam D
//! \return constexpr Side
template <Direction D>
constexpr Side GetSideOfRoad()
{
    if constexpr (D == Direction::Upstream)
    {
        return Side::Left;
    }
    else
    {
        return Side::Right;
    }
}
//! Returns what side of the road a lane with the given driving direction is on.
//!
//! \return Side
Side GetSideOfRoad(Direction);
} // namespace osiql

namespace osiql::detail {
constexpr const std::array<std::string_view, 5> directionToString{
    "Unknown",
    "Other",
    "Downstream",
    "Upstream",
    "Bidirectional",
};
}
