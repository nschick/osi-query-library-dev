/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Component/Identifiable.h"

#include <osi3/osi_sensorview.pb.h>

namespace osiql {
template <>
Id Identifiable<osi3::SensorView>::GetId() const
{
    return GetHandle().sensor_id().value();
}
} // namespace osiql
