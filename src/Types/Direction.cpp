/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Direction.h"

namespace osiql {
Direction GetDirection(Side s)
{
    return s == Side::Left ? GetDirection<Side::Left>() : GetDirection<Side::Right>();
}
Side GetSideOfRoad(Direction d)
{
    return d == Direction::Upstream ? GetSideOfRoad<Direction::Upstream>() : GetSideOfRoad<Direction::Downstream>();
}

std::ostream &operator<<(std::ostream &os, Direction direction)
{
    return os << detail::directionToString[static_cast<size_t>(direction)];
}
} // namespace osiql
