/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Matrix.h"

namespace osiql {
Matrix<2> RotationMatrix(double angle)
{
    const double cos{std::cos(angle)};
    const double sin{std::sin(angle)};
    return std::array<Row<2>, 2>{
        Row<2>{cos, -sin},
        Row<2>{sin, cos} //
    };
}

Matrix<3> RotationMatrix(double angle, const Vector<double, 2> &center)
{
    const double cos{std::cos(angle)};
    const double iCos{1.0 - cos};
    const double sin{std::sin(angle)};
    return std::array<Row<3>, 3>{
        Row<3>{cos, -sin, center.x * iCos + center.y * sin},
        Row<3>{sin, cos, center.y * iCos - center.x * sin},
        Row<3>{0.0, 0.0, 0.1} //
    };
}

Matrix<3> RotationMatrix(const Vector<double, 3> &rotation)
{
    const double cosX{std::cos(rotation.x)};
    const double cosY{std::cos(rotation.y)};
    const double cosZ{std::cos(rotation.z)};
    const double sinX{std::sin(rotation.x)};
    const double sinY{std::sin(rotation.y)};
    const double sinZ{std::sin(rotation.z)};
    return std::array<Row<3>, 3>{
        Row<3>{cosY * cosZ, sinX * sinY * cosZ - cosX * sinZ, cosX * sinY * cosZ + sinX * sinZ},
        Row<3>{cosY * sinZ, sinX * sinY * sinZ + cosX * cosZ, cosX * sinY * sinZ - sinX * cosZ},
        Row<3>{-sinY, sinX * cosY, cosX * cosY} //
    };
}

Matrix<3> RotationMatrix(double angle, const Vector<double, 3> &axis)
{
    const Vector<double, 3> a{Norm(axis)};
    // Antimetric matrix of the rotation axis. See Rodrigues' Rotation Formula
    const Matrix<3> A({Row<3>{0.0, -a.z, a.y},
                       Row<3>{a.z, 0.0, -a.x},
                       Row<3>{-a.y, a.x, 0.0}});
    return Matrix<3>{} + A * std::sin(angle) + (A * A) * (1.0 - std::cos(angle));
}

Matrix<3> AntimetricMatrix(const Vector<double, 3> &v)
{
    return Matrix<3>({Row<3>{0.0, -v.z, v.y},
                      Row<3>{v.z, 0.0, -v.x},
                      Row<3>{-v.y, v.x, 0.0}});
}
} // namespace osiql
