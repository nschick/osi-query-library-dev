/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Ordering.h"

#include "OsiQueryLibrary/Point/Vector2d.h"

namespace osiql {
double Ordering<RoadPoint>::GetDistance() const
{
    return distance;
}

bool operator<(const Waypoint &lhs, const Waypoint &rhs)
{
    return lhs.GetDistance() < rhs.GetDistance();
}
bool operator<(double lhs, const Waypoint &rhs)
{
    return lhs < rhs.GetDistance();
}
bool operator<(const Waypoint &lhs, double rhs)
{
    return lhs.GetDistance() < rhs;
}

std::ostream &operator<<(std::ostream &os, const Waypoint &rhs)
{
    return os << '[' << rhs.GetDistance() << ": " << *static_cast<const RoadPoint *>(&rhs) << ", " << rhs.GetXY() << ']';
}
} // namespace osiql
