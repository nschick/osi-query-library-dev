/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Side.h"

namespace osiql {
std::ostream &operator<<(std::ostream &os, Side side)
{
    switch (side)
    {
    case Side::Left:
        return os << "Left";
    case Side::Right:
        return os << "Right";
    case Side::Both:
        return os << "Both";
    case Side::None:
        return os << "None";
    default:
        return os << "Undefined";
    }
}
} // namespace osiql
