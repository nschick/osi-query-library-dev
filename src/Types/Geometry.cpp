/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Types/Geometry.h"

namespace osiql {
std::optional<Vector2d> Line::GetIntersection(const Line &other) const
{
    const Vector2d d1{start - end};
    const Vector2d d2{other.start - other.end};
    const double dt{d1.Cross(d2)};
    return dt == 0.0 ? std::nullopt : std::optional<Vector2d>{(d2 * start.Cross(end) - d1 * other.start.Cross(other.end)) / dt};
}

namespace detail {
double SetAngleToValidRange(double angle)
{ // From CommonHelper
    double result{std::fmod(angle, 2.0 * M_PI)};
    if (result > M_PI)
        return result - 2.0 * M_PI;
    if (result <= -M_PI)
        return result + 2.0 * M_PI;
    return result;
}
} // namespace detail

std::ostream &operator<<(std::ostream &os, const Line &line)
{
    return os << '[' << line.start << " - " << line.end << ']';
}

std::ostream &operator<<(std::ostream &os, const Box &polygon)
{
    os << "(x[" << polygon.min_corner().x << ", " << polygon.max_corner().x << "], y[" << polygon.min_corner().y << ", " << polygon.max_corner().y << "])";
    return os;
}

std::ostream &operator<<(std::ostream &os, const Polygon &polygon)
{
    if (polygon.empty())
    {
        return os << "Empty";
    }
    os << polygon.front();
    for (auto it{std::next(polygon.begin())}; it != polygon.end(); ++it)
    {
        os << ", " << *it;
    }
    return os;
}
} // namespace osiql
