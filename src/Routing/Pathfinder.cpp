/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Routing/Pathfinder.h"

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
namespace detail {
Pathfinder::Node::Node(const osiql::RoadPoint &point) :
    osiql::RoadPoint{point}, xy{point.GetLane().GetRoad().referenceLine.GetXY(point)}
{
}

Pathfinder::Pathfinder(const osiql::RoadPoint &start, const osiql::RoadPoint &end) :
    goal(end), origin(start)
{
    goal.distanceFromOrigin = std::numeric_limits<double>::max();
    openNodes.push_back(std::make_unique<Node>(start));
    openNodes.back()->distanceToGoal = (goal.xy - openNodes.back()->xy).SquaredLength();
    openNodes.back()->routeDistanceEstimate = openNodes.back()->distanceToGoal;
}

double Pathfinder::GetSolutionCost() const
{
    return goal.distanceFromOrigin;
}

std::unique_ptr<Pathfinder::Node> Pathfinder::PopBestNode()
{
    auto it{std::min_element(openNodes.begin(), openNodes.end(), [](const auto &lhs, const auto &rhs) {
        return lhs->routeDistanceEstimate < rhs->routeDistanceEstimate;
    })};
    std::unique_ptr<Node> bestNode{std::move(*it)};
    std::swap(*it, *std::prev(openNodes.end()));
    openNodes.resize(openNodes.size() - 1);
    return bestNode;
}

bool Pathfinder::IsClosedNode(const Node &node) const
{
    return std::find_if(closedNodes.begin(), closedNodes.end(), [&node](const std::unique_ptr<Node> &closedNode) {
               return closedNode->xy == node.xy;
           }) != closedNodes.end(); // TODO: Performance - store sorted(?)
}
} // namespace detail

std::ostream &operator<<(std::ostream &os, const detail::Pathfinder::Node &node)
{
    return os << "[Road \"" << node.GetRoad().GetId() << "\", " << Coordinates<Road>(node)
              << ", " << node.distanceFromOrigin << " | " << node.GetRoad().GetLength() << ']';
}

std::ostream &operator<<(std::ostream &os, const detail::Pathfinder &pathfinder)
{
    const detail::Pathfinder::Node *node{&pathfinder.goal};
    os << "Pathfinder: " << (*node);
    while (node->parent)
    {
        node = node->parent;
        os << " <- " << (*node);
    }
    if (*node != pathfinder.origin)
    {
        os << " <- ???";
    }
    return os;
}
} // namespace osiql
