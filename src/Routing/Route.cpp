/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Routing/Route.h"

#include <numeric>

namespace osiql {
Route::LaneEntry::LaneEntry(const Lane &lane, double latitude, double distance) :
    latitude(latitude), distance(distance), lane(lane)
{
}

const Lane &Route::LaneEntry::GetLane() const
{
    return lane;
}

Route::Route(const RoadPoint &point, Orientation orientation) :
    orientation(orientation), points(std::vector<Waypoint>{Waypoint{point}})
{
}
Route::Route(const detail::RoadPoint &point, Orientation orientation) :
    orientation(orientation), points(std::vector<Waypoint>{Waypoint{point}})
{
}

Route::Route(std::vector<RoadPoint> &&points, Orientation orientation) :
    orientation(orientation)
{
    if (points.empty())
    {
        std::ostringstream stream;
        stream << "Route must contain at least one point.\n";
        throw std::runtime_error(stream.str());
    }
    this->points.reserve(points.size());
    std::transform(std::make_move_iterator(points.begin()), std::make_move_iterator(points.end()), std::back_inserter(this->points), [](RoadPoint &&point) {
        return Waypoint{point};
    });
    if (orientation == Orientation::Backwards)
    {
        for (auto point{std::next(begin())}; point != end(); ++point)
        {
            point->distance = std::prev(point)->distance                                                                                    //
                              + std::abs(std::prev(point)->GetLane().GetEndLatitude<Orientation::Backwards>() - std::prev(point)->latitude) //
                              + std::abs(point->latitude - point->GetLane().GetStartLatitude<Orientation::Backwards>());
        }
    }
    else
    {
        for (auto point{std::next(begin())}; point != end(); ++point)
        {
            point->distance = std::prev(point)->distance                                                                                   //
                              + std::abs(std::prev(point)->GetLane().GetEndLatitude<Orientation::Forwards>() - std::prev(point)->latitude) //
                              + std::abs(point->latitude - point->GetLane().GetStartLatitude<Orientation::Forwards>());
        }
    }
}

typename std::vector<Waypoint>::const_iterator Route::begin() const
{
    return points.begin();
}
typename std::vector<Waypoint>::iterator Route::begin()
{
    return points.begin();
}

typename std::vector<Waypoint>::const_iterator Route::end() const
{
    return points.end();
}
typename std::vector<Waypoint>::iterator Route::end()
{
    return points.end();
}

const std::vector<Waypoint> &Route::GetPoints() const
{
    return points;
}

std::vector<Waypoint> &Route::GetPoints()
{
    return points;
}

double Route::GetLength() const
{
    return back().distance;
}

double Route::GetDistance(const RoadPoint &point) const
{
    return osiql::GetDistance(begin(), end(), point, orientation);
}

RoadPoint Route::GetRoadPoint(double d, Orientation o) const
{
    return o == Orientation::Backwards ? GetRoadPoint<Orientation::Backwards>(d) : GetRoadPoint<Orientation::Forwards>(d);
}

double Route::GetDeviation(const RoadPoint &location) const
{
    // Find the closest point to the location that is actually part of the route
    const auto it{std::find_if(begin(), end(), [&location](const Waypoint &point) {
        return point.GetRoad() == location.GetRoad();
    })};
    if (it == end())
    {
        std::cerr << "Route::GetDeviation was called with local point " << location << ", which is not located on any road of the route. Returning infinity.\n";
        return std::numeric_limits<double>::infinity();
    }
    const Lane &lane{it->GetLane()};
    const double centerLongitude{0.5 * (lane.GetBoundaryPoints<Side::Left>().GetLongitude(location.latitude) + //
                                        lane.GetBoundaryPoints<Side::Right>().GetLongitude(location.latitude))};
    return location.longitude - centerLongitude;
}

double Route::GetCurvature(double distance) const
{
    const auto it{GetRoutePoint(distance)};
    if (it == end())
    {
        return std::numeric_limits<double>::quiet_NaN();
    }
    const auto &lane{it->GetLane()};
    // TODO: Rewrite nodes to always refer to the start of their lane/road
    const double distanceFromLaneToNodePoint{it->latitude - osiql::GetLatitude(lane, lane.GetDirection(orientation))};
    const double distanceFromNodePointToTarget{distance - it->GetDistance()};
    const double latitude{lane.GetLatitude(distanceFromLaneToNodePoint + distanceFromNodePointToTarget, lane.GetDirection(orientation))};
    return it->GetRoad().referenceLine.GetCurvature(latitude);
}

std::vector<Waypoint>::const_iterator Route::FindWaypoint(const Lane &lane) const
{
    return std::find_if(begin(), end(), [&road = lane.GetRoad()](const Waypoint &point) {
        return point.GetRoad() == road;
    });
}

std::vector<const Lane *> Route::GetLaneChain(const Lane &lane) const
{
    auto it{FindWaypoint(lane)};
    if (it == end())
    {
        return {};
    }
    std::vector<const Lane *> result{&lane};
    result.reserve(size());

    auto reverseIt{std::make_reverse_iterator(std::next(it))};
    for (; reverseIt != rend(); ++reverseIt)
    {
        while (const Lane * lane{result.back()->GetConnectedLane(reverseIt->GetRoad().GetStreetId(), !orientation)})
        {
            result.push_back(lane);
        }
    }
    std::reverse(result.begin(), result.end());
    for (; it != end(); ++it)
    {
        while (const Lane * lane{result.back()->GetConnectedLane(it->GetRoad().GetStreetId(), orientation)})
        {
            result.push_back(lane);
        }
    }
    return result;
}

std::vector<Waypoint>::const_iterator GetClosestWaypoint(std::vector<Waypoint>::const_iterator first,
                                                         std::vector<Waypoint>::const_iterator pastLast,
                                                         const Vector2d &globalPoint)
{
    // NOTE: C++20 range projection would be nice here
    std::vector<double> distances(static_cast<size_t>(std::distance(first, pastLast)));
    std::transform(first, pastLast, distances.begin(), [&globalPoint](const Waypoint &routePoint) {
        return routePoint.GetRoad().GetDistanceTo(globalPoint);
    });
    const auto it{std::min_element(distances.begin(), distances.end())};
    return std::next(first, std::distance(distances.begin(), it));
}

std::vector<Waypoint>::const_iterator GetClosestWaypoint(std::vector<Waypoint>::const_iterator first,
                                                         std::vector<Waypoint>::const_iterator pastLast,
                                                         double distance)
{
    return --std::upper_bound(std::next(first), pastLast, distance, lessEqual<Direction::Downstream, double, Waypoint>);
}

std::vector<Waypoint>::const_iterator Route::GetClosestWaypoint(const Vector2d &globalPoint) const
{
    return osiql::GetClosestWaypoint(begin(), end(), globalPoint);
}

std::vector<Waypoint>::const_iterator Route::GetClosestWaypoint(double distance) const
{
    return osiql::GetClosestWaypoint(begin(), end(), distance);
}

RoadPoint Route::GetRoadPoint(const Vector2d &globalPoint) const
{
    return osiql::GetRoadPoint(begin(), end(), globalPoint);
}

RoadPose Route::GetRoadPose(const Vector2d &globalPoint, double globalAngle) const
{
    return osiql::GetRoadPose(begin(), end(), globalPoint, globalAngle);
}

std::vector<Waypoint>::const_iterator Route::GetRoutePoint(double distance) const
{
    if (distance < 0.0 || distance > GetLength())
    {
        return end();
    }
    auto it{std::upper_bound(begin(), end(), distance)};
    if (it == end())
    {
        return std::prev(end());
    }
    if (it == begin())
    {
        return begin();
    }
    --it; // FIXME: Handle case where point is on last node's road but behind the last node.
    if (it->GetLane().GetDirection(orientation) == Direction::Upstream)
    {
        const double distanceFromStartOfRoad{-it->GetDistanceTo<Direction::Upstream>(osiql::GetLatitude<Direction::Upstream>(it->GetRoad()))};
        return (distanceFromStartOfRoad >= GetDistance(*it) - distance) ? it : std::prev(it);
    }
    const double distanceFromStartOfRoad{-it->GetDistanceTo<Direction::Downstream>(osiql::GetLatitude<Direction::Downstream>(it->GetRoad()))};
    return (distanceFromStartOfRoad >= GetDistance(*it) - distance) ? it : std::prev(it);
}

RoadPoint Route::ProjectPoint(const RoadPoint &point, double distance) const
{
    std::vector<const Lane *> lanes{GetLaneChain(point.GetLane())};
    auto it{std::find_if(lanes.begin(), lanes.end(), [&road = point.GetRoad()](const Lane *lane) {
        return lane->GetRoad() == road;
    })};
    if (it == lanes.end())
    {
        return point;
    }
    if (distance >= 0.0)
    {
        auto lane{it};
        double distanceToEndOfLane{-GetLatitudeDifference(*lane, point, (*lane)->GetDirection(orientation))};
        if (distanceToEndOfLane >= distance)
        {
            return LanePoint{**lane, Coordinates<Lane>{IncreaseLatitude(GetLaneLatitude(point), distance, (*lane)->GetDirection(orientation)), GetLaneLongitude(point)}};
        }
        while (++lane != lanes.end())
        {
            const double length{osiql::Length(**lane)};
            if (distance <= length)
            {
                return LanePoint{**lane, Coordinates<Lane>{(*lane)->GetLatitude(distance, (*lane)->GetDirection(orientation)), GetLaneLongitude(point)}};
            }
            distance -= length;
        }
        return LanePoint{*lanes.back(), Coordinates<Lane>{lanes.back()->GetLatitude(Length(*lanes.back()) + distance, lanes.back()->GetDirection(orientation)), GetLaneLongitude(point)}};
    }
    else
    {
        distance = -distance;
        auto lane{std::make_reverse_iterator(std::next(it))};
        double distanceToEndOfLane{-GetLatitudeDifference(*lane, point, (*lane)->GetDirection(orientation))};
        if (distanceToEndOfLane >= distance)
        {
            return LanePoint{**lane, Coordinates<Lane>{IncreaseLatitude(GetLaneLatitude(point), distance, (*lane)->GetDirection(orientation)), GetLaneLongitude(point)}};
        }
        while (++lane != lanes.rend())
        {
            const double length{osiql::Length(**lane)};
            if (distance <= length)
            {
                return LanePoint{**lane, Coordinates<Lane>{(*lane)->GetLatitude(distance, (*lane)->GetDirection(orientation)), GetLaneLongitude(point)}};
            }
            distance -= length;
        }
        return LanePoint{*lanes.front(), Coordinates<Lane>{lanes.front()->GetLatitude(Length(*lanes.front()) + distance, lanes.front()->GetDirection(orientation)), GetLaneLongitude(point)}};
    }
}

std::ostream &operator<<(std::ostream &os, const Route &route)
{
    os << "Route (" << route.orientation << ") {";
    if (!route.empty())
    {
        os << '\n';
        for (const auto &node : route)
        {
            os << "    " << node << " Road " << node.GetLane().GetRoadId() << " (" << node.GetRoad().GetId() << ")\n";
        }
    }
    return os << '}';
}
} // namespace osiql
