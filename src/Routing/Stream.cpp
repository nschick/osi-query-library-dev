/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Routing/Stream.h"

#include "OsiQueryLibrary/Object/MovingObject.h"
#include "OsiQueryLibrary/Street/ReferenceLine.h"

namespace osiql {
Stream::Distance::Distance(const Node *node, double distance) :
    node(node), value(distance)
{
}

std::vector<const Node *> Stream::GetLinearPath(const Node &start, const Node &end) const
{
    std::vector<const Node *> result;
    for (const Node *node{&end}; node && (node != &start); node = node->parent)
    {
        result.push_back(node);
    }
    result.push_back(&start);
    std::reverse(result.begin(), result.end());
    return result;
}

const Node *Stream::GetClosestNodeContaining(const RoadPoint &location) const
{
    const auto matches{GetEarliestMatches([&](const Node &node) {
        return node.lane.GetRoadId() == location.GetLane().GetRoadId();
    })};
    return matches.empty() ? nullptr : *std::min_element(matches.begin(), matches.end(), [](const Node *A, const Node *B) {
        return std::abs(A->distance) < std::abs(B->distance);
    });
}

const Node *Stream::FindNode(const RoadId &id) const
{
    return root.SearchBreadthFirst([&id](const Node &node) { return node.lane.GetRoadId() == id; });
}

const Node *Stream::FindPeripheralNode(const RoadId &id) const
{
    return root.SearchPeripheryBreadthFirst([&id](const Node &node) { return node.lane.GetRoadId() == id; });
}

std::vector<Stream::Distance> Stream::GetDistancesToLaneEnds(const std::vector<Lane::Type> &types) const
{
    std::vector<const Node *> matches{GetLastMatches([&](const Node &node) {
        return std::find(types.begin(), types.end(), node.lane.GetType()) != types.end();
    })};
    std::vector<Distance> result(matches.size());
    std::transform(matches.begin(), matches.end(), result.begin(), [&](const Node *node) {
        return Distance{node, node->distance + Length(node->lane)};
    });
    std::sort(result.begin(), result.end(), [](const Distance &A, const Distance &B) {
        return A.value < B.value;
    });
    return result;
}

StreamPoint::StreamPoint(const RoadPoint &point, const Stream &stream)
{
    t = point.longitude - stream.origin.longitude;
    node = stream.FindNode(point.GetLane().GetRoadId());
    if (!node)
    {
        node = stream.FindPeripheralNode(point.GetLane().GetRoadId());
    }
    s = node ? node->distance - point.GetLane().GetDistanceBetween(point.latitude, GetLatitude(point.GetLane(), !stream.orientation), stream.orientation) : std::numeric_limits<double>::max(); // TODO: stream.origin.GetDistanceTo(point)
}

std::ostream &operator<<(std::ostream &os, const Stream &stream)
{
    os << "Stream (" << stream.orientation << "): " << stream.origin << '\n';
    stream.root.SearchBreadthFirst([&](const Node &node) {
        os << "Node (" << node.GetDepth() << "): "
           << "Distance " << node.distance << ", Lane " << node.lane.GetId() << " (" << node.lane.GetDirection() << ", " << node.lane.GetType() << ", " << Length(node.lane) << "): "
           << node.GetChildren().size() << ' ' << ((node.GetChildren().size() == 1) ? "Child " : "Children ");
        if (!node.GetChildren().empty())
        {
            os << '(';
            auto it{node.GetChildren().begin()};
            os << (it++)->lane.GetId();
            for (; it != node.GetChildren().end(); ++it)
            {
                os << ", " << it->lane.GetId();
            }
            os << ')';
        }
        os << ", " << node.GetPeriphery().size() << ' ' << ((node.GetPeriphery().size() == 1) ? "Peripheral " : "Peripherals ");
        if (!node.GetPeriphery().empty())
        {
            os << '(';
            auto it{node.GetPeriphery().begin()};
            os << (it++)->lane.GetId();
            for (; it != node.GetPeriphery().end(); ++it)
            {
                os << ", " << it->lane.GetId();
            }
            os << ')';
        }
        os << '\n';

        return false;
    });
    return os;
}

std::ostream &operator<<(std::ostream &os, const Stream::Distance &distance)
{
    return os << '[' << distance.value << "] " << *distance.node;
}
} // namespace osiql
