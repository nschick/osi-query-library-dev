/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Routing/Node.h"

#include "OsiQueryLibrary/Routing/Stream.h"

namespace osiql {

Node::Node(Stream &stream, double range, LaneChangeBehavior laneChangeBehavior) :
    lane(stream.origin.GetLane())
{ // Root constructor
    distance = stream.origin.GetLane().GetDistanceBetween(
        stream.origin.latitude,
        GetLatitude(stream.origin.GetLane(), stream.orientation),
        stream.orientation //
    );

    range -= Length(lane) + distance;
    if (range > 0.0)
    {
        constructChildNodes(stream, range, laneChangeBehavior); // TODO: Templatize orientation
    }
}

Node::Node(Stream &stream, const Node &parent, const Lane &lane, double range, LaneChangeBehavior laneChangeBehavior) :
    lane(lane), distance(stream.range - range), parent(&parent)
{ // Child constructor
    range -= Length(lane);
    if (range > 0.0)
    {
        constructChildNodes(stream, range, laneChangeBehavior);
    }
}

Node::Node(Stream &stream, const Node &root, const Node &parent, const Lane &lane, double backwardRange, LaneChangeBehavior laneChangeBehavior) :
    lane(lane), distance(root.distance + stream.backwardRange - backwardRange), parent(&parent)
{ // Peripheral constructor
    backwardRange += Length(lane);
    if (backwardRange < 0.0)
    {
        constructPeripheralNodes(stream, root, backwardRange, laneChangeBehavior);
    }
}

const std::vector<Node> &Node::GetChildren() const
{
    return children;
}

const std::vector<Node> &Node::GetPeriphery() const
{
    return periphery;
}

size_t Node::GetDepth() const
{
    size_t depth{0};
    const Node *node{parent};
    while (node)
    {
        ++depth;
        node = node->parent;
    }
    return depth;
}

bool Node::HasAncestor(const Node &ancestor) const
{
    const Node *node{this};
    while (node && (node->lane.GetId() != ancestor.lane.GetId()))
    {
        node = node->parent;
    }
    return node != nullptr;
}

const Node *Node::GetAncestor(size_t distance) const
{
    const Node *node{this};
    while (node && distance)
    {
        node = node->parent;
        --distance;
    }
    return node;
}

// ----- Private methods:

void Node::constructChildNodes(Stream &stream, double range, LaneChangeBehavior laneChangeBehavior)
{
    const std::vector<Lane *> outgoingLanes{GetSuccessorLanes(stream, laneChangeBehavior)};
    children.reserve(outgoingLanes.size());
    for (const Lane *childLane : outgoingLanes)
    {
        children.emplace_back(stream, *this, *childLane, range, laneChangeBehavior);
    }
}

void Node::constructPeripheralNodes(Stream &stream, const Node &root, double backwardRange, LaneChangeBehavior laneChangeBehavior)
{
    const std::vector<Lane *> incomingLanes{GetPeripheralSuccessorLanes(stream, laneChangeBehavior)};
    if (!incomingLanes.empty())
    {
        periphery.reserve(incomingLanes.size());
        for (const Lane *childLane : incomingLanes)
        {
            if (!stream.FindNode(childLane->GetRoadId()))
            {
                periphery.emplace_back(stream, root, *this, *childLane, backwardRange, laneChangeBehavior);
            }
        }
    }
}

std::vector<Lane *> Node::GetSuccessorLanes(Stream &stream, LaneChangeBehavior laneChangeBehavior) const
{
    switch (laneChangeBehavior)
    {
    case LaneChangeBehavior::WithLaneChanges:
    case LaneChangeBehavior::WithLaneChangesForwards: {
        const Road &road{lane.GetRoad()};
        const std::vector<Lane *> &lanes{road.GetConnectedLanes(GetSideOfRoad(lane.GetDirection()), stream.orientation)};
        std::set<RoadId> pool;
        std::vector<Lane *> result;
        result.reserve(lanes.size());
        std::copy_if(lanes.begin(), lanes.end(), std::back_insert_iterator(result), [&pool](Lane *lane) { return pool.insert(lane->GetRoadId()).second; });
        return result;
    }
    case LaneChangeBehavior::WithoutLaneChanges:
    case LaneChangeBehavior::WithLaneChangesBackwards:
        const std::vector<Lane *> successors{stream.orientation == Orientation::Forwards ? lane.GetConnectedLanes<Orientation::Forwards>() : lane.GetConnectedLanes<Orientation::Backwards>()};
        return {successors.begin(), successors.end()};
    }
    return {}; // impossible
}

std::vector<Lane *> Node::GetPeripheralSuccessorLanes(Stream &stream, LaneChangeBehavior laneChangeBehavior) const
{
    switch (laneChangeBehavior)
    {
    case LaneChangeBehavior::WithLaneChanges:
    case LaneChangeBehavior::WithLaneChangesBackwards: {
        const Road &road{lane.GetRoad()};
        const std::vector<Lane *> &lanes{road.GetConnectedLanes(GetSideOfRoad(lane.GetDirection()), !stream.orientation)};
        std::set<RoadId> pool;
        std::vector<Lane *> result;
        result.reserve(lanes.size());
        std::copy_if(lanes.begin(), lanes.end(), std::back_insert_iterator(result), [&pool](const Lane *lane) { return pool.insert(lane->GetRoadId()).second; });
        return result;
    }
    case LaneChangeBehavior::WithoutLaneChanges:
    case LaneChangeBehavior::WithLaneChangesForwards:
    default:
        if (stream.orientation == Orientation::Forwards)
        {
            return lane.GetDirection<Orientation::Forwards>() == Direction::Upstream
                       ? GetNarrowPeripheralSuccessorLanes<Orientation::Forwards, Direction::Upstream>()
                       : GetNarrowPeripheralSuccessorLanes<Orientation::Forwards, Direction::Downstream>();
        }
        else
        {
            return lane.GetDirection<Orientation::Backwards>() == Direction::Upstream
                       ? GetNarrowPeripheralSuccessorLanes<Orientation::Backwards, Direction::Upstream>()
                       : GetNarrowPeripheralSuccessorLanes<Orientation::Backwards, Direction::Downstream>();
        }
    }
}

std::ostream &operator<<(std::ostream &os, LaneChangeBehavior laneChangeBehavior)
{
    if (laneChangeBehavior == LaneChangeBehavior::WithLaneChangesBackwards)
        return os << "WithLaneChangesBackwards";
    else if (laneChangeBehavior == LaneChangeBehavior::WithLaneChangesForwards)
        return os << "WithLaneChangesForwards";
    else if (laneChangeBehavior == LaneChangeBehavior::WithLaneChanges)
        return os << "WithLaneChanges";
    else // laneChangeBehavior == LaneChangeBehavior::WithoutLaneChanges
        return os << "WithoutLaneChanges";
}

std::ostream &operator<<(std::ostream &os, const Node &node)
{
    return os << "Node {Lane: " << node.lane.GetId() << ", Distance: " << node.distance << '}';
}
} // namespace osiql
