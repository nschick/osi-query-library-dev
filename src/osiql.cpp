/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/osiql.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
// See https://github.com/artem-ogre/CDT/tree/master
#include <CDT.h>
#pragma GCC diagnostic pop

namespace osiql {
Query::Query(const osi3::GroundTruth &groundTruth) :
    world{std::make_unique<World>(groundTruth)}
{
    for (Lane &lane : world->lanes)
    {
        IndexSegments(lane);
    }
    Update(groundTruth.moving_object());

    if (world->handle.has_host_vehicle_id())
    {
        SetHostVehicle(world->handle.host_vehicle_id().value());
    }
}

void Query::Update(const Container<osi3::MovingObject> &allMovingObjects)
{
    Set<std::shared_ptr<MovingObject>> newObjects{World::FetchMovingObjects(allMovingObjects)};
    if (!newObjects.empty())
    {
        auto newIt{newObjects.begin()};
        for (auto it{world->movingObjects.begin()}; it != world->movingObjects.end();)
        {
            if ((*it)->GetId() > (*newIt)->GetId()) // Add
            {
                ++newIt;
            }
            else if ((*it)->GetId() < (*newIt)->GetId()) // Delete
            {
                world->movingObjects.erase(it++);
            }
            else // Update
            {
                **it = (*newIt)->GetHandle();
                ++it;
                ++newIt;
                if (newIt == newObjects.end())
                {
                    break;
                }
            }
        }
        world->movingObjects.merge(newObjects); // Add new objects
    }

    // Update the global shape and local positions of each object
    for (Lane &lane : world->lanes)
    {
        lane.movingObjects.clear();
    }
    for (const std::shared_ptr<MovingObject> &object : world->movingObjects)
    {
        Update(*object);
    }
}

void Query::Update(MovingObject &object)
{
    object.positions.clear();
    object.intersectionBounds.clear();
    std::vector<LocalBounds<Lane>> intersectionBounds{FindInternalIntersectionBounds<Lane>(object.GetShape())};
    std::set<RoadId> visitedRoads;
    for (LocalBounds<Lane> &bounds : intersectionBounds)
    {
        // TODO: Use unordered_map instead of unordered_set
        osiql::GetLane(bounds).movingObjects.insert(LocalBounds<const MovingObject>{bounds, object});
        if (visitedRoads.insert(osiql::GetLane(bounds).GetRoadId()).second)
        {
            auto it{osiql::GetLane(bounds).GetRoad().referenceLine.end(object.GetXY())};
            if (it == osiql::GetLane(bounds).GetRoad().referenceLine.begin())
            {
                ++it;
            }
            detail::RoadPoint point{osiql::GetLane(bounds).GetRoad().GetPoint(object.GetXY())};

            const double localAngle{detail::SetAngleToValidRange(object.GetYaw() - osiql::GetLane(bounds).GetRoad().referenceLine.GetAngle(point.latitude))};
            // TODO: Consider Direction for GetAngle edge case (two nearest edges -> ambiguous local angle)
            object.positions.emplace_back(std::move(point), localAngle);
        }
        object.intersectionBounds.push_back(bounds);
    }
}

const World &Query::GetWorld() const
{
    return *world;
}

const Lane *Query::GetLane(Id id) const
{
    return world->Get<Lane>(id);
}
const LaneBoundary *Query::GetLaneBoundary(Id id) const
{
    return world->Get<LaneBoundary>(id);
}
const ReferenceLine *Query::GetReferenceLine(Id id) const
{
    return world->Get<ReferenceLine>(id);
}
const MovingObject *Query::GetMovingObject(Id id) const
{
    return world->Get<MovingObject>(id);
}
const RoadMarking *Query::GetRoadMarking(Id id) const
{
    return world->Get<RoadMarking>(id);
}
const StaticObject *Query::GetStaticObject(Id id) const
{
    return world->Get<StaticObject>(id);
}
const TrafficLight *Query::GetTrafficLight(Id id) const
{
    return world->Get<TrafficLight>(id);
}
const TrafficSign *Query::GetTrafficSign(Id id) const
{
    return world->Get<TrafficSign>(id);
}

const Vehicle *Query::GetHostVehicle() const
{
    return world->ego;
}
void Query::SetHostVehicle(Id id)
{
    if (const auto it{world->movingObjects.find(id)}; it != world->movingObjects.end())
    {
        // if ((*it)->GetType() != MovingObject::Type::Vehicle)
        // {
        //     std::ostringstream stream;
        //     stream << "Unable to set host vehicle to an object of type " << (*it)->GetType() << ".\n";
        //     throw std::runtime_error(stream.str());
        // }
        world->ego = static_cast<const Vehicle *>(it->get());
    }
    else
    {
        world->ego = nullptr;
    }
}

Route Query::GetRandomRoute(double maxDistance) const
{
    if (!world->ego)
    {
        std::cerr << "Unable to generate route from host vehicle because the GroundTruth has no assigned host vehicle. Use SetHostVehicle to assign a host vehicle. Returning an empty route.\n";
        return {};
    }
    return GetHostVehicle()->GetRandomRoute(rng, maxDistance);
}

std::vector<RoadPoint> Query::FindPoints(const Vector2d &point) const
{
    std::vector<RoadPoint> points;
    const auto customInserter = [&](const std::pair<Box, LaneSegment> &value) {
        if (std::find_if(points.begin(), points.end(), [&lane = value.second.GetLane()](const RoadPoint &point) {
                return point.GetLane() == lane;
            }) == points.end())
        {
            points.emplace_back(value.second.GetLane(), value.second.GetLane().GetRoad().referenceLine.GetCoordinates(point));
        }
    };
    rTree.query(
        boost::geometry::index::satisfies([&point](const std::pair<Box, LaneSegment> &value) {
            return value.second.Contains(point);
        }),
        boost::make_function_output_iterator(customInserter) //
    );
    return points;
}

std::vector<Pose<RoadPoint>> Query::FindPoses(const Vector2d &point, double angle) const
{
    std::vector<Pose<RoadPoint>> poses;
    const auto customInserter = [&](const std::pair<Box, LaneSegment> &value) {
        if (std::find_if(poses.begin(), poses.end(), [&lane = value.second.GetLane()](const RoadPoint &point) {
                return point.GetLane() == lane;
            }) == poses.end())
        {
            poses.emplace_back(value.second.GetLane(), value.second.GetLane().GetRoad().referenceLine.GetPose(point, angle));
        }
    };
    rTree.query(
        boost::geometry::index::satisfies([&point](const std::pair<Box, LaneSegment> &value) { return value.second.Contains(point); }),
        boost::make_function_output_iterator(customInserter) //
    );
    return poses;
}

void Query::IndexSegments(Lane &lane)
{
    const Boundary::Points &leftBoundary{lane.GetBoundaryPoints<Side::Left>()};
    const Boundary::Points &rightBoundary{lane.GetBoundaryPoints<Side::Right>()};

    CDT::Triangulation<double> mesh;
    // Define vertices
    std::vector<Vector2d> points;
    points.reserve(leftBoundary.size() + rightBoundary.size());
    std::copy(leftBoundary.begin(), leftBoundary.end(), std::back_inserter(points));
    auto rBegin{rightBoundary.begin<Direction::Upstream>()};
    if (rightBoundary.back().xy == leftBoundary.back().xy)
    {
        ++rBegin;
    }
    auto rEnd{rightBoundary.end<Direction::Upstream>()};
    if (rightBoundary.front().xy == leftBoundary.front().xy)
    {
        --rEnd;
    }
    std::copy(rBegin, rEnd, std::back_inserter(points));
    if (points.front() == points.back()) // This can happen if the lane starts or ends with a width of 0
    {
        points.resize(points.size() - 1);
    }
    mesh.insertVertices(
        points.begin(), points.end(), [](const Vector2d &p) -> double { return p.x; }, [](const Vector2d &p) -> double { return p.y; } //
    );
    // Define edges
    std::vector<std::pair<size_t, size_t>> edges(points.size());
    edges.front() = {points.size() - 1, 0u};
    for (size_t i{1u}; i < edges.size(); ++i)
    {
        edges[i] = {i - 1, i};
    }
    mesh.insertEdges(
        edges.begin(), edges.end(), [](const auto &pair) { return pair.first; }, [](const auto &pair) { return pair.second; } //
    );
    mesh.eraseOuterTriangles();
    // Create segments
    for (const CDT::Triangle &triangle : mesh.triangles)
    {
        LaneSegment segment(lane, {points[triangle.vertices[0]], points[triangle.vertices[1]], points[triangle.vertices[2]]});
        Box bounds{segment.GetBounds()};
        rTree.insert(std::make_pair(bounds, std::move(segment)));
    }
}

std::vector<std::pair<Box, LaneSegment>> Query::GetSegmentsContaining(const Vector2d &point) const
{
    std::vector<std::pair<Box, LaneSegment>> result;
    rTree.query(
        boost::geometry::index::satisfies([&point](const std::pair<Box, LaneSegment> &value) { return value.second.Contains(point); }),
        std::back_inserter(result) //
    );
    return result;
}

std::ostream &operator<<(std::ostream &os, const Query &rhs)
{
    return os << rhs.GetWorld();
}
} // namespace osiql
