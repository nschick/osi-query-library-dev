/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/World.h"

#include "OsiQueryLibrary/Object/Vehicle.h"

namespace osiql {
World::World(const osi3::GroundTruth &world) :
    handle(world),
    version(GetVersion(world)),
    laneBoundaries(fetchAll<LaneBoundary>(world.logical_lane_boundary())),
    laneMarkings(fetchAll<LaneMarking>(world.lane_boundary())),
    lanes(fetchAll<Lane>(world.logical_lane())),
    referenceLines(fetchAll<ReferenceLine>(world.reference_line())),
    // movingObjects are assigned inside Query::Update()
    roadMarkings(fetchAll<RoadMarking>(world.road_marking())),
    staticObjects(fetchAll<StaticObject>(world.stationary_object())),
    // trafficLights are defined in the body because they are a special case
    trafficSigns(fetchAll<TrafficSign>(world.traffic_sign()))
{
    for (Lane &lane : lanes)
    {
        LinkAdjacentLanes(lane);
        // LinkSuccessorLanes needs to be called after CreateRoads() because it also links the successors of the corresponding road.
        // Note that successors and predecessors in osi are in regards to the road definition direction (ergo reference line direction)
        // whereas osiql flips this for upstream lanes so that successors and predecessors are always in a lane's driving direction.
        LinkSuccessorLanes(lane);
        LinkPredecessorLanes(lane);
    }

    // Loop through all lanes and create roads from their road id and reference line.
    CreateRoads();
    for (Road &road : roads)
    {
        for (auto it{road.laneRows.rows.begin()}; it != road.laneRows.it; ++it)
        { // clang-format off
            for(Lane* lane : *it)
            {
                lane->road = &road;
            }
            const std::vector<Lane *> &predecessors{it->front()->GetConnectedLanes<Orientation::Backwards>()};
            std::copy_if(predecessors.begin(), predecessors.end(), std::back_inserter(road.downstreamPredecessors), [&lanes = road.downstreamPredecessors](const Lane* lane) {
                return std::find_if(lanes.begin(), lanes.end(), [insertion = lane](const Lane* lane) {
                    return *lane == *insertion;
                }) == lanes.end();
            });
            const std::vector<Lane *> &successors{it->back()->GetConnectedLanes<Orientation::Forwards>()};
            std::copy_if(successors.begin(), successors.end(), std::back_inserter(road.downstreamSuccessors), [&lanes = road.downstreamPredecessors](const Lane* lane) {
                return std::find_if(lanes.begin(), lanes.end(), [insertion = lane](const Lane* lane) {
                    return *lane == *insertion;
                }) == lanes.end();
            });
        } // clang-format on
        for (auto it{road.laneRows.it}; it != road.laneRows.rows.end(); ++it)
        { // clang-format off
            for(Lane* lane : *it)
            {
                lane->road = &road;
            }
            const std::vector<Lane *> &successors{it->front()->GetConnectedLanes<Orientation::Forwards>()};
            std::copy_if(successors.begin(), successors.end(), std::back_inserter(road.upstreamSuccessors), [&lanes = road.downstreamPredecessors](const Lane* lane) {
                return std::find_if(lanes.begin(), lanes.end(), [insertion = lane](const Lane* lane) {
                    return *lane == *insertion;
                }) == lanes.end();
            });
            const std::vector<Lane *> &predecessors{it->back()->GetConnectedLanes<Orientation::Backwards>()};
            std::copy_if(predecessors.begin(), predecessors.end(), std::back_inserter(road.upstreamPredecessors), [&lanes = road.downstreamPredecessors](const Lane* lane) {
                return std::find_if(lanes.begin(), lanes.end(), [insertion = lane](const Lane* lane) {
                    return *lane == *insertion;
                }) == lanes.end();
            });
        } // clang-format on
    }

    // Combine the boundaries of successive lanes on a road into individual chains.
    for (Road &road : roads)
    {
        CreateBoundaries(road);
    }
    LinkBoundariesAndMarkings();

    // NOTE: osiql ignores existing moving object assignments and computes them independently
    // LinkToLanes(movingObjects);
    LinkToLanes(staticObjects);
    LinkToLanes(roadMarkings);
    LinkToLanes(trafficSigns);
    CreateAndLinkTrafficLights();
}

World::~World()
{
    // When a moving object is destroyed, it detaches itself from its touched lanes. Lanes shouldn't
    // be modified directly prior to destruction and object destruction fails if its connected lanes
    // no longer exist, so the best option is to prevent objects from detaching themselves in advance:
    for (const std::shared_ptr<MovingObject> &object : movingObjects)
    {
        object->positions.clear();
    }
}

template <>
const Collection<Lane> &World::GetAll<Lane>() const
{
    return lanes;
}
template <>
const Collection<LaneBoundary> &World::GetAll<LaneBoundary>() const
{
    return laneBoundaries;
}
template <>
const Collection<LaneMarking> &World::GetAll<LaneMarking>() const
{
    return laneMarkings;
}
template <>
const Collection<MovingObject> &World::GetAll<MovingObject>() const
{
    return movingObjects;
}
template <>
const Collection<ReferenceLine> &World::GetAll<ReferenceLine>() const
{
    return referenceLines;
}
template <>
const Collection<Road> &World::GetAll<Road>() const
{
    return roads;
}
template <>
const Collection<RoadMarking> &World::GetAll<RoadMarking>() const
{
    return roadMarkings;
}
template <>
const Collection<StaticObject> &World::GetAll<StaticObject>() const
{
    return staticObjects;
}
template <>
const Collection<TrafficLight> &World::GetAll<TrafficLight>() const
{
    return trafficLights;
}
template <>
const Collection<TrafficSign> &World::GetAll<TrafficSign>() const
{
    return trafficSigns;
}

void World::CreateRoads()
{
    Set<Lane *> allLanes;
    std::transform(lanes.begin(), lanes.end(), std::inserter(allLanes, allLanes.end()), [](Lane &lane) {
        return &lane;
    });
    while (!allLanes.empty())
    {
        Lane &lane{**allLanes.begin()};
        assert(lane.GetHandle().has_reference_line_id());
        ReferenceLine &referenceLine{Find<ReferenceLine>(lane.GetHandle().reference_line_id().value())};
        Road &road{osiql::Emplace<Road>(roads, detail::RoadSpecifications{referenceLine, lane})};
        for (const LaneRow &row : road.laneRows.rows)
        {
            for (const Lane *lane : row)
            {
                auto it{allLanes.find(lane->GetId())};
                if (it != allLanes.end())
                {
                    allLanes.erase(it);
                }
            }
        }
    }
}

void World::LinkAdjacentLanes(Lane &lane)
{
    lane.leftAdjacentLanes.resize(static_cast<size_t>(lane.GetHandle().left_adjacent_lane_size()));
    const auto &left{lane.GetHandle().left_adjacent_lane()};
    std::transform(left.begin(), left.end(), lane.leftAdjacentLanes.begin(), [&](const osi3::LogicalLane_LaneRelation &relation) {
        return &Find<Lane>(relation.other_lane_id().value());
    });

    lane.rightAdjacentLanes.resize(static_cast<size_t>(lane.GetHandle().right_adjacent_lane_size()));
    const auto &right{lane.GetHandle().right_adjacent_lane()};
    std::transform(right.begin(), right.end(), lane.rightAdjacentLanes.begin(), [&](const osi3::LogicalLane_LaneRelation &relation) {
        return &Find<Lane>(relation.other_lane_id().value());
    });
}

void World::LinkSuccessorLanes(Lane &lane)
{
    std::vector<Lane *> &container{(lane.GetDirection() == Direction::Upstream) ? lane.incomingLanes : lane.outgoingLanes};
    for (const osi3::LogicalLane_LaneConnection &connection : lane.GetHandle().successor_lane())
    {
        Lane &nextLane{Find<Lane>(connection.other_lane_id().value())};
        container.push_back(&nextLane);
    }
}

void World::LinkPredecessorLanes(Lane &lane)
{
    std::vector<Lane *> &container{(lane.GetDirection() == Direction::Upstream) ? lane.outgoingLanes : lane.incomingLanes};
    for (const osi3::LogicalLane_LaneConnection &connection : lane.GetHandle().predecessor_lane())
    {
        Lane &prevLane{Find<Lane>(connection.other_lane_id().value())};
        container.push_back(&prevLane);
    }
}

void World::LinkBoundariesAndMarkings()
{
    for (LaneBoundary &boundary : laneBoundaries)
    {
        for (const osi3::Identifier &id : boundary.GetHandle().physical_boundary_id())
        {
            const LaneMarking &marking{Find<LaneMarking>(id.value())};
            const auto closestToBoundaryStart{std::min_element(marking.begin(), marking.end(), [target = Vector2d(boundary.front())](const LaneMarking::Point &lhs, const LaneMarking::Point &rhs) {
                return (target - lhs).SquaredLength() < (target - rhs).SquaredLength();
            })};
            const auto closestToBoundaryEnd{std::min_element(closestToBoundaryStart, marking.end(), [target = Vector2d(boundary.back())](const LaneMarking::Point &lhs, const LaneMarking::Point &rhs) {
                return (target - lhs).SquaredLength() < (target - rhs).SquaredLength();
            })};
            const auto closestToMarkingStart{std::min_element(boundary.begin(), boundary.end(), [target = static_cast<Vector2d>(marking.front())](const LaneBoundary::Point &lhs, const LaneBoundary::Point &rhs) {
                return (target - lhs).SquaredLength() < (target - rhs).SquaredLength();
            })};
            const auto closestToMarkingEnd{std::min_element(closestToMarkingStart, boundary.end(), [target = static_cast<Vector2d>(marking.back())](const LaneBoundary::Point &lhs, const LaneBoundary::Point &rhs) {
                return (target - lhs).SquaredLength() < (target - rhs).SquaredLength();
            })};
            boundary.markingRanges.push_back({
                LaneMarking::Partition{marking, closestToBoundaryStart, std::next(closestToBoundaryEnd)},
                closestToMarkingStart->s_position(),
                closestToMarkingEnd->s_position() //
            });                                   // NOTE: C++20 would be nice here (supports emplace_back for aggregates)
        }
    }
    for (const Road &element : roads)
    {
        Road &road{const_cast<Road &>(element)};
        for (Boundary &roadBoundary : road.boundaries)
        {
            for (Boundary::Partition &partition : roadBoundary.partitions)
            {
                for (const LaneBoundary::MarkingRange &range : partition.boundary.markingRanges)
                {
                    if ((partition.front().latitude < range.max) && (range.min < partition.back().latitude))
                    {
                        const auto closestToPartitionStart{std::min_element(              //
                            range.markingPartition.begin(), range.markingPartition.end(), //
                            [target = static_cast<Vector2d>(partition.front())](const LaneMarking::Point &lhs, const LaneMarking::Point &rhs) {
                                return (target - lhs).SquaredLength() < (target - rhs).SquaredLength();
                            })};
                        const auto closestToPartitionEnd{std::min_element(         //
                            closestToPartitionStart, range.markingPartition.end(), //
                            [target = static_cast<Vector2d>(partition.back())](const LaneMarking::Point &lhs, const LaneMarking::Point &rhs) {
                                return (target - lhs).SquaredLength() < (target - rhs).SquaredLength();
                            })};
                        partition.markings.emplace_back(range.markingPartition.marking, closestToPartitionStart, std::next(closestToPartitionEnd));
                    }
                }
            }
        }
    }
}

std::vector<LaneBoundary *> World::GetLaneBoundaries(std::vector<Lane *> &row,
                                                     std::string side,
                                                     const Container<osi3::Identifier> &(*getBoundaryIds)(const Lane *))
{
    std::vector<LaneBoundary *> boundaries;
    {
        size_t size{0u};
        for (const Lane *lane : row)
        {
            size += static_cast<size_t>(getBoundaryIds(lane).size());
        }
        boundaries.reserve(size);
        // Walk through the chain of lanes and through each lane's chain of osi lane boundaries
        // and stitch them together into a vector of lane boundaries.
        auto laneIt{row.begin()};
        for (const osi3::Identifier &identifier : getBoundaryIds(*laneIt))
        {
            boundaries.push_back(&Find<LaneBoundary>(identifier.value()));
        }
        if (boundaries.empty())
        {
            std::ostringstream stream;
            stream << "Lane " << (*laneIt)->GetId() << " has no " << side << " boundaries.\n";
            throw std::runtime_error(stream.str());
        }
        ++laneIt;
        for (; laneIt != row.end(); ++laneIt)
        {
            const Container<osi3::Identifier> &ids{getBoundaryIds(*laneIt)};
            if (ids.empty())
            {
                std::ostringstream stream;
                stream << "Lane " << (*laneIt)->GetId() << " has no " << side << " boundaries.\n";
                throw std::runtime_error(stream.str());
            }
            // Skip boundary if it's the same as the last lane's.
            auto boundaryIt{ids.begin()};
            if (boundaries.back()->GetId() == boundaryIt->value())
            {
                ++boundaryIt;
            }
            // Add boundaries
            for (; boundaryIt != ids.end(); ++boundaryIt)
            {
                boundaries.push_back(&Find<LaneBoundary>(boundaryIt->value()));
            }
        }
    }
    return boundaries;
}

Boundary CreateBaseBoundary(const std::vector<Lane *> &row, const std::vector<LaneBoundary *> &boundaries)
{
    Boundary boundary;
    size_t size{1 + row.size()};
    for (LaneBoundary *laneBoundary : boundaries)
    {
        size += static_cast<size_t>(laneBoundary->GetHandle().boundary_line_size());
    }
    boundary.points.reserve(size);
    boundary.partitions.reserve(boundaries.size());
    std::transform(boundaries.begin(), boundaries.end(), std::back_inserter(boundary.partitions), [&boundary](const LaneBoundary *laneBoundary) {
        return Boundary::Partition(*laneBoundary, boundary.points.begin(), boundary.points.begin());
    });
    return boundary;
}

Boundary World::CreateSideBoundary(std::vector<Lane *> &row,
                                   std::string side,
                                   const Container<osi3::Identifier> &(*getIds)(const Lane *),
                                   Boundary::Points &(*getPoints)(Lane *),
                                   Boundary::Partitions &(*getPartitions)(Lane *))
{
    std::vector<LaneBoundary *> boundaries{GetLaneBoundaries(row, side, getIds)};
    Boundary boundary{CreateBaseBoundary(row, boundaries)};

    std::vector<LaneBoundary *>::iterator boundaryIt{boundaries.begin()};
    Container<const LaneBoundary::Point>::const_iterator pointIt{(*boundaryIt)->begin()};
    std::vector<Lane *>::iterator laneIt{row.begin()};
    std::vector<Boundary::Partition>::iterator partitionIt{boundary.partitions.begin()};
    std::vector<osiql::BoundaryPoint>::const_iterator startIt{boundary.begin()};
    std::vector<Boundary::Partition>::const_iterator partitionStartIt{boundary.partitions.begin()};
    std::vector<BoundaryPoint>::const_iterator boundaryStartIt{boundary.begin()};

    while (laneIt != row.end())
    {
        const double s{GetLatitude<Direction::Upstream>(**laneIt)};
        while (pointIt->s_position() < s)
        {
            boundary.points.push_back(*pointIt);
            ++pointIt;
            if (pointIt == (*boundaryIt)->end())
            {
                partitionIt->a = boundaryStartIt;
                partitionIt->z = boundary.end();
                boundaryStartIt = std::prev(boundary.points.end());
                ++boundaryIt;
                if (boundaryIt == boundaries.end())
                {
                    std::ostringstream stream;
                    stream << "Boundary " << (*std::prev(boundaryIt))->GetId() << " ends at s=" << std::prev(pointIt)->s_position()
                           << ", which is before the end of the road at s=" << s << ".\n";
                    throw std::runtime_error(stream.str());
                }
                pointIt = std::next((*boundaryIt)->begin());
            }
        }
        if (pointIt->s_position() == s)
        {
            boundary.points.push_back(*pointIt);
            ++pointIt;
            getPoints(*laneIt) = Boundary::Points(startIt, boundary.end());
            startIt = std::prev(boundary.end());
            getPartitions(*laneIt) = Boundary::Partitions(partitionStartIt, std::next(partitionIt));
            partitionStartIt = partitionIt;
            ++laneIt;
            if (pointIt == (*boundaryIt)->end())
            {
                partitionIt->a = boundaryStartIt;
                partitionIt->z = boundary.end();
                boundaryStartIt = std::prev(boundary.points.end());
                ++boundaryIt;
                if (boundaryIt == boundaries.end())
                {
                    if (laneIt == row.end())
                    {
                        break;
                    }
                    std::ostringstream stream;
                    stream << "Boundary " << (*std::prev(boundaryIt))->GetId() << " ends at s=" << std::prev(pointIt)->s_position()
                           << ", which is before the end of the road at s=" << s << ".\n";
                    throw std::runtime_error(stream.str());
                }
                pointIt = std::next((*boundaryIt)->begin());
            }
        }
        else
        { // Insert custom point
            BoundaryPoint nextPoint{*pointIt};
            const BoundaryPoint &prevPoint{boundary.back()};

            const double ratio{(s - prevPoint.latitude) / (nextPoint.latitude - prevPoint.latitude)};
            // TODO: Point may be wrong if the reference line has more detail than the boundary between nextPoint.latitude and prevPoint.latitude.
            // Interpolate it using the reference line instead.
            boundary.points.emplace_back(
                Vector2d{prevPoint.x * (1.0 - ratio) + nextPoint.x * ratio, prevPoint.y * (1.0 - ratio) + nextPoint.y * ratio},                                       //
                Coordinates<Road>{prevPoint.latitude * (1.0 - ratio) + nextPoint.latitude * ratio, prevPoint.longitude * (1.0 - ratio) + nextPoint.longitude * ratio} //
            );

            getPoints(*laneIt) = Boundary::Points(startIt, boundary.end());
            startIt = boundary.end();
            getPartitions(*laneIt) = Boundary::Partitions(partitionStartIt, std::next(partitionIt));
            partitionStartIt = partitionIt;
            ++laneIt;
        }
    }
    return boundary;
}

Boundary World::CreateLeftBoundary(std::vector<Lane *> &row)
{
    return CreateSideBoundary(
        row, "left",
        [](const Lane *l) -> const Container<osi3::Identifier> & { return l->GetHandle().left_boundary_id(); },
        [](Lane *l) -> Boundary::Points & { return l->leftBoundaryPoints; },
        [](Lane *l) -> Boundary::Partitions & { return l->leftBoundaryPartitions; } //
    );
}

Boundary World::CreateRightBoundary(std::vector<Lane *> &row)
{
    return CreateSideBoundary(
        row, "right",
        [](const Lane *l) -> const Container<osi3::Identifier> & { return l->GetHandle().right_boundary_id(); },
        [](Lane *l) -> Boundary::Points & { return l->rightBoundaryPoints; },
        [](Lane *l) -> Boundary::Partitions & { return l->rightBoundaryPartitions; } //
    );
}

Boundary World::CreateBoundary(std::vector<Lane *> &leftRow, std::vector<Lane *> &rightRow)
{
    std::vector<LaneBoundary *> boundaries{GetLaneBoundaries(rightRow, "left", [](const Lane *l) -> const Container<osi3::Identifier> & { return l->GetHandle().left_boundary_id(); })};
    Boundary boundary{CreateBaseBoundary(rightRow, boundaries)};

    std::vector<LaneBoundary *>::iterator boundaryIt{boundaries.begin()};
    Container<const LaneBoundary::Point>::const_iterator pointIt{(*boundaryIt)->begin()};
    std::vector<Lane *>::iterator leftLaneIt{leftRow.begin()};
    std::vector<Lane *>::iterator rightLaneIt{rightRow.begin()};
    std::vector<Boundary::Partition>::iterator partitionIt{boundary.partitions.begin()};
    std::vector<osiql::BoundaryPoint>::const_iterator leftStartIt{boundary.begin()};
    std::vector<osiql::BoundaryPoint>::const_iterator rightStartIt{boundary.begin()};
    std::vector<Boundary::Partition>::const_iterator leftPartitionStartIt{boundary.partitions.begin()};
    std::vector<Boundary::Partition>::const_iterator rightPartitionStartIt{boundary.partitions.begin()};
    std::vector<BoundaryPoint>::const_iterator partitionStartIt{boundary.begin()};

    while (leftLaneIt != leftRow.end())
    {
        const double s{std::min(GetLatitude<Direction::Upstream>(**leftLaneIt), GetLatitude<Direction::Upstream>(**rightLaneIt))};
        while (pointIt->s_position() < s)
        {
            boundary.points.push_back(*pointIt);
            ++pointIt;
            if (pointIt == (*boundaryIt)->end())
            {
                partitionIt->a = partitionStartIt;
                partitionIt->z = boundary.end();
                partitionStartIt = std::prev(boundary.points.end());
                ++boundaryIt;
                if (boundaryIt == boundaries.end())
                {
                    std::ostringstream stream;
                    stream << "Boundary " << (*std::prev(boundaryIt))->GetId() << " ends at s=" << std::prev(pointIt)->s_position()
                           << ", which is before the end of the road at s=" << s << ".\n";
                    throw std::runtime_error(stream.str());
                }
                pointIt = std::next((*boundaryIt)->begin());
            }
        }
        if (pointIt->s_position() == s)
        {
            boundary.points.push_back(*pointIt);
            ++pointIt;
            if (GetLatitude<Direction::Upstream>(**leftLaneIt) == s)
            {
                (*leftLaneIt)->rightBoundaryPoints = Boundary::Points(leftStartIt, boundary.end());
                leftStartIt = std::prev(boundary.end());
                (*leftLaneIt)->rightBoundaryPartitions = Boundary::Partitions(leftPartitionStartIt, std::next(partitionIt));
                leftPartitionStartIt = partitionIt;
                ++leftLaneIt;
            }
            if (GetLatitude<Direction::Upstream>(**rightLaneIt) == s)
            {
                (*rightLaneIt)->leftBoundaryPoints = Boundary::Points(rightStartIt, boundary.end());
                rightStartIt = std::prev(boundary.end());
                (*rightLaneIt)->leftBoundaryPartitions = Boundary::Partitions(rightPartitionStartIt, std::next(partitionIt));
                rightPartitionStartIt = partitionIt;
                ++rightLaneIt;
            }
            if (pointIt == (*boundaryIt)->end())
            {
                partitionIt->a = partitionStartIt;
                partitionIt->z = boundary.end();
                partitionStartIt = std::prev(boundary.points.end());
                ++boundaryIt;
                if (boundaryIt == boundaries.end())
                {
                    if (leftLaneIt == leftRow.end())
                    {
                        break;
                    }
                    std::ostringstream stream;
                    stream << "Right lane row of boundary " << (*std::prev(boundaryIt))->GetId() << " ends at s=" << std::prev(pointIt)->s_position()
                           << ", which is before the end of the road at s=" << GetLatitude<Direction::Upstream>(*leftRow.back()) << ".\n";
                    throw std::runtime_error(stream.str());
                }
                pointIt = std::next((*boundaryIt)->begin());
            }
        }
        else
        { // Insert custom point
            BoundaryPoint nextPoint{*pointIt};
            const BoundaryPoint &prevPoint{boundary.back()};

            const double ratio{(s - prevPoint.latitude) / (nextPoint.latitude - prevPoint.latitude)};
            // TODO: Point may be entirely wrong! Interpolate from reference line instead!
            boundary.points.emplace_back(
                Vector2d{prevPoint.x * (1.0 - ratio) + nextPoint.x * ratio, prevPoint.y * (1.0 - ratio) + nextPoint.y * ratio},                                       //
                Coordinates<Road>{prevPoint.latitude * (1.0 - ratio) + nextPoint.latitude * ratio, prevPoint.longitude * (1.0 - ratio) + nextPoint.longitude * ratio} //
            );

            if (GetLatitude<Direction::Upstream>(**leftLaneIt) == s)
            {
                (*leftLaneIt)->rightBoundaryPoints = Boundary::Points(leftStartIt, boundary.end());
                leftStartIt = boundary.end();
                (*leftLaneIt)->rightBoundaryPartitions = Boundary::Partitions(leftPartitionStartIt, std::next(partitionIt));
                leftPartitionStartIt = partitionIt;
                ++leftLaneIt;
            }
            if (GetLatitude<Direction::Upstream>(**rightLaneIt) == s)
            {
                (*rightLaneIt)->leftBoundaryPoints = Boundary::Points(rightStartIt, boundary.end());
                rightStartIt = boundary.end();
                (*rightLaneIt)->leftBoundaryPartitions = Boundary::Partitions(rightPartitionStartIt, std::next(partitionIt));
                rightPartitionStartIt = partitionIt;
                ++rightLaneIt;
            }
        }
    }
    return boundary;
}

void World::CreateBoundaries(Road &road)
{
    road.boundaries.reserve(road.laneRows.rows.size() + 1);
    road.boundaries.push_back(CreateRightBoundary(road.laneRows.rows.front().lanes));
    for (auto it{std::next(road.laneRows.rows.begin())}; it != road.laneRows.rows.end(); ++it)
    {
        road.boundaries.push_back(CreateBoundary(it->lanes, std::prev(it)->lanes));
    }
    road.boundaries.push_back(CreateLeftBoundary(road.laneRows.rows.back().lanes));
}

const MovingObject &World::GetHostVehicle() const
{
    return *ego;
}

Set<std::shared_ptr<MovingObject>> World::FetchMovingObjects(const Container<typename MovingObject::Handle> &container)
{
    Set<std::shared_ptr<MovingObject>> result;
    std::vector<decltype(result)::const_iterator> trailerVehicles;
    for (const typename MovingObject::Handle &entity : container)
    {
        if (entity.has_type() && entity.type() == osi3::MovingObject_Type_TYPE_VEHICLE)
        {
            const auto vehicle{result.emplace(std::make_unique<Vehicle>(entity)).first};
            const MovingObject::Handle &handle{(*vehicle)->GetHandle()};
            if (handle.has_vehicle_classification() && handle.vehicle_classification().has_trailer_id())
            {
                trailerVehicles.push_back(vehicle);
            }
        }
        else
        {
            result.emplace(std::make_unique<MovingObject>(entity));
        }
    }
    for (const auto &vehicle : trailerVehicles)
    {
        static_cast<Vehicle *>(vehicle->get())->trailer = result.find((*vehicle)->GetHandle().vehicle_classification().trailer_id().value())->get();
    }
    return result;
}

void World::CreateAndLinkTrafficLights()
{
    // An osi3::TrafficLight is an individual bulb. For convenience, we group bulbs into one traffic light entity.
    // We can't group by xy-position because horizontal or tilted traffic lights exist.
    // We can't group by lane assignment s-coordinate because different lights can be assigned to the same lane's s coordinate.
    // Instead, bulbs are part of the same traffic light if their set of lane assignments is identical, however, there is no guarantee
    // that assignments will be in the same order. Therefore, we need to create a sorted intermediary that uses a custom comparator.

    struct CompareAssignmentSets
    {
        bool operator()(const std::set<Assignment<Lane>> &lhs, const std::set<Assignment<Lane>> &rhs) const
        {
            if (lhs.size() != rhs.size())
                return lhs.size() < rhs.size();
            for (auto a{lhs.begin()}, b{rhs.begin()}; a != lhs.end(); std::advance(a, 1), std::advance(b, 1))
            {
                if (GetLatitude(*a) != GetLatitude(*b))
                    return GetLatitude(*a) < GetLatitude(*b);
                if (a->GetEntity().GetId() != b->GetEntity().GetId())
                    return a->GetEntity().GetId() < b->GetEntity().GetId();
                if (GetLongitude(*a) != GetLongitude(*b))
                    return GetLongitude(*a) < GetLongitude(*b);
            }
            return false;
        }
    };
    // Keys are the sets of lane assignments of each light bulb
    // Values are the light bulbs that have their key as lane assignments.
    // That way, bulbs are grouped by their lane assignments:

    std::map<std::set<Assignment<Lane>>, std::vector<const osi3::TrafficLight *>, CompareAssignmentSets> protoLights;
    for (const osi3::TrafficLight &bulb : handle.traffic_light())
    {
        std::set<Assignment<Lane>> assignments;
        for (const osi3::LogicalLaneAssignment &assignment : bulb.classification().logical_lane_assignment())
        {
            assignments.emplace(Find<Lane>(assignment.assigned_lane_id().value()), assignment);
        }
        protoLights[std::move(assignments)].emplace_back(&bulb);
    }
    // Now create a new traffic light from each grouping of bulbs
    for (auto &[laneAssignments, trafficBulbs] : protoLights)
    {
        // Define the traffic light
        trafficLights.emplace_back(std::move(trafficBulbs));
        // Link the traffic light to its lanes
        for (const Assignment<Lane> &assignment : laneAssignments)
        {
            trafficLights.back().positions.emplace_back(assignment);
        }
        // Map each bulb to the created traffic light
        for (const osi3::TrafficLight *bulb : trafficLights.back().bulbs)
        {
            lightBulbs.emplace(bulb->id().value(), &trafficLights.back());
        }
    }
    // Link the lanes back to the created traffic lights
    for (TrafficLight &light : trafficLights)
    {
        for (auto &assignment : light.positions)
        {
            assignment.GetEntity().GetAll<TrafficLight>().emplace_back(light, assignment.GetHandle());
        }
    }
}

std::ostream &operator<<(std::ostream &os, const World &rhs)
{
    if (rhs.ego)
    {
        os << "GroundTruth of host vehicle " << rhs.ego->GetId() << ":";
    }
    else
    {
        os << "GroundTruth without host vehicle:";
    }
    os << "\nReference Lines: [(x, y), s]\n";
    for (const ReferenceLine &reference : rhs.referenceLines)
    {
        os << reference << '\n';
    }
    os << "\nBoundaries: {PassingRule | (x, y), (s, t)}\n";
    for (const LaneBoundary &boundary : rhs.laneBoundaries)
    {
        os << boundary << '\n';
    }
    os << "\nLanes (" << rhs.lanes.size() << "):\n";
    for (const Lane &lane : rhs.lanes)
    {
        os << lane << '\n';
    }
    os << "\nRoads (" << rhs.roads.size() << "):\n";
    for (const Road &road : rhs.roads)
    {
        os << road << '\n';
    }
    // os << "\nMoving Objects:\n";
    // for (const auto &[id, object] : rhs.movingObjects)
    // {
    //     os << *object << '\n';
    // }
    // os << "\nRoad Markings:\n";
    // for (const auto &[id, marking] : rhs.roadMarkings)
    // {
    //     os << marking << '\n';
    // }
    // os << "\nStationary Objects:\n";
    // for (const auto &[id, object] : rhs.staticObjects)
    // {
    //     os << object << '\n';
    // }
    // os << "\nTraffic Lights:\n";
    // for (const auto &[id, light] : rhs.trafficLights)
    // {
    //     os << light << '\n';
    // }
    // os << "\nTraffic Signs:\n";
    // for (const auto &[id, sign] : rhs.trafficSigns)
    // {
    //     os << sign << '\n';
    // }
    return os << '\n';
}
} // namespace osiql
