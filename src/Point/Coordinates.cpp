/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/Coordinates.h"

namespace osiql {
Coordinates<Road>::Coordinates(double latitude, double longitude) :
    latitude(latitude), longitude(longitude)
{
}

Coordinates<Road>::Coordinates(const osi3::LogicalLaneBoundary::LogicalBoundaryPoint &point) :
    latitude(point.s_position()), longitude(point.t_position())
{
}

Coordinates<Road>::Coordinates(const osi3::ReferenceLine::ReferenceLinePoint &point) :
    latitude(point.s_position()), longitude(0.0)
{
}

} // namespace osiql
