/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/Point.h"

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/ReferenceLine.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
// detail::LocalPoint<T, U>

template <>
const Road &detail::LocalPoint<Lane, const Lane>::GetRoad() const
{
    return GetLane().GetRoad();
}
template <>
const Road &detail::LocalPoint<Lane, Lane>::GetRoad() const
{
    return GetLane().GetRoad();
}
template <>
const Road &detail::LocalPoint<Road, const Lane>::GetRoad() const
{
    return GetLane().GetRoad();
}
template <>
const Road &detail::LocalPoint<Road, Lane>::GetRoad() const
{
    return GetLane().GetRoad();
}

LanePoint::operator Vector2d() const
{
    return GetLane().GetRoad().referenceLine.GetXY(*this);
}
detail::LanePoint::operator Vector2d() const
{
    return GetLane().GetRoad().referenceLine.GetXY(*this);
}
RoadPoint::operator Vector2d() const
{
    return GetLane().GetRoad().referenceLine.GetXY(*this);
}
detail::RoadPoint::operator Vector2d() const
{
    return GetLane().GetRoad().referenceLine.GetXY(*this);
}

// detail::MutablePoint<T>

template <>
detail::MutablePoint<Road>::MutablePoint(Assignment<Lane> &assignment) :
    detail::LocalPoint<Road, Lane>(assignment.entity, Coordinates<Road>(assignment))
{
}
template <>
detail::MutablePoint<Lane>::MutablePoint(Assignment<Lane> &assignment) :
    detail::LocalPoint<Lane, Lane>(assignment.entity, MutablePoint<Road>(assignment))
{
}

// detail::FixedPoint<T>

template <>
detail::FixedPoint<Road>::FixedPoint(const Assignment<Lane> &assignment) :
    detail::LocalPoint<Lane, const Lane>(assignment.entity, Coordinates<Road>(assignment))
{
}
template <>
detail::FixedPoint<Lane>::FixedPoint(const Assignment<Lane> &assignment) :
    detail::LocalPoint<Lane, const Lane>(assignment.entity, FixedPoint<Road>(assignment))
{
}

template <>
detail::FixedPoint<Road>::FixedPoint(const detail::MutablePoint<Road> &point) :
    detail::LocalPoint<Road, const Lane>(point.GetLane(), point)
{
}
template <>
detail::FixedPoint<Lane>::FixedPoint(const detail::MutablePoint<Lane> &point) :
    detail::LocalPoint<Lane, const Lane>(point.GetLane(), point)
{
}

// RoadPoint<T>

RoadPoint::RoadPoint(const LanePoint &point) :
    detail::FixedPoint<Road>(point.GetLane(), point)
{
}
RoadPoint::RoadPoint(const detail::LanePoint &point) :
    detail::FixedPoint<Road>(point.GetLane(), point)
{
}
detail::RoadPoint::RoadPoint(detail::LanePoint &point) :
    detail::MutablePoint<Road>(point.GetLane(), point)
{
}

RoadPoint::operator Coordinates<Lane>() const
{
    return {GetLaneLatitude(*this), GetLaneLongitude(*this)};
}
detail::RoadPoint::operator Coordinates<Lane>() const
{
    return {GetLaneLatitude(*this), GetLaneLongitude(*this)};
}

template <>
bool RoadPoint::IsAheadOf<Direction::Upstream>(const RoadPoint &point, double maxRange) const;

bool IsAheadOf(const RoadPoint &point, double maxRange, Direction direction) const;

template <Orientation O = Orientation::Forwards>
bool IsAheadOf(const RoadPoint &point, double maxRange) const;

bool IsAheadOf(const RoadPoint &point, double maxRange) const;

// LanePoint<T>

LanePoint::LanePoint(const RoadPoint &point) :
    detail::FixedPoint<Lane>(point.GetLane(), point)
{
}
LanePoint::LanePoint(const detail::RoadPoint &point) :
    detail::FixedPoint<Lane>(point.GetLane(), point)
{
}
detail::LanePoint::LanePoint(detail::RoadPoint &point) :
    detail::MutablePoint<Lane>(point.GetLane(), point)
{
}

LanePoint::operator Coordinates<Road>() const
{
    const double roadLatitude{GetRoadLatitude()};
    const double leftLongitude{GetLane().GetBoundaryPoints<Side::Left>().GetLongitude(roadLatitude)};
    const double rightLongitude{GetLane().GetBoundaryPoints<Side::Right>().GetLongitude(roadLatitude)};
    return {roadLatitude, longitude + (leftLongitude * 0.5 + rightLongitude * 0.5)};
}
detail::LanePoint::operator Coordinates<Road>() const
{
    const double roadLatitude{GetRoadLatitude()};
    const double leftLongitude{GetLane().GetBoundaryPoints<Side::Left>().GetLongitude(roadLatitude)};
    const double rightLongitude{GetLane().GetBoundaryPoints<Side::Right>().GetLongitude(roadLatitude)};
    return {roadLatitude, longitude + (leftLongitude * 0.5 + rightLongitude * 0.5)};
}

double LanePoint::GetRoadLatitude() const
{
    if (GetLane().GetDirection() == Direction::Upstream)
    {
        return GetLatitude<Direction::Upstream>(GetLane()) - latitude;
    }
    else
    {
        return GetLatitude<Direction::Downstream>(GetLane()) + latitude;
    }
}
template <>
double detail::LanePoint::GetRoadLatitude() const
{
    if (GetLane().GetDirection() == Direction::Upstream)
    {
        return GetLatitude<Direction::Upstream>(GetLane()) - latitude;
    }
    else
    {
        return GetLatitude<Direction::Downstream>(GetLane()) + latitude;
    }
}
} // namespace detail

// operator==

template <>
bool operator==(const detail::LocalPoint<Lane, const Lane> &lhs, const detail::LocalPoint<Lane, const Lane> &rhs)
{
    if (lhs.GetLane() == rhs.GetLane())
    {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
    }
    else
    {
        return (lhs.GetLane().GetRoad() == rhs.GetLane().GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
    }
}
template <>
bool operator==(const detail::LocalPoint<Lane, const Lane> &lhs, const detail::LocalPoint<Lane, Lane> &rhs)
{
    if (lhs.GetLane() == rhs.GetLane())
    {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
    }
    else
    {
        return (lhs.GetLane().GetRoad() == rhs.GetLane().GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
    }
}
template <>
bool operator==(const detail::LocalPoint<Lane, Lane> &lhs, const detail::LocalPoint<Lane, const Lane> &rhs)
{
    if (lhs.GetLane() == rhs.GetLane())
    {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
    }
    else
    {
        return (lhs.GetLane().GetRoad() == rhs.GetLane().GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
    }
}
template <>
bool operator==(const detail::LocalPoint<Lane, Lane> &lhs, const detail::LocalPoint<Lane, Lane> &rhs)
{
    if (lhs.GetLane() == rhs.GetLane())
    {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
    }
    else
    {
        return (lhs.GetLane().GetRoad() == rhs.GetLane().GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
    }
}

template <>
bool operator==(const detail::LocalPoint<Lane, const Lane> &lhs, const detail::LocalPoint<Road, const Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == rhs);
}
template <>
bool operator==(const detail::LocalPoint<Lane, const Lane> &lhs, const detail::LocalPoint<Road, Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == rhs);
}
template <>
bool operator==(const detail::LocalPoint<Lane, Lane> &lhs, const detail::LocalPoint<Road, const Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == rhs);
}
template <>
bool operator==(const detail::LocalPoint<Lane, Lane> &lhs, const detail::LocalPoint<Road, Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == rhs);
}

template <>
bool operator==(const detail::LocalPoint<Road, const Lane> &lhs, const detail::LocalPoint<Lane, const Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (lhs == Coordinates<Road>(rhs));
}
template <>
bool operator==(const detail::LocalPoint<Road, const Lane> &lhs, const detail::LocalPoint<Lane, Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (lhs == Coordinates<Road>(rhs));
}
template <>
bool operator==(const detail::LocalPoint<Road, Lane> &lhs, const detail::LocalPoint<Lane, const Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (lhs == Coordinates<Road>(rhs));
}
template <>
bool operator==(const detail::LocalPoint<Road, Lane> &lhs, const detail::LocalPoint<Lane, Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (lhs == Coordinates<Road>(rhs));
}

template <>
bool operator==(const detail::LocalPoint<Road, const Lane> &lhs, const detail::LocalPoint<Road, const Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs)));
}
template <>
bool operator==(const detail::LocalPoint<Road, const Lane> &lhs, const detail::LocalPoint<Road, Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
}
template <>
bool operator==(const detail::LocalPoint<Road, Lane> &lhs, const detail::LocalPoint<Road, const Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs)));
}
template <>
bool operator==(const detail::LocalPoint<Road, Lane> &lhs, const detail::LocalPoint<Road, Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
}

// operator<<

template <>
std::ostream &operator<<(std::ostream &os, const detail::LocalPoint<Road, const Lane> &rhs)
{
    return os << "[Road \"" << rhs.GetRoad().GetId() << "\", " << Coordinates<Road>(rhs) << ']';
}
template <>
std::ostream &operator<<(std::ostream &os, const detail::LocalPoint<Road, Lane> &rhs)
{
    return os << "[Road \"" << rhs.GetRoad().GetId() << "\", " << Coordinates<Road>(rhs) << ']';
}

template <>
std::ostream &operator<<(std::ostream &os, const detail::LocalPoint<Lane, const Lane> &rhs)
{
    return os << "[Lane " << rhs.GetLane().GetId() << ", " << Coordinates<Lane>(rhs) << ']';
}
template <>
std::ostream &operator<<(std::ostream &os, const detail::LocalPoint<Lane, const Lane> &rhs)
{
    return os << "[Lane " << rhs.GetLane().GetId() << ", " << Coordinates<Lane>(rhs) << ']';
}
} // namespace osiql
