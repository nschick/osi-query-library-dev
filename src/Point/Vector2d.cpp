/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/Vector2d.h"

#include <iostream>

#include "OsiQueryLibrary/Street/LaneBoundary.h"
#include "OsiQueryLibrary/Street/LaneMarking.h"
#include "OsiQueryLibrary/Street/ReferenceLine.h"
#include "OsiQueryLibrary/Types/Common.h"
#include "OsiQueryLibrary/Types/Geometry.h"

namespace osiql {
double GetX(const Vector2d &point)
{
    return point.x;
}

double GetY(const Vector2d &point)
{
    return point.x;
}

Vector2d::Vector2d(const Vector<double, 3> &point) :
    Vector<double, 2>(point.x, point.y)
{
}

Vector2d::Vector2d(const osi3::Vector2d &point) :
    Vector<double, 2>(point.x(), point.y())
{
}

Vector2d::Vector2d(const osi3::Vector3d &point) :
    Vector<double, 2>(point.x(), point.y())
{
}

Vector2d::Vector2d(const LaneBoundary::Point &point) :
    Vector2d(point.position())
{
}

Vector2d::Vector2d(const LaneMarking::Point &point) :
    Vector2d(point.position())
{
}

Vector2d::Vector2d(const ReferenceLine::Point &point) :
    Vector2d(point.world_position())
{
}

Vector2d Vector2d::Rotate(double angle) const noexcept
{
    const double cos{std::cos(angle)};
    const double sin{std::sin(angle)};
    return {x * cos - y * sin, x * sin + y * cos};
}

[[nodiscard]] double Vector2d::Angle() const noexcept
{
    return std::atan2(y, x);
}

Vector2d Vector2d::Projection(double yaw) const
{
    const Vector2d line{std::cos(yaw), std::sin(yaw)};
    return line.Dot(*this) * line / line.SquaredLength();
}

Vector2d Vector2d::Projection(const Vector2d &a, const Vector2d &b) const
{
    const Vector2d ab{b - a};
    return a + (ab.Dot(*this - a) * ab / ab.SquaredLength());
}

double Vector2d::ProjectionLength(double yaw) const
{
    return x * std::cos(yaw) + y * std::sin(yaw);
}

Side Vector2d::GetSide(const Vector2d &a, const Vector2d &b) const
{
    const double determinant{(b - a).Cross(*this - a)};
    if (determinant > 0.0)
    {
        return Side::Left;
    }
    else if (determinant < 0.0)
    {
        return Side::Right;
    }
    return Side::None;
}

Vector2d Vector2d::GetPathTo(const Vector2d &a, const Vector2d &b) const
{
    const Vector2d &ab{b - a};
    const double length{ab.SquaredLength()};
    if (length != 0.0)
    {
        const double ratio{std::max(0.0, std::min(1.0, (*this - a).Dot(ab) / length))};
        const Vector2d projection{a * (1.0 - ratio) + b * ratio};
        return *this - projection;
    }
    return *this - a;
}

double Vector2d::GetSignedDistanceTo(const Vector2d &a, const Vector2d &b) const
{
    return GetSide(a, b) == Side::Right ? -GetPathTo(a, b).Length() : GetPathTo(a, b).Length();
}

void PrintTo(const Vector2d &point, std::ostream *os)
{
    *os << point;
}

std::ostream &operator<<(std::ostream &os, Winding w)
{
    return os << detail::windingToString[static_cast<size_t>(w)];
}
} // namespace osiql
