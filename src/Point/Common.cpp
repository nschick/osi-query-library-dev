/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/Common.h"

#include <limits>

#include "OsiQueryLibrary/Point/Vector2d.h"

namespace osiql {
double GetX(const osi3::ReferenceLine::ReferenceLinePoint &point)
{
    return point.world_position().x();
}
double GetY(const osi3::ReferenceLine::ReferenceLinePoint &point)
{
    return point.world_position().y();
}

std::ostream &operator<<(std::ostream &os, const osi3::ReferenceLine::ReferenceLinePoint &p)
{
    if constexpr (HAS_MEMBER(osi3::ReferenceLine::ReferenceLinePoint, has_t_axis_yaw()))
    {
        return os << '[' << Vector2d(p) << ", " << GetLatitude(p) << ", " << (p.has_t_axis_yaw() ? std::to_string(p.t_axis_yaw()) : "_") << ']';
    }
    else
    {
        return os << '[' << Vector2d(p) << ", " << GetLatitude(p) << ']';
    }
}
} // namespace osiql
