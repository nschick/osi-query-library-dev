/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/LanePoint.h"

#include "OsiQueryLibrary/Point/RoadPoint.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
LanePoint::LanePoint(const Lane &lane, const Coordinates<Lane> &coordinates) :
    Coordinates<Lane>(coordinates), lane(lane)
{
}
detail::LanePoint::LanePoint(Lane &lane, const Coordinates<Lane> &coordinates) :
    Coordinates<Lane>(coordinates), lane(lane)
{
}

LanePoint::LanePoint(const Lane &lane, Coordinates<Lane> &&coordinates) :
    Coordinates<Lane>(std::forward<Coordinates<Lane>>(coordinates)), lane(lane)
{
}
detail::LanePoint::LanePoint(Lane &lane, Coordinates<Lane> &&coordinates) :
    Coordinates<Lane>(std::forward<Coordinates<Lane>>(coordinates)), lane(lane)
{
}

double GetLaneLongitude(const Lane &lane, const Coordinates<Road> &coordinates)
{
    const auto &leftBoundary{lane.GetBoundaryPoints<osiql::Side::Left>()};
    const double left{leftBoundary.GetLongitude(coordinates.latitude)};

    const auto &rightBoundary{lane.GetBoundaryPoints<osiql::Side::Right>()};
    const double right{rightBoundary.GetLongitude(coordinates.latitude)};

    const double width{right - left};
    if (width != 0.0)
    {
        const double ratio{(coordinates.longitude - left) / width};
        const double halfWidth{width * 0.5};
        return -halfWidth * (1.0 - ratio) + halfWidth * ratio;
    }
    else
    {
        return 0.0;
    }
}
double GetLaneLatitude(const Lane &lane, double roadLatitude)
{
    if (lane.GetDirection() == Direction::Upstream)
    {
        return GetLatitude<Direction::Upstream>(lane) - roadLatitude;
    }
    else
    {
        return roadLatitude - GetLatitude<Direction::Downstream>(lane);
    }
}

LanePoint::LanePoint(const detail::RoadPoint &point) :
    Coordinates<Lane>{GetLaneLatitude(point.GetLane(), point.latitude), GetLaneLongitude(point.GetLane(), point)}, lane(point.GetLane())
{
}
detail::LanePoint::LanePoint(detail::RoadPoint &point) :
    Coordinates<Lane>{GetLaneLatitude(point.GetLane(), point.latitude), GetLaneLongitude(point.GetLane(), point)}, lane(point.GetLane())
{
}

LanePoint::LanePoint(const RoadPoint &point) :
    Coordinates<Lane>{GetLaneLatitude(point.GetLane(), point.latitude), GetLaneLongitude(point.GetLane(), point)}, lane(point.GetLane())
{
}

LanePoint::LanePoint(const Assignment<Lane> &assignment) :
    Coordinates<Lane>(RoadPoint(assignment)), lane(assignment.GetEntity())
{
}
detail::LanePoint::LanePoint(Assignment<Lane> &assignment) :
    Coordinates<Lane>(detail::RoadPoint(assignment)), lane(assignment.GetEntity())
{
}

LanePoint::operator Coordinates<Road>() const
{
    const double roadLatitude{GetRoadLatitude()};
    const double leftLongitude{GetLane().GetBoundaryPoints<Side::Left>().GetLongitude(roadLatitude)};
    const double rightLongitude{GetLane().GetBoundaryPoints<Side::Right>().GetLongitude(roadLatitude)};
    return {roadLatitude, longitude + (leftLongitude * 0.5 + rightLongitude * 0.5)};
}
detail::LanePoint::operator Coordinates<Road>() const
{
    const double roadLatitude{GetRoadLatitude()};
    const double leftLongitude{GetLane().GetBoundaryPoints<Side::Left>().GetLongitude(roadLatitude)};
    const double rightLongitude{GetLane().GetBoundaryPoints<Side::Right>().GetLongitude(roadLatitude)};
    return {roadLatitude, longitude + (leftLongitude * 0.5 + rightLongitude * 0.5)};
}

const Lane &LanePoint::GetLane() const
{
    return lane;
}
const Lane &detail::LanePoint::GetLane() const
{
    return lane;
}
Lane &detail::LanePoint::GetLane()
{
    return lane;
}

const Road &LanePoint::GetRoad() const
{
    return GetLane().GetRoad();
}
const Road &detail::LanePoint::GetRoad() const
{
    return GetLane().GetRoad();
}
Road &detail::LanePoint::GetRoad()
{
    return GetLane().GetRoad();
}

double LanePoint::GetRoadLatitude() const
{
    if (GetLane().GetDirection() == Direction::Downstream)
    {
        return GetLatitude<Direction::Downstream>(GetLane()) + latitude;
    }
    else
    {
        return GetLatitude<Direction::Upstream>(GetLane()) - latitude;
    }
}
double detail::LanePoint::GetRoadLatitude() const
{
    if (GetLane().GetDirection() == Direction::Downstream)
    {
        return GetLatitude<Direction::Downstream>(GetLane()) + latitude;
    }
    else
    {
        return GetLatitude<Direction::Upstream>(GetLane()) - latitude;
    }
}

Vector2d LanePoint::GetXY() const
{
    return GetRoad().referenceLine.GetXY(*this);
}
Vector2d detail::LanePoint::GetXY() const
{
    return GetRoad().referenceLine.GetXY(*this);
}

const Lane &GetLane(const LanePoint &point)
{
    return point.GetLane();
}
const Lane &GetLane(const detail::LanePoint &point)
{
    return point.GetLane();
}
Lane &GetLane(detail::LanePoint &point)
{
    return point.GetLane();
}

bool operator==(const LanePoint &lhs, const LanePoint &rhs)
{
    if (lhs.GetLane() == rhs.GetLane())
    {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
    }
    else
    {
        return (lhs.GetLane().GetRoad() == rhs.GetLane().GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
    }
}
bool operator==(const detail::LanePoint &lhs, const LanePoint &rhs)
{
    if (lhs.GetLane() == rhs.GetLane())
    {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
    }
    else
    {
        return (lhs.GetLane().GetRoad() == rhs.GetLane().GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
    }
}

bool operator==(const LanePoint &lhs, const detail::LanePoint &rhs)
{
    if (lhs.GetLane() == rhs.GetLane())
    {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
    }
    else
    {
        return (lhs.GetLane().GetRoad() == rhs.GetLane().GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
    }
}
bool operator==(const detail::LanePoint &lhs, const detail::LanePoint &rhs)
{
    if (lhs.GetLane() == rhs.GetLane())
    {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
    }
    else
    {
        return (lhs.GetLane().GetRoad() == rhs.GetLane().GetRoad()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
    }
}

bool operator==(const LanePoint &lhs, const Assignment<Lane> &rhs)
{
    return (lhs.GetLane() == rhs.GetEntity()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
}
bool operator==(const detail::LanePoint &lhs, const Assignment<Lane> &rhs)
{
    return (lhs.GetLane() == rhs.GetEntity()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
}

bool operator==(const Assignment<Lane> &lhs, const LanePoint &rhs)
{
    return (lhs.GetEntity() == rhs.GetLane()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
}
bool operator==(const Assignment<Lane> &lhs, const detail::LanePoint &rhs)
{
    return (lhs.GetEntity() == rhs.GetLane()) && (Coordinates<Road>(lhs) == Coordinates<Road>(rhs));
}

bool operator==(const LanePoint &lhs, const RoadPoint &rhs)
{
    return (lhs.GetLane().GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == rhs);
}

bool operator==(const detail::LanePoint &lhs, const RoadPoint &rhs)
{
    return (lhs.GetLane().GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == rhs);
}

bool operator==(const LanePoint &lhs, const detail::RoadPoint &rhs)
{
    return (lhs.GetLane().GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == rhs);
}

bool operator==(const detail::LanePoint &lhs, const detail::RoadPoint &rhs)
{
    return (lhs.GetLane().GetRoad() == rhs.GetRoad()) && (Coordinates<Road>(lhs) == rhs);
}

bool operator!=(const LanePoint &lhs, const LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::LanePoint &lhs, const LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const LanePoint &lhs, const detail::LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::LanePoint &lhs, const detail::LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const LanePoint &lhs, const Assignment<Lane> &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::LanePoint &lhs, const Assignment<Lane> &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const Assignment<Lane> &lhs, const LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const Assignment<Lane> &lhs, const detail::LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const LanePoint &lhs, const RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::LanePoint &lhs, const RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const LanePoint &lhs, const detail::RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::LanePoint &lhs, const detail::RoadPoint &rhs)
{
    return !(lhs == rhs);
}

std::ostream &operator<<(std::ostream &os, const LanePoint &rhs)
{
    return os << "[Lane " << rhs.GetLane().GetId() << ", " << Coordinates<Lane>(rhs) << ']';
}

std::ostream &operator<<(std::ostream &os, const detail::LanePoint &rhs)
{
    return os << "[Lane " << rhs.GetLane().GetId() << ", " << Coordinates<Lane>(rhs) << ']';
}
} // namespace osiql
