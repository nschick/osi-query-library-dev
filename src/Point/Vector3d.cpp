/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/Vector3d.h"

namespace osiql {
Vector3d::Vector3d(const osi3::Vector3d &point) :
    Vector<double, 3>(point.x(), point.y(), point.z())
{
}

Vector3d::Vector3d(const osi3::Vector2d &point, double z) :
    Vector<double, 3>(point.x(), point.y(), z)
{
}

Vector3d::Vector3d(const osi3::LogicalLaneBoundary::LogicalBoundaryPoint &point) :
    Vector3d(point.position())
{
}

Vector3d::Vector3d(const osi3::LaneBoundary::BoundaryPoint &point) :
    Vector3d(point.position())
{
}

Vector3d::Vector3d(const osi3::ReferenceLine::ReferenceLinePoint &point) :
    Vector3d(point.world_position())
{
}

void PrintTo(const Vector3d &point, std::ostream *os)
{
    *os << point;
}
} // namespace osiql
