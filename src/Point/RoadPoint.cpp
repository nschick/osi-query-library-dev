/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Point/RoadPoint.h"

#include "OsiQueryLibrary/Point/LanePoint.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
RoadPoint::RoadPoint(const Lane &lane, const Coordinates<Road> &coordinates) :
    Coordinates<Road>(coordinates), lane(lane)
{
}
detail::RoadPoint::RoadPoint(Lane &lane, const Coordinates<Road> &coordinates) :
    Coordinates<Road>(coordinates), lane(lane)
{
}

RoadPoint::RoadPoint(const Lane &lane, Coordinates<Road> &&coordinates) :
    Coordinates<Road>(std::forward<Coordinates<Road>>(coordinates)), lane(lane)
{
}
detail::RoadPoint::RoadPoint(Lane &lane, Coordinates<Road> &&coordinates) :
    Coordinates<Road>(std::forward<Coordinates<Road>>(coordinates)), lane(lane)
{
}

RoadPoint::RoadPoint(const detail::RoadPoint &point) :
    Coordinates<Road>(point), lane(point.GetLane())
{
}

RoadPoint::RoadPoint(const LanePoint &point) :
    Coordinates<Road>(point), lane(point.GetLane())
{
}
RoadPoint::RoadPoint(const detail::LanePoint &point) :
    Coordinates<Road>(point), lane(point.GetLane())
{
}
detail::RoadPoint::RoadPoint(detail::LanePoint &point) :
    Coordinates<Road>(point), lane(point.GetLane())
{
}

RoadPoint::RoadPoint(const Assignment<Lane> &assignment) :
    Coordinates<Road>(assignment), lane(assignment.GetEntity())
{
}
detail::RoadPoint::RoadPoint(Assignment<Lane> &assignment) :
    Coordinates<Road>(assignment), lane(assignment.GetEntity())
{
}

RoadPoint::operator Coordinates<Lane>() const
{
    return {GetLaneLatitude(*this), GetLaneLongitude(*this)};
}
detail::RoadPoint::operator Coordinates<Lane>() const
{
    return {GetLaneLatitude(*this), GetLaneLongitude(*this)};
}

const Lane &RoadPoint::GetLane() const
{
    return lane;
}
const Lane &detail::RoadPoint::GetLane() const
{
    return lane;
}
Lane &detail::RoadPoint::GetLane()
{
    return lane;
}

const Lane &GetLane(const RoadPoint &point)
{
    return point.GetLane();
}
const Lane &GetLane(const detail::RoadPoint &point)
{
    return point.GetLane();
}
Lane &GetLane(detail::RoadPoint &point)
{
    return point.GetLane();
}

const Road &RoadPoint::GetRoad() const
{
    return GetLane().GetRoad();
}
const Road &detail::RoadPoint::GetRoad() const
{
    return GetLane().GetRoad();
}

Vector2d RoadPoint::GetXY() const
{
    return GetRoad().referenceLine.GetXY(*this);
}
Vector2d detail::RoadPoint::GetXY() const
{
    return GetRoad().referenceLine.GetXY(*this);
}

template <>
double RoadPoint::GetDistanceTo<Orientation::Forwards>(const RoadPoint &destination, double maxRange) const
{
    if ((GetRoad() == destination.GetRoad()) && (GetSideOfRoad(GetLane()) == GetSideOfRoad(destination.GetLane())))
    {
        const double d{GetLane().GetDistanceBetween<Orientation::Forwards>(latitude, destination.latitude)};
        if (d >= 0.0 && d <= maxRange)
        {
            return d;
        }
        return std::numeric_limits<double>::infinity();
    }

    double distanceToEndOfRoad{latitude};
    const std::vector<Lane *> *lanes;

    if (GetLane().GetDirection<Orientation::Forwards>() == Direction::Upstream)
    {
        if (distanceToEndOfRoad > maxRange)
        {
            return std::numeric_limits<double>::infinity();
        }
        lanes = &GetRoad().GetConnectedLanes<Side::Left, Orientation::Forwards>();
    }
    else // Direction::Downstream
    {
        distanceToEndOfRoad = GetRoad().GetLength() - latitude;
        if (distanceToEndOfRoad > maxRange)
        {
            return std::numeric_limits<double>::infinity();
        }
        lanes = &GetRoad().GetConnectedLanes<Side::Right, Orientation::Forwards>();
    }
    std::set<RoadId> roads;
    for (const Lane *lane : *lanes)
    {
        if (roads.insert(lane->GetRoadId()).second)
        {
            const RoadPoint pointOnNextRoad{*lane, Coordinates<Road>{lane->GetStartLatitude(), 0.0}};
            const double remainingDistance{pointOnNextRoad.GetDistanceTo<Orientation::Forwards>(destination, maxRange - distanceToEndOfRoad)};
            if (remainingDistance != std::numeric_limits<double>::infinity())
            {
                return distanceToEndOfRoad + remainingDistance;
            }
        }
    }
    return std::numeric_limits<double>::infinity();
}

template <>
double RoadPoint::GetDistanceTo<Orientation::Backwards>(const RoadPoint &destination, double maxRange) const
{
    if ((GetRoad() == destination.GetRoad()) && (GetSideOfRoad(GetLane()) == GetSideOfRoad(destination.GetLane())))
    {
        const double d{GetLane().GetDistanceBetween<Orientation::Backwards>(latitude, destination.latitude)};
        if (d >= 0.0 && d <= maxRange)
        {
            return d;
        }
        return std::numeric_limits<double>::infinity();
    }

    double distanceToEndOfRoad{latitude};
    const std::vector<Lane *> *lanes;

    if (GetLane().GetDirection<Orientation::Backwards>() == Direction::Upstream)
    {
        if (distanceToEndOfRoad > maxRange)
        {
            return std::numeric_limits<double>::infinity();
        }
        lanes = &GetRoad().GetConnectedLanes<Side::Left, Orientation::Backwards>();
    }
    else // Direction::Downstream
    {
        distanceToEndOfRoad = GetRoad().GetLength() - latitude;
        if (distanceToEndOfRoad > maxRange)
        {
            return std::numeric_limits<double>::infinity();
        }
        lanes = &GetRoad().GetConnectedLanes<Side::Right, Orientation::Backwards>();
    }
    std::set<RoadId> roads;
    for (const Lane *lane : *lanes)
    {
        if (roads.insert(lane->GetRoadId()).second)
        {
            const RoadPoint pointOnNextRoad{*lane, Coordinates<Road>{lane->GetEndLatitude(), 0.0}};
            const double remainingDistance{pointOnNextRoad.GetDistanceTo<Orientation::Backwards>(destination, maxRange - distanceToEndOfRoad)};
            if (remainingDistance != std::numeric_limits<double>::infinity())
            {
                return distanceToEndOfRoad + remainingDistance;
            }
        }
    }
    return std::numeric_limits<double>::infinity();
}

template <>
double RoadPoint::GetDistanceTo<Orientation::Forwards>(double targetLatitude) const
{
    return GetDistanceTo(targetLatitude, GetLane().GetDirection<Orientation::Forwards>());
}
template <>
double RoadPoint::GetDistanceTo<Orientation::Backwards>(double targetLatitude) const
{
    return GetDistanceTo(targetLatitude, GetLane().GetDirection<Orientation::Backwards>());
}
double RoadPoint::GetDistanceTo(double l, Orientation o) const
{
    return o == Orientation::Backwards ? GetDistanceTo<Orientation::Backwards>(l) : GetDistanceTo<Orientation::Forwards>(l);
}
template <>
double RoadPoint::GetDistanceTo<Direction::Downstream>(double targetLatitude) const
{
    return targetLatitude - latitude;
}
template <>
double RoadPoint::GetDistanceTo<Direction::Upstream>(double targetLatitude) const
{
    return latitude - targetLatitude;
}
double RoadPoint::GetDistanceTo(double l, Direction d) const
{
    return d == Direction::Upstream ? GetDistanceTo<Direction::Upstream>(l) : GetDistanceTo<Direction::Downstream>(l);
}

// std::pair<double, double> RoadPoint::GetDeviation(const std::vector<RoadPoint> &points, const Route &route) const
// {
//     // NOTE: Warning! This is a shortcut. It's possible that the leftmost point on the road is not the furthest left of the driveline.
//     // This is however very unlikely and the margin of error is small.
//     const auto [min, max]{std::minmax_element(points.begin(), points.end(), [](const RoadPoint &lhs, const RoadPoint &rhs) {
//         return lhs.longitude < rhs.longitude;
//     })};
//     const double t{route.GetDeviation(*this)};
//     return {route.GetDeviation(*min) - t, route.GetDeviation(*max) - t};
// }
// std::pair<double, double> detail::RoadPoint::GetDeviation(const std::vector<RoadPoint> &points, const Route &route) const
// {
//     // NOTE: Warning! This is a shortcut. It's possible that the leftmost point on the road is not the furthest left of the driveline.
//     // This is however very unlikely and the margin of error is small.
//     const auto [min, max]{std::minmax_element(points.begin(), points.end(), [](const RoadPoint &lhs, const RoadPoint &rhs) {
//         return lhs.longitude < rhs.longitude;
//     })};
//     const double t{route.GetDeviation(*this)};
//     return {route.GetDeviation(*min) - t, route.GetDeviation(*max) - t};
// }

bool operator==(const RoadPoint &lhs, const RoadPoint &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
}
bool operator==(const RoadPoint &lhs, const detail::RoadPoint &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
}
bool operator==(const detail::RoadPoint &lhs, const RoadPoint &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
}
bool operator==(const detail::RoadPoint &lhs, const detail::RoadPoint &rhs)
{
    return (lhs.GetRoad() == rhs.GetRoad()) && (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude);
}
bool operator==(const RoadPoint &lhs, const Assignment<Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetEntity().GetRoad()) && (lhs.latitude == GetLatitude(rhs)) && (lhs.longitude == GetLongitude(rhs));
}
bool operator==(const detail::RoadPoint &lhs, const Assignment<Lane> &rhs)
{
    return (lhs.GetRoad() == rhs.GetEntity().GetRoad()) && (lhs.latitude == GetLatitude(rhs)) && (lhs.longitude == GetLongitude(rhs));
}
bool operator==(const RoadPoint &lhs, const LanePoint &rhs)
{
    return (lhs.GetRoad() == rhs.GetLane().GetRoad()) && (lhs == Coordinates<Road>(rhs));
}
bool operator==(const detail::RoadPoint &lhs, const LanePoint &rhs)
{
    return (lhs.GetRoad() == rhs.GetLane().GetRoad()) && (lhs == Coordinates<Road>(rhs));
}
bool operator==(const RoadPoint &lhs, const detail::LanePoint &rhs)
{
    return (lhs.GetRoad() == rhs.GetLane().GetRoad()) && (lhs == Coordinates<Road>(rhs));
}
bool operator==(const detail::RoadPoint &lhs, const detail::LanePoint &rhs)
{
    return (lhs.GetRoad() == rhs.GetLane().GetRoad()) && (lhs == Coordinates<Road>(rhs));
}

bool operator==(const Assignment<Lane> &lhs, const RoadPoint &rhs)
{
    return (lhs.GetEntity().GetRoad() == rhs.GetRoad()) && (GetLatitude(lhs) == rhs.latitude) && (GetLongitude(lhs) == rhs.longitude);
}
bool operator==(const Assignment<Lane> &lhs, const detail::RoadPoint &rhs)
{
    return (lhs.GetEntity().GetRoad() == rhs.GetRoad()) && (GetLatitude(lhs) == rhs.latitude) && (GetLongitude(lhs) == rhs.longitude);
}

bool operator!=(const RoadPoint &lhs, const RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::RoadPoint &lhs, const RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const RoadPoint &lhs, const detail::RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::RoadPoint &lhs, const detail::RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const RoadPoint &lhs, const Assignment<Lane> &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::RoadPoint &lhs, const Assignment<Lane> &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const Assignment<Lane> &lhs, const RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const Assignment<Lane> &lhs, const detail::RoadPoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const RoadPoint &lhs, const LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::RoadPoint &lhs, const LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const RoadPoint &lhs, const detail::LanePoint &rhs)
{
    return !(lhs == rhs);
}
bool operator!=(const detail::RoadPoint &lhs, const detail::LanePoint &rhs)
{
    return !(lhs == rhs);
}

std::ostream &operator<<(std::ostream &os, const detail::RoadPoint &rhs)
{
    return os << "[Lane " << rhs.GetLane().GetId() << ", " << Coordinates<Road>(rhs) << ']';
}
std::ostream &operator<<(std::ostream &os, const RoadPoint &rhs)
{
    return os << "[Lane " << rhs.GetLane().GetId() << ", " << Coordinates<Road>(rhs) << ']';
}
} // namespace osiql
