/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Lane/BoundaryEnclosure.h"

#include "OsiQueryLibrary/Component/Comparable.h"

namespace osiql::detail {
const Boundary::Points &BoundaryEnclosure::GetBoundaryPoints(Side s, Orientation o) const
{
    return o == Orientation::Forwards ? GetBoundaryPoints<Orientation::Forwards>(s) : GetBoundaryPoints<Orientation::Backwards>(s);
}

bool BoundaryEnclosure::Contains(const osiql::RoadPoint &point) const
{
    if ((GetHandle().start_s() <= point.latitude) && (GetHandle().end_s() >= point.latitude))
    {
        if (leftBoundaryPoints.GetLongitude(point.latitude) >= point.longitude)
        {
            return rightBoundaryPoints.GetLongitude(point.latitude) <= point.longitude;
        }
    }
    return false;
}

double BoundaryEnclosure::GetAngle(double s, Orientation o) const
{
    return GetAngle(s, GetDirection(o));
}
double BoundaryEnclosure::GetAngle(double s, Direction d) const
{
    return d == Direction::Upstream ? GetAngle<Direction::Upstream>(s) : GetAngle<Direction::Downstream>(s);
}

double BoundaryEnclosure::GetCurvature(double s, Orientation o) const
{
    return o == Orientation::Backwards ? GetCurvature<Orientation::Backwards>(s) : GetCurvature<Orientation::Forwards>(s);
}

double BoundaryEnclosure::GetCurvature(double s, Direction d) const
{
    return d == Direction::Upstream ? GetCurvature<Direction::Upstream>(s) : GetCurvature<Direction::Downstream>(s);
}
} // namespace osiql::detail
