/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Lane/OrientedLane.h"

namespace osiql {
namespace detail {
Direction OrientedLane::GetDirection(Orientation o) const
{
    return o == Orientation::Backwards ? !GetDirection() : GetDirection();
}

Orientation OrientedLane::GetOrientation(Direction d) const
{
    return GetDirection() == d ? Orientation::Forwards : Orientation::Backwards;
}

Side OrientedLane::GetSideOfRoad() const
{
    return GetDirection() == Direction::Upstream ? Side::Left : Side::Right;
}

double OrientedLane::GetLatitude(double distance, Direction d) const
{
    return d == Direction::Upstream ? this->GetLatitude<Direction::Upstream>(distance) : this->GetLatitude<Direction::Downstream>(distance);
}

double OrientedLane::GetStartLatitude(Direction d) const
{
    return d == Direction::Upstream ? GetStartLatitude<Direction::Upstream>() : GetStartLatitude<Direction::Downstream>();
}

double OrientedLane::GetEndLatitude(Direction d) const
{
    return d == Direction::Upstream ? GetEndLatitude<Direction::Upstream>() : GetEndLatitude<Direction::Downstream>();
}
} // namespace detail

double GetLatitude(const detail::OrientedLane &lane, Direction d, Orientation o)
{
    return o == Orientation::Backwards ? GetLatitude<Orientation::Backwards>(lane, d) : GetLatitude<Orientation::Forwards>(lane, d);
}

double GetLatitude(const detail::OrientedLane &lane, Direction d)
{
    return d == Direction::Upstream ? GetLatitude<Direction::Upstream>(lane) : GetLatitude<Direction::Downstream>(lane);
}
} // namespace osiql
