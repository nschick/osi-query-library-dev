/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/LaneBoundary.h"

namespace osiql {
Container<LaneBoundary::Point>::const_iterator LaneBoundary::begin() const
{
    return GetHandle().boundary_line().begin();
}

Container<LaneBoundary::Point>::const_iterator LaneBoundary::end() const
{
    return GetHandle().boundary_line().end();
}

Side LaneBoundary::GetPassingRule(Direction d) const
{
    return d == Direction::Upstream ? GetPassingRule<Direction::Upstream>() : GetPassingRule<Direction::Downstream>();
}

bool LaneBoundary::AllowsPassing(Side s, Direction d) const
{
    return s == Side::Left ? AllowsPassing<Side::Left>(d) : AllowsPassing<Side::Right>(d);
}

double GetX(const LaneBoundary::Point &point)
{
    return point.position().x();
}

double GetY(const LaneBoundary::Point &point)
{
    return point.position().y();
}

std::ostream &operator<<(std::ostream &os, const LaneBoundary::Point &point)
{
    return os << '[' << Vector2d(point) << ", " << Coordinates<Road>(point) << ']';
}
std::ostream &operator<<(std::ostream &os, const LaneBoundary::MarkingRange &range)
{
    return os << '[' << range.min << " - " << range.max << ", " << range.markingPartition << ']';
}
std::ostream &operator<<(std::ostream &os, const LaneBoundary &boundary)
{
    return os << '{' << boundary.GetPassingRule<Direction::Downstream>() << " | " << boundary.front() << " - " << boundary.back() << '}';
}
} // namespace osiql
