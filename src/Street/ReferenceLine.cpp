/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/ReferenceLine.h"

#include <utility>

namespace osiql {
ReferenceLine::ReferenceLine(const osi3::ReferenceLine &handle, const Version &version) :
    Identifiable<osi3::ReferenceLine>::Identifiable(handle), version(version), longitudinalAxes(size()), intersections(size() - 1)
{
}

typename Container<ReferenceLine::Point>::const_iterator ReferenceLine::begin() const
{
    return GetHandle().poly_line().begin();
}

typename Container<ReferenceLine::Point>::const_iterator ReferenceLine::end() const
{
    return GetHandle().poly_line().end();
}

const std::optional<Vector2d> &ReferenceLine::GetLongitudinalAxisIntersection(size_t i) const
{
    if (!intersections[i])
    {
        auto vertex{std::next(begin(), static_cast<std::ptrdiff_t>(i))};
        const Line startingLateralAxis{*vertex, *vertex + GetLongitudinalAxis(vertex)};
        const Line endingLateralAxis{*std::next(vertex), *std::next(vertex) + GetLongitudinalAxis(std::next(vertex))};
        intersections[i] = std::make_unique<std::optional<Vector2d>>(startingLateralAxis.GetIntersection(endingLateralAxis));
    }
    return *intersections[i];
}

Coordinates<Road> ReferenceLine::GetCoordinates(const Vector2d &p) const
{
    if constexpr (HAS_MEMBER(Point, has_t_axis_yaw())) // If osiql is built with OSI version 3.6.x or above
    {
        // osiql might still be run with an older OSI version than it was built with:
        if (GetVersion() < Version(3u, 6u, 0u))
        {
            return Points<Container<Point>>::GetCoordinates(p);
        }
        // The version might match, but the data could be missing or intentionally older:
        if (!GetHandle().has_type() || (GetHandle().type() == osi3::ReferenceLine_Type_TYPE_POLYLINE))
        {
            return Points<Container<Point>>::GetCoordinates(p);
        }
        auto it{end(p)};
        if (it == begin())
        {
            ++it;
        }
        const size_t i{static_cast<size_t>(std::distance(begin(), it))};
        const std::optional<Vector2d> &vanishingPoint{GetLongitudinalAxisIntersection(i - 1)};
        if (vanishingPoint.has_value())
        {
            const Line edge{*std::prev(it), *it};
            const Vector2d intersection{Line{vanishingPoint.value(), p}.GetIntersection(edge).value()};
            const double ratio{
                edge.start.x != edge.end.x
                    ? (intersection.x - edge.start.x) / (edge.end.x - edge.start.x)
                    : (intersection.y - edge.start.y) / (edge.end.y - edge.start.y) //
            };
            return Coordinates<Road>{
                GetLatitude(*std::prev(it)) * (1.0 - ratio) + GetLatitude(*it) * ratio,
                p.GetSide(edge.start, edge.end) == Side::Right ? -(p - intersection).Length() : (p - intersection).Length() //
            };
        }
        else
        {
            const Vector2d axis{GetLongitudinalAxis(it)};
            const Line edge{*std::prev(it), *it};
            const Vector2d path{edge.end - edge.start};
            if ((path.GetPerpendicular<Winding::CounterClockwise>() - axis).SquaredLength() > 0.001)
            {
                const Vector2d intersection{edge.GetIntersection(Line{p, p + axis}).value()};
                const double ratio{
                    edge.start.x != edge.end.x
                        ? (intersection.x - edge.start.x) / (edge.end.x - edge.start.x)
                        : (intersection.y - edge.start.y) / (edge.end.y - edge.start.y) //
                };
                return Coordinates<Road>{
                    GetLatitude(*std::prev(it)) * (1.0 - ratio) + GetLatitude(*it) * ratio,
                    p.GetSide(edge.start, edge.end) == Side::Right ? -(p - intersection).Length() : (p - intersection).Length() //
                };
            }
        }
        return p.GetCoordinates(*std::prev(it), *it);
    }
    else // OSI 3.5
    {
        return Points<Container<Point>>::GetCoordinates(p);
    }
}

Vector2d ReferenceLine::GetXY(const Coordinates<Road> &point) const
{
    if constexpr (HAS_MEMBER(Point, has_t_axis_yaw())) // If osiql is built with OSI version 3.6.x or above
    {
        // osiql might still be run with an older OSI version than it was built with:
        if (GetVersion() < Version(3u, 6u, 0u))
        {
            return Points<Container<Point>>::GetXY(point);
        }
        if (!GetHandle().has_type() || (GetHandle().type() == osi3::ReferenceLine_Type_TYPE_POLYLINE))
        {
            return Points<Container<Point>>::GetXY(point);
        }
        auto it{end(point.latitude)};
        if (it == begin())
        {
            ++it;
        }
        else if (it == end())
        {
            --it;
        }
        const double ratio{(point.latitude - std::prev(it)->s_position()) / (it->s_position() - std::prev(it)->s_position())};
        const Vector2d pointOnEdge{Vector2d{std::prev(it)->world_position()} * (1.0 - ratio) + Vector2d{it->world_position()} * ratio};
        const size_t edgeIndex{static_cast<size_t>(std::distance(begin(), it) - 1)};
        const std::optional<Vector2d> &vanishingPoint{GetLongitudinalAxisIntersection(edgeIndex)};
        if (vanishingPoint.has_value())
        {
            const Vector2d tAxis{Norm(vanishingPoint.value() - pointOnEdge) * point.longitude};
            if (vanishingPoint.value().GetSide(*std::prev(it), *it) == Side::Left)
            {
                return pointOnEdge + tAxis;
            }
            else
            {
                return pointOnEdge - tAxis;
            }
        }
        else
        {
            return pointOnEdge + GetLongitudinalAxis(it) * point.longitude;
        }
    }
    else // OSI 3.5
    {
        return Points<Container<Point>>::GetXY(point);
    }
}

const Version &ReferenceLine::GetVersion() const
{
    return version;
}

std::ostream &operator<<(std::ostream &os, const ReferenceLine &line)
{
    return os << "Reference Line " << line.GetId() << ": " << static_cast<const Points<Container<ReferenceLine::Point>> &>(line);
}
} // namespace osiql
