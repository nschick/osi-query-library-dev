/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Road/LaneRow.h"

namespace osiql {
LaneRow::LaneRow(std::vector<Lane *> &&lanes) :
    lanes(std::forward<std::vector<Lane *>>(lanes))
{
}

typename std::vector<Lane *>::const_iterator LaneRow::begin() const
{
    return lanes.begin();
}
typename std::vector<Lane *>::iterator LaneRow::begin()
{
    return lanes.begin();
}

typename std::vector<Lane *>::const_iterator LaneRow::end() const
{
    return lanes.end();
}
typename std::vector<Lane *>::iterator LaneRow::end()
{
    return lanes.end();
}

Direction LaneRow::GetDirection(Orientation o) const
{
    return o == Orientation::Forwards ? GetDirection<Orientation::Forwards>() : GetDirection<Orientation::Backwards>();
}

Side GetSideOfRoad(const LaneRow &row)
{
    return GetSideOfRoad(*row.front());
}

LaneRows::LaneRows(std::vector<LaneRow> &&rows, std::ptrdiff_t offset) :
    rows(rows), it(std::next(this->rows.begin(), offset))
{
    assert(!rows.empty());
}

typename std::vector<LaneRow>::const_iterator LaneRows::begin(Side s) const
{
    return s == Side::Left ? it : rows.begin();
}

typename std::vector<LaneRow>::const_iterator LaneRows::end(Side s) const
{
    return s == Side::Left ? rows.end() : it;
}

size_t LaneRows::size(Side s) const
{
    return s == Side::Left ? size<Side::Left>() : size<Side::Right>();
}

const LaneRow &LaneRows::at(Side s, size_t i) const
{
    return s == Side::Left ? at<Side::Left>(i) : at<Side::Right>(i);
}
} // namespace osiql
