/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Road/RoadSpecifications.h"

#include "OsiQueryLibrary/Street/Lane/Adjoinable.h"

namespace osiql::detail {
RoadSpecifications::RoadSpecifications(const ReferenceLine &referenceLine, Lane &initialLane) :
    referenceLine(referenceLine)
{
    InitializeLaneRows(initialLane);
    UpdateMin();
    UpdateMax();
    size_t i{0u};
    for (auto row{std::make_reverse_iterator(std::next(rows.begin(), center))}; row != rows.rend(); ++row, ++i)
    {
        for (Lane *lane : *row)
        {
            lane->index = i;
        }
    }
    i = 0u;
    for (auto row{std::next(rows.begin(), center)}; row != rows.end(); ++row, ++i)
    {
        for (Lane *lane : *row)
        {
            lane->index = i;
        }
    }
}

void RoadSpecifications::InitializeLaneRows(Lane &initialLane)
{
    Lane *currentLane{&initialLane};
    rows = {LaneRow{std::vector<Lane *>{currentLane}}};

    while (Lane * nextLane{currentLane->GetDirection() == Direction::Upstream ? currentLane->GetAdjacentLane<Side::Left>() : currentLane->GetAdjacentLane<Side::Right>()})
    {
        rows.push_back(LaneRow{std::vector<Lane *>{nextLane}});
        currentLane = nextLane;
    }
    std::reverse(rows.begin(), rows.end());
    currentLane = &initialLane;
    while (Lane * nextLane{currentLane->GetDirection() == Direction::Upstream ? currentLane->GetAdjacentLane<Side::Right>() : currentLane->GetAdjacentLane<Side::Left>()})
    {
        rows.push_back(LaneRow{std::vector<Lane *>{nextLane}});
        currentLane = nextLane;
    }
    const auto it{std::find_if(rows.begin(), rows.end(), [](const LaneRow &row) {
        const auto &lane{row.front()->GetHandle()};
        return std::any_of(lane.source_reference().begin(), lane.source_reference().end(), [](const osi3::ExternalReference &reference) {
            return reference.type() == "net.asam.opendrive" &&
                   reference.identifier_size() >= 3 &&
                   std::stoi(reference.identifier(2)) == 1;
        });
    })};
    center = std::distance(rows.begin(), it == rows.end() ? std::find_if(rows.begin(), rows.end(), [](const LaneRow &lane) { // clang-format off
        return lane.front()->GetSideOfRoad() == Side::Left;
    }) : it); // clang-format on
}

void RoadSpecifications::UpdateMin()
{ // clang-format off
    min = std::distance(rows.begin(), std::max_element(rows.begin(), rows.end(), [](const LaneRow &lhs, const LaneRow &rhs) { 
        return lhs.back()->GetHandle().start_s() < rhs.back()->GetHandle().start_s();
    }));
} // clang-format on

void RoadSpecifications::UpdateMax()
{ // clang-format off
    max = std::distance(rows.begin(), std::min_element(rows.begin(), rows.end(), [](const LaneRow &lhs, const LaneRow &rhs) { 
        return lhs.back()->GetHandle().end_s() < rhs.back()->GetHandle().end_s();
    })); 
} // clang-format on
} // namespace osiql::detail
