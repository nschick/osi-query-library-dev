/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/LaneSegment.h"

#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
LaneSegment::LaneSegment(Lane &lane, std::array<Vector2d, 3> &&triangle) :
    triangle(std::forward<std::array<Vector2d, 3>>(triangle)), lane(lane)
{
}

Box LaneSegment::GetBounds() const
{
    const auto [xMin, xMax] = std::minmax_element(triangle.begin(), triangle.end(), [](const Vector2d &lhs, const Vector2d &rhs) {
        return lhs.x < rhs.x;
    });
    const auto [yMin, yMax] = std::minmax_element(triangle.begin(), triangle.end(), [](const Vector2d &lhs, const Vector2d &rhs) {
        return lhs.y < rhs.y;
    });
    return Box(Vector2d{xMin->x, yMin->y}, Vector2d{xMax->x, yMax->y});
}

double LaneSegment::GetDistanceTo(const Vector2d &globalPoint) const
{
    if (Contains(globalPoint))
    {
        return 0.0;
    }
    const std::array<double, 3> distances{
        std::abs(globalPoint.GetSignedDistanceTo(triangle[1], triangle[0])),
        std::abs(globalPoint.GetSignedDistanceTo(triangle[2], triangle[1])),
        std::abs(globalPoint.GetSignedDistanceTo(triangle[0], triangle[2])) //
    };
    return *std::min_element(distances.begin(), distances.end());
}

bool LaneSegment::Contains(const Vector2d &point) const
{
    if (std::isnan(point.x) || std::isnan(point.y) || std::isinf(point.x) || std::isinf(point.y))
    {
        return false;
    }
    return point.IsWithin(triangle);
}

const Lane &LaneSegment::GetLane() const
{
    return lane;
}

Lane &LaneSegment::GetLane()
{
    return lane;
}

std::ostream &operator<<(std::ostream &os, const LaneSegment &segment)
{
    return os << "(lane " << segment.GetLane().GetId() << "): [" << segment.triangle[0] << ", " << segment.triangle[1] << ", " << segment.triangle[2] << ']';
}
} // namespace osiql
