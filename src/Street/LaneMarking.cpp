/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/LaneMarking.h"

namespace osiql {
// LaneMarking::Vertex

double LaneMarking::Vertex::GetWidth() const
{
    return handle.has_width() ? handle.width() : 0.0;
}

double LaneMarking::Vertex::GetHeight() const
{
    return handle.has_height() ? handle.height() : 0.0;
}

LaneMarking::Vertex::operator Vector2d() const
{
    return {x(), y()};
}

double LaneMarking::Vertex::x() const
{
    return handle.position().has_x() ? handle.position().x() : std::numeric_limits<double>::quiet_NaN();
}

double LaneMarking::Vertex::y() const
{
    return handle.position().has_y() ? handle.position().y() : std::numeric_limits<double>::quiet_NaN();
}

LaneMarking::Vertex::Dash LaneMarking::Vertex::GetDash() const
{
    return handle.has_dash() ? static_cast<Dash>(handle.dash()) : Dash::Unknown;
}

// LaneMarking::Partition

LaneMarking::Partition::Partition(const LaneMarking &marking, Container<LaneMarking::Point>::const_iterator first, Container<LaneMarking::Point>::const_iterator pastLast) :
    marking(marking), a(first), z(pastLast)
{
}

typename Container<LaneMarking::Point>::const_iterator LaneMarking::Partition::begin() const
{
    return a;
}

typename Container<LaneMarking::Point>::const_iterator LaneMarking::Partition::end() const
{
    return z;
}

// LaneMarking

LaneMarking::Type LaneMarking::GetType() const
{
    return GetHandle().has_classification() ? (GetHandle().classification().has_type() ? static_cast<Type>(GetHandle().classification().type()) : Type::Unknown) : Type::Unknown;
}

LaneMarking::Color LaneMarking::GetColor() const
{
    return GetHandle().has_classification() ? (GetHandle().classification().has_color() ? static_cast<Color>(GetHandle().classification().color()) : Color::Unknown) : Color::Unknown;
}

Container<LaneMarking::Point>::const_iterator LaneMarking::begin() const
{
    return GetHandle().boundary_line().begin();
}
Container<LaneMarking::Point>::const_iterator LaneMarking::end() const
{
    return GetHandle().boundary_line().end();
}

// Free functions

double GetX(const LaneMarking::Point &point)
{
    return point.position().x();
}

double GetY(const LaneMarking::Point &point)
{
    return point.position().y();
}

// Debug methods

std::ostream &operator<<(std::ostream &os, LaneMarking::Vertex::Dash dash)
{
    return os << detail::laneMarkingDashToString[static_cast<size_t>(dash)];
}
std::ostream &operator<<(std::ostream &os, const LaneMarking::Vertex &point)
{
    return os << '[' << point.GetDash() << Vector2d(point) << ", " << point.GetWidth() << ", " << point.GetHeight() << ']';
}

std::ostream &operator<<(std::ostream &os, LaneMarking::Type type)
{
    return os << detail::laneMarkingTypeToString[static_cast<size_t>(type)];
}
std::ostream &operator<<(std::ostream &os, LaneMarking::Color color)
{
    return os << detail::laneMarkingColorToString[static_cast<size_t>(color)];
}

std::ostream &operator<<(std::ostream &os, const LaneMarking::Point &point)
{
    return os << "[(" << point.position().x() << ", " << point.position().y() << "), " << static_cast<LaneMarking::Vertex::Dash>(point.dash()) << ", " << point.width() << ']';
}

std::ostream &operator<<(std::ostream &os, const LaneMarking::Partition &partition)
{
    return os << partition.marking.GetType() << " | " << partition.front() << " - " << partition.back();
}

std::ostream &operator<<(std::ostream &os, const LaneMarking &marking)
{
    os << "Type: " << marking.GetType() << ", Color: " << marking.GetColor() << " | ";
    if (marking.GetHandle().boundary_line_size() == 1)
    {
        os << "1 point: " << marking.front();
    }
    else
    {
        os << marking.GetHandle().boundary_line_size() << " points";
    }
    return os;
}
} // namespace osiql
