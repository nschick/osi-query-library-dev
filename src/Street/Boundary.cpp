/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Boundary.h"

namespace osiql {
// Boundary::Partition

Boundary::Partition::Partition(const LaneBoundary &boundary,
                               std::vector<BoundaryPoint>::const_iterator first,
                               std::vector<BoundaryPoint>::const_iterator pastLast) :
    boundary(boundary), a(first), z(pastLast)
{
}

typename std::vector<BoundaryPoint>::const_iterator Boundary::Partition::begin() const
{
    return a;
}

typename std::vector<BoundaryPoint>::const_iterator Boundary::Partition::end() const
{
    return z;
}

Side Boundary::Partition::GetPassingRule(Direction direction) const
{
    return direction == Direction::Upstream ? GetPassingRule<Direction::Upstream>() : GetPassingRule<Direction::Downstream>();
}

bool Boundary::Partition::AllowsPassing(Side side, Direction direction) const
{
    return side == Side::Left ? AllowsPassing<Side::Left>(direction) : AllowsPassing<Side::Right>(direction);
}

// Boundary::Points

Boundary::Points::Points(std::vector<BoundaryPoint>::const_iterator first, std::vector<BoundaryPoint>::const_iterator pastLast) :
    a(first), z(pastLast)
{
}
typename std::vector<BoundaryPoint>::const_iterator Boundary::Points::begin() const
{
    return a;
}
typename std::vector<BoundaryPoint>::const_iterator Boundary::Points::end() const
{
    return z;
}

// Boundary::Partitions

Boundary::Partitions::Partitions(std::vector<Boundary::Partition>::const_iterator first, std::vector<Boundary::Partition>::const_iterator pastLast) :
    a(first), z(pastLast)
{
}
typename std::vector<Boundary::Partition>::const_iterator Boundary::Partitions::begin() const
{
    return a;
}
typename std::vector<Boundary::Partition>::const_iterator Boundary::Partitions::end() const
{
    return z;
}

// Boundary

typename std::vector<BoundaryPoint>::const_iterator Boundary::begin() const
{
    return points.begin();
}

typename std::vector<BoundaryPoint>::const_iterator Boundary::end() const
{
    return points.end();
}

std::ostream &operator<<(std::ostream &os, const BoundaryPoint &point)
{
    return os << "[xy" << Vector2d(point) << ", st" << Coordinates<Road>(point) << ']';
}
std::ostream &operator<<(std::ostream &os, const Boundary::Points &points)
{
    return os << '{' << points.size() << " points | " << points.front() << " - " << points.back() << '}';
}
std::ostream &operator<<(std::ostream &os, const Boundary::Partition &partition)
{
    return os << '{' << partition.GetPassingRule() << " | " << partition.front() << " - " << partition.back() << '}';
}
std::ostream &operator<<(std::ostream &os, const Boundary &boundary)
{
    return os << '{' << boundary.partitions.size() << " partitions | " << boundary.front() << " - " << boundary.back() << '}';
}
} // namespace osiql
