/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Lane.h"

#include "OsiQueryLibrary/Object/MovingObject.h"
#include "OsiQueryLibrary/Street/LaneSegment.h"
#include "OsiQueryLibrary/Street/Road.h"

namespace osiql {
double GetLatitude(const Lane &lane, Direction d, Orientation o)
{
    if (d == Direction::Upstream)
    {
        return o == Orientation::Forwards ? GetLatitude<Direction::Upstream>(lane) : GetLatitude<Direction::Upstream, Orientation::Backwards>(lane);
    }
    else
    {
        return o == Orientation::Forwards ? GetLatitude<Direction::Downstream>(lane) : GetLatitude<Direction::Downstream, Orientation::Backwards>(lane);
    }
}

Lane::Type Lane::GetType() const
{
    if (GetHandle().has_type())
    {
        return static_cast<Lane::Type>(GetHandle().type());
    }
    else
    {
        std::cerr << "Lane " << GetId() << " has no assigned type.\n";
        return Type::Undefined;
    }
}

Side GetSideOfRoad(const Lane &lane)
{
    return GetSideOfRoad(lane.GetDirection());
}

const Road &Lane::GetRoad() const
{
    return *road;
}

Road &Lane::GetRoad()
{
    return *road;
}

int Lane::GetOpenDriveId() const
{
    return GetSideOfRoad() == Side::Left ? static_cast<int>(index + 1) : -static_cast<int>(index + 1);
}

double Lane::GetWidth(double s) const // Needs to be defined after specializing GetBoundaryPoints<Side>(Orientation)
{
    if (s < osiql::GetLatitude<Direction::Downstream>(*this))
    {
        const double offset{s - osiql::GetLatitude<Direction::Downstream>(*this)};
        const std::vector<Lane *> &lanes{GetDirection() == Direction::Upstream ? GetConnectedLanes<Orientation::Forwards>() : GetConnectedLanes<Orientation::Backwards>()};
        for (const Lane *lane : lanes)
        {
            const double modifiedS{
                (lane->GetDirection() == GetDirection()) ? (osiql::GetLatitude<!Direction::Downstream>(*lane) + offset) : (osiql::GetLatitude<Direction::Downstream>(*lane) - offset) //
            };
            const double width{lane->GetWidth(modifiedS)};
            if (width != 0.0)
            {
                return width;
            }
        }
    }
    else if (s > osiql::GetLatitude<!Direction::Downstream>(*this))
    {
        const double offset{s - osiql::GetLatitude<!Direction::Downstream>(*this)};
        const std::vector<Lane *> &lanes{GetDirection() == Direction::Upstream ? GetConnectedLanes<Orientation::Backwards>() : GetConnectedLanes<Orientation::Forwards>()};
        for (const Lane *lane : lanes)
        {
            const double modifiedS{
                (lane->GetDirection() == GetDirection())
                    ? (osiql::GetLatitude<Direction::Downstream>(*lane) + offset)
                    : (osiql::GetLatitude<!Direction::Downstream>(*lane) - offset) //
            };
            const double width{lane->GetWidth(modifiedS)};
            if (width != 0.0)
            {
                return width;
            }
        }
    }
    else
    {
        const double l{GetBoundaryPoints<Side::Right>().GetLongitude(s)};
        const double r{GetBoundaryPoints<Side::Left>().GetLongitude(s)};
        return std::abs(l - r);
    }
    return 0.0;
}

double Lane::GetDistanceTo(const Vector2d &globalPoint) const
{
    // TODO: This implementation is basic & inefficient
    const Coordinates<Road> coordinates{GetRoad().referenceLine.GetCoordinates(globalPoint)};
    if (osiql::Contains(*this, coordinates.latitude))
    {
        const double left{GetBoundaryPoints<Side::Left, Direction::Downstream>().GetLongitude(coordinates.latitude)};
        const double right{GetBoundaryPoints<Side::Right, Direction::Downstream>().GetLongitude(coordinates.latitude)};
        if (left <= coordinates.longitude && coordinates.longitude <= right)
        {
            return 0.0;
        }
    }
    const Boundary::Points &left{GetBoundaryPoints<Side::Left, Direction::Downstream>()};
    const Boundary::Points &right{GetBoundaryPoints<Side::Left, Direction::Downstream>()};

    double distance{globalPoint.GetSignedDistanceToChain(left.begin(), left.end())};
    if (distance < 0.0)
    {
        distance = std::max(distance, globalPoint.GetSignedDistanceToChain(right.rbegin(), right.rend()));
    }
    else
    {
        distance = std::min(distance, globalPoint.GetSignedDistanceToChain(right.rbegin(), right.rend()));
    }
    if (distance < 0.0)
    {
        distance = std::max(distance, globalPoint.GetSignedDistanceTo(left.front(), right.front()));
    }
    else
    {
        distance = std::min(distance, globalPoint.GetSignedDistanceTo(left.front(), right.front()));
    }
    if (distance < 0.0)
    {
        distance = std::max(distance, globalPoint.GetSignedDistanceTo(left.back(), right.back()));
    }
    else
    {
        distance = std::min(distance, globalPoint.GetSignedDistanceTo(left.back(), right.back()));
    }
    return distance;
}

RoadPoint Lane::FindPointNearby(const Vector2d &globalPoint) const
{
    const double shortestDistance{GetRoad().GetDistanceTo(globalPoint, GetSideOfRoad())};
    if (shortestDistance == 0.0)
    {
        return GetRoad().GetPoint(globalPoint, GetSideOfRoad());
    }
    std::set<RoadId> pool;
    const Lane *lane{FindLaneOfClosestRoad<Orientation::Forwards>(globalPoint, pool, shortestDistance * shortestDistance)};
    if (*this == *lane)
    {
        lane = FindLaneOfClosestRoad<Orientation::Backwards>(globalPoint, pool, shortestDistance * shortestDistance);
    }
    return lane->GetRoad().GetPoint(globalPoint, lane->GetSideOfRoad());
}

RoadPoint Lane::GetPoint(const Vector2d &point) const
{
    return {*this, GetRoad().referenceLine.GetCoordinates(point)};
}

Pose<RoadPoint> Lane::GetPose(const Vector2d &point, double globalYaw) const
{
    return Pose<RoadPoint>{*this, GetRoad().referenceLine.GetPose(point, globalYaw)};
}

template <Orientation O>
const Lane *Lane::FindLaneOfClosestRoad(const Vector2d &globalPoint, std::set<RoadId> &pool, double currentDistance, double *distanceResult) const
{
    const std::vector<Lane *> &connections{GetRoad().GetConnectedLanes<O>(GetSideOfRoad())};
    std::pair<const Lane *, double> result{this, currentDistance};
    for (const Lane *successor : connections)
    {
        if (pool.insert(successor->GetRoadId()).second)
        {
            double distance{successor->GetRoad().GetDistanceTo(globalPoint, successor->GetSideOfRoad())};
            if (distance < currentDistance)
            {
                if (distance == 0.0)
                {
                    if (distanceResult)
                    {
                        *distanceResult = 0.0;
                    }
                    return successor;
                }
                if (distance <= result.second)
                {
                    const Lane *lane{successor->FindLaneOfClosestRoad<O>(globalPoint, pool, distance, &distance)};
                    result = std::make_pair(lane, distance);
                }
            }
        }
    }
    if (distanceResult)
    {
        *distanceResult = result.second;
    }
    return result.first;
}

std::pair<Side, size_t> Lane::GetLaneChangesTo(Orientation o, const Lane &target) const
{
    return o == Orientation::Forwards ? GetLaneChangesTo(target) : GetLaneChangesTo<Orientation::Backwards>(target);
}

bool operator<(const Lane &lhs, const Lane &rhs)
{
    return lhs.GetId() < rhs.GetId();
}
bool operator==(const Lane &lhs, const Lane &rhs)
{
    return lhs.GetId() == rhs.GetId();
}
bool operator!=(const Lane &lhs, const Lane &rhs)
{
    return lhs.GetId() != rhs.GetId();
}

std::ostream &operator<<(std::ostream &os, Lane::Type type)
{
    return os << detail::laneTypeToString[static_cast<size_t>(type)];
}

std::ostream &operator<<(std::ostream &os, const Lane &lane)
{
    os << "Lane " << lane.GetId() << " (" << lane.index << ", " << lane.GetDirection() << ", Road \"" << lane.GetRoadId() << "\"): ";
    os << "s[" << GetLatitude<Direction::Downstream>(lane) << ", " << GetLatitude<Direction::Upstream>(lane) << "], left[";
    {
        if (lane.GetHandle().left_boundary_id().empty())
        {
            os << "NONE";
        }
        else
        {
            auto it{lane.GetHandle().left_boundary_id().begin()};
            os << (it++)->value();
            for (; it != lane.GetHandle().left_boundary_id().end(); ++it)
            {
                os << ", " << it->value();
            }
        }
    }
    os << "], right[";
    {
        if (lane.GetHandle().right_boundary_id().empty())
        {
            os << "NONE";
        }
        else
        {
            auto it{lane.GetHandle().right_boundary_id().begin()};
            os << (it++)->value();
            for (; it != lane.GetHandle().right_boundary_id().end(); ++it)
            {
                os << ", " << it->value();
            }
        }
    }
    os << "], ref " << lane.GetRoad().referenceLine.GetId() << ", " << lane.movingObjects.size() << " objs, out[";
    {
        const auto &lanes{lane.GetConnectedLanes<Orientation::Forwards>()};
        if (lanes.empty())
        {
            os << "None";
        }
        else
        {
            auto it{lanes.begin()};
            os << (*it++)->GetId();
            for (; it != lanes.end(); ++it)
            {
                os << ", " << (*it)->GetId();
            }
        }
    }
    os << "], in[";
    {
        const auto &lanes{lane.GetConnectedLanes<Orientation::Backwards>()};
        if (lanes.empty())
        {
            os << "None";
        }
        else
        {
            auto it{lanes.begin()};
            os << (*it++)->GetId();
            for (; it != lanes.end(); ++it)
            {
                os << ", " << (*it)->GetId();
            }
        }
    }
    return os << ']';
}
} // namespace osiql
