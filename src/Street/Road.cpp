/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Street/Road.h"

#include <algorithm>
#include <numeric>

#include "OsiQueryLibrary/Component/LaneAssignable.h"
#include "OsiQueryLibrary/Street/Lane.h"
#include "OsiQueryLibrary/Street/LaneSegment.h"

namespace osiql {
Id Road::quantity = Id{0u};

bool operator<(const Road &lhs, const Road &rhs)
{
    return lhs.GetId() < rhs.GetId();
}
bool operator<(const Road &lhs, Id rhs)
{
    return lhs.GetId() < rhs;
}
bool operator<(Id lhs, const Road &rhs)
{
    return lhs < rhs.GetId();
}
bool operator==(const Road &lhs, const Road &rhs)
{
    return lhs.GetId() == rhs.GetId();
}
bool operator!=(const Road &lhs, const Road &rhs)
{
    return lhs.GetId() != rhs.GetId();
}

Road::Road(detail::RoadSpecifications &&specs) :
    id(++Road::quantity), referenceLine(specs.referenceLine), laneRows(std::move(specs.rows), specs.center)
{
}

Id Road::GetId() const
{
    return id;
}

const RoadId &Road::GetStreetId() const
{
    return laneRows.rows.front().front()->GetRoadId();
}

const Lane &Road::GetClosestLane(const Coordinates<Road> &coordinates) const
{
    const std::vector<Lane *> &lanes{laneRows.rows[0].lanes};
    const Lane *lane{*std::upper_bound(lanes.begin(), std::prev(lanes.end()), coordinates.latitude, [](double latitude, const Lane *lane) {
        return GetLatitude<Direction::Upstream>(*lane) > latitude;
    })};
    while (!lane->GetAdjacentLanes<Side::Left, Direction::Downstream>().empty() && lane->GetBoundaryPoints<Side::Left, Direction::Downstream>().GetLongitude(coordinates.latitude) < coordinates.longitude)
    {
        lane = lane->GetAdjacentLane<Side::Left, Direction::Downstream>(coordinates.latitude);
    }
    return *lane;
}
Lane &Road::GetClosestLane(const Coordinates<Road> &coordinates)
{
    return const_cast<Lane &>(const_cast<const Road *>(this)->GetClosestLane(coordinates));
}

const Lane *Road::GetClosestLane(const Coordinates<Road> &coordinates, Side s) const
{
    return s == Side::Left ? GetClosestLane<Side::Left>(coordinates) : GetClosestLane<Side::Right>(coordinates);
}
Lane *Road::GetClosestLane(const Coordinates<Road> &coordinates, Side s)
{
    return const_cast<Lane *>(const_cast<const Road *>(this)->GetClosestLane(coordinates, s));
}

std::vector<const Road *> Road::GetRoads(const std::vector<Lane *> &lanes)
{
    std::set<RoadId> pool;
    std::vector<const Road *> result;
    for (const Lane *lane : lanes)
    {
        if (pool.insert(lane->GetRoadId()).second)
        {
            result.push_back(&lane->GetRoad());
        }
    }
    return result;
}

RoadPoint Road::GetPoint(const Vector2d &globalPoint) const
{
    Coordinates<Road> coordinates{referenceLine.GetCoordinates(globalPoint)};
    const Lane &lane{GetClosestLane(coordinates)};
    return {lane, std::move(coordinates)};
}
detail::RoadPoint Road::GetPoint(const Vector2d &globalPoint)
{
    Coordinates<Road> coordinates{referenceLine.GetCoordinates(globalPoint)};
    Lane &lane{GetClosestLane(coordinates)};
    return {lane, std::move(coordinates)};
}

RoadPoint Road::GetPoint(const Vector2d &p, Side s) const
{
    return s == Side::Left ? GetPoint<Side::Left>(p) : GetPoint<Side::Right>(p);
}
detail::RoadPoint Road::GetPoint(const Vector2d &p, Side s)
{
    return s == Side::Left ? GetPoint<Side::Left>(p) : GetPoint<Side::Right>(p);
}

Pose<RoadPoint> Road::GetPose(const Vector2d &globalPoint, double angle) const
{
    Pose<Coordinates<Road>> pose{referenceLine.GetPose(globalPoint, angle)};
    const Lane &lane{GetClosestLane(pose)};
    return {lane, std::move(pose)};
}
Pose<detail::RoadPoint> Road::GetPose(const Vector2d &globalPoint, double angle)
{
    Pose<Coordinates<Road>> pose{referenceLine.GetPose(globalPoint, angle)};
    Lane &lane{GetClosestLane(pose)};
    return {lane, std::move(pose)};
}

Pose<RoadPoint> Road::GetPose(const Vector2d &p, double a, Side s) const
{
    return s == Side::Left ? GetPose<Side::Left>(p, a) : GetPose<Side::Right>(p, a);
}
Pose<detail::RoadPoint> Road::GetPose(const Vector2d &p, double a, Side s)
{
    return s == Side::Left ? GetPose<Side::Left>(p, a) : GetPose<Side::Right>(p, a);
}

double Road::GetDistanceTo(const Vector2d &globalPoint) const
{
    const Coordinates<Road> coordinates{referenceLine.GetCoordinates(globalPoint)};
    const Lane &lane{GetClosestLane(coordinates)};
    return lane.GetDistanceTo(globalPoint);
}
double Road::GetDistanceTo(const Vector2d &p, Side s) const
{
    return s == Side::Left ? GetDistanceTo<Side::Left>(p) : GetDistanceTo<Side::Right>(p);
}

const std::vector<Lane *> &Road::GetConnectedLanes(Side s, Orientation o) const
{
    return o == Orientation::Forwards ? GetConnectedLanes<Orientation::Forwards>(s) : GetConnectedLanes<Orientation::Backwards>(s);
}

double Road::GetLength() const
{
    // TODO: Is it guaranteed that each road spans an entire reference line? If so:
    // return referenceLine.GetLength();
    // Otherwise...:
    return laneRows.rows[0].back()->GetBoundaryPoints<Side::Left>().back().latitude - laneRows.rows[0].front()->GetBoundaryPoints<Side::Left>().front().latitude;
}

std::ostream &operator<<(std::ostream &os, const Road &road)
{
    const auto lanes{std::accumulate(road.laneRows.rows.begin(), road.laneRows.rows.end(), 0u, [](size_t lhs, const LaneRow &rhs) {
        return lhs + rhs.lanes.size();
    })};
    os << "Road \"" << road.GetId() << "\" (";
    if (lanes == 1u)
    {
        os << "1 lane, ";
    }
    else
    {
        os << lanes << " lanes, ";
    }
    if (road.laneRows.rows.size() == 1u)
    {
        os << road.laneRows.rows.size() << " row | ";
    }
    else
    {
        os << road.laneRows.rows.size() << " rows | ";
    }
    os << road.laneRows.size<Side::Right>() << " Right/" << road.laneRows.size<Side::Left>() << " Left)";
    if (const auto roads = Road::GetRoads(road.GetConnectedLanes<Side::Right, Orientation::Forwards>()); !roads.empty())
    {
        os << "\n    Outgoing downstream roads: ";
        for (const auto *next : roads)
        {
            os << next->GetId() << ", ";
        }
    }
    if (const auto roads = Road::GetRoads(road.GetConnectedLanes<Side::Left, Orientation::Forwards>()); !roads.empty())
    {
        os << "\n    Outgoing upstream roads: ";
        for (const auto *next : roads)
        {
            os << next->GetId() << ", ";
        }
    }
    if (const auto roads = Road::GetRoads(road.GetConnectedLanes<Side::Right, Orientation::Backwards>()); !roads.empty())
    {
        os << "\n    Incoming downstream roads: ";
        for (const auto *prev : roads)
        {
            os << prev->GetId() << ", ";
        }
    }
    if (const auto roads = Road::GetRoads(road.GetConnectedLanes<Side::Left, Orientation::Backwards>()); !roads.empty())
    {
        os << "\n    Incoming upstream roads: ";
        for (const auto *prev : roads)
        {
            os << prev->GetId() << ", ";
        }
    }
    return os;
}
} // namespace osiql
