/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/TrafficSign.h"

namespace osiql {
TrafficSign::Type TrafficSign::GetType() const
{
    if (GetHandle().has_main_sign() && GetHandle().main_sign().has_classification() && GetHandle().main_sign().classification().has_type())
    {
        return static_cast<Type>(GetHandle().main_sign().classification().type());
    }
    return Type::Unknown;
}

std::ostream &operator<<(std::ostream &os, TrafficSign::Type type)
{
    return os << detail::trafficSignTypeToString[static_cast<size_t>(type)];
}

std::ostream &operator<<(std::ostream &os, const TrafficSign &sign)
{
    os << sign.GetId() << ", " << sign.GetType();
    if (!sign.GetLaneAssignments().empty())
    {
        os << '[';
        auto it{sign.GetLaneAssignments().begin()};
        os << it->assigned_lane_id().value();
        for (; it != sign.GetLaneAssignments().end(); ++it)
        {
            os << ", " << it->assigned_lane_id().value();
        }
        os << ']';
    }
    return os;
}
} // namespace osiql
