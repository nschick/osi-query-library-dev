/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/RoadMarking.h"

namespace osiql {
RoadMarking::Type RoadMarking::GetType() const
{
    if (GetHandle().has_classification() && GetHandle().classification().has_type())
    {
        return static_cast<Type>(GetHandle().classification().type());
    }
    return Type::Undefined;
}

TrafficSign::Type RoadMarking::GetSignType() const
{
    if (GetHandle().has_classification() && GetHandle().classification().has_traffic_main_sign_type())
    {
        return static_cast<TrafficSign::Type>(GetHandle().classification().traffic_main_sign_type());
    }
    return TrafficSign::Type::Unknown;
}

Value RoadMarking::GetValue() const
{
    if (GetHandle().has_classification() && GetHandle().classification().has_value())
    {
        return GetHandle().classification().value();
    }
    return {};
}

std::ostream &operator<<(std::ostream &os, RoadMarking::Type type)
{
    return os << detail::roadMarkingTypeToString[static_cast<size_t>(type)];
}
} // namespace osiql
