/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/MovingObject.h"

#include <google/protobuf/util/message_differencer.h>

#include "OsiQueryLibrary/Routing/Stream.h"

namespace osiql {
MovingObject::~MovingObject()
{
    // Remove the connection from each connected lane to this object

    for (Pose<detail::RoadPoint> &pose : positions)
    {
        std::unordered_set<LocalBounds<const MovingObject>, Hash<Id>, Equal> &movingObjects{pose.GetLane().movingObjects};
        movingObjects.erase(movingObjects.find(*this));
    }
}

void MovingObject::operator=(const osi3::MovingObject &handle)
{
    const osi3::BaseMoving &base{handle.base()};
    if (!google::protobuf::util::MessageDifferencer::Equals(GetBase().position(), base.position()))
    {
        Locatable<osi3::MovingObject>::OutdateTranslation();
    }
    if (!google::protobuf::util::MessageDifferencer::Equals(GetBase().orientation(), base.orientation()))
    {
        OutdateRotation();
    }
    if (!google::protobuf::util::MessageDifferencer::Equals(GetBase().orientation_rate(), base.orientation_rate()))
    {
        OutdateSpin();
    }
    if (!google::protobuf::util::MessageDifferencer::Equals(GetBase().velocity(), base.velocity()))
    {
        OutdateVelocity();
    }
    this->handle.CopyFrom(handle);
}

void MovingObject::OutdateRotation()
{
    Locatable<osi3::MovingObject>::OutdateRotation();
    spinAndRotationMatrix.reset();
}

void MovingObject::OutdateSpin()
{
    spinMatrix.reset();
    spinAndRotationMatrix.reset();
    localMovementMatrix.reset();
    globalMovementMatrix.reset();
}

void MovingObject::OutdateVelocity()
{
    velocityMatrix.reset();
    localMovementMatrix.reset();
    globalMovementMatrix.reset();
}

Vector3d MovingObject::GetVelocity() const
{
    if (GetBase().has_velocity())
    {
        return GetBase().velocity();
    }
    std::cerr << "Error: GetVelocity() - Moving object " << GetId() << " has no assigned velocity\n";
    return {};
}

Vector3d MovingObject::GetVelocity(const Vector3d &localPoint) const
{
    return GetGlobalMovementMatrix() * localPoint;
}

Vector3d MovingObject::GetAcceleration() const
{
    if (GetBase().has_acceleration())
    {
        return GetBase().acceleration();
    }
    std::cerr << "Error: GetAcceleration() - Moving object " << GetId() << " has no assigned acceleration\n";
    return {};
}

Vector3d MovingObject::GetAngularVelocity() const
{
    if (GetBase().has_orientation_rate())
    {
        return {
            GetBase().orientation_rate().has_roll() ? GetBase().orientation_rate().roll() : 0.0,
            GetBase().orientation_rate().has_pitch() ? GetBase().orientation_rate().pitch() : 0.0,
            GetBase().orientation_rate().has_yaw() ? GetBase().orientation_rate().yaw() : 0.0,
        };
    }
    std::cerr << "Error: GetAngularVelocity() - Moving object " << GetId() << " has no assigned angular velocity.\n";
    return {};
}

double MovingObject::GetRollVelocity() const
{
    if (GetBase().has_orientation_rate())
    {
        if (GetBase().orientation_rate().has_roll())
        {
            return GetBase().orientation_rate().roll();
        }
        std::cerr << "Error: GetRollVelocity() - Angular velocity of moving object " << GetId() << " has no assigned roll.\n";
        return 0.0;
    }
    std::cerr << "Error: GetRollVelocity() - Moving object " << GetId() << " has no assigned rotational velocity.\n";
    return 0.0;
}

double MovingObject::GetPitchVelocity() const
{
    if (GetBase().has_orientation_rate())
    {
        if (GetBase().orientation_rate().has_pitch())
        {
            return GetBase().orientation_rate().pitch();
        }
        std::cerr << "Error: GetPitchVelocity() - Angular velocity of moving object " << GetId() << " has no assigned pitch.\n";
        return 0.0;
    }
    std::cerr << "Error: GetPitchVelocity() - Moving object " << GetId() << " has no assigned rotational velocity.\n";
    return 0.0;
}

double MovingObject::GetYawVelocity() const
{
    if (GetBase().has_orientation_rate())
    {
        if (GetBase().orientation_rate().has_yaw())
        {
            return GetBase().orientation_rate().yaw();
        }
        std::cerr << "Error: GetYawVelocity() - Angular velocity of moving object " << GetId() << " has no assigned yaw.\n";
        return 0.0;
    }
    std::cerr << "Error: GetYawVelocity() - Moving object " << GetId() << " has no assigned rotational velocity.\n";
    return 0.0;
}

Vector3d MovingObject::GetAngularMomentum(const Vector3d &localPoint) const
{
    return GetSpinMatrix() * localPoint;
}

Vector3d MovingObject::GetAngularAcceleration() const
{
    if (GetBase().has_orientation_acceleration())
    {
        return {
            GetBase().orientation_acceleration().has_roll() ? GetBase().orientation_acceleration().roll() : 0.0,
            GetBase().orientation_acceleration().has_pitch() ? GetBase().orientation_acceleration().pitch() : 0.0,
            GetBase().orientation_acceleration().has_yaw() ? GetBase().orientation_acceleration().yaw() : 0.0,
        };
    }
    std::cerr << "Error: GetAngularAcceleration() - Moving object " << GetId() << " has no assigned angular acceleration.\n";
    return {};
}

double MovingObject::GetRollAcceleration() const
{
    if (GetBase().has_orientation_acceleration())
    {
        if (GetBase().orientation_acceleration().has_roll())
        {
            return GetBase().orientation_acceleration().roll();
        }
        std::cerr << "Error: GetRollAcceleration() - Angular acceleration of moving object " << GetId() << " has no assigned roll.\n";
        return 0.0;
    }
    std::cerr << "Error: GetRollAcceleration() - Moving object " << GetId() << " has no assigned rotational acceleration.\n";
    return 0.0;
}

double MovingObject::GetPitchAcceleration() const
{
    if (GetBase().has_orientation_acceleration())
    {
        if (GetBase().orientation_acceleration().has_pitch())
        {
            return GetBase().orientation_acceleration().pitch();
        }
        std::cerr << "Error: GetPitchAcceleration() - Angular acceleration of moving object " << GetId() << " has no assigned pitch.\n";
        return 0.0;
    }
    std::cerr << "Error: GetPitchAcceleration() - Moving object " << GetId() << " has no assigned rotational acceleration.\n";
    return 0.0;
}

double MovingObject::GetYawAcceleration() const
{
    if (GetBase().has_orientation_acceleration())
    {
        if (GetBase().orientation_acceleration().has_yaw())
        {
            return GetBase().orientation_acceleration().yaw();
        }
        std::cerr << "Error: GetYawAcceleration() - Angular acceleration of moving object " << GetId() << " has no assigned yaw.\n";
        return 0.0;
    }
    std::cerr << "Error: GetYawAcceleration() - Moving object " << GetId() << " has no assigned rotational acceleration.\n";
    return 0.0;
}

MovingObject::Type MovingObject::GetType() const
{
    return GetHandle().has_type() ? static_cast<Type>(GetHandle().type()) : Type::Unknown;
}

const Matrix<3> &MovingObject::GetSpinMatrix() const
{
    if (!spinMatrix)
    {
        spinMatrix = std::make_unique<Matrix<3>>(AntimetricMatrix(GetAngularVelocity()));
    }
    return *spinMatrix;
}

const Matrix<4> &MovingObject::GetVelocityMatrix() const
{
    if (!velocityMatrix)
    {
        velocityMatrix = std::make_unique<Matrix<4>>(TranslationMatrix(GetVelocity()));
    }
    return *velocityMatrix;
}

const Matrix<4> &MovingObject::GetLocalMovementMatrix() const
{
    if (!localMovementMatrix)
    {
        localMovementMatrix = std::make_unique<Matrix<4>>(GetVelocityMatrix() * GetSpinMatrix());
    }
    return *localMovementMatrix;
}

const Matrix<4> &MovingObject::GetGlobalMovementMatrix() const
{
    if (!globalMovementMatrix)
    {
        globalMovementMatrix = std::make_unique<Matrix<4>>(GetVelocityMatrix() * GetSpinAndRotationMatrix());
    }
    return *globalMovementMatrix;
}

const Matrix<3> &MovingObject::GetSpinAndRotationMatrix() const
{
    if (!spinAndRotationMatrix)
    {
        spinAndRotationMatrix = std::make_unique<Matrix<3>>(GetRotationMatrix() * GetSpinMatrix());
    }
    return *spinAndRotationMatrix;
}

Distances MovingObject::GetDistancesToBoundaries(const Road &road, Direction d) const
{
    if (d == Direction::Upstream)
    {
        return GetDistancesToBoundaries<Direction::Upstream>(road);
    }
    else
    {
        return GetDistancesToBoundaries<Direction::Downstream>(road);
    }
}

std::vector<LocalBounds<Lane>>::const_iterator MovingObject::GetLocalIntersectionBounds(const Lane &lane) const
{
    return std::find_if(intersectionBounds.begin(), intersectionBounds.end(), [&lane](const LocalBounds<Lane> &bounds) {
        return bounds.GetEntity() == lane;
    });
}

std::vector<LocalBounds<Lane>>::iterator MovingObject::GetLocalIntersectionBounds(Lane &lane)
{
    return std::find_if(intersectionBounds.begin(), intersectionBounds.end(), [&lane](const LocalBounds<Lane> &bounds) {
        return bounds.GetEntity() == lane;
    });
}

std::ostream &operator<<(std::ostream &os, const MovingObject &object)
{
    os << object.GetType() << ' ' << object.GetId() << ": " << object.GetXY() << " [" << object.GetSize() << "], Local positions: ";
    if (!object.positions.empty())
    {
        auto it{object.positions.begin()};
        os << *(it++);
        for (; it != object.positions.end(); ++it)
        {
            os << ", " << *it;
        }
    }
    return os;
}
std::ostream &operator<<(std::ostream &os, const MovingObject::Type type)
{
    return os << detail::movingObjectTypeToString[static_cast<size_t>(type)];
}
} // namespace osiql
