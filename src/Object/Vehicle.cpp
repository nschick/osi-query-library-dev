/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/Vehicle.h"

namespace osiql {
const osi3::MovingObject_VehicleClassification_LightState *GetLightState(const osi3::MovingObject &object)
{
    if (object.has_vehicle_classification() && object.vehicle_classification().has_light_state())
    {
        return &object.vehicle_classification().light_state();
    }
    return nullptr;
}

Vehicle::BrakeLightState Vehicle::GetBrakeLightState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_brake_light_state())
    {
        return static_cast<Vehicle::BrakeLightState>(state->brake_light_state());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned brake light state\n";
    return BrakeLightState::Unknown;
}

Vehicle::GenericLightState Vehicle::GetFrontFogLightState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_front_fog_light())
    {
        return static_cast<Vehicle::GenericLightState>(state->front_fog_light());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned front fog light state\n";
    return GenericLightState::Unknown;
}
Vehicle::GenericLightState Vehicle::GetRearFogLightState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_rear_fog_light())
    {
        return static_cast<Vehicle::GenericLightState>(state->rear_fog_light());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned rear fog light state\n";
    return GenericLightState::Unknown;
}
Vehicle::GenericLightState Vehicle::GetHeadLightState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_head_light())
    {
        return static_cast<Vehicle::GenericLightState>(state->head_light());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned head light state\n";
    return GenericLightState::Unknown;
}
Vehicle::GenericLightState Vehicle::GetHighBeamState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_high_beam())
    {
        return static_cast<Vehicle::GenericLightState>(state->high_beam());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned high beam state\n";
    return GenericLightState::Unknown;
}
Vehicle::GenericLightState Vehicle::GetReversingLightState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_reversing_light())
    {
        return static_cast<Vehicle::GenericLightState>(state->reversing_light());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned reversing light state\n";
    return GenericLightState::Unknown;
}
Vehicle::GenericLightState Vehicle::GetRearLicensePlateIlluminationState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_license_plate_illumination_rear())
    {
        return static_cast<Vehicle::GenericLightState>(state->license_plate_illumination_rear());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned emergency rear license plate illumination state\n";
    return GenericLightState::Unknown;
}
Vehicle::GenericLightState Vehicle::GetEmergencyVehicleIlluminationState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_emergency_vehicle_illumination())
    {
        return static_cast<Vehicle::GenericLightState>(state->emergency_vehicle_illumination());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned emergency vehicle illumination state\n";
    return GenericLightState::Unknown;
}
Vehicle::GenericLightState Vehicle::GetServiceVehicleIlluminationState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_service_vehicle_illumination())
    {
        return static_cast<Vehicle::GenericLightState>(state->service_vehicle_illumination());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned service vehicle illumination state\n";
    return GenericLightState::Unknown;
}

Vehicle::IndicatorState Vehicle::GetIndicatorState() const
{
    if (const auto state = GetLightState(GetHandle()); state && state->has_indicator_state())
    {
        return static_cast<Vehicle::IndicatorState>(state->indicator_state());
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned indicator light state\n";
    return IndicatorState::Unknown;
}

Vehicle::VehicleType Vehicle::GetVehicleType() const
{
    if (GetHandle().has_vehicle_classification())
    {
        const auto &classification{GetHandle().vehicle_classification()};
        if (classification.has_type())
        {
            return static_cast<Vehicle::VehicleType>(classification.type());
        }
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned vehicle type\n";
    return VehicleType::Unknown;
}

Vehicle::Role Vehicle::GetRole() const
{
    if (GetHandle().has_vehicle_classification())
    {
        const auto &classification{GetHandle().vehicle_classification()};
        if (classification.has_role())
        {
            return static_cast<Vehicle::Role>(classification.role());
        }
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned vehicle role\n";
    return Role::Unknown;
}

double Vehicle::GetSteeringWheelAngle() const
{
    if (GetHandle().has_vehicle_attributes() && GetHandle().vehicle_attributes().has_steering_wheel_angle() && !std::isnan(GetHandle().vehicle_attributes().steering_wheel_angle()))
    {
        return GetHandle().vehicle_attributes().steering_wheel_angle();
    }
    std::cout << "Warning: Moving object " << GetId() << " has no assigned valid steering wheel angle. Returning 0.0\n";
    return 0.0;
}

std::ostream &operator<<(std::ostream &os, Axle a)
{
    return os << detail::axleToString[static_cast<size_t>(a)];
}
std::ostream &operator<<(std::ostream &os, typename Vehicle::BrakeLightState value)
{
    return os << detail::brakeLightStateToString[static_cast<size_t>(value)];
}
std::ostream &operator<<(std::ostream &os, typename Vehicle::GenericLightState value)
{
    return os << detail::genericLightStateToString[static_cast<size_t>(value)];
}
std::ostream &operator<<(std::ostream &os, typename Vehicle::IndicatorState value)
{
    return os << detail::indicatorStateToString[static_cast<size_t>(value)];
}
std::ostream &operator<<(std::ostream &os, typename Vehicle::VehicleType value)
{
    return os << detail::vehicleTypeToString[static_cast<size_t>(value)];
}
std::ostream &operator<<(std::ostream &os, typename Vehicle::Role value)
{
    return os << detail::vehicleRoleToString[static_cast<size_t>(value)];
}
} // namespace osiql
