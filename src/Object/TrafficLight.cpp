/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "OsiQueryLibrary/Object/TrafficLight.h"

namespace osiql {
TrafficLight::TrafficLight(std::vector<const osi3::TrafficLight *> &&bulbs) :
    Object(*bulbs.front()), bulbs(bulbs)
{
    // TODO: Determine type

    // TODO: Sort bulbs
}
} // namespace osiql
