################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# conanfile for building OsiQueryLibrary with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, cmake_layout
from conan.tools.scm import Git
from conan.tools.files import load, update_conandata

required_conan_version = ">=1.33.0"

class OsiQueryLibraryConan(ConanFile):
    name = "OsiQueryLibrary"
    version= "0.0.1"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
    }
    generators = ['CMakeToolchain', 'CMakeDeps', 'VirtualBuildEnv', 'cmake_find_package']
    _cmake = None
    short_paths = True

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    @property
    def _build_subfolder(self):
        return "build_subfolder"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def requirements(self):
        self.requires("protobuf/3.20.0")
        self.requires("boost/1.72.0")
        self.requires("cdt/1.3.0")
        self.options["boost"].shared=True
        self.options["boost"].without_atomic=True
        self.options["boost"].without_chrono=True
        self.options["boost"].without_container=True
        self.options["boost"].without_context=True
        self.options["boost"].without_contract=True
        self.options["boost"].without_coroutine=True
        self.options["boost"].without_date_time=True
        self.options["boost"].without_exception=True
        self.options["boost"].without_fiber=True
        self.options["boost"].without_graph=True
        self.options["boost"].without_graph_parallel=True
        self.options["boost"].without_iostreams=True 
        self.options["boost"].without_locale=True
        self.options["boost"].without_log=True
        self.options["boost"].without_math=True
        self.options["boost"].without_mpi=True
        self.options["boost"].without_program_options=True
        self.options["boost"].without_python=True
        self.options["boost"].without_random=True
        self.options["boost"].without_regex=True
        self.options["boost"].without_serialization=True
        self.options["boost"].without_stacktrace=True
        self.options["boost"].without_test=True
        self.options["boost"].without_thread=True
        self.options["boost"].without_timer=True
        self.options["boost"].without_type_erasure=True
        self.options["boost"].without_wave=True
        self.requires("open_simulation_interface/3.5.0@openpass/testing")
        self.requires("gtest/1.11.0")

    def build_requirements(self):
        self.build_requires("protobuf/3.20.0")
        self.options["protobuf"].shared = False

    def source(self):
        git = Git(self)
        sources = self.conan_data["sources"]
        git.clone(url=sources["url"], target=".")
        git.checkout(commit=sources["commit"])

    def layout(self):
        cmake_layout(self)
        self.cpp.build.libdirs.append("src")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()