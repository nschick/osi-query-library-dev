# OSI Query Library

The osi-query-library (osiql) is a read-only wrapper for [osi](https://github.com/OpenSimulationInterface/open-simulation-interface). Its main features are:

- wrapper classes for common OSI-objects with helpful getters and utility methods
- bidirectional linking of objects to lanes
- lane location (st-coordinate) queries

All wrapper objects have public handles to their corresponding `osi3` entities.

## Build Instructions

### Dependencies

| Library | Version | Additional Information |
| ------- | ------- | ---- |
| boost Geometry  | Tested with 1.72.0 | [boost doc](https://www.boost.org) and [boost Geometry](https://www.boost.org/doc/libs/1_72_0/libs/geometry/doc/html/index.html) |
| CDT   | Tested with 1.3.0 | https://github.com/artem-ogre/CDT |
| Open Simulation Interface   | 3.5 and 3.6 | [OSI doc](https://opensimulationinterface.github.io/osi-antora-generator/asamosi/latest/interface/setup/installing_prerequisites.html) and [OpenPASS doc](https://www.eclipse.org/openpass/content/html/installation_guide/20_install_prerequisites.html) |
| - protobuf<br />- protobuf-shared   | Tested with 3.20.0 | [OSI doc](https://opensimulationinterface.github.io/osi-antora-generator/asamosi/latest/interface/setup/installing_prerequisites.html) and [OpenPASS doc](https://www.eclipse.org/openpass/content/html/installation_guide/20_install_prerequisites.html) |
| googletest | Tested with 1.13.0 | https://github.com/google/googletest |

## Build Example

```bash
git submodule init
git submodule update
mkdir build && cd build
cmake -DCMAKE_PREFIX_PATH="${LIBRARY_DIRECTORY}/boost;
${LIBRARY_DIRECTORY}/CDT;
${LIBRARY_DIRECTORY}/protobuf-shared;
${LIBRARY_DIRECTORY}/protobuf;
${LIBRARY_DIRECTORY}/osi;
${LIBRARY_DIRECTORY}/googletest"
-DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/OpenPASS/osiql-install ..
make -j4
make install
```

## Building Documentation

The osiql has a Sphinx documentation. To generate the HTML version of this documentation, Python 3.x and the following prerequisites must be installed:

```bash
apt install python3-sphinx
pip install sphinx-rtd-theme
pip install sphinx-tabs
```

Then, from the project's root directory:

```bash
cd doc
sphinx-build -b html source html
```

In addition, Doxygen code documentation can be built from the project's root directory:

```bash
apt install doxygen
doxygen
xdg-open Doxygen/html/index.html
```

## Usage Example

Given a `osi3::GroundTruth groundTruth` and an `osi3::TrafficAction::AcquirePositionAction destination`, generate the shortest route from the host vehicle's current position to the destination in each timestep:

```cpp
#include <iostream>
#include <OsiQueryLibrary/osiql.h>

...

osiql::Query query{groundTruth};

osiql::Route route{query.GetRoute(destination)};
if (route.empty())
{
    std::cout << "No route from the host vehicle to the "
              << "destination exists without changing lanes.\n";
}

// ... increment timestep here ...
// After modifying the groundTruth, update the osiql query if there has
// been a change in lane assignments or objects have been added/removed:
query.Update(alteredGroundTruth.moving_object());

route = query.GetRoute(destination);
if (route.empty())
{
    std::cout << "The host vehicle has left the lane that can "
              << "reach the given destination.\n"
}
else if (route.front() == route.back())
{
    std::cout << "Destination reached!\n";
}
```

An osiql::Query should be constructed only once and then reused for all requests.
