/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <array>
#include <numeric>
//
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OsiQueryLibrary/osiql.h"

using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::ReturnRef;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

class QueryTest : public ::testing::Test
{
public:
    QueryTest()
    {
        // clang-format off
        std::vector<osiql::Vector2d> points{
            {0.0, -2.0}, {0.0, -1.0}, {0.0,  0.0}, {0.0, 1.0}, {0.0, 2.0}, //
            {2.0, -2.0}, {2.0,  0.0}, {2.0,  2.0}, //
            {4.0,  1.0}, {4.0,  2.0}, {4.0,  3.0}, //
            {6.0, -3.0}, {6.0, -2.0}, {6.0, -1.0}, {6.0,  1.0}, //
            {8.0, -4.0}, {8.0, -2.0}, {8.0,  0.0}, //
        };
        std::vector<std::vector<size_t>> boundaryToPointIndices{
            {0, 5}, {1, 5}, {2, 6}, {3, 7}, {4, 7}, // 0-4
            {10, 7}, {9, 6}, {8, 5}, // 5-7
            {8, 14}, {9, 14}, {10, 14}, //  8-10
            {17, 13, 7}, {16, 12, 6}, {15, 11, 5} // 11-13
        };
        std::vector<std::pair<size_t, size_t>> laneSuccessorIndices{{1, 5}, {1, 9}, {2, 4}, {2, 8}};
        std::vector<std::pair<size_t, size_t>> lanePredecessorIndices{{4, 7}, {5, 6}};
        // clang-format on
        std::vector<std::vector<size_t>> roadToLaneIndices{{0, 1, 2, 3}, {4, 5}, {6, 7}, {8, 9}};

        // Fill ground truth using the data above

        const size_t numberOfRoads{roadToLaneIndices.size()};
        const size_t numberOfLanes{boundaryToPointIndices.size() - numberOfRoads};

        // ----- Create reference lines
        std::vector<osi3::ReferenceLine *> referenceLines;
        std::vector<osi3::LogicalLaneBoundary *> boundaries;
        std::vector<osi3::LogicalLane *> lanes;
        for (size_t i{0u}; i < roadToLaneIndices.size(); ++i)
        {
            const bool backwards{boundaryToPointIndices[roadToLaneIndices[i][0] + i].front() > boundaryToPointIndices[roadToLaneIndices[i][0] + i].back()};
            const size_t backwardsIndex{roadToLaneIndices[i].size() >> 1};
            const size_t polyLineIndex{i + roadToLaneIndices[i][backwardsIndex]};
            const auto &pointIndices{*std::next(boundaryToPointIndices.begin(), polyLineIndex)};

            referenceLines.push_back(groundTruth.add_reference_line());
            referenceLines.back()->mutable_id()->set_value(300u + i);
            osi3::ReferenceLine_ReferenceLinePoint *firstReferenceLinePoint{referenceLines.back()->add_poly_line()};
            firstReferenceLinePoint->mutable_world_position()->set_x(points[pointIndices.front()].x);
            firstReferenceLinePoint->mutable_world_position()->set_y(points[pointIndices.front()].y);
            firstReferenceLinePoint->mutable_world_position()->set_z(0.0);
            firstReferenceLinePoint->set_s_position(0.0);

            double endS{0.0};
            std::adjacent_find(pointIndices.begin(), pointIndices.end(), [&](const size_t lhs, const size_t rhs) {
                endS += (points[lhs] - points[rhs]).Length();
                osi3::ReferenceLine_ReferenceLinePoint *point{referenceLines.back()->add_poly_line()};
                point->mutable_world_position()->set_x(points[rhs].x);
                point->mutable_world_position()->set_y(points[rhs].y);
                point->mutable_world_position()->set_z(0.0);
                point->set_s_position(endS);
                return false;
            });
            for (size_t j{0u}; j < roadToLaneIndices[i].size(); ++j)
            {
                const auto &indices{boundaryToPointIndices[boundaries.size()]};
                boundaries.push_back(groundTruth.add_logical_lane_boundary());
                boundaries.back()->mutable_id()->set_value(200u + boundaries.size() - 1u);
                boundaries.back()->mutable_reference_line_id()->set_value(referenceLines.back()->id().value());
                for (size_t k{0u}; k < referenceLines.back()->poly_line_size(); ++k)
                {
                    osi3::LogicalLaneBoundary_LogicalBoundaryPoint *point{boundaries.back()->add_boundary_line()};
                    point->set_s_position(referenceLines.back()->poly_line(k).s_position());
                    const double t{points[indices[k]].y - referenceLines.back()->poly_line(k).world_position().y()};
                    point->set_t_position(backwards ? -t : t);
                    point->mutable_position()->set_x(points[indices[k]].x);
                    point->mutable_position()->set_y(points[indices[k]].y);
                    point->mutable_position()->set_z(0.0);
                }

                lanes.push_back(groundTruth.add_logical_lane());
                lanes.back()->mutable_id()->set_value(100u + lanes.size() - 1u);
                lanes.back()->set_type(osi3::LogicalLane_Type_TYPE_NORMAL);
                lanes.back()->set_start_s(0.0);
                lanes.back()->set_end_s(endS);
                lanes.back()->set_move_direction(j < backwardsIndex ? osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S : osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S);
                auto source{lanes.back()->add_source_reference()};
                source->add_identifier("Road" + std::to_string(i));
                source->add_identifier("0");  // s of t_road_lanes_laneSection (what does that mean?)
                source->add_identifier("-1"); // id of t_road_lanes_laneSection_left_lane, t_road_lanes_laneSection_right_lane (what does that mean?)
                lanes.back()->mutable_reference_line_id()->set_value(referenceLines.back()->id().value());
                lanes.back()->add_right_boundary_id()->set_value(boundaries.back()->id().value());

                if (j > 0u)
                {
                    osi3::LogicalLane *adjacentLane{(*std::prev(lanes.end(), 2))};
                    adjacentLane->add_left_boundary_id()->set_value(boundaries.back()->id().value());

                    osi3::LogicalLane_LaneRelation *right{lanes.back()->add_right_adjacent_lane()};
                    right->mutable_other_lane_id()->set_value(adjacentLane->id().value());
                    right->set_start_s(0.0);
                    right->set_start_s_other(0.0);
                    right->set_end_s(endS);
                    right->set_end_s_other(endS);
                    osi3::LogicalLane_LaneRelation *left{adjacentLane->add_left_adjacent_lane()};
                    left->mutable_other_lane_id()->set_value(lanes.back()->id().value());
                    left->set_start_s(0.0);
                    left->set_start_s_other(0.0);
                    left->set_end_s(endS);
                    left->set_end_s_other(endS);
                }
            }
            // Add final leftmost boundary of the road
            const auto &indices{boundaryToPointIndices[boundaries.size()]};
            boundaries.push_back(groundTruth.add_logical_lane_boundary());
            boundaries.back()->mutable_id()->set_value(200u + boundaries.size() - 1u);
            boundaries.back()->mutable_reference_line_id()->set_value(referenceLines.back()->id().value());
            // Define boundary polyline points
            for (size_t k{0u}; k < referenceLines.back()->poly_line_size(); ++k)
            {
                osi3::LogicalLaneBoundary_LogicalBoundaryPoint *point{boundaries.back()->add_boundary_line()};
                point->set_s_position(referenceLines.back()->poly_line(k).s_position());
                const double t{points[indices[k]].y - referenceLines.back()->poly_line(k).world_position().y()};
                point->set_t_position(backwards ? -t : t);
                point->mutable_position()->set_x(points[indices[k]].x);
                point->mutable_position()->set_y(points[indices[k]].y);
                point->mutable_position()->set_z(0.0);
            }
            lanes.back()->add_left_boundary_id()->set_value(boundaries.back()->id().value());
        }

        for (const auto [a, b] : lanePredecessorIndices)
        {
            osi3::LogicalLane_LaneConnection *connection{lanes[a]->add_predecessor_lane()};
            connection->set_at_begin_of_other_lane(true);
            connection->mutable_other_lane_id()->set_value(lanes[b]->id().value());
            connection = lanes[b]->add_predecessor_lane();
            connection->set_at_begin_of_other_lane(true);
            connection->mutable_other_lane_id()->set_value(lanes[a]->id().value());
        }
        for (const auto [a, b] : laneSuccessorIndices)
        {
            osi3::LogicalLane_LaneConnection *connection{lanes[a]->add_successor_lane()};
            connection->set_at_begin_of_other_lane(false);
            connection->mutable_other_lane_id()->set_value(lanes[b]->id().value());
            connection = lanes[b]->add_successor_lane();
            connection->set_at_begin_of_other_lane(false);
            connection->mutable_other_lane_id()->set_value(lanes[a]->id().value());
        }

        // ----- Create moving object
        osi3::MovingObject *object{groundTruth.add_moving_object()};
        object->mutable_id()->set_value(1000u);
        groundTruth.mutable_host_vehicle_id()->set_value(object->id().value());

        object->mutable_base()->mutable_position()->set_x(1.0);
        object->mutable_base()->mutable_position()->set_y(-1.0);
        object->mutable_base()->mutable_dimension()->set_width(0.5);
        object->mutable_base()->mutable_dimension()->set_length(1.0);
        object->mutable_base()->mutable_orientation()->set_yaw(0.0);
        // ----- Create intentionally incomplete lane assignment
        osi3::LogicalLaneAssignment *assignment{object->mutable_moving_object_classification()->add_logical_lane_assignment()};
        assignment->mutable_assigned_lane_id()->set_value(101u);
        // Intentionally left empty: assignment->set_angle_to_lane(_);
        assignment->set_s_position(1.0);
        assignment->set_t_position(-1.0);
        query = std::make_unique<osiql::Query>(groundTruth);
        query->SetHostVehicle(1000u);
    }
    osi3::GroundTruth groundTruth;
    std::unique_ptr<osiql::Query> query;
};

TEST_F(QueryTest, GetAdjacentLaneFromLeftmostLane)
{
    const std::array<osiql::Id, 4> ids{103, 102, 101, 100};
    const osiql::Lane *lane{query->GetLane(ids[0])};
    ASSERT_EQ(lane->index, 1u);

    auto resultingLane{lane->GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane))};
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane, nullptr);

    resultingLane = lane->GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane->GetId(), ids[1]);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane->GetId(), ids[1]);

    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(*lane, 0u);
    EXPECT_EQ(resultingLane->GetId(), ids[0]);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(*lane, 0u);
    EXPECT_EQ(resultingLane->GetId(), ids[0]);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(*lane, 0u);
    EXPECT_EQ(resultingLane->GetId(), ids[0]);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(*lane, 0u);
    EXPECT_EQ(resultingLane->GetId(), ids[0]);

    for (size_t i{1u}; i < ids.size(); ++i)
    {
        resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(*lane, i);
        EXPECT_EQ(resultingLane, nullptr);
        resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(*lane, i);
        EXPECT_EQ(resultingLane->GetId(), ids[i]);
        resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(*lane, i);
        EXPECT_EQ(resultingLane->GetId(), ids[i]);
        resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(*lane, i);
        EXPECT_EQ(resultingLane, nullptr);
    }

    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(*lane, 4u);
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(*lane, 4u);
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(*lane, 4u);
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(*lane, 4u);
    EXPECT_EQ(resultingLane, nullptr);

    resultingLane = lane->GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane->GetId(), ids[1]);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane->GetId(), ids[1]);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane, nullptr);
}
TEST_F(QueryTest, GetAdjacentLaneFromRightmostLane)
{
    const std::array<osiql::Id, 4> ids{100, 101, 102, 103};
    const osiql::Lane *lane{query->GetLane(ids[0])};
    ASSERT_EQ(lane->index, 1u);

    auto resultingLane{lane->GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane))};
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane, nullptr);

    resultingLane = lane->GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane->GetId(), ids[1]);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane->GetId(), ids[1]);

    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(*lane, 0u);
    EXPECT_EQ(resultingLane->GetId(), ids[0]);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(*lane, 0u);
    EXPECT_EQ(resultingLane->GetId(), ids[0]);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(*lane, 0u);
    EXPECT_EQ(resultingLane->GetId(), ids[0]);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(*lane, 0u);
    EXPECT_EQ(resultingLane->GetId(), ids[0]);

    for (size_t i{1u}; i < ids.size(); ++i)
    {
        resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(*lane, i);
        EXPECT_EQ(resultingLane, nullptr);
        resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(*lane, i);
        EXPECT_EQ(resultingLane->GetId(), ids[i]);
        resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(*lane, i);
        EXPECT_EQ(resultingLane->GetId(), ids[i]);
        resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(*lane, i);
        EXPECT_EQ(resultingLane, nullptr);
    }

    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(*lane, 4u);
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(*lane, 4u);
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(*lane, 4u);
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetRoad().GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(*lane, 4u);
    EXPECT_EQ(resultingLane, nullptr);

    resultingLane = lane->GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Forwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane, nullptr);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Right, osiql::Orientation::Backwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane->GetId(), ids[1]);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Forwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane->GetId(), ids[1]);
    resultingLane = lane->GetAdjacentLane<osiql::Side::Left, osiql::Orientation::Backwards>(osiql::GetLatitude<osiql::Direction::Downstream>(*lane));
    EXPECT_EQ(resultingLane, nullptr);
}

TEST_F(QueryTest, GetAdjacentLane)
{
    // GetLocalLane from road
    const osiql::Road &road{query->GetLane(100)->GetRoad()};
    ASSERT_EQ(road.laneRows.rows[0].lanes[0]->GetId(), 100);
    // road has four lanes, two on each side. -3/3 and beyond would crash.
    ASSERT_EQ(road.laneRows.size<osiql::Side::Left>(), 2u);
    ASSERT_EQ(road.laneRows.size<osiql::Side::Right>(), 2u);
    // Lanes to the right of the reference line have negative relative ids:
    ASSERT_EQ(road.laneRows.begin<osiql::Side::Right>()->front()->GetId(), road.laneRows.at<osiql::Side::Right>(0).front()->GetId());
    ASSERT_EQ(std::next(road.laneRows.begin<osiql::Side::Right>())->front()->GetId(), road.laneRows.at<osiql::Side::Right>(1).front()->GetId());
    // begin(Direction) starts on the lane with the lowest relative id of the given direction:
    ASSERT_EQ(road.laneRows.begin<osiql::Side::Left>()->front()->GetId(), road.laneRows.at<osiql::Side::Left>(0).front()->GetId());
    ASSERT_EQ(std::next(road.laneRows.begin<osiql::Side::Left>())->front()->GetId(), road.laneRows.at<osiql::Side::Left>(1).front()->GetId());
}

// Lane Width Tests

TEST_F(QueryTest, GetLaneWidth_OnVaryingWidthLane_Returns_InterpolatedWidth)
{
    ASSERT_DOUBLE_EQ(query->GetLane(101u)->GetWidth(0.0), 1.0);
    ASSERT_DOUBLE_EQ(query->GetLane(101u)->GetWidth(1.0), 1.5);
    ASSERT_DOUBLE_EQ(query->GetLane(101u)->GetWidth(2.0), 2.0);
}
TEST_F(QueryTest, GetLaneWidth_NextLaneAhead_Returns_ValidSuccessorWidth)
{
    const double length{osiql::Length(*query->GetLane(104u))};
    ASSERT_DOUBLE_EQ(query->GetLane(101u)->GetWidth(2.0 + length * 0.5), 1.5);
}
TEST_F(QueryTest, GetLaneWidth_PreviousLaneBehind_Returns_ValidPredecessorWidth)
{
    const double length{osiql::Length(*query->GetLane(104u))};
    ASSERT_DOUBLE_EQ(query->GetLane(106u)->GetWidth(-length * 0.25), 1.25);
    ASSERT_DOUBLE_EQ(query->GetLane(106u)->GetWidth(-length * 0.5), 1.5);
    ASSERT_DOUBLE_EQ(query->GetLane(106u)->GetWidth(-length * 0.75), 1.75);
}
TEST_F(QueryTest, GetLaneWidth_NoSuccessorLane_Returns_Zero)
{
    ASSERT_DOUBLE_EQ(query->GetLane(100u)->GetWidth(2.5), 0.0);
    ASSERT_DOUBLE_EQ(query->GetLane(100u)->GetWidth(-0.5), 0.0);
}
TEST_F(QueryTest, GetLaneWidth_MultiSegmentLane_Returns_CorrectInterpolation)
{
    const double length{osiql::Length(*query->GetLane(109u))};
    ASSERT_DOUBLE_EQ(query->GetLane(101u)->GetWidth(2.0 + length - 1.0), 1.5);
    ASSERT_DOUBLE_EQ(query->GetLane(101u)->GetWidth(2.0 + length), 2.0);
}
TEST_F(QueryTest, GetLaneWidth_StartingOnUpstreamLane_Returns_CorrectValue)
{
    const double length{osiql::Length(*query->GetLane(109u))};
    ASSERT_DOUBLE_EQ(query->GetLane(109u)->GetWidth(2.0 + (length - 2.0) * 0.5), 1.5);
    ASSERT_DOUBLE_EQ(query->GetLane(109u)->GetWidth(length + 3.0), 0.0);
}

// Distance from Object to Polyline Tests

TEST_F(QueryTest, GetDistanceTo_Polyline_Returns_CorrectValue)
{
    const osiql::MovingObject *object{query->GetMovingObject(1000u)};
    const osiql::Lane *lane{query->GetLane(101u)};
    const osiql::Boundary::Points &left{lane->GetBoundaryPoints<osiql::Side::Left>()};
    const osiql::Boundary::Points &right{lane->GetBoundaryPoints<osiql::Side::Right>()};
    EXPECT_DOUBLE_EQ(object->GetSignedDistancesTo(left.begin(), left.end()).min, 0.75);
    EXPECT_DOUBLE_EQ(object->GetSignedDistancesTo(right.begin(), right.end()).max, 0.0);
}

TEST_F(QueryTest, GetDistanceTo_AdjacentLanePolyline_Returns_CorrectValue)
{
    const osiql::MovingObject *object{query->GetMovingObject(1000u)};
    const osiql::Boundary::Points &right1{query->GetLane(103u)->GetBoundaryPoints<osiql::Side::Right>()};
    const osiql::Boundary::Points &left1{query->GetLane(102u)->GetBoundaryPoints<osiql::Side::Left>()};
    const osiql::Boundary::Points &left2{query->GetLane(101u)->GetBoundaryPoints<osiql::Side::Left>()};
    const osiql::Boundary::Points &right2{query->GetLane(100u)->GetBoundaryPoints<osiql::Side::Right>()};
    EXPECT_DOUBLE_EQ(object->GetSignedDistancesTo(right1.begin(), right1.end()).min, 2.75);
    EXPECT_EQ(object->GetSignedDistancesTo(left1.begin(), left1.end()), object->GetSignedDistancesTo(left2.begin(), left2.end()));
    EXPECT_DOUBLE_EQ(object->GetSignedDistancesTo(right2.begin(), right2.end()).max, -0.75);
}

TEST_F(QueryTest, GetDistanceTo_IntersectingPolyline_Returns_Positive_And_Negative_Distance)
{
    groundTruth.mutable_moving_object(0)->mutable_base()->mutable_dimension()->set_width(2.5);
    query = std::make_unique<osiql::Query>(groundTruth);
    const osiql::MovingObject *object{query->GetMovingObject(1000u)};
    const osiql::Lane *lane{query->GetLane(101u)};
    const osiql::Boundary::Points &left{lane->GetBoundaryPoints<osiql::Side::Left>()};
    EXPECT_LE(object->GetSignedDistancesTo(left.begin(), left.end()).min, 0.0);
    EXPECT_GE(object->GetSignedDistancesTo(left.begin(), left.end()).max, 0.0);
}

// Lane Curvature Tests

TEST_F(QueryTest, GetCurvature_StraightReferenceLine_Returns_Zero)
{
    const osiql::Lane *lane{query->GetLane(101u)};
    ASSERT_DOUBLE_EQ(lane->GetRoad().referenceLine.GetCurvature(0.0), 0.0);
}

TEST_F(QueryTest, GetCurvature_Forwards_Equals_BackwardsInverted)
{
    const osiql::Lane *lane{query->GetLane(108u)};
    const double downstreamCurvature{lane->GetRoad().referenceLine.GetCurvature(1.0, osiql::Direction::Downstream)};
    const double upstreamCurvature{lane->GetRoad().referenceLine.GetCurvature(1.0, osiql::Direction::Upstream)};
    ASSERT_DOUBLE_EQ(downstreamCurvature, -upstreamCurvature);
}

TEST_F(QueryTest, GetObstruction)
{
    const osiql::MovingObject &ego{*query->GetHostVehicle()};
    const osiql::RoadPoint origin{ego.positions.front()};
    // The host vehicle has the global position (1, -1), which happens to be on lane 101, with s,t-coordinates (1,-1).
    // At that point, the lane has a width of 1.5, making the projection ratio 2/3. With that in mind, we now compare the
    // obstruction of other points relative to 2/3 of the width at their lane's s-coordinate.
    osi3::TrafficAction::AcquireGlobalPositionAction action;
    action.mutable_position()->set_x(7.0);
    action.mutable_position()->set_y(-3.0);
    const osiql::Route route{query->GetRoute(action)};
    ASSERT_THAT(ego.positions, Not(IsEmpty()));
    // The route's local points both have absolute t-offset of 1 and relative t-offset of 2/3, so their obstruction should be identical.
    // Local shape is: (0.5, -1.25), (1.5, -1.25), (1.5, -0.75), (0.5, -0.75)
    // Driveline distance: -0.625, -0.375, 0.125, 0.125
    const auto [min1, max1]{ego.GetObstruction(origin, route)};
    EXPECT_THAT(min1, DoubleEq(-3.0 / 8.0));
    EXPECT_THAT(max1, DoubleEq(3.0 / 8.0));
    const auto [min2, max2]{ego.GetObstruction(route.back(), route)};
    EXPECT_THAT(min2, DoubleEq(1.0 / 8.0));
    EXPECT_THAT(max2, DoubleEq(7.0 / 8.0));

    const auto [min3, max3]{ego.GetObstruction(osiql::RoadPoint{route.back().GetLane(), osiql::Coordinates<osiql::Road>{0.0, 0.0}}, route)};
    EXPECT_THAT(min3, DoubleEq(-9.0 / 8.0));
    EXPECT_THAT(max3, DoubleEq(-3.0 / 8.0));
    // EXPECT_THAT(ego.GetObstruction(route.GetPoints(), osiql::RoadPoint{0.0, 1.0, route.back().lane}), DoubleEq(1.0 / 3.0));
    // EXPECT_THAT(ego.GetObstruction(route.GetPoints(), osiql::RoadPoint{0.0, 2.0, route.back().lane}), DoubleEq(-2.0 / 3.0));
    // // The projection extends behind itself, thus points behind the route have valid obstructions as well:
    // EXPECT_THAT(ego.GetObstruction(route.GetPoints(), osiql::RoadPoint{0.0, -1.0, route.front().lane}), DoubleEq(-1.0 / 3.0));
    // EXPECT_THAT(ego.GetObstruction(route.GetPoints(), osiql::RoadPoint{0.0, 0.0, route.front().lane}), DoubleEq(2.0 / 3.0));
}
