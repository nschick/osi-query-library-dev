/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OsiQueryLibrary/osiql.h"

using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::Lt;
using ::testing::SizeIs;

class RouteTest : public ::testing::Test
{
public:
    RouteTest()
    {
        // Refer to route_test.drawio for the ground truth illustration

        using lineCoordinates = std::pair<osiql::Vector2d, osiql::Vector2d>;

        // XY coordinates of two points forming the outer/reference lines
        static std::vector<lineCoordinates> outerBoundaryLines{
            // Point A to B
            {{-6.0, 0.0}, {0.0, 6.0}},
            // Point B to C
            {{0.0, 6.0}, {6.0, 0.0}},
            // Point C to D
            {{6.0, 0.0}, {0.0, -6.0}},
            // Point D to A
            {{0.0, -6.0}, {-6.0, 0.0}}};

        // XY coordinates of two points forming the inner lines
        static std::vector<lineCoordinates> innerBoundaryLines{
            // Point E to F
            {{-3.5, 0.0}, {0.0, -3.5}},
            // Point F to G
            {{0.0, 3.5}, {3.5, 0.0}},
            // Point G to H
            {{3.5, 0.0}, {0.0, -3.5}},
            // Point H to E
            {{0.0, -3.5}, {-3.5, 0.0}}};

        constexpr auto s_distance{6.0}; // Distance between points (A -> B)
        constexpr auto t_distance{2.0}; // Distance between the inner and outer points (A -> E)
        constexpr auto ref_line_id{100u};
        constexpr auto left_boundary_id{200u};
        constexpr auto right_boundary_id{300u};
        constexpr auto object_id{400u};

        auto i{0u}; // Index also serves as the unique lane id

        // Construct ground truth with four roads(one lane each) forming a diamond loop in clockwise direction.
        // Note: The terms road and lane are synonymous in this ground truth as we only have one lane per road.
        std::vector<osi3::ReferenceLine *> referenceLines;
        std::vector<osi3::LogicalLaneBoundary *> boundaries;
        for (const auto &xyCoordinates : outerBoundaryLines)
        {
            auto lane = lanes.emplace_back(ground.add_logical_lane());

            lane->mutable_id()->set_value(i);
            lane->set_type(osi3::LogicalLane_Type_TYPE_NORMAL);

            /** Create outer/reference lines **/
            auto referenceLine = referenceLines.emplace_back(ground.add_reference_line());
            referenceLine->mutable_id()->set_value(ref_line_id + i);

            const auto addReferenceLinePoint = [&referenceLine](double s, const osiql::Vector2d &point) {
                // Points: A -> B -> C -> D -> A
                osi3::ReferenceLine_ReferenceLinePoint *referenceLinePoint = referenceLine->add_poly_line();
                referenceLinePoint->mutable_world_position()->set_x(point.x);
                referenceLinePoint->mutable_world_position()->set_y(point.y);
                referenceLinePoint->set_s_position(s); // Distance between points
            };

            // Point A (B, C, D)
            addReferenceLinePoint(0.0 /*as starting point*/, xyCoordinates.first);

            // Point B (C, D, A)
            addReferenceLinePoint(s_distance, xyCoordinates.second);

            // Set the reference line id to this lane
            lane->mutable_reference_line_id()->set_value(referenceLine->id().value());

            /** Create lane boundaries **/
            auto boundary = boundaries.emplace_back(ground.add_logical_lane_boundary());
            boundary->mutable_id()->set_value(left_boundary_id + i);

            const auto addBoundaryPoint = [&boundary](double s, double t, const osiql::Vector2d &point) {
                osi3::LogicalLaneBoundary_LogicalBoundaryPoint *boundaryPoint = boundary->add_boundary_line();
                // Points AB (BC, CD, DA), and EF (FG, GH, HE)
                boundaryPoint->mutable_position()->set_x(point.x);
                boundaryPoint->mutable_position()->set_y(point.y);
                boundaryPoint->set_s_position(s);
                boundaryPoint->set_t_position(t); // Distance between the inner and outer points
            };

            // Add reference/outer line as our LEFT boundary of the lane
            addBoundaryPoint(0.0 /*as starting point*/, 0.0, xyCoordinates.first);
            addBoundaryPoint(s_distance, 0.0, xyCoordinates.second);

            lane->add_left_boundary_id()->set_value(boundary->id().value());

            // Add inner line as our RIGHT boundary of the lane
            boundary = boundaries.emplace_back(ground.add_logical_lane_boundary());
            boundary->mutable_id()->set_value(right_boundary_id + i);

            const auto &coordinates = innerBoundaryLines[i];
            addBoundaryPoint(0.0 /*as starting point*/, t_distance, coordinates.first);
            addBoundaryPoint(s_distance, t_distance, coordinates.second);

            lane->add_right_boundary_id()->set_value(boundary->id().value());

            auto sourceLane = lane->add_source_reference();
            sourceLane->add_identifier("Road" + std::to_string(i));

            lane->set_move_direction(osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);
            lane->set_start_s(0.0);
            lane->set_end_s(s_distance); // Using outer/reference line's distance between points

            i++;
        }

        /** Connect all the lanes forming a diamond loop: A -> B -> C -> D -> A **/
        const auto connectLanes = [&](size_t A, size_t B) {
            // Lane A is the predecessor of Lane B
            osi3::LogicalLane_LaneConnection *predecessorLane = lanes[B]->add_predecessor_lane();
            predecessorLane->set_at_begin_of_other_lane(true);
            predecessorLane->mutable_other_lane_id()->set_value(lanes[A]->id().value());

            // Lane B is a successor of A
            osi3::LogicalLane_LaneConnection *successorLane = lanes[A]->add_successor_lane();
            successorLane->set_at_begin_of_other_lane(true);
            successorLane->mutable_other_lane_id()->set_value(lanes[B]->id().value());
        };

        connectLanes(0, 1); // A -> B
        connectLanes(1, 2); // B -> C
        connectLanes(2, 3); // C- > D
        connectLanes(3, 0); // D -> A

        /** Create moving objects **/
        // XY coordinates of our objects based on their respective center points
        static std::vector<osiql::Vector2d> objectCoordinates{
            {3.5, 1.5},    // Object A
            {1.5, -3.5},   // Object B
            {-1.5, -3.5},  // Object C
            {-3.5, 1.5},   // Object D
            {4.5, 0.0},    // Obect E at the end of the lane( parallel to the Y axis)
            {-0.5, -4.5}}; // Obect F at the start of the lane

        const auto addObject = [&](size_t i) {
            auto object = movingObjects.emplace_back(ground.add_moving_object());
            object->mutable_id()->set_value(object_id + i);
            object->set_type(osi3::MovingObject_Type_TYPE_VEHICLE);

            // Set the attributes of the object e.g. position, orientation, dimension, etc.
            osi3::BaseMoving *base{object->mutable_base()};
            base->mutable_position()->set_x(objectCoordinates[i].x);
            base->mutable_position()->set_y(objectCoordinates[i].y);
            base->mutable_dimension()->set_width(1.0);
            base->mutable_dimension()->set_length(2.0);
            base->mutable_orientation()->set_yaw(0.0);
        };

        addObject(0);
        addObject(1);
        addObject(2);
        addObject(3);
        addObject(4);
        addObject(5);

        query = std::make_unique<osiql::Query>(ground);
    }

    osi3::GroundTruth ground;
    std::vector<osi3::LogicalLane *> lanes;
    std::vector<osi3::MovingObject *> movingObjects;
    std::unique_ptr<osiql::Query> query;
};

TEST_F(RouteTest, GetShortestRoute_ForwardSearch)
{
    const auto objectA{query->GetMovingObject(movingObjects[0]->id().value())};
    const auto objectB{query->GetMovingObject(movingObjects[1]->id().value())};
    const auto objectD{query->GetMovingObject(movingObjects[3]->id().value())};

    auto route{objectA->GetShortestRoute<osiql::Orientation::Forwards>(objectB->positions)};
    EXPECT_THAT(route, SizeIs(2));

    route = objectB->GetShortestRoute<osiql::Orientation::Forwards>(objectA->positions);
    EXPECT_THAT(route, SizeIs(4));

    route = objectD->GetShortestRoute<osiql::Orientation::Forwards>(objectA->positions);
    EXPECT_THAT(route, SizeIs(2));

    route = objectD->GetShortestRoute<osiql::Orientation::Forwards>(objectB->positions);
    EXPECT_THAT(route, SizeIs(3));
}

TEST_F(RouteTest, GetShortestRoute_BackwardSearch)
{
    const auto objectA{query->GetMovingObject(movingObjects[0]->id().value())};
    const auto objectB{query->GetMovingObject(movingObjects[1]->id().value())};
    const auto objectC{query->GetMovingObject(movingObjects[2]->id().value())};
    const auto objectD{query->GetMovingObject(movingObjects[3]->id().value())};

    auto route{objectA->GetShortestRoute<osiql::Orientation::Backwards>(objectB->positions)};
    EXPECT_THAT(route, SizeIs(4));

    route = objectB->GetShortestRoute<osiql::Orientation::Backwards>(objectA->positions);
    EXPECT_THAT(route, SizeIs(2));

    route = objectC->GetShortestRoute<osiql::Orientation::Backwards>(objectA->positions);
    EXPECT_THAT(route, SizeIs(3));

    route = objectD->GetShortestRoute<osiql::Orientation::Backwards>(objectC->positions);
    EXPECT_THAT(route, SizeIs(2));
}

TEST_F(RouteTest, GetShortestRoute_AnyDirection)
{
    const auto objectA{query->GetMovingObject(movingObjects[0]->id().value())};
    const auto objectB{query->GetMovingObject(movingObjects[1]->id().value())};
    const auto objectC{query->GetMovingObject(movingObjects[2]->id().value())};
    const auto objectD{query->GetMovingObject(movingObjects[3]->id().value())};

    auto route{objectA->GetShortestRoute<osiql::Orientation::Any>(objectB->positions)};
    EXPECT_THAT(route, SizeIs(2));

    route = objectB->GetShortestRoute<osiql::Orientation::Any>(objectA->positions);
    EXPECT_THAT(route, SizeIs(2));

    route = objectA->GetShortestRoute<osiql::Orientation::Any>(objectD->positions);
    EXPECT_THAT(route, SizeIs(2));

    route = objectC->GetShortestRoute<osiql::Orientation::Any>(objectB->positions);
    EXPECT_THAT(route, SizeIs(2));
}

TEST_F(RouteTest, GetShortestRoute_FromLaneSeam)
{
    const auto objectE{query->GetMovingObject(movingObjects[4]->id().value())};
    const auto objectF{query->GetMovingObject(movingObjects[5]->id().value())};

    // Get shortest path from Object E - positioned at the end of the lane to Object F
    auto route{objectE->GetShortestRoute<osiql::Orientation::Forwards>(objectF->positions)};
    EXPECT_THAT(route, SizeIs(2));

    route = objectE->GetShortestRoute<osiql::Orientation::Backwards>(objectF->positions);
    EXPECT_THAT(route, SizeIs(3));
}

TEST_F(RouteTest, GetShortestRoute_ToLaneSeam)
{
    osiql::RoadPoint start{*query->GetLane(3), osiql::Coordinates<osiql::Road>{0.0, 0.0}};
    osiql::RoadPoint t_destination{*query->GetLane(2), osiql::Coordinates<osiql::Road>{0.0, 0.0}};
    osiql::RoadPoint t_destinationOffLane{*query->GetLane(2), osiql::Coordinates<osiql::Road>{0.0, 2.5}};
    osiql::RoadPoint t_destinationOnLane{*query->GetLane(2), osiql::Coordinates<osiql::Road>{0.0, -1.5}};

    double distance{0.0};
    auto route = query->GetShortestRoute<osiql::Orientation::Forwards>(start, t_destination, &distance);

    EXPECT_THAT(route, SizeIs(4));
    EXPECT_DOUBLE_EQ(distance, 18.0);

    route = query->GetShortestRoute<osiql::Orientation::Backwards>(start, t_destination, &distance);

    EXPECT_THAT(route, SizeIs(2));
    EXPECT_DOUBLE_EQ(distance, 6.0);

    route = query->GetShortestRoute<osiql::Orientation::Forwards>(start, t_destinationOffLane, &distance);

    EXPECT_THAT(route, SizeIs(4));
    EXPECT_DOUBLE_EQ(distance, 18.0);

    route = query->GetShortestRoute<osiql::Orientation::Backwards>(start, t_destinationOffLane, &distance);

    EXPECT_THAT(route, SizeIs(2));
    EXPECT_DOUBLE_EQ(distance, 6.0);

    route = query->GetShortestRoute<osiql::Orientation::Forwards>(start, t_destinationOnLane, &distance);

    EXPECT_THAT(route, SizeIs(4));
    EXPECT_DOUBLE_EQ(distance, 18.0);

    route = query->GetShortestRoute<osiql::Orientation::Backwards>(start, t_destinationOnLane, &distance);

    EXPECT_THAT(route, SizeIs(2));
    EXPECT_DOUBLE_EQ(distance, 6.0);
}

TEST_F(RouteTest, GetDistanceTo_ReturnsCorrectShortestDistance)
{
    const auto objectA{query->GetMovingObject(movingObjects[0]->id().value())};
    const auto objectB{query->GetMovingObject(movingObjects[1]->id().value())};

    const auto distance = objectA->GetDistanceTo<osiql::Orientation::Forwards>(*objectB);
    EXPECT_THAT(distance, Lt(6.0));
}
