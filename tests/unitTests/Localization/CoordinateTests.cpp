/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OsiQueryLibrary/osiql.h"
#include "util/GroundTruth.h"

using ::testing::_;
using ::testing::DoubleEq;
using ::testing::ElementsAre;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

class CoordinateTest : public ::testing::Test
{
public:
    CoordinateTest()
    {
        using Boundary = std::vector<osiql::Vector2d>;
        using BoundaryChain = std::vector<Boundary>;
        using Road = std::vector<BoundaryChain>;
        const std::vector<Road> roads{
            Road{
                BoundaryChain{Boundary{{-5.0, -5.0}, {5.0, -5.0}, {5.0, 5.0}, {1.0, 5.0}, {1.0, 6.0}, {-1.0, 6.0}, {-1.0, 5.0}, {-5.0, 5.0}}},
                BoundaryChain{Boundary{{-4.0, -4.0}, {4.0, -4.0}, {4.0, -2.0}}, Boundary{{4.0, -2.0}, {4.0, 2.0}}, Boundary{{4.0, 2.0}, {4.0, 4.0}, {-4.0, 4.0}}},
                BoundaryChain{Boundary{{-3.0, -3.0}, {3.0, -3.0}, {3.0, 3.0}, {-3.0, 3.0}}},
                BoundaryChain{Boundary{{-2.0, -2.0}, {2.0, -2.0}, {2.0, 0.0}}, Boundary{{2.0, 0.0}, {2.0, 2.0}, {-2.0, 2.0}}},
                BoundaryChain{Boundary{{-1.0, -1.0}, {1.0, -1.0}, {1.0, 1.0}, {-1.0, 1.0}}} //
            },
            Road{
                BoundaryChain{Boundary{{6.0, 1.0}, {10.0, -3.0}}},
                BoundaryChain{Boundary{{6.0, 3.0}, {10.0, -1.0}}} //
            },
            Road{
                BoundaryChain{Boundary{{7.0, -3.0}, {10.0, 1.0}}},
                BoundaryChain{Boundary{{6.0, -2.0}, {9.0, 2.0}}} //
            },
            Road{
                BoundaryChain{Boundary{{10.0, 3.0}, {10.0, 5.0}}},
                BoundaryChain{Boundary{{10.0, 3.0}, {8.0, 5.0}}} //
            }                                                    //
        };
        std::vector<std::vector<std::vector<double>>> lanes{
            std::vector<std::vector<double>>{
                std::vector<double>{0.0, 1.0},
                std::vector<double>{0.0, 1.0, 2.0},
                std::vector<double>{0.0, 1.0},
                std::vector<double>{0.0, 1.0, 2.0, 3.0} //
            },
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}} //
        };
        for (size_t i{0u}; i < roads.size(); ++i)
        {
            groundTruth.AddRoad(roads[i], roads[i].size() >> 1, lanes[i]);
        }
        std::cout << "Number of first road's leftmost lanes: " << groundTruth.roads.front().laneRows.back().size() << '\n';
        std::cout << "Total number of lanes: " << groundTruth.handle.logical_lane_size() << " (should be 10)\n";

        query = std::make_unique<osiql::Query>(groundTruth.handle);
    }

    std::unique_ptr<osiql::Query> query;
    osiql::test::GroundTruth groundTruth;
};

TEST_F(CoordinateTest, DISABLED_PolygonOutsideLane_Returns_EmptyIntersectionBounds)
{
    // Touching no point on a lane
    osiql::Polygon polygon{CreatePolygon(osiql::Box{{-0.5, -0.5}, {0.5, 0.5}})};
    std::vector<osiql::LocalBounds<const osiql::Lane>> result{query->FindLocalIntersectionBounds<osiql::Lane>(polygon)};
    EXPECT_THAT(result, IsEmpty());

    // Polygon corner touching lane corner
    polygon = CreatePolygon(osiql::Box{{-2.0, 0.0}, {-1.0, 1.0}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, IsEmpty());

    // Polygon corner touching lane edge
    polygon = CreatePolygon(osiql::Box{{-2.5, 0.5}, {-1.5, 1.5}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, IsEmpty());

    // Polygon edge touching lane corner
    polygon = CreatePolygon(osiql::Box{{-6.0, 4.5}, {-5.0, 5.5}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, IsEmpty());

    // Polygon corner touching multiple lanes
    polygon = CreatePolygon(osiql::Box{{-3.0, 1.0}, {-2.0, 2.0}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon); // Obtuse angle
    EXPECT_THAT(result, IsEmpty());

    polygon = CreatePolygon(osiql::Box{{5.0, 5.0}, {6.0, 6.0}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon); // Right angle
    EXPECT_THAT(result, IsEmpty());

    polygon = CreatePolygon(osiql::Box{{5.5, 3.0}, {6.0, 4.0}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon); // Acute angle
    EXPECT_THAT(result, IsEmpty());

    // Polygon edges touching multiple lanes
    polygon = CreatePolygon(osiql::Box{{0.0, 0.0}, {1.0, 1.0}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, IsEmpty());
}

TEST_F(CoordinateTest, DISABLED_PolygonInsideLane_Returns_IntersectionBounds)
{
    // Touching no border (0 intersection points)
    osiql::Polygon polygon{CreatePolygon(osiql::Box{{-0.5, -1.75}, {0.5, -1.25}})};
    std::vector<osiql::LocalBounds<const osiql::Lane>> result{query->FindLocalIntersectionBounds<osiql::Lane>(polygon)};
    EXPECT_THAT(result, SizeIs(1));

    // Touching border corner with corner (1 intersection point)
    polygon = CreatePolygon(osiql::Box{{0.0, 4.5}, {1.0, 5.0}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, SizeIs(1));

    // Touching border edge with corner (1 intersection point)
    polygon = CreatePolygon(osiql::Box{{6.5, 1.5}, {7.0, 2.0}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, SizeIs(1));

    // Touching border corner with edge (1 intersection points)
    polygon = osiql::Polygon({{0.5, 4.5}, {0.5, 5.5}, {1.5, 4.5}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, SizeIs(1));

    // Touching two border edges with corners (2 intersection points)
    polygon = osiql::Polygon({{0.5, 6.0}, {1.0, 5.5}, {0.5, 5.5}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon); // Same boundary chain
    EXPECT_THAT(result, SizeIs(1));

    polygon = osiql::Polygon({{0.0, 2.0}, {0.5, 2.5}, {0.0, 3.0}, {-0.5, 2.5}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon); // different boundary chain
    EXPECT_THAT(result, SizeIs(1));

    // Touching border edge with edge (2 intersection point)
    polygon = osiql::Polygon(CreatePolygon(osiql::Box{{-0.5, 1.5}, {0.5, 2.0}}));
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, SizeIs(1));

    // Touching a border with edge and another border with a corner (3 intersection points)
    polygon = osiql::Polygon({{0.0, 1.0}, {-1.0, 2.0}, {1.0, 2.0}});
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, SizeIs(1));

    // Touching two borders with edges (4 intersection points)
    polygon = osiql::Polygon(CreatePolygon(osiql::Box{{-0.5, 1.0}, {0.5, 2.0}}));
    result = query->FindLocalIntersectionBounds<osiql::Lane>(polygon);
    EXPECT_THAT(result, SizeIs(1));
}

TEST_F(CoordinateTest, DISABLED_PartiallyInsideLane_Returns_PartialIntersectionBounds)
{
    // The prior two tests already show that intersection testing correctly determines whether a shape is inside or outside a lane
    // These partial intersection tests serve to verify that the returned intersection bounds have the correct coordinates:

    // Intersecting with start of lane

    // Intersecting with end of lane

    // Intersecting with left side of lane

    // Intersecting with right side of lane

    // Fully covering entire lane
}

TEST_F(CoordinateTest, DISABLED_GlobalPoint_To_LocalPoint)
{
    // At start of lane

    // At end of lane

    // Corner of single lane

    // Corner of multiple lanes

    // Edge of single lane

    // Edge of multiple lanes

    // Forced conversion:

    // Left of lane

    // Right of lane

    // Before lane

    // After lane
}

TEST_F(CoordinateTest, DISABLED_LocalPoint_To_GlobalPoint)
{
}
