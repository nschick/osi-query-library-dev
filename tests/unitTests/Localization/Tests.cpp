/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <array>
#include <numeric>
//
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OsiQueryLibrary/osiql.h"

// Lane tests are intended to verify the behavior of lanes with multiple boundaries and successive
// lanes with the same reference line. The groundTruth is laid out to test these scenarios:
// - Retrieve the s & t coordinates & angle/curvature on a curved lane with multiple boundaries
// - Retrieve the traversed lanes of a route, once for an ambiguous successor lane and once for an ambiguous adjacent lane

using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::ReturnRef;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

using Vector2d = osiql::Vector2d;
using osiql::operator<<;

// Returns the point along a given Bézier curve interpolated for a given t in [0.0, 1.0].
template <size_t n>
Vector2d GetPoint(double t, const std::array<Vector2d, n> &curve)
{
    if constexpr (n == 4)
    {
        const double t_{1 - t};
        return t_ * t_ * t_ * curve[0] + 3.0 * t_ * t_ * t * curve[1] + 3.0 * t_ * t * t * curve[2] + t * t * t * curve[3];
    }
    else if constexpr (n == 3)
    {
        const double t_{1 - t};
        return t_ * t_ * curve[0] + 2.0 * t_ * t * curve[1] + t * t * curve[2];
    }
    else if constexpr (n == 2)
    {
        return (1.0 - t) * curve[0] + t * curve[1];
    }
    else
    {
        std::cerr << "Interpolating the point of a bezier curve of degree n=" << n << " is not supported. Returning coordinate origin.";
        return {};
    }
}

class LocalizationTest : public ::testing::Test
{
public:
    osi3::GroundTruth groundTruth{};
    std::unique_ptr<osiql::Query> query{nullptr};

    int laneId{100};
    int boundaryId{200};
    int referenceLineId{300};

    void createStartingRoad()
    {
        const std::array<const std::array<Vector2d, 3>, 2> points{
            std::array<Vector2d, 3>{Vector2d{0.0, -30.0}, {-100.0, -30.0}, {-100.0, 0.0}},
            std::array<Vector2d, 3>{Vector2d{0.0, -20.0}, {-090.0, -20.0}, {-090.0, 0.0}} //
        };
        // the start and end of boundaries on the curved entry lane
        const std::array<const std::vector<double>, 2> breaks{
            std::vector<double>{0.0, 0.5, 1.0},
            std::vector<double>{0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0} //
        };
    }

    void createMiddleRoad()
    {
        // Anchor points of a quadratic bézier curve describing the boundaries of this road
        const std::array<const std::array<Vector2d, 4>, 5> points{
            std::array<Vector2d, 4>{Vector2d{-100.0, 0.0}, {-100.0, 100.0}, {100.0, 100.0}, {100.0, 0.0}},
            std::array<Vector2d, 4>{Vector2d{-090.0, 0.0}, {-090.0, 090.0}, {090.0, 090.0}, {090.0, 0.0}},
            std::array<Vector2d, 4>{Vector2d{-080.0, 0.0}, {-080.0, 080.0}, {080.0, 080.0}, {080.0, 0.0}},
            std::array<Vector2d, 4>{Vector2d{-070.0, 0.0}, {-070.0, 070.0}, {070.0, 070.0}, {070.0, 0.0}},
            std::array<Vector2d, 4>{Vector2d{-060.0, 0.0}, {-060.0, 060.0}, {060.0, 060.0}, {060.0, 0.0}} //
        };
        const std::array<const std::vector<osiql::Lane::Type>, 4> laneTypes{
            std::vector<osiql::Lane::Type>{osiql::Lane::Type::Normal, osiql::Lane::Type::Normal},
            std::vector<osiql::Lane::Type>{osiql::Lane::Type::Normal, osiql::Lane::Type::Other, osiql::Lane::Type::Normal, osiql::Lane::Type::Other, osiql::Lane::Type::Normal, osiql::Lane::Type::Other},
            std::vector<osiql::Lane::Type>{osiql::Lane::Type::Other, osiql::Lane::Type::Normal, osiql::Lane::Type::Other, osiql::Lane::Type::Normal, osiql::Lane::Type::Normal, osiql::Lane::Type::Other},
            std::vector<osiql::Lane::Type>{osiql::Lane::Type::Normal} //
        };
        // Starts and ends of lanes in t values in [0.0, 1.0] to be interpolated along the corresponding bezier curve
        const std::array<const std::vector<double>, 5> breaks{
            std::vector<double>{0.0, 0.1, 1.0},
            std::vector<double>{0.0, 0.2, 0.35, 0.6, 0.8, 1.0},
            std::vector<double>{0.0, 0.1, 0.5, 0.7, 0.75, 0.9, 1.0},
            std::vector<double>{0.0, 1.0},
            std::vector<double>{0.0, 1.0} //
        };

        std::vector<double> ts{
            0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45,     //
            0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0 //
        };

        std::array<std::vector<osi3::LogicalLane *>, 4> lanes;
        std::vector<std::vector<osi3::LogicalLaneBoundary *>> boundaries(breaks.size());

        // The first boundary chain is special. We can't create a lane yet and we need to create a reference line:
        osi3::ReferenceLine *referenceLine{groundTruth.add_reference_line()};
        referenceLine->mutable_id()->set_value(referenceLineId++);

        osi3::ReferenceLine_ReferenceLinePoint *referenceLinePoint{referenceLine->add_poly_line()};
        referenceLinePoint->mutable_world_position()->set_x(points[0][0].x);
        referenceLinePoint->mutable_world_position()->set_y(points[0][0].y);
        referenceLinePoint->set_s_position(0.0);
        for (auto t{std::next(ts.begin())}; t != ts.end(); ++t)
        {
            const Vector2d point{GetPoint(*t, points[0])};
            const double s{referenceLine->poly_line().rbegin()->s_position()};
            const Vector2d prev{*referenceLine->poly_line().rbegin()};
            osi3::ReferenceLine_ReferenceLinePoint *referenceLinePoint{referenceLine->add_poly_line()};
            referenceLinePoint->mutable_world_position()->set_x(point.x);
            referenceLinePoint->mutable_world_position()->set_y(point.y);
            referenceLinePoint->set_s_position(s + (point - prev).Length());
        }

        for (size_t i{0u}; i < points.size(); ++i)
        {
            auto breakIt{std::next(breaks[i].begin())};
            auto pointIt{referenceLine->poly_line().begin()};
            auto t{ts.begin()};

            for (; breakIt != breaks[i].end(); ++breakIt)
            {
                osi3::LogicalLaneBoundary *boundary{groundTruth.add_logical_lane_boundary()};
                boundaries[i].push_back(boundary);
                boundary->mutable_id()->set_value(boundaryId++);
                boundary->mutable_reference_line_id()->set_value(referenceLine->id().value());

                while ((pointIt != referenceLine->poly_line().end()) && (*t <= *breakIt))
                {
                    osi3::LogicalLaneBoundary_LogicalBoundaryPoint *point{boundary->add_boundary_line()};
                    const Vector2d values{GetPoint(*t, points[i])};
                    point->mutable_position()->set_x(values.x);
                    point->mutable_position()->set_y(values.y);
                    point->set_s_position(pointIt->s_position());
                    point->set_t_position(-10.0 * i);
                    ++pointIt;
                    ++t;
                }
                --pointIt;
                --t;
                // TODO: Assign t_axis_yaw of reference line points
            }
        }
        // Create lanes
        for (size_t i{0u}; i < lanes.size(); ++i)
        {
            for (size_t j{0u}; j < breaks[i].size() - 1; ++j)
            {
                osi3::LogicalLane *lane{groundTruth.add_logical_lane()};
                lanes[i].push_back(lane);
                lane->mutable_id()->set_value(laneId++);
                lane->set_move_direction(osi3::LogicalLane_MoveDirection::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);
                lane->set_type(static_cast<osi3::LogicalLane_Type>(laneTypes[i][j]));
                lane->add_source_reference()->add_identifier("Road 2");
                lane->mutable_reference_line_id()->set_value(referenceLine->id().value());
                const auto id{boundaries[i][j]->id().value()};
                const double startS{boundaries[i][j]->boundary_line(0).s_position()};
                lane->set_start_s(startS);
                const double endS{boundaries[i][j]->boundary_line().rbegin()->s_position()};
                lane->set_end_s(endS);
                lane->add_left_boundary_id()->set_value(boundaries[i][j]->id().value());
                for (osi3::LogicalLaneBoundary *boundary : boundaries[i + 1])
                {
                    if (boundary->boundary_line().rbegin()->s_position() <= lane->start_s())
                    {
                        continue;
                    }
                    else if (boundary->boundary_line(0).s_position() >= lane->end_s())
                    {
                        break;
                    }
                    else
                    {
                        lane->add_right_boundary_id()->set_value(boundary->id().value());
                    }
                }
            }
            // Connect successors
            for (size_t j{1u}; j < lanes[i].size(); ++j)
            {
                osi3::LogicalLane_LaneConnection *connection{lanes[i][j - 1]->add_successor_lane()};
                connection->set_at_begin_of_other_lane(true);
                connection->mutable_other_lane_id()->set_value(lanes[i][j]->id().value());
                connection = lanes[i][j]->add_predecessor_lane();
                connection->set_at_begin_of_other_lane(false);
                connection->mutable_other_lane_id()->set_value(lanes[i][j - 1]->id().value());
            }
        }
        // TODO: connect adjacent lanes
        for (size_t i{1u}; i < lanes.size(); ++i)
        {
            auto leftLane{lanes[i - 1].begin()};
            for (auto *rightLane : lanes[i])
            {
                if ((*leftLane)->end_s() <= rightLane->start_s())
                {
                    ++leftLane;
                }
                else if ((*leftLane)->start_s() >= rightLane->end_s())
                {
                    continue;
                }
                else
                {
                    osi3::LogicalLane_LaneRelation *relation{(*leftLane)->add_right_adjacent_lane()};
                    relation->set_start_s((*leftLane)->start_s());
                    relation->set_start_s_other(rightLane->start_s());
                    relation->set_end_s((*leftLane)->end_s());
                    relation->set_end_s_other(rightLane->end_s());
                    relation->mutable_other_lane_id()->set_value(rightLane->id().value());
                    relation = rightLane->add_left_adjacent_lane();
                    relation->set_start_s(rightLane->start_s());
                    relation->set_start_s_other((*leftLane)->start_s());
                    relation->set_end_s(rightLane->end_s());
                    relation->set_end_s_other((*leftLane)->end_s());
                    relation->mutable_other_lane_id()->set_value((*leftLane)->id().value());
                }
            }
        }
    }
    void createEndingRoad()
    {
        const std::array<const std::array<Vector2d, 2>, 2> points{
            std::array<Vector2d, 2>{Vector2d{70.0, 0.0}, {70.0, -20.0}},
            std::array<Vector2d, 2>{Vector2d{60.0, 0.0}, {60.0, -20.0}} //
        };
    }

    LocalizationTest()
    {
        createStartingRoad(); // TODO: Implement
        createMiddleRoad();
        createEndingRoad(); // TODO: Implement

        query = std::make_unique<osiql::Query>(groundTruth);
    }
};

// Performs multiple localization tests on a road that does not conform to OpenDRIVE specifications, but is still a valid OSI input.
TEST_F(LocalizationTest, DISABLED_CurvedRoad)
{
    std::cout << "01\n";
    const osiql::Lane *laneA{query->GetLane(100)};
    const osiql::Lane *laneB{query->GetLane(101)};

    const osiql::ReferenceLine &referenceLine{*query->GetWorld().GetAll<osiql::ReferenceLine>().begin()};
    std::cout << referenceLine << '\n';

    // Point at start of road
    const std::vector<osiql::RoadPoint> a1{query->FindPoints(Vector2d{-95, 0})};
    ASSERT_THAT(a1.size(), Eq(1));
    std::cout << "Length: " << Vector2d(a1[0].latitude, a1[0].longitude).Length() << '\n';
    EXPECT_THAT(a1[0].latitude, DoubleEq(0.0));
    EXPECT_THAT(a1[0].longitude, DoubleEq(-5.0));
    EXPECT_THAT(a1[0].GetLane().GetId(), 100);

    // Using https://people.math.osu.edu/conant.38/bezier/
    // Interpolating the center of the end of lane 100 returns: ((-94.4, 27)+(-84.96, 24.3))/2 = (-89.68, 25.65)

    const Vector2d leftA{laneA->GetRoad().referenceLine.GetXY({osiql::GetLatitude<osiql::Direction::Upstream>(*laneA), 0.0})};
    const Vector2d rightA{laneA->GetRoad().referenceLine.GetXY({osiql::GetLatitude<osiql::Direction::Upstream>(*laneA), -10.0})};
    const Vector2d globalA{laneA->GetRoad().referenceLine.GetXY({osiql::GetLatitude<osiql::Direction::Upstream>(*laneA), -5.0})};

    const std::vector<osiql::RoadPoint> a2{query->FindPoints(globalA)};
    for (auto item : a2)
    {
        std::cout << "Local position: " << item << '\n';
    }

    std::cout << "Start: " << osiql::GetLatitude<osiql::Direction::Upstream>(*laneA) << ", " << osiql::GetLatitude<osiql::Direction::Downstream>(*laneB) << '\n'
              << "Left: " << Vector2d(laneA->GetBoundaryPoints<osiql::Side::Left>().back())
              << ", " << Vector2d(laneB->GetBoundaryPoints<osiql::Side::Left>().back()) << '\n'
              << "Right: " << Vector2d(laneA->GetBoundaryPoints<osiql::Side::Right>().back())
              << ", " << Vector2d(laneB->GetBoundaryPoints<osiql::Side::Right>().back()) << '\n'
              << "Global: " << globalA << ", " << laneB->GetRoad().referenceLine.GetXY({osiql::GetLatitude<osiql::Direction::Downstream>(*laneB), -5.0}) << '\n';
    std::cout << '\n';
    const auto &left{laneB->GetBoundaryPoints<osiql::Side::Left>()};
    for (const auto &point : left)
    {
        std::cout << point << ", ";
    }
    std::cout << '\n';
    const auto &right{laneB->GetBoundaryPoints<osiql::Side::Right>()};
    for (const auto &point : right)
    {
        std::cout << point << ", ";
    }
    std::cout << '\n';
    // const osiql::LaneBoundary::Point &l{[0]};
    // const osiql::LaneBoundary::Point &r{[0]};
    // std::cout << "Left: " << l << '\n';
    // std::cout << "Right: " << r << '\n';
    ASSERT_THAT(a2, SizeIs(2));

    EXPECT_THAT(a2[0].latitude, DoubleEq(osiql::GetLatitude<osiql::Direction::Upstream>(*laneA)));
    EXPECT_THAT(a2[0].longitude, DoubleEq(-5.0));
    EXPECT_THAT(a2[0].GetLane().GetId(), 100);
    EXPECT_THAT(a2[1].latitude, DoubleEq(osiql::GetLatitude<osiql::Direction::Upstream>(*laneA)));
    EXPECT_THAT(a2[1].longitude, DoubleEq(-5.0));
    EXPECT_THAT(a2[1].GetLane().GetId(), 101);

    // Identical point at the start of the prior lane's successor lane
    const Vector2d globalB{laneB->GetRoad().referenceLine.GetXY({osiql::GetLatitude<osiql::Direction::Downstream>(*laneB), -5.0})};
    const auto b1{query->FindPoints(globalB)};

    // const auto b{query->FindPoint<Road>s({95, 0})};

    // laneA->GetXY(laneA->GetEnd(), -5.0)

    // const osiql::RoadPoint startPoint{0.0, -5.0, query->GetLane(100)};
    // const osiql::Lane *endLane{query->GetLane(101)};
    // const osiql::RoadPoint endPoint{endLane->GetEnd() * 0.5, -5.0, endLane};
    // const auto path{startPoint.GetShortestRoute(endPoint)};
    // ASSERT_EQ(path.size(), 2);
}
