/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>
//
#include "OsiQueryLibrary/osiql.h"
#include "util/GroundTruth.h"

using ::testing::_;
using ::testing::DoubleEq;
using ::testing::ElementsAre;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

class TAxisYawTests : public ::testing::Test
{
public:
    TAxisYawTests()
    {
        using Boundary = std::vector<osiql::Vector2d>;
        using BoundaryChain = std::vector<Boundary>;
        using Road = std::vector<BoundaryChain>;
        const std::vector<Road> roads{
            Road{
                BoundaryChain{Boundary{{0.0, -6.0}, {6.0, 0.0}}},
                BoundaryChain{Boundary{{0.0, -4.0}, {4.0, 0.0}}},
                BoundaryChain{Boundary{{0.0, -2.0}, {2.0, 0.0}}} //
            },
            Road{
                BoundaryChain{Boundary{{6.0, 0.0}, {3.0, 3.0}}},
                BoundaryChain{Boundary{{4.0, 0.0}, {2.0, 2.0}}},
                BoundaryChain{Boundary{{2.0, 0.0}, {1.0, 1.0}}} //
            },
            Road{
                BoundaryChain{Boundary{{3.0, 3.0}, {0.0, 6.0}, {-6.0, 0.0}}},
                BoundaryChain{Boundary{{2.0, 2.0}, {0.0, 4.0}, {-4.0, 0.0}}},
                BoundaryChain{Boundary{{1.0, 1.0}, {0.0, 2.0}, {-2.0, 0.0}}} //
            },
            Road{
                BoundaryChain{Boundary{{-3.0, -1.0}, {-3.0, -3.0}}},
                BoundaryChain{Boundary{{-2.0, -1.0}, {-1.0, -3.0}}} //
            },
            // Road{
            //     BoundaryChain{Boundary{{-6.0, -1.0}, {-6.0, -5.0}}},
            //     BoundaryChain{Boundary{{-4.0, -3.0}, {-2.0, -3.0}}} //
            // },
            Road{
                BoundaryChain{Boundary{{11.0, -1.0}, {9.0, -1.0}, {9.0, 1.0}, {11.0, 1.0}}},
                BoundaryChain{Boundary{{11.0, -3.0}, {7.0, -3.0}, {7.0, 3.0}, {11.0, 3.0}}} //
            }                                                                               //
        };
        std::vector<std::vector<std::vector<double>>> lanes{
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}, std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}, std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}, std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}} //
        };
        for (size_t i{0u}; i < roads.size(); ++i)
        {
            groundTruth.AddRoad(roads[i], roads[i].size() >> 1, lanes[i]);
        }

        groundTruth.roads[0].referenceLine->mutable_poly_line()->begin()->set_t_axis_yaw(M_PI_2);
        groundTruth.roads[0].referenceLine->mutable_poly_line()->rbegin()->set_t_axis_yaw(M_PI);

        groundTruth.roads[1].referenceLine->mutable_poly_line()->begin()->set_t_axis_yaw(M_PI);
        groundTruth.roads[1].referenceLine->mutable_poly_line()->rbegin()->set_t_axis_yaw(5.0 * M_PI_4);

        groundTruth.roads[2].referenceLine->mutable_poly_line()->begin()->set_t_axis_yaw(5.0 * M_PI_4);
        groundTruth.roads[2].referenceLine->mutable_poly_line()->rbegin()->set_t_axis_yaw(0.0);

        groundTruth.roads[3].referenceLine->mutable_poly_line()->begin()->set_t_axis_yaw(M_PI);
        groundTruth.roads[3].referenceLine->mutable_poly_line()->rbegin()->set_t_axis_yaw(M_PI);

        // groundTruth.roads[4].referenceLine->mutable_poly_line()->begin()->set_t_axis_yaw(3.0 * M_PI_4);
        // groundTruth.roads[4].referenceLine->mutable_poly_line()->rbegin()->set_t_axis_yaw(5.0 * M_PI_4);

        query = std::make_unique<osiql::Query>(groundTruth.handle);
    }

    std::unique_ptr<osiql::Query> query;
    osiql::test::GroundTruth groundTruth;
};

TEST_F(TAxisYawTests, Localizations)
{
    using namespace osiql;
    { // 1
        const Vector2d globalPoint{0.0, -4.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(2));
        const Coordinates<Road> localCoordinates{0.0, 0.0};
        EXPECT_EQ(points[0], localCoordinates);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 2
        const Vector2d globalPoint{1.0, -1.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(1));
        Coordinates<Road> localCoordinates{2.0 * sqrt(2.0), sqrt(2.0)};
        EXPECT_DOUBLE_EQ(points[0].latitude, localCoordinates.latitude);
        EXPECT_DOUBLE_EQ(points[0].longitude, localCoordinates.longitude);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 3
        const Vector2d globalPoint{4.0, 0.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(4));
        Coordinates<Road> localCoordinates{4.0 * sqrt(2.0), 0.0};
        EXPECT_EQ(points[0], localCoordinates);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 4
        const Vector2d globalPoint{4.0, 4.0};
        auto points{query->FindPoints(globalPoint)};
        EXPECT_THAT(points, IsEmpty());
        RoadPoint localCoordinates{*query->GetLane(12), Coordinates<Road>{2.0 * sqrt(2.0), -2.0 * sqrt(2.0)}};
        EXPECT_EQ(localCoordinates.GetXY(), globalPoint);
    }
    { // 5
        const Vector2d globalPoint{0.0, 5.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(1));
        Coordinates<Road> localCoordinates{2.0 * sqrt(2.0), -1.0};
        EXPECT_EQ(points[0], localCoordinates);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    // { // 6
    //     const Vector2d globalPoint{-5.0, -4.0};
    //     auto points{query->FindPoints(globalPoint)};
    //     ASSERT_THAT(points, SizeIs(1));
    //     std::cout << "Mystery point: " << points[0] << '\n';
    // }
    { // 7
        const Vector2d globalPoint{-2.5, -2.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(1));
        Coordinates<Road> localCoordinates{0.5 * sqrt(5.0), -1.0};
        EXPECT_EQ(points[0], localCoordinates);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 8
        const Vector2d globalPoint{-6.0, 0.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(1));
        Coordinates<Road> localCoordinates{6.0 * sqrt(2.0), -2.0};
        EXPECT_EQ(points[0], localCoordinates);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 9
        const Vector2d globalPoint{9.5, -2.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(1));
        Coordinates<Road> localCoordinates{2.0, -sqrt(5.0) * 0.5};
        EXPECT_DOUBLE_EQ(points[0].latitude, localCoordinates.latitude);
        EXPECT_DOUBLE_EQ(points[0].longitude, localCoordinates.longitude);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 10
        const Vector2d globalPoint{8.0, -2.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(1));
        Coordinates<Road> localCoordinates{4.0, -sqrt(2.0)};
        EXPECT_DOUBLE_EQ(points[0].latitude, localCoordinates.latitude);
        EXPECT_NEAR(points[0].longitude, localCoordinates.longitude, 1e-4);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 11
        const Vector2d globalPoint{8.0, 0.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(1));
        Coordinates<Road> localCoordinates{7.0, -1.0};
        EXPECT_EQ(points[0], localCoordinates);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 12
        const Vector2d globalPoint{9.0, -1.0};
        auto points{query->FindPoints(globalPoint)};
        ASSERT_THAT(points, SizeIs(1));
        Coordinates<Road> localCoordinates{4.0, -2.0 * sqrt(2.0)};
        EXPECT_NEAR(points[0].latitude, localCoordinates.latitude, 1e-4);
        EXPECT_NEAR(points[0].longitude, localCoordinates.longitude, 1e-4);
        EXPECT_EQ(points[0].GetXY(), globalPoint);
    }
    { // 13
        const Vector2d globalPoint{-1.0, -5.0};
        const Road &road{query->GetWorld().GetAll<Road>().front()};
        const Coordinates<Road> localCoordinates{road.referenceLine.GetCoordinates(globalPoint)};
        EXPECT_NEAR(localCoordinates.latitude, -sqrt(2.0), 1e-4);
        EXPECT_NEAR(localCoordinates.longitude, 0.0, 1e-4);
        EXPECT_EQ(road.referenceLine.GetXY(localCoordinates), globalPoint);
    }
    { // 14
        const Vector2d globalPoint{-7, -1.0};
        const Road &road{query->GetWorld().GetAll<Road>()[2]};
        const Coordinates<Road> localCoordinates{road.referenceLine.GetCoordinates(globalPoint)};
        const Vector2d intersection{Line{globalPoint, {0.0, 0.0}}.GetIntersection({{0.0, 4.0}, {-4.0, 0.0}}).value()};
        const double expectedLatitude{6.0 * sqrt(2.0) + (intersection - Vector2d{-4.0, 0.0}).Length()};
        const double expectedLongitude{-(globalPoint - intersection).Length()};
        EXPECT_NEAR(localCoordinates.latitude, expectedLatitude, 1e-4);
        EXPECT_NEAR(localCoordinates.longitude, expectedLongitude, 1e-4);
        EXPECT_EQ(road.referenceLine.GetXY(localCoordinates), globalPoint);
    }
}
