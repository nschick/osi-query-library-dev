/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OsiQueryLibrary/Object/MovingObject.h"
#include "OsiQueryLibrary/Point/Vector2d.h"
#include "OsiQueryLibrary/Point/Vector3d.h"

using ::testing::Eq;

using osiql::Vector2d;
using osiql::Vector3d;

class MovingObjectTest : public ::testing::Test
{
public:
    MovingObjectTest() :
        object(handle)
    {
        SetSpin(0.0);
    }

    void SetPosition(const osiql::Vector3d &position)
    {
        handle.mutable_base()->mutable_position()->set_x(position.x);
        handle.mutable_base()->mutable_position()->set_y(position.y);
        handle.mutable_base()->mutable_position()->set_z(position.z);
        object = handle;
    }

    void SetVelocity(const osiql::Vector3d &velocity)
    {
        handle.mutable_base()->mutable_velocity()->set_x(velocity.x);
        handle.mutable_base()->mutable_velocity()->set_y(velocity.y);
        handle.mutable_base()->mutable_velocity()->set_z(velocity.z);
        object = handle;
    }

    void SetRotation(const osiql::Vector3d &rotation)
    {
        handle.mutable_base()->mutable_orientation()->set_roll(rotation.x);
        handle.mutable_base()->mutable_orientation()->set_pitch(rotation.y);
        handle.mutable_base()->mutable_orientation()->set_yaw(rotation.z);
        object = handle;
    }

    void SetRotation(double yaw)
    {
        handle.mutable_base()->mutable_orientation()->set_roll(0.0);
        handle.mutable_base()->mutable_orientation()->set_pitch(0.0);
        handle.mutable_base()->mutable_orientation()->set_yaw(yaw);
        object = handle;
    }

    void SetSpin(const osiql::Vector3d &spin)
    {
        handle.mutable_base()->mutable_orientation_rate()->set_roll(spin.x);
        handle.mutable_base()->mutable_orientation_rate()->set_pitch(spin.y);
        handle.mutable_base()->mutable_orientation_rate()->set_yaw(spin.z);
        object = handle;
    }

    void SetSpin(double spin)
    {
        handle.mutable_base()->mutable_orientation_rate()->set_roll(0.0);
        handle.mutable_base()->mutable_orientation_rate()->set_pitch(0.0);
        handle.mutable_base()->mutable_orientation_rate()->set_yaw(spin);
        object = handle;
    }

    osi3::MovingObject handle;
    osiql::MovingObject object;
};

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints01)
{
    SetVelocity({3, 4, 0});
    SetRotation(M_PI_2);
    SetSpin(0.0);
    EXPECT_EQ(object.GetVelocity(Vector3d(1, 0, 0)), Vector3d(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints02)
{
    SetVelocity({3, 4, 0});
    SetRotation(M_PI_2);
    SetSpin(0.0);
    EXPECT_EQ(object.GetVelocity(Vector3d(1, -1, 1)), Vector3d(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints03)
{
    SetVelocity({3, 4, 0});
    SetRotation(M_PI_2);
    SetSpin(0.5);
    EXPECT_EQ(object.GetVelocity(Vector3d(0, 0, 0)), Vector3d(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints04)
{
    SetVelocity({3, 4, 0});
    SetRotation(0.0);
    SetSpin(0.5);
    EXPECT_EQ(object.GetVelocity(Vector3d(1, -2, 3)), Vector3d(4, 4.5, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints05)
{
    SetVelocity({3, 4, 0});
    SetRotation(M_PI_2);
    SetSpin(0.5);
    std::cout << "Spin: " << object.GetSpinMatrix() * Vector3d(1, -2, 3) << '\n';
    EXPECT_EQ(object.GetVelocity(Vector3d(1, -2, 3)), Vector3d(2.5, 5, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints06)
{
    SetVelocity({3, 4, 0});
    SetRotation({0.3, 0.2, 0.1});
    SetSpin(0.0);
    EXPECT_EQ(object.GetVelocity(Vector3d(1, -1, 1)), Vector3d(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints07)
{
    SetVelocity({3, 4, 0});
    SetRotation(M_PI_2);
    SetSpin({0, -0.5, 0});
    EXPECT_EQ(object.GetVelocity(Vector3d(0, 0, 0)), Vector3d(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints08)
{
    SetVelocity({3, 4, 0});
    SetRotation(M_PI_2);
    SetSpin({0, -0.5, 0});
    std::cout << "Spin: " << object.GetSpinMatrix() * Vector3d(1, -2, 3) << '\n';
    EXPECT_EQ(object.GetVelocity(Vector3d(1, -2, 3)), Vector3d(3, 2.5, 0.5));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints09)
{
    SetVelocity({3, 4, 0});
    SetRotation({0, -M_PI_2, M_PI_2});
    SetSpin({0, -0.5, 0});
    std::cout << "Spin: " << object.GetSpinMatrix() * Vector3d(1, -2, 3) << '\n';
    EXPECT_EQ(object.GetVelocity(Vector3d(1, -2, 3)), Vector3d(3, 3.5, -1.5));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints10)
{
    SetVelocity({3, 4, 0});
    SetRotation(M_PI_2);
    SetSpin({0.5, 0, 0});
    EXPECT_EQ(object.GetVelocity(Vector3d(0, 0, 0)), Vector3d(3, 4, 0));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints11)
{
    SetVelocity({3, 4, 0});
    SetRotation(M_PI_2);
    SetSpin({0.5, 0, 0});
    std::cout << "Spin: " << object.GetSpinMatrix() * Vector3d(1, -2, 3) << '\n';
    EXPECT_EQ(object.GetVelocity(Vector3d(1, -2, 3)), Vector3d(4.5, 4, -1));
}

TEST_F(MovingObjectTest, VelocityAtDifferentLocalPoints12)
{
    SetVelocity({3, 4, 0});
    SetRotation({-M_PI_2, 0, M_PI_2});
    SetSpin({0.5, 0, 0});
    std::cout << "Spin: " << object.GetSpinMatrix() * Vector3d(1, -2, 3) << '\n';
    EXPECT_EQ(object.GetVelocity(Vector3d(1, -2, 3)), Vector3d(4, 4, 1.5));
}
