/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OsiQueryLibrary/Point/Vector2d.h"
#include "OsiQueryLibrary/Point/Vector3d.h"
#include "OsiQueryLibrary/Types/Geometry.h"
#include "OsiQueryLibrary/Types/Matrix.h"
#include "OsiQueryLibrary/osiql.h"

using namespace osiql;

TEST(MatrixTest, Default_Initialization_Creates_Identity_Matrix)
{
    EXPECT_EQ(Matrix<2>(), Matrix<2>({Row<2>{1.0, 0.0},
                                      Row<2>{0.0, 1.0}}));
    EXPECT_EQ(Matrix<3>(), Matrix<3>({Row<3>{1.0, 0.0, 0.0},
                                      Row<3>{0.0, 1.0, 0.0},
                                      Row<3>{0.0, 0.0, 1.0}}));
    EXPECT_EQ(Matrix<4>(), Matrix<4>({Row<4>{1.0, 0.0, 0.0, 0.0},
                                      Row<4>{0.0, 1.0, 0.0, 0.0},
                                      Row<4>{0.0, 0.0, 1.0, 0.0},
                                      Row<4>{0.0, 0.0, 0.0, 1.0}}));
}

TEST(MatrixTest, Matrix_Addition_And_Subtraction)
{
    Matrix<3> A({Row<3>{10.0, 20.0, 10.0},
                 Row<3>{4.0, 5.0, 6.0},
                 Row<3>{2.0, 3.0, 5.0}});
    Matrix<3> B({Row<3>{3.0, 2.0, 4.0},
                 Row<3>{3.0, 3.0, 9.0},
                 Row<3>{4.0, 4.0, 2.0}});
    Matrix<3> C({Row<3>{13.0, 22.0, 14.0},
                 Row<3>{7.0, 8.0, 15.0},
                 Row<3>{6.0, 7.0, 7.0}});
    EXPECT_EQ(A + B, C);
    EXPECT_EQ(C - B, A);
}

TEST(MatrixTest, Matrix_Multiplication)
{
    Matrix<3> A({Row<3>{10.0, 20.0, 10.0},
                 Row<3>{4.0, 5.0, 6.0},
                 Row<3>{2.0, 3.0, 5.0}});
    Matrix<3> B({Row<3>{3.0, 2.0, 4.0},
                 Row<3>{3.0, 3.0, 9.0},
                 Row<3>{4.0, 4.0, 2.0}});
    Matrix<3> C({Row<3>{130.0, 120.0, 240.0},
                 Row<3>{51.0, 47.0, 73.0},
                 Row<3>{35.0, 33.0, 45.0}});
    EXPECT_EQ(A * B, C);
}

TEST(MatrixTest, Scalar_Multiplication_And_Division)
{
    Matrix<3> A({Row<3>{10.0, 20.0, 10.0},
                 Row<3>{4.0, 5.0, 6.0},
                 Row<3>{2.0, 3.0, 5.0}});
    EXPECT_EQ(A * 2.0, Matrix<3>({Row<3>{20.0, 40.0, 20.0},
                                  Row<3>{8.0, 10.0, 12.0},
                                  Row<3>{4.0, 6.0, 10.0}}));
    EXPECT_EQ(A / 2.0, Matrix<3>({Row<3>{5.0, 10.0, 5.0},
                                  Row<3>{2.0, 2.5, 3.0},
                                  Row<3>{1.0, 1.5, 2.5}}));
}

TEST(MatrixTest, Matrix_Vector_Multiplication)
{
    Matrix<4> A;
    Vector2d v{1.0, 2.0};
    EXPECT_EQ(A * v, Vector2d(1.0, 2.0));
}

TEST(MatrixTest, Submatrices)
{
    Matrix<3> A({Row<3>{6.0, 1.0, 1.0},
                 Row<3>{4.0, -2.0, 5.0},
                 Row<3>{2.0, 8.0, 7.0}});

    // gtest can't handle functions with more than one template parameter inside EXPECT_EQ, so we assign them outside:
    Matrix<2> A_11{Submatrix<1, 1>(A)};
    Matrix<2> A_12{Submatrix<1, 2>(A)};
    Matrix<2> A_13{Submatrix<1, 3>(A)};
    Matrix<2> A_21{Submatrix<2, 1>(A)};
    Matrix<2> A_22{Submatrix<2, 2>(A)};
    Matrix<2> A_23{Submatrix<2, 3>(A)};
    Matrix<2> A_31{Submatrix<3, 1>(A)};
    Matrix<2> A_32{Submatrix<3, 2>(A)};
    Matrix<2> A_33{Submatrix<3, 3>(A)};

    EXPECT_EQ(A_11, Matrix<2>({Row<2>{-2.0, 5.0}, Row<2>{8.0, 7.0}}));
    EXPECT_EQ(A_12, Matrix<2>({Row<2>{4.0, 5.0}, Row<2>{2.0, 7.0}}));
    EXPECT_EQ(A_13, Matrix<2>({Row<2>{4.0, -2.0}, Row<2>{2.0, 8.0}}));
    EXPECT_EQ(A_21, Matrix<2>({Row<2>{1.0, 1.0}, Row<2>{8.0, 7.0}}));
    EXPECT_EQ(A_22, Matrix<2>({Row<2>{6.0, 1.0}, Row<2>{2.0, 7.0}}));
    EXPECT_EQ(A_23, Matrix<2>({Row<2>{6.0, 1.0}, Row<2>{2.0, 8.0}}));
    EXPECT_EQ(A_31, Matrix<2>({Row<2>{1.0, 1.0}, Row<2>{-2.0, 5.0}}));
    EXPECT_EQ(A_32, Matrix<2>({Row<2>{6.0, 1.0}, Row<2>{4.0, 5.0}}));
    EXPECT_EQ(A_33, Matrix<2>({Row<2>{6.0, 1.0}, Row<2>{4.0, -2.0}}));
}

TEST(MatrixTest, Determinant)
{
    Matrix<3> A({Row<3>{6.0, 1.0, 1.0},
                 Row<3>{4.0, -2.0, 5.0},
                 Row<3>{2.0, 8.0, 7.0}});
    EXPECT_EQ(Det(A), -306.0);
}

TEST(MatrixTest, Cofactor_Matrix)
{
    Matrix<3> A({Row<3>{1.0, 2.0, 3.0},
                 Row<3>{0.0, 4.0, 5.0},
                 Row<3>{1.0, 0.0, 6.0}});

    EXPECT_EQ(Cof(A), Matrix<3>({Row<3>{24.0, 5.0, -4.0},
                                 Row<3>{-12.0, 3.0, 2.0},
                                 Row<3>{-2.0, -5.0, 4.0}}));
}

TEST(MatrixTest, Transposed_Matrix)
{
    Matrix<3> A({Row<3>{1.0, 2.0, 3.0},
                 Row<3>{0.0, 4.0, 5.0},
                 Row<3>{1.0, 0.0, 6.0}});

    EXPECT_EQ(Transpose(A), Matrix<3>({Row<3>{1.0, 0.0, 1.0},
                                       Row<3>{2.0, 4.0, 0.0},
                                       Row<3>{3.0, 5.0, 6.0}}));
}

TEST(MatrixTest, Inverted_Matrix)
{
    Matrix<3> A({Row<3>{1.0, 2.0, -1.0},
                 Row<3>{2.0, 1.0, 2.0},
                 Row<3>{-1.0, 2.0, 1.0}});

    EXPECT_EQ(Inv(A), Matrix<3>({Row<3>{3.0 / 16.0, 1.0 / 4.0, -5.0 / 16.0},
                                 Row<3>{1.0 / 4.0, 0.0, 1.0 / 4.0},
                                 Row<3>{-5.0 / 16.0, 1.0 / 4.0, 3.0 / 16.0}}));
}

TEST(MatrixTest, Translation)
{
    Matrix<3> A{TranslationMatrix(Vector<double, 2>{5.0, 3.0})};
    Matrix<3> B{};
    B.Translate({5.0, 3.0});

    EXPECT_EQ(A, B);
    EXPECT_EQ(A * Vector2d(0.0, 0.0), Vector2d(5.0, 3.0));
    EXPECT_EQ(A * Vector2d(5.0, 3.0), Vector2d(10.0, 6.0));
}

TEST(MatrixTest, Rotation2D)
{
    // Rotate around origin
    Matrix<3> A{RotationMatrix(M_PI_2)};
    EXPECT_EQ(A * Vector2d(1.0, 0.0), Vector2d(0.0, 1.0));
    // Rotate around given point
    Matrix<3> B{RotationMatrix(M_PI_2, Vector2d{1.0, 0.0})};
    EXPECT_EQ(B * Vector2d(0.0, 0.0), Vector2d(1.0, -1.0));
}

TEST(MatrixTest, Rotation3D)
{
    // Rotate around x-axis
    Matrix<4> X{RotationMatrix(M_PI_2, Vector3d{1.0, 0.0, 0.0})};

    EXPECT_EQ(X * Vector3d(1.0, 0.0, 0.0), Vector3d(1.0, 0.0, 0.0));
    EXPECT_EQ(X * Vector3d(1.0, 1.0, 0.0), Vector3d(1.0, 0.0, 1.0));

    // Rotate around y-axis
    Matrix<4> Y{RotationMatrix(M_PI_2, Vector3d{0.0, 1.0, 0.0})};

    EXPECT_EQ(Y * Vector3d(0.0, 1.0, 0.0), Vector3d(0.0, 1.0, 0.0));
    EXPECT_EQ(Y * Vector3d(1.0, 1.0, 0.0), Vector3d(0.0, 1.0, -1.0));

    // Rotate around z-axis
    Matrix<4> Z{RotationMatrix(M_PI_2, Vector3d{0.0, 0.0, 1.0})};

    EXPECT_EQ(Z * Vector3d(0.0, 0.0, 1.0), Vector3d(0.0, 0.0, 1.0));
    EXPECT_EQ(Z * Vector3d(1.0, 1.0, 1.0), Vector3d(-1.0, 1.0, 1.0));

    // Rotate around arbitrary axis (through origin)
    Matrix<4> A{RotationMatrix(M_PI, Vector3d{1.0, 1.0, 1.0})};

    EXPECT_EQ(A * Vector3d(1.0, 1.0, 1.0), Vector3d(1.0, 1.0, 1.0));

    // Combined rotations
    const Vector3d point{1.0, 0.0, 0.0};
    const Vector3d rotation{0.0, 0.0, M_PI};

    EXPECT_EQ(RotationMatrix(rotation) * point, Vector3d(-1.0, 0.0, 0.0));
    EXPECT_EQ(RotationMatrix(rotation * 0.5) * point, Vector3d(0.0, 1.0, 0.0));
    EXPECT_EQ(RotationMatrix(rotation * 0.5) * point, Vector3d(0.0, 1.0, 0.0));

    // FIXME: If a test fails, the print output of Vector3d is incorrect. Why?
}

TEST(MatrixTest, Transformation_On_Flat_Surface)
{
    const Matrix<4> velocity{TranslationMatrix(Vector3d(4, 0, 0))};   // Moving forwards 5m/s
    const Matrix<3> rotation{RotationMatrix(Vector3d(0, 0, M_PI_2))}; // facing y-axis horizon
    const Matrix<3> spin{RotationMatrix(Vector3d(0, 0, M_PI_2))};     // Spinning to the left at 0.25 rps
    const Matrix<4> transformation{Matrix<4>(rotation) * velocity * Matrix<4>(spin)};
    // Object center:
    EXPECT_EQ(transformation * Vector3d(0, 0, 0), Vector3d(0, 4, 0));
    // Object corner:
    //  spin*(1, 1) = (-1, 1)
    //  velocity*(-1, 1) = (3, 1)
    //  rotation*(3, 1) = (-1, 3)
    EXPECT_EQ(transformation * Vector3d(1, 1, 0), Vector3d(-1, 3, 0));
}

TEST(MathTest, LineIntersections)
{
    // Overlapping lines have no intersection
    {
        auto overlappingIntersection{Line{{0.0, 0.0}, {0.0, 1.0}}.GetIntersection(Line{{0.0, 0.0}, {0.0, 1.0}})};
        EXPECT_FALSE(overlappingIntersection.has_value());
        overlappingIntersection = Line{{0.0, 0.0}, {0.0, -1.0}}.GetIntersection(Line{{0.0, 0.0}, {0.0, -1.0}});
        EXPECT_FALSE(overlappingIntersection.has_value());
        overlappingIntersection = Line{{0.0, 0.0}, {0.0, 1.0}}.GetIntersection(Line{{0.0, 1.0}, {0.0, 0.0}});
        EXPECT_FALSE(overlappingIntersection.has_value());
        overlappingIntersection = Line{{0.0, 0.0}, {0.0, -1.0}}.GetIntersection(Line{{0.0, -1.0}, {0.0, 0.0}});
        EXPECT_FALSE(overlappingIntersection.has_value());
    }
    // Parallel lines have no intersection
    {
        auto parallelIntersection{Line{{0.0, 0.0}, {0.0, 1.0}}.GetIntersection({{1.0, 0.0}, {1.0, 1.0}})};
        EXPECT_FALSE(parallelIntersection.has_value());
    }
    // Normal intersection:
    {
        auto intersection{Line{Vector2d{-4.0, 0.0}, Vector2d{0.0, 3.0}}.GetIntersection(Line{Vector2d{2.0, 0.0}, Vector2d{2.0, 1.0}})};
        ASSERT_TRUE(intersection.has_value());
        const Vector2d expectedResult{2.0, 4.5};
        EXPECT_EQ(intersection.value(), expectedResult);
    }
}

TEST(MathTest, HasIntersection_NotIntersecting_Returns_False)
{
    const Polygon a{Vector2d{1.0, 1.0}, {1.0, 2.0}, {0.0, 2.0}, {0.0, 1.0}};
    const Polygon b{Vector2d{1.0, -1.0}, {1.0, 0.0}, {0.0, 0.0}, {0.0, -1.0}};
    EXPECT_FALSE(osiql::detail::HasIntersections(a, b));
    EXPECT_FALSE(osiql::detail::HasIntersections(b, a));
}

TEST(MathTest, AngleClamping)
{
    std::array<double, 4> results{0.0, M_PI_2, M_PI, -M_PI_2};
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-12 * M_PI_2), 0.0, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-8 * M_PI_2), 0.0, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-4 * M_PI_2), 0.0, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(0.0), 0.0, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(4 * M_PI_2), 0.0, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(8 * M_PI_2), 0.0, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(12 * M_PI_2), 0.0, 1e-5);

    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-11 * M_PI_2), M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-7 * M_PI_2), M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-3 * M_PI_2), M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(1 * M_PI_2), M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(5 * M_PI_2), M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(9 * M_PI_2), M_PI_2, 1e-5);

    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-10 * M_PI_2), M_PI, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-6 * M_PI_2), M_PI, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-2 * M_PI_2), M_PI, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(2 * M_PI_2), M_PI, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(6 * M_PI_2), M_PI, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(10 * M_PI_2), M_PI, 1e-5);

    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-9 * M_PI_2), -M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-5 * M_PI_2), -M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(-1 * M_PI_2), -M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(3 * M_PI_2), -M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(7 * M_PI_2), -M_PI_2, 1e-5);
    EXPECT_NEAR(osiql::detail::SetAngleToValidRange(11 * M_PI_2), -M_PI_2, 1e-5);
}

// TODO: TEST(MatrixTest, Transformation_On_Incline)
