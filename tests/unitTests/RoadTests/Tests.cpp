/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OsiQueryLibrary/osiql.h"
#include "util/GroundTruth.h"

using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::ReturnRef;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

struct RoadTest : public ::testing::Test
{
    RoadTest()
    {
        using Boundary = std::vector<osiql::Vector2d>;
        using BoundaryChain = std::vector<Boundary>;
        using Road = std::vector<BoundaryChain>;
        const std::vector<Road> roads{
            Road{
                BoundaryChain{Boundary{{0.0, -1.0}, {1.0, -1.0}}, Boundary{{1.0, -1.0}, {2.0, -1.0}}},
                BoundaryChain{Boundary{{0.0, 0.0}, {1.0, 0.0}}, Boundary{{1.0, 0.0}, {2.0, 0.0}}},
                BoundaryChain{Boundary{{0.0, 1.0}, {1.0, 1.0}}, Boundary{{1.0, 1.0}, {2.0, 1.0}}},
            },
            Road{
                BoundaryChain{Boundary{{2.0, -1.0}, {4.0, -1.0}}},
                BoundaryChain{Boundary{{2.0, 0.0}, {4.0, 0.0}}},
                BoundaryChain{Boundary{{2.0, 1.0}, {4.0, 1.0}}},
            } //
        };
        std::vector<std::vector<std::vector<double>>> lanes{
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}, std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 1.0}, std::vector<double>{0.0, 1.0}},
            std::vector<std::vector<double>>{std::vector<double>{0.0, 2.0}},
        };
        for (size_t i{0u}; i < roads.size(); ++i)
        {
            ground.AddRoad(roads[i], roads[i].size() >> 1, lanes[i]);
        }

        // Connect successors
        auto &road1{ground.roads[0]};
        auto &endOfRoad1{road1.laneRows.back()};
        auto &road2{ground.roads[1]};
        auto &startOfRoad2{road2.laneRows.front()};

        for (size_t i{0u}; i < endOfRoad1.size(); ++i)
        {
            osi3::LogicalLane_LaneConnection *connection{endOfRoad1[i]->add_successor_lane()};
            connection->set_at_begin_of_other_lane(true);
            connection->mutable_other_lane_id()->set_value(startOfRoad2[i]->id().value());
            connection = startOfRoad2[i]->add_predecessor_lane();
            connection->set_at_begin_of_other_lane(false);
            connection->mutable_other_lane_id()->set_value(endOfRoad1[i]->id().value());
        }
        const std::array<osiql::Vector2d, 11> positions{
            osiql::Vector2d{0.5, -1.0},
            osiql::Vector2d{1.25, -0.5},
            osiql::Vector2d{2.0, -1.0},
            osiql::Vector2d{4.25, -1.25},
            osiql::Vector2d{4.5, -0.5},
            osiql::Vector2d{4.25, 0.5},
            osiql::Vector2d{3.0, 1.5},
            osiql::Vector2d{2.0, 1.25},
            osiql::Vector2d{1.0, 1.25},
            osiql::Vector2d{1.75, 0.5},
            osiql::Vector2d{3.0, 0.0},
        };
        std::array<osi3::MovingObject *, 11> objects;
        for (size_t i{0u}; i < objects.size(); ++i)
        {
            objects[i] = ground.handle.add_moving_object();
            objects[i]->mutable_id()->set_value(1000 + i);
            osi3::Vector3d *position{objects[i]->mutable_base()->mutable_position()};
            position->set_x(positions[i].x);
            position->set_y(positions[i].y);
            osi3::Dimension3d *dimensions{objects[i]->mutable_base()->mutable_dimension()};
            dimensions->set_width(0.5);
            dimensions->set_length(0.5);
        }
        objects[7]->mutable_base()->mutable_orientation()->set_yaw(M_PI_2);

        std::cout << "Number of first road's leftmost lanes: " << ground.roads.front().laneRows.back().size() << '\n';
        std::cout << "Total number of lanes: " << ground.handle.logical_lane_size() << " (should be 6)\n";

        query = std::make_unique<osiql::Query>(ground.handle);
    }

    osiql::test::GroundTruth ground;
    std::unique_ptr<osiql::Query> query;
};

TEST_F(RoadTest, GetAll_MovingObjects)
{
    const auto &roads{query->GetWorld().GetAll<osiql::Road>()};
    {
        const auto relations{roads[0].GetAll<osiql::MovingObject, osiql::Side::Right>()};
        ASSERT_THAT(relations, SizeIs(3));
        EXPECT_THAT(relations[0].GetEntity().GetId(), Eq(1000));
        EXPECT_THAT(relations[1].GetEntity().GetId(), Eq(1001));
        EXPECT_THAT(relations[2].GetEntity().GetId(), Eq(1002));
    }
    {
        const auto relations{roads[0].GetAll<osiql::MovingObject, osiql::Side::Left>()};
        ASSERT_THAT(relations, SizeIs(1));
        EXPECT_THAT(relations[0].GetEntity().GetId(), Eq(1009));
    }
    {
        const auto relations{roads[1].GetAll<osiql::MovingObject, osiql::Side::Right>()};
        ASSERT_THAT(relations, SizeIs(2));
        EXPECT_THAT(relations[0].GetEntity().GetId(), Eq(1002));
        EXPECT_THAT(relations[1].GetEntity().GetId(), Eq(1010));
    }
    {
        const auto relations{roads[1].GetAll<osiql::MovingObject, osiql::Side::Left>()};
        ASSERT_THAT(relations, SizeIs(1));
        EXPECT_THAT(relations[0].GetEntity().GetId(), Eq(1010));
    }
}
