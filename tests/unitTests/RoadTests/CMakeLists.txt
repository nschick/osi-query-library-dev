################################################################################
# Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME Road_Tests)
set(COMPONENT_SOURCE_DIR ${CMAKE_SOURCE_DIR}/src)
set(COMPONENT_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include)
set(COMPONENT_TEST_DIR ${CMAKE_SOURCE_DIR}/tests)

find_package(GTest REQUIRED)

add_executable(${COMPONENT_TEST_NAME} EXCLUDE_FROM_ALL
${COMPONENT_TEST_DIR}/util/GroundTruth.h
${COMPONENT_TEST_DIR}/util/GroundTruth.cpp
# Tests
${COMPONENT_TEST_DIR}/gtest/unitTestMain.cpp
${COMPONENT_TEST_DIR}/gtest/mainHelper.cpp

${COMPONENT_TEST_DIR}/unitTests/RoadTests/Tests.cpp
)

target_compile_features(${COMPONENT_TEST_NAME} PRIVATE cxx_std_17)

target_include_directories(${COMPONENT_TEST_NAME}
  PUBLIC
    ${COMPONENT_INCLUDE_DIR}
    ${COMPONENT_TEST_DIR}
  PUBLIC 
    ${CMAKE_SOURCE_DIR}/tests/gtest
)

target_link_libraries(${COMPONENT_TEST_NAME}
  ${CMAKE_PROJECT_NAME}
  GTest::gtest
  GTest::gmock
  pthread
  open_simulation_interface::open_simulation_interface_pic
)

add_test(NAME ${COMPONENT_TEST_NAME}_build COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} --target ${COMPONENT_TEST_NAME})
add_test(NAME ${COMPONENT_TEST_NAME} COMMAND ${COMPONENT_TEST_NAME} --default-xml)
set_tests_properties(${COMPONENT_TEST_NAME} PROPERTIES DEPENDS ${COMPONENT_TEST_NAME}_build)
