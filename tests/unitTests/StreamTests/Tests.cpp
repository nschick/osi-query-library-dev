/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <array>
#include <numeric>
//
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OsiQueryLibrary/osiql.h"

using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::IsEmpty;
using ::testing::ReturnRef;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

class StreamTest : public ::testing::Test
{
public:
    StreamTest()
    {
        // ----- Lanes begin

        std::array<bool, 9> reversed{false, true, false, true, true, true, true, false, false};
        std::array<std::array<osiql::Vector2d, 4>, 9> points{
            // clang-format off
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{ 0,  0}, osiql::Vector2d{ 0,  2}, osiql::Vector2d{ 8,  4}, osiql::Vector2d{ 8,  6}},
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{ 8,  6}, osiql::Vector2d{ 8,  4}, osiql::Vector2d{ 2,  8}, osiql::Vector2d{ 2,  6}}, // reversed
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{ 8,  4}, osiql::Vector2d{ 8,  6}, osiql::Vector2d{16,  2}, osiql::Vector2d{16,  4}},
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{14, 10}, osiql::Vector2d{14,  8}, osiql::Vector2d{ 8,  6}, osiql::Vector2d{ 8,  4}}, // reversed
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{14, 10}, osiql::Vector2d{14,  8}, osiql::Vector2d{ 8, 12}, osiql::Vector2d{ 8, 10}}, // reversed
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{24,  4}, osiql::Vector2d{24,  2}, osiql::Vector2d{16,  4}, osiql::Vector2d{16,  2}}, // reversed
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{22, 10}, osiql::Vector2d{22,  8}, osiql::Vector2d{16,  4}, osiql::Vector2d{16,  2}}, // reversed
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{14,  8}, osiql::Vector2d{14, 10}, osiql::Vector2d{22,  8}, osiql::Vector2d{22, 10}},
            std::array<osiql::Vector2d, 4>{osiql::Vector2d{22,  8}, osiql::Vector2d{22, 10}, osiql::Vector2d{26,  8}, osiql::Vector2d{26, 10}}
        }; // clang-format on
        std::transform(points.begin(), points.end(), lengths.begin(), [](const auto points) { return (points[2] - points[0]).Length(); });
        std::array<osi3::LogicalLane *, 9> lanes;
        for (size_t i{0u}; i < lanes.size(); ++i)
        {
            osi3::LogicalLane *&lane{lanes[i]};
            lane = groundTruth.add_logical_lane();
            lane->mutable_id()->set_value(i);
            lane->set_type(osi3::LogicalLane_Type_TYPE_NORMAL);
            auto source{lane->add_source_reference()};

            source->add_identifier("Road" + std::to_string(i));
            source->add_identifier("0");  // s of t_road_lanes_laneSection (what does that mean?)
            source->add_identifier("-1"); // id of t_road_lanes_laneSection_left_lane, t_road_lanes_laneSection_right_lane (what does that mean?)

            osi3::ReferenceLine *referenceLine = groundTruth.add_reference_line();
            referenceLine->mutable_id()->set_value(100u + i);
            osi3::ReferenceLine_ReferenceLinePoint *referenceLinePoint = referenceLine->add_poly_line();
            referenceLinePoint->mutable_world_position()->set_x(points[i][reversed[i] ? 0 : 1].x);
            referenceLinePoint->mutable_world_position()->set_y(points[i][reversed[i] ? 0 : 1].y);
            referenceLinePoint->set_s_position(0.0);
            referenceLinePoint = referenceLine->add_poly_line();
            referenceLinePoint->mutable_world_position()->set_x(points[i][reversed[i] ? 2 : 3].x);
            referenceLinePoint->mutable_world_position()->set_y(points[i][reversed[i] ? 2 : 3].y);
            referenceLinePoint->set_s_position(lengths[i]);
            lane->mutable_reference_line_id()->set_value(referenceLine->id().value());

            osi3::LogicalLaneBoundary *boundary = groundTruth.add_logical_lane_boundary();
            boundary->mutable_id()->set_value(200u + i);
            osi3::LogicalLaneBoundary_LogicalBoundaryPoint *boundaryPoint = boundary->add_boundary_line();
            boundaryPoint->mutable_position()->set_x(points[i][reversed[i] ? 1 : 0].x);
            boundaryPoint->mutable_position()->set_y(points[i][reversed[i] ? 1 : 0].y);
            boundaryPoint->set_s_position(0.0);
            boundaryPoint->set_t_position(reversed[i] ? 0.0 : -2.0);
            boundaryPoint = boundary->add_boundary_line();
            boundaryPoint->mutable_position()->set_x(points[i][reversed[i] ? 3 : 2].x);
            boundaryPoint->mutable_position()->set_y(points[i][reversed[i] ? 3 : 2].y);
            boundaryPoint->set_s_position(lengths[i]);
            boundaryPoint->set_t_position(reversed[i] ? 0.0 : -2.0);
            lane->add_right_boundary_id()->set_value(boundary->id().value());

            boundary = groundTruth.add_logical_lane_boundary();
            boundary->mutable_id()->set_value(300u + i);
            boundaryPoint = boundary->add_boundary_line();
            boundaryPoint->mutable_position()->set_x(points[i][reversed[i] ? 0 : 1].x);
            boundaryPoint->mutable_position()->set_y(points[i][reversed[i] ? 0 : 1].y);
            boundaryPoint->set_s_position(0.0);
            boundaryPoint->set_t_position(reversed[i] ? -2.0 : 0.0);
            boundaryPoint = boundary->add_boundary_line();
            boundaryPoint->mutable_position()->set_x(points[i][reversed[i] ? 2 : 3].x);
            boundaryPoint->mutable_position()->set_y(points[i][reversed[i] ? 2 : 3].y);
            boundaryPoint->set_s_position(lengths[i]);
            boundaryPoint->set_t_position(reversed[i] ? -2.0 : 0.0);
            lane->add_left_boundary_id()->set_value(boundary->id().value());

            lane->set_move_direction(reversed[i] ? osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S : osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);
            lane->set_start_s(0.0);
            lane->set_end_s(lengths[i]);
        }

        // Add lane overlaps (with inaccurate or downright false s values)

        auto addOverlap = [&](size_t i, size_t j, bool atStartOfLane) {
            osi3::LogicalLane_LaneRelation *relation = lanes[i]->add_overlapping_lane();
            double iStart = (reversed[i] != atStartOfLane) ? 0.0 : lengths[i] - 0.9;
            double iEnd = iStart + 0.9;
            double jStart = (reversed[j] != atStartOfLane) ? 0.0 : lengths[j] - 0.9;
            double jEnd = jStart + 0.9;
            relation->set_start_s(iStart);
            relation->set_end_s(iEnd);
            relation->set_start_s_other(jStart);
            relation->set_end_s_other(jEnd);
            relation->mutable_other_lane_id()->set_value(lanes[j]->id().value()); // also assigns mirrored overlap
        };
        addOverlap(0, 1, false);
        addOverlap(2, 3, true);
        addOverlap(3, 4, true);
        addOverlap(5, 6, false);
        addOverlap(6, 7, false);

        const auto connectLanes = [&](size_t i, size_t j) {
            osi3::LogicalLane_LaneConnection *connectionA = reversed[i] ? lanes[i]->add_predecessor_lane() : lanes[i]->add_successor_lane();
            connectionA->set_at_begin_of_other_lane(!reversed[j]);
            connectionA->mutable_other_lane_id()->set_value(lanes[j]->id().value()); // assigns mirrored connection as well
            osi3::LogicalLane_LaneConnection *connectionB = reversed[j] ? lanes[j]->add_successor_lane() : lanes[j]->add_predecessor_lane();
            connectionB->set_at_begin_of_other_lane(reversed[i]);
            connectionB->mutable_other_lane_id()->set_value(lanes[i]->id().value());
        };
        // std::array<bool, 9> reversed{false, true, false, true, true, true, true, false, false};
        connectLanes(0, 2);
        connectLanes(0, 3);
        connectLanes(1, 2);
        connectLanes(1, 3);
        connectLanes(2, 5);
        connectLanes(2, 6);
        connectLanes(3, 7);
        connectLanes(4, 7);
        connectLanes(6, 8);
        connectLanes(7, 8);

        // ----- Lanes done
        // ----- Objects begin

        const auto addObject = [&](size_t i) {
            movingObjects.push_back(groundTruth.add_moving_object());
            movingObjects.back()->mutable_id()->set_value(1000u + i);
            movingObjects.back()->set_type(osi3::MovingObject_Type_TYPE_VEHICLE);
            osi3::BaseMoving *base{movingObjects.back()->mutable_base()};
            const osiql::Vector2d P{(points[i][0] + points[i][3]) * 0.5};
            base->mutable_position()->set_x(P.x);
            base->mutable_position()->set_y(P.y);
            base->mutable_dimension()->set_width(1.0);
            base->mutable_dimension()->set_length(2.0);
            base->mutable_orientation()->set_yaw(0.0);
            //base->mutable_orientation()->set_yaw((points[i][0] + points[i][2]).Angle());
            osiql::Container<osi3::Vector2d> *polygon{base->mutable_base_polygon()};
            const std::vector<osiql::Vector2d> shape{{-1.0, -0.5}, {-1.0, 0.5}, {1.0, 0.5}, {1.0, -0.5}};
            for (const osiql::Vector2d &point : shape)
            {
                osi3::Vector2d vertex;
                vertex.set_x(point.x);
                vertex.set_y(point.y);
                polygon->Add(std::move(vertex));
            }
            osi3::LogicalLaneAssignment *assignment{movingObjects.back()->mutable_moving_object_classification()->add_logical_lane_assignment()};
            assignment->mutable_assigned_lane_id()->set_value(lanes[i]->id().value());
            assignment->set_s_position(lengths[i] * 0.5);
            assignment->set_t_position(-1.0);
            assignment->set_angle_to_lane(-((points[i][2] - points[i][0]).Angle()));
        };
        for (size_t i{0u}; i < lanes.size(); ++i)
        {
            addObject(i);
        }
        groundTruth.mutable_host_vehicle_id()->set_value(1000u);

        // osi3::TrafficSign* sign{groundTruth.add_traffic_sign()};
        // sign->mutable_id()->set_value(1000);
        // sign->mutable_main_sign().base()
        // // ----- Objects done
        query = std::make_unique<osiql::Query>(groundTruth);
    }
    std::array<double, 9> lengths;
    osi3::GroundTruth groundTruth{};
    std::vector<osi3::MovingObject *> movingObjects;
    std::unique_ptr<osiql::Query> query;
};

TEST_F(StreamTest, GetShortestRoute_Returns_OnePointPerRoad)
{
    const osiql::RoadPoint startPoint{*query->GetLane(0), osiql::Coordinates<osiql::Road>{1.0, 0.0}};
    const osiql::RoadPoint endPoint{*query->GetLane(8), osiql::Coordinates<osiql::Road>{3.5, 0.0}};
    const auto path{query->GetShortestRoute<osiql::Orientation::Backwards>(startPoint, endPoint)};
    //ASSERT_EQ(path.size(), 4);
}

TEST_F(StreamTest, GetRandomPath)
{
    const osiql::RoadPoint startPoint{*query->GetLane(0), osiql::Coordinates<osiql::Road>{0.0, 0.0}};
    const osiql::Stream stream(startPoint, 26.0);
    const auto rng = [] { return 0.99; };
    const std::vector<const osiql::Node *> result{stream.GetRandomPath(rng)};
    ASSERT_THAT(result, SizeIs(4u));
}

TEST_F(StreamTest, GetRandomRoute_DistanceZeroReturnsOnePoint)
{
    const osiql::Route result{query->GetRandomRoute(0.0)};
    ASSERT_THAT(result, SizeIs(1u));
}
TEST_F(StreamTest, GetRandomRoute_Forwards)
{
    const double maxDistance{10.0};
    const osiql::Route result{query->GetRandomRoute(maxDistance)};
    ASSERT_THAT(result, Not(IsEmpty()));
    ASSERT_THAT(query->GetWorld().GetHostVehicle().positions, Not(IsEmpty()));
    const osiql::Pose<osiql::RoadPoint> &origin{query->GetWorld().GetHostVehicle().positions.front()};
    // All possible paths are longer than 10. Check that the last returned location has a distance of 10.
    const double distance{query->GetDistanceBetween(origin, result.back())};
    ASSERT_DOUBLE_EQ(distance, maxDistance);
}
TEST_F(StreamTest, GetRandomRoute_Backwards)
{
    groundTruth.mutable_host_vehicle_id()->set_value(groundTruth.moving_object().rbegin()->id().value());
    query = std::make_unique<osiql::Query>(groundTruth);

    const double maxDistance{-10.0};
    const osiql::Route result{query->GetRandomRoute(maxDistance)};
    ASSERT_THAT(result, Not(IsEmpty()));
    ASSERT_THAT(query->GetWorld().GetHostVehicle().positions, Not(IsEmpty()));
    const osiql::Pose<osiql::RoadPoint> &origin{query->GetWorld().GetHostVehicle().positions.front()};
    // All possible paths are longer than 10. Check that the last returned location has a distance of 10.
    const double distance{query->GetDistanceBetween(origin, result.back())};
    ASSERT_DOUBLE_EQ(distance, maxDistance);
}

TEST_F(StreamTest, GetShortestRoute_ReturnsEmptyIfOnStartingLaneButBehindSearchArea)
{
    const osiql::MovingObject *ego{query->GetHostVehicle()};
    osiql::Route forwardResult{ego->GetShortestRoute<osiql::Orientation::Forwards>(query->FindPoints({1.0, 1.0}))};
    ASSERT_THAT(forwardResult, IsEmpty());
    osiql::Route backwardResult{ego->GetShortestRoute<osiql::Orientation::Backwards>(query->FindPoints({6.0, 4.0}))};
    ASSERT_THAT(backwardResult, IsEmpty());
}
TEST_F(StreamTest, GetShortestRoute_ReturnsOnePointIfAlreadyAtDestination)
{
    const osiql::MovingObject *ego{query->GetHostVehicle()};
    const auto points{query->FindPoints(ego->GetXY())};
    osiql::Route forwardResult{ego->GetShortestRoute<osiql::Orientation::Forwards>(points)};
    EXPECT_THAT(forwardResult, SizeIs(1u));
    osiql::Route backwardResult{ego->GetShortestRoute<osiql::Orientation::Backwards>(points)};
    EXPECT_THAT(backwardResult, SizeIs(1u));
}
TEST_F(StreamTest, GetShortestRoute_ReturnsTwoPointsIfOnSameNode)
{
    const osiql::MovingObject *ego{query->GetHostVehicle()};
    osiql::Route forwardResult{ego->GetShortestRoute<osiql::Orientation::Forwards>(query->FindPoints({6.0, 4.0}))};
    EXPECT_THAT(forwardResult, SizeIs(2u));
    osiql::Route backwardResult{ego->GetShortestRoute<osiql::Orientation::Backwards>(query->FindPoints({0.0, 1.0}))};
    EXPECT_THAT(backwardResult, SizeIs(2u));
}
TEST_F(StreamTest, GetShortestRoute_Forwards)
{
    const osiql::MovingObject *ego{query->GetHostVehicle()};
    osiql::Route result{ego->GetShortestRoute<osiql::Orientation::Forwards>(query->FindPoints({26.0, 9.0}))};
    ASSERT_THAT(result, SizeIs(4u));
}
TEST_F(StreamTest, GetShortestRoute_Backwards)
{
    // Change the ego agent to the last added moving object, which is on the final lane of the stream.
    query->SetHostVehicle(groundTruth.moving_object().rbegin()->id().value());
    const osiql::MovingObject *ego{query->GetHostVehicle()};
    osiql::Route result{ego->GetShortestRoute<osiql::Orientation::Backwards>(query->FindPoints({0.0, 0.0}))};
    ASSERT_THAT(result, SizeIs(4u));
}

TEST_F(StreamTest, GetRoute_EmptyAcquirePositionAction_ReturnsEmpty)
{
    ASSERT_THAT(query->GetRoute(osi3::TrafficAction::AcquireGlobalPositionAction{}), IsEmpty());
}

TEST_F(StreamTest, GetRoute_AcquirePositionActionWithValidPoint_ReturnsRoute)
{
    static constexpr double VALID_POS_FIRST_LANE_X{6.0};
    static constexpr double VALID_POS_FIRST_LANE_Y{4.0};

    osi3::TrafficAction::AcquireGlobalPositionAction action;
    action.mutable_position()->set_x(VALID_POS_FIRST_LANE_X);
    action.mutable_position()->set_y(VALID_POS_FIRST_LANE_Y);
    const auto route{query->GetRoute(action)};
    ASSERT_THAT(route, SizeIs(2));
}

TEST_F(StreamTest, GetRoute_AcquirePositionActionWithInvalidPoint_ReturnsEmpty)
{
    const auto query{std::make_unique<osiql::Query>(groundTruth)};
    osi3::TrafficAction::AcquireGlobalPositionAction action;
    action.mutable_position()->set_x(1000.0);
    action.mutable_position()->set_y(1000.0);
    ASSERT_THAT(query->GetRoute(action), IsEmpty());
}

TEST_F(StreamTest, GetRoute_EmptyFollowPathAction_ReturnsEmpty)
{
    const auto query{std::make_unique<osiql::Query>(groundTruth)};
    ASSERT_THAT(query->GetRoute(osi3::TrafficAction::FollowPathAction{}), IsEmpty());
}

//TODO: Make tests with host on second lane segment and second points on different lane segments
static constexpr osiql::Vector2d POINT_IN_FRONT_OF_HOST{8.0, 5.0};
static constexpr osiql::Vector2d POINT_BEFORE_HOST{0.0, 1.0};

template <typename Action>
void ADD_POINT(Action &action, const osiql::Vector2d &p)
{
    auto point{action.add_path_point()};
    point->mutable_position()->set_x(p.x);
    point->mutable_position()->set_y(p.y);
}

TEST_F(StreamTest, GetRoute_FollowPathAction_PointInLaneDirection_CalculatesRoute)
{
    const auto query{std::make_unique<osiql::Query>(groundTruth)};
    osi3::TrafficAction::FollowPathAction action;
    ADD_POINT(action, POINT_IN_FRONT_OF_HOST);
    EXPECT_THAT(query->GetRoute(action), SizeIs(2));
}

TEST_F(StreamTest, GetRoute_FollowPathAction_PointAgainstLaneDirection_CalculatesRoute)
{
    const auto query{std::make_unique<osiql::Query>(groundTruth)};
    osi3::TrafficAction::FollowPathAction action;
    ADD_POINT(action, POINT_BEFORE_HOST);
    EXPECT_THAT(query->GetRoute(action), SizeIs(2));
}

TEST_F(StreamTest, GetRoute_FollowPathAction_PointsFlippingLaneDirections_CalculatesRoute)
{
    const auto query{std::make_unique<osiql::Query>(groundTruth)};
    osi3::TrafficAction::FollowPathAction action;
    ADD_POINT(action, POINT_BEFORE_HOST);
    ADD_POINT(action, POINT_IN_FRONT_OF_HOST);
    ADD_POINT(action, POINT_BEFORE_HOST);
    ADD_POINT(action, POINT_IN_FRONT_OF_HOST);
    ADD_POINT(action, POINT_BEFORE_HOST);
    EXPECT_THAT(query->GetRoute(action), SizeIs(6));
}

TEST_F(StreamTest, GetRoute_FollowPathAction_OriginIsDestination_ReturnsOnePoint)
{
    const auto query{std::make_unique<osiql::Query>(groundTruth)};
    osi3::TrafficAction::FollowPathAction action;
    ADD_POINT(action, query->GetHostVehicle()->GetXY());
    const auto route{query->GetRoute(action)};
    EXPECT_THAT(route, SizeIs(1));
}

TEST_F(StreamTest, GetRoute_FollowPathAction_OriginOnUpstreamLane_ReturnsValidRoute)
{
    groundTruth.mutable_host_vehicle_id()->set_value(movingObjects[3]->id().value());
    const auto query{std::make_unique<osiql::Query>(groundTruth)};
    osi3::TrafficAction::FollowPathAction action;
    ADD_POINT(action, POINT_BEFORE_HOST);
    EXPECT_THAT(query->GetRoute(action), SizeIs(2));
}

TEST_F(StreamTest, GetRoute_FollowPathAction_DestinationBehindOriginOnUpstreamLane_ReturnsValidRoute)
{
    groundTruth.mutable_host_vehicle_id()->set_value(movingObjects[3]->id().value());
    const auto query{std::make_unique<osiql::Query>(groundTruth)};
    osi3::TrafficAction::FollowPathAction action;
    ADD_POINT(action, {24.0, 9.0});
    EXPECT_THAT(query->GetRoute(action), SizeIs(3));
}

TEST_F(StreamTest, GetCustomPath)
{
    const osiql::RoadPoint startPoint{*query->GetLane(0), osiql::Coordinates<osiql::Road>{0.0, 0.0}};
    const double range{lengths[0] + lengths[3] + lengths[7] + lengths[8]};
    const osiql::Stream stream(startPoint, range);
    const std::vector<const osiql::Node *> result{
        stream.GetCustomPath([](const osiql::Node *node) { return &node->GetChildren().back(); }) //
    };
    ASSERT_THAT(result, SizeIs(4u));
}

TEST_F(StreamTest, GetDistancesToLaneEnds)
{
    const osiql::RoadPoint startPoint{*query->GetLane(0), osiql::Coordinates<osiql::Road>{0.0, 0.0}};
    // A stream will include all lanes containing (not touching) the search distance.
    // Therefore, distance to end may be greather than stream distance.
    const osiql::Stream forwardStream(startPoint, lengths[0] + lengths[3] + lengths[7] + lengths[8]);
    const std::vector<osiql::Stream::Distance> forwardResult{forwardStream.GetDistancesToLaneEnds()};
    ASSERT_THAT(forwardResult, SizeIs(3));
    EXPECT_THAT(forwardResult[0].value, Eq(lengths[0] + lengths[2] + lengths[5]));
    EXPECT_THAT(forwardResult[1].value, Eq(lengths[0] + lengths[3] + lengths[7] + lengths[8]));
    EXPECT_THAT(forwardResult[2].value, Eq(lengths[0] + lengths[2] + lengths[6] + lengths[8]));

    const osiql::RoadPoint endPoint{*query->GetLane(8), osiql::Coordinates<osiql::Road>{lengths[8], 0.0}};
    const osiql::Stream backwardStream(endPoint, -lengths[8] - lengths[6] - lengths[2] - lengths[0]);
    const std::vector<osiql::Stream::Distance> backwardResult{backwardStream.GetDistancesToLaneEnds()};
    ASSERT_THAT(backwardResult, SizeIs(5));
    EXPECT_THAT(backwardResult[0].value, Eq(lengths[8] + lengths[7] + lengths[4]));
    EXPECT_THAT(backwardResult[1].value, Eq(lengths[8] + lengths[7] + lengths[3] + lengths[1]));
    EXPECT_THAT(backwardResult[2].value, Eq(lengths[8] + lengths[6] + lengths[2] + lengths[1]));
    EXPECT_THAT(backwardResult[3].value, Eq(lengths[8] + lengths[7] + lengths[3] + lengths[0]));
    EXPECT_THAT(backwardResult[4].value, Eq(lengths[8] + lengths[6] + lengths[2] + lengths[0]));
}

TEST_F(StreamTest, GetDistanceBetweenPoints_ForwardsBackwards_Returns_SameResult)
{
    const osiql::RoadPoint startPoint{*query->GetLane(0), osiql::Coordinates<osiql::Road>{2.0, 0.0}};
    const osiql::RoadPoint endPoint{*query->GetLane(8), osiql::Coordinates<osiql::Road>{3.0, 0.0}};
    const osiql::Stream forwardStream(startPoint, lengths[0] + lengths[2] + lengths[6] + lengths[8]);
    const osiql::Stream backwardStream(endPoint, -lengths[0] - lengths[2] - lengths[6] - lengths[8]);
    EXPECT_DOUBLE_EQ(query->GetDistanceBetween(startPoint, endPoint), -query->GetDistanceBetween(endPoint, startPoint));

    const std::vector<osiql::Stream::Distance> zeroDistanceResult{};
    EXPECT_DOUBLE_EQ(query->GetDistanceBetween(startPoint, startPoint), 0.0);
}

TEST_F(StreamTest, Find)
{
    const osiql::RoadPoint location{*query->GetLane(0), osiql::Coordinates<osiql::Road>{0.0, 0.0}};
    const osiql::Stream stream(location, lengths[0] + lengths[2] + lengths[6] + lengths[8]);
    std::cout << stream << '\n';
    std::vector<std::pair<const osiql::MovingObject *, osiql::Stream::Distance>> result{stream.Find<osiql::MovingObject>()};
    // The stream contains 7 of the 9 lanes with one object on each lane. The same object is found a second time on the last lane,
    // but only the occurrence with the shortest distance is stored
    EXPECT_THAT(result, SizeIs(7));

    std::vector<std::pair<const osiql::MovingObject *, osiql::Stream::Distance>> peripheralResult{stream.FindPeripheral<osiql::MovingObject>()};
    EXPECT_THAT(peripheralResult, SizeIs(0));

    const osiql::RoadPoint peripheralOrigin{*query->GetLane(8), osiql::Coordinates<osiql::Road>{4.0, 0.0}};
    const osiql::Stream shortStream(peripheralOrigin, 100.0, 10.0);
    std::vector<std::pair<const osiql::MovingObject *, osiql::Stream::Distance>> shortResult{shortStream.FindPeripheral<osiql::MovingObject>()};
    const osiql::Stream mediumStream(peripheralOrigin, 100.0, 15.0);
    std::vector<std::pair<const osiql::MovingObject *, osiql::Stream::Distance>> mediumResult{mediumStream.FindPeripheral<osiql::MovingObject>()};
    const osiql::Stream longStream(peripheralOrigin, 100.0, 20.0);
    std::vector<std::pair<const osiql::MovingObject *, osiql::Stream::Distance>> longResult{longStream.FindPeripheral<osiql::MovingObject>()};
    EXPECT_THAT(shortResult, SizeIs(2));
    // TODO: Investigate!
    // FIXME: This used to be 5 & 7 instead of 2 & 5. Which is correct?
    EXPECT_THAT(mediumResult, SizeIs(2));
    EXPECT_THAT(longResult, SizeIs(5));

    const std::vector<std::pair<const osiql::TrafficSign *, osiql::Stream::Distance>> trafficSigns{
        longStream.FindAll<osiql::TrafficSign>(osiql::LaneChangeBehavior::WithLaneChanges) //
    };
    EXPECT_THAT(trafficSigns, IsEmpty());
}

// Get Distance Between Objects

TEST_F(StreamTest, GetDistanceTo_ObjectSelf_Returns_Zero)
{
    const osiql::MovingObject &ego{*query->GetHostVehicle()};
    ASSERT_THAT(ego.positions.empty(), Eq(false));
    EXPECT_THAT(ego.GetDistanceTo(ego), DoubleEq(0.0));
}

TEST_F(StreamTest, GetDistanceTo_ObjectInWrongDirection_Returns_Max)
{
    const osiql::MovingObject &ego{*query->GetHostVehicle()};
    const osiql::MovingObject &object{*query->GetMovingObject(1008u)};
    ASSERT_DOUBLE_EQ(ego.GetDistanceTo<osiql::Orientation::Backwards>(object), std::numeric_limits<double>::max());
}

TEST_F(StreamTest, GetDistanceTo_ToAndFromObject_Returns_SameDistance)
{
    const osiql::MovingObject &ego{*query->GetHostVehicle()};
    const osiql::MovingObject &object{*query->GetMovingObject(1008u)};
    // Object to point
    const double distanceA{ego.GetDistanceTo<osiql::Orientation::Forwards>(object.positions.front())};
    const double distanceB{object.GetDistanceTo<osiql::Orientation::Backwards>(ego.positions.front())};
    EXPECT_DOUBLE_EQ(distanceA, distanceB);
    // Object to object
    EXPECT_DOUBLE_EQ(ego.GetDistanceTo<osiql::Orientation::Forwards>(object), object.GetDistanceTo<osiql::Orientation::Backwards>(ego));
}
