..
  *******************************************************************************
  Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

osiql Documentation
===================

osiql (Open Simulation Interface Query Library) is a wrapper of the `ASAM Open Simulation Interface (OSI) <https://github.com/OpenSimulationInterface/open-simulation-interface>`_ using modern C++17 features and idioms to provide a terse, object-oriented interface to OSI data in addition to offering various utility methods and features. Specifically, it provides ways to convert between global and local road-based coordinate systems, offers means to generate random routes or the shortest route between two points and adds functions for querying an object's/point's surroundings.

Aside from the added features, one major benefit of using osiql is that related objects are connected directly via pointers instead of being assigned ids, allowing for much greater performance on any query across objects as opposed to working directly on an OSI object. All wrapped OSI-Objects have public handles to ensure that any unwrapped method is still accessible.

Motivation
**********

There are many use cases where one would want to access an object from a related object. OSI models objects and their relation to one another, but it does so by storing the related object's unique identifier. Since objects in OSI are stored in unsorted, contiguous containers, looking up a property of a related object becomes an expensive operation for larger road networks. This also inhibits encapsulation (without a God object), as each object needs to refer to the ``osi3::GroundTruth`` it is a part of in order to offer most of its functionality. 

osiql was conceived to solve these issues. Wrapper classes of OSI-objects are constructed once when initializing an ``osiql::Query`` and have direct access to related objects using pointers or references. osiql also introduces shared base classes to wrapped objects, which allow for similar OSI-objects to be functionally treated the same, such as ``osi3::ReferenceLine`` and ``osi3::LogicalLaneBoundary`` or aspects of ``osi3::MovingObject``, ``osi3::RoadMarking``, ``osi3::StationaryObject``, ``osi3::TrafficLight`` and ``osi3::TrafficSign``. This has the benefit of increasing accessor uniformity across classes: The methods ``osi3::ReferenceLine::poly_line()`` and ``osi3::LogicalLaneBoundary::boundary_line()`` now share the same interface via ``osiql::Points<T>``. Naming differences such as ``osi3::ReferenceLine::ReferenceLinePoint::world_position()`` and ``osi3::LaneBoundary::BoundaryPoint::position()`` are no longer a concern.

.. toctree::
   :caption: Main osiql classes
   :glob:
   :maxdepth: 1

   Extensions/*

.. toctree::
   :caption: OSI-Wrapper-Objects
   :glob:
   :maxdepth: 1

   Wrappers/*

.. toctree::
   :caption: Additional Types & Abstractions
   :glob:
   :maxdepth: 1

   Types/*
