..
  *******************************************************************************
  Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Route
=====

A route in osiql is a chain of local points representing the shortest path following an input set of local points. It is similar to a stream without branches or periphery. A user can query all objects of a given type along the route and receive them sorted by distance from the start of the route. In general, one can check whether a point or object lies on a route and how far away it is from the beginning of said route.