..
  *******************************************************************************
  Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Road
====

Roads do not exist in OSI. In osiql, a ``osiql::Road`` is, similar to a LaneSection in OpenDRIVE or a LaneGroup in NDS, a set of adjacent lanes. It allows users to iterate over lanes on a given side of the road, find which of its lane is closest to a given point perform coordinate conversion like a Lane and access successors/predecessors of all adjacent lanes at once.