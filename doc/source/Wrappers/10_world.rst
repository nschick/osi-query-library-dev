..
  *******************************************************************************
  Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

World (osi3::GroundTruth)
=========================

osiql::World is a wrapper of a osi3::GroundTruth that stores all its wrapper objects in separate containers to allow fetching objects by their id more efficiently than OSI's linear lookup. These are the stored types:

* Lane Boundaries (osi3::LogicalLaneBoundary)
* Lane Markings (osi3::LaneBoundary)
* Lanes (osi3::LogicalLane);
* Reference Lines (osi3::ReferenceLine)
* Roads (No corresponding OSI object)
* Moving Objects (osi3::MovingObject)
* Road Markings (osi3::RoadMarking)
* Static Objects (osi3::StationaryObject)
* Light Bulbs (osi3::TrafficLight)
* Traffic Lights (Set of light bulbs)
* Traffic Signs (osi3::TrafficSign)

While its host vehicle is read from the GroundTruth by default, it can also be manually replaced using ``SetHostVehicle``.

See the doxygen documentation for all methods that osiql::World provides.