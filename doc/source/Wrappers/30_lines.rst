..
  *******************************************************************************
  Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Reference lines & bounaries
===========================

The reference line defines the the coordinate system of all lanes that refer to it. While this is not required in OSI, osiql assumes that all adjacent lanes of a lane refer to the same reference line. Queries may fail if this is not the case.